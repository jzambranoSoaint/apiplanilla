package soaint.marcaaguadocx.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

public class FileUtil {
    public static final String UPLOADS_FOLDER = "sgduploads";
    public static final int FILE_TYPE_IMG = 1;
    public static final int FILE_TYPE_DOCX = 2;

    public static byte[] readFile(File file) throws Exception {
        InputStream is = new FileInputStream(file);
        long length = file.length();        //
        if (length > Integer.MAX_VALUE) {
            throw new Exception("File too large");
        }
        byte[] bytes = new byte[(int) length];
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }
        if (offset < bytes.length) {
            throw new Exception("Could not completely read file " + file.getName());
        }
        is.close();
        return bytes;
    }


    public static File createTemporaryFile(int type) throws Exception {
        String ext = "docx";
        switch (type) {
            case FileUtil.FILE_TYPE_DOCX:
                ext = "docx";
                break;
            case FileUtil.FILE_TYPE_IMG:
                ext = "png";
                break;
        }
        createTempFilePath();
        return new File(getTempFilePath() + File.separator + "marcaaguadocx4j_" + System.currentTimeMillis() + "." + ext);
    }

    public static void createFile(String base64File, File f) throws Exception {
        createFile(Base64.decode(base64File), f);
    }

    public static void createFile(byte[] bytes, File f) throws Exception {
        createTempFilePath();
        FileOutputStream fileOuputStream = new FileOutputStream(f);
        fileOuputStream.write(bytes);
        fileOuputStream.close();
    }


    public static boolean createTempFilePath() {
        File f = new File(getTempFilePath());
        if (!f.exists()) {
            return f.mkdir();
        }
        return true;
    }

    public static String getTempFilePath() {
        String tempDirectory = System.getProperty("java.io.tmpdir");

        if (tempDirectory == null) {
            tempDirectory = "";
        }
        return tempDirectory + File.separator + UPLOADS_FOLDER;
    }

    public static void deleteFilesGeneral() {
        try {
            File f = new File(getTempFilePath());
            if (f != null && f.isDirectory()) {
                File fs[] = f.listFiles();
                if (fs != null && fs.length > 0) {
                    for (File ff : fs) {
                        if (!ff.isDirectory()) {
                            long t = ((System.currentTimeMillis() - ff.lastModified()) / 1000 / 60 / 60);
                            if (t > 0) {
                                ff.delete();
                            }
                        }
                    }
                }
            }
            cleanTemp();
        } catch (Exception e) {
        }
    }

    public static void cleanTemp() {
        String tmp = System.getProperty("java.io.tmpdir");
        String fileName = null;
        if (tmp != null && !tmp.equals("")) {
            File fs[] = new File(tmp).listFiles();
            for (File f : fs) {
                fileName = f.getName().toLowerCase();
                if (!f.isDirectory() && (fileName.startsWith("img") && f.getName().endsWith(".img")) || fileName.startsWith("marcaaguadocx4j_")) {
                    f.delete();
                }
            }

        }

    }
}
