package soaint.marcaaguadocx.bo;

public class PropiedadesGeneral {
    private String name;
    private Object value;

    public String getNombre() {
        return name;
    }
    public void setNombre(String nombre) {
        this.name = nombre;
    }
    public Object getValor() {
        return value;
    }
    public void setValor(Object valor) {
        this.value = valor;
    }
}
