package soaint.marcaaguadocx.bo;

import java.util.List;

public class PropiedadesGeneralesTabla {
    private List<String> labels;
    private List<ValoresTabla> values;

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public List<ValoresTabla> getValues() {
        return values;
    }

    public void setValues(List<ValoresTabla> values) {
        this.values = values;
    }
}
