package soaint.marcaaguadocx.bo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

public class PropiedadDocumento {
    private List<PropiedadesGeneral> tags;
    private String base64;
    private List<PropiedadesGeneralesTabla> tables;

    public PropiedadDocumento(String json) {

        Gson gson = new GsonBuilder().create();
        PropiedadDocumento obj = gson.fromJson(json, PropiedadDocumento.class);

        this.tags = obj.getTags();
        this.base64 = obj.getBase64();
        this.tables = obj.getTables();
    }

    public List<PropiedadesGeneral> getTags() {
        if (tags == null) {
            tags = new ArrayList<PropiedadesGeneral>();
        }
        return tags;
    }

    public void setTags(List<PropiedadesGeneral> tags) {
        this.tags = tags;
    }

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    public List<PropiedadesGeneralesTabla> getTables() {
        if (tables == null) {
            tables = new ArrayList<PropiedadesGeneralesTabla>();
        }
        return tables;
    }

    public void setTables(List<PropiedadesGeneralesTabla> tables) {
        this.tables = tables;
    }
}
