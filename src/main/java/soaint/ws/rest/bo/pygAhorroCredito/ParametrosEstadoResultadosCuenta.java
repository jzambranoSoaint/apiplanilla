package soaint.ws.rest.bo.pygAhorroCredito;

public class ParametrosEstadoResultadosCuenta {
    private EstadoResultadosCuenta estadoResultadosCuenta;

    public EstadoResultadosCuenta getEstadoResultadosCuenta() {
        if (estadoResultadosCuenta == null) {
            estadoResultadosCuenta = new EstadoResultadosCuenta();
        }
        return estadoResultadosCuenta;
    }

    public void setEstadoResultadosCuenta(EstadoResultadosCuenta estadoResultadosCuenta) {
        this.estadoResultadosCuenta = estadoResultadosCuenta;
    }
}
