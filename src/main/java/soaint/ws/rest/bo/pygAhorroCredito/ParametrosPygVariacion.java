package soaint.ws.rest.bo.pygAhorroCredito;

public class ParametrosPygVariacion {
    private PygVariacion pygVariacion;

    public PygVariacion getPygVariacion() {
        if (pygVariacion == null) {
            pygVariacion = new PygVariacion();
        }
        return pygVariacion;
    }

    public void setPygVariacion(PygVariacion pygVariacion) {
        this.pygVariacion = pygVariacion;
    }
}
