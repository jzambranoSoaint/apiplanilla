package soaint.ws.rest.bo.pygAhorroCredito;

public class PygVariacion {
    private String partInteresesRecibidosCarteraCorteUno;
    private String partInteresesRecibidosCarteraCorteDos;
    private String partInteresesRecibidosCarteraCorteTres;
    private String partInteresesRecibidosCarteraCuantiaUnoDos;
    private String partInteresesRecibidosCarteraPorcentajeUnoDos;
    private String partInteresesRecibidosCarteraCuantiaUnoTres;
    private String partInteresesRecibidosCarteraPorcentajeUnoTres;
    private String partInteresesPagadosDepositosCorteUno;
    private String partInteresesPagadosDepositosCorteDos;
    private String partInteresesPagadosDepositosCorteTres;
    private String partInteresesPagadosDepositosCuantiaUnoDos;
    private String partInteresesPagadosDepositosPorcentajeUnoDos;
    private String partInteresesPagadosDepositosCuantiaUnoTres;
    private String partInteresesPagadosDepositosPorcentajeUnoTres;
    private String partMargenIntermediacionCorteUno;
    private String partMargenIntermediacionCorteDos;
    private String partMargenIntermediacionCorteTres;
    private String partMargenIntermediacionCuantiaUnoDos;
    private String partMargenIntermediacionPorcentajeUnoDos;
    private String partMargenIntermediacionCuantiaUnoTres;
    private String partMargenIntermediacionPorcentajeUnoTres;
    private String partOtrosIngresosCorteUno;
    private String partOtrosIngresosCorteDos;
    private String partOtrosIngresosCorteTres;
    private String partOtrosIngresosCuantiaUnoDos;
    private String partOtrosIngresosPorcentajeUnoDos;
    private String partOtrosIngresosCuantiaUnoTres;
    private String partOtrosIngresosPorcentajeUnoTres;
    private String partOtrosEgresosCorteUno;
    private String partOtrosEgresosCorteDos;
    private String partOtrosEgresosCorteTres;
    private String partOtrosEgresosCuantiaUnoDos;
    private String partOtrosEgresosPorcentajeUnoDos;
    private String partOtrosEgresosCuantiaUnoTres;
    private String partOtrosEgresosPorcentajeUnoTres;
    private String partMargenFinancieroBrutoCorteUno;
    private String partMargenFinancieroBrutoCorteDos;
    private String partMargenFinancieroBrutoCorteTres;
    private String partMargenFinancieroBrutoCuantiaUnoDos;
    private String partMargenFinancieroBrutoPorcentajeUnoDos;
    private String partMargenFinancieroBrutoCuantiaUnoTres;
    private String partMargenFinancieroBrutoPorcentajeUnoTres;
    private String partOtrosIngresosOperacionalesCorteUno;
    private String partOtrosIngresosOperacionalesCorteDos;
    private String partOtrosIngresosOperacionalesCorteTres;
    private String partOtrosIngresosOperacionalesCuantiaUnoDos;
    private String partOtrosIngresosOperacionalesPorcentajeUnoDos;
    private String partOtrosIngresosOperacionalesCuantiaUnoTres;
    private String partOtrosIngresosOperacionalesPorcentajeUnoTres;
    private String partMargenBrutoCorteUno;
    private String partMargenBrutoCorteDos;
    private String partMargenBrutoCorteTres;
    private String partMargenBrutoCuantiaUnoDos;
    private String partMargenBrutoPorcentajeUnoDos;
    private String partMargenBrutoCuantiaUnoTres;
    private String partMargenBrutoPorcentajeUnoTres;
    private String partGastosPersonalesCorteUno;
    private String partGastosPersonalesCorteDos;
    private String partGastosPersonalesCorteTres;
    private String partGastosPersonalesCuantiaUnoDos;
    private String partGastosPersonalesPorcentajeUnoDos;
    private String partGastosPersonalesCuantiaUnoTres;
    private String partGastosPersonalesPorcentajeUnoTres;
    private String partGastosGeneralesCorteUno;
    private String partGastosGeneralesCorteDos;
    private String partGastosGeneralesCorteTres;
    private String partGastosGeneralesCuantiaUnoDos;
    private String partGastosGeneralesPorcentajeUnoDos;
    private String partGastosGeneralesCuantiaUnoTres;
    private String partGastosGeneralesPorcentajeUnoTres;
    private String partDepreciacionesAmortizacionesCorteUno;
    private String partDepreciacionesAmortizacionesCorteDos;
    private String partDepreciacionesAmortizacionesCorteTres;
    private String partDepreciacionesAmortizacionesCuantiaUnoDos;
    private String partDepreciacionesAmortizacionesPorcentajeUnoDos;
    private String partDepreciacionesAmortizacionesCuantiaUnoTres;
    private String partDepreciacionesAmortizacionesPorcentajeUnoTres;
    private String partEgresosOperacionalesCorteUno;
    private String partEgresosOperacionalesCorteDos;
    private String partEgresosOperacionalesCorteTres;
    private String partEgresosOperacionalesCuantiaUnoDos;
    private String partEgresosOperacionalesPorcentajeUnoDos;
    private String partEgresosOperacionalesCuantiaUnoTres;
    private String partEgresosOperacionalesPorcentajeUnoTres;
    private String partTotalGatosAdminCorteUno;
    private String partTotalGatosAdminCorteDos;
    private String partTotalGatosAdminCorteTres;
    private String partTotalGatosAdminCuantiaUnoDos;
    private String partTotalGatosAdminPorcentajeUnoDos;
    private String partTotalGatosAdminCuantiaUnoTres;
    private String partTotalGatosAdminPorcentajeUnoTres;
    private String partExcedenteOperacionalAntesCorteUno;
    private String partExcedenteOperacionalAntesCorteDos;
    private String partExcedenteOperacionalAntesCorteTres;
    private String partExcedenteOperacionalAntesCuantiaUnoDos;
    private String partExcedenteOperacionalAntesPorcentajeUnoDos;
    private String partExcedenteOperacionalAntesCuantiaUnoTres;
    private String partExcedenteOperacionalAntesPorcentajeUnoTres;
    private String partDeterioroCarteraCorteUno;
    private String partDeterioroCarteraCorteDos;
    private String partDeterioroCarteraCorteTres;
    private String partDeterioroCarteraCuantiaUnoDos;
    private String partDeterioroCarteraPorcentajeUnoDos;
    private String partDeterioroCarteraCuantiaUnoTres;
    private String partDeterioroCarteraPorcentajeUnoTres;
    private String partDeterioroCuentasPorCobrarCorteUno;
    private String partDeterioroCuentasPorCobrarCorteDos;
    private String partDeterioroCuentasPorCobrarCorteTres;
    private String partDeterioroCuentasPorCobrarCuantiaUnoDos;
    private String partDeterioroCuentasPorCobrarPorcentajeUnoDos;
    private String partDeterioroCuentasPorCobrarCuantiaUnoTres;
    private String partDeterioroCuentasPorCobrarPorcentajeUnoTres;
    private String partDeterioroInversionesCorteUno;
    private String partDeterioroInversionesCorteDos;
    private String partDeterioroInversionesCorteTres;
    private String partDeterioroInversionesCuantiaUnoDos;
    private String partDeterioroInversionesPorcentajeUnoDos;
    private String partDeterioroInversionesCuantiaUnoTres;
    private String partDeterioroInversionesPorcentajeUnoTres;
    private String partOtrosDeteriorosCorteUno;
    private String partOtrosDeteriorosCorteDos;
    private String partOtrosDeteriorosCorteTres;
    private String partOtrosDeteriorosCuantiaUnoDos;
    private String partOtrosDeteriorosPorcentajeUnoDos;
    private String partOtrosDeteriorosCuantiaUnoTres;
    private String partOtrosDeteriorosPorcentajeUnoTres;
    private String partTotalDeteriorosCorteUno;
    private String partTotalDeteriorosCorteDos;
    private String partTotalDeteriorosCorteTres;
    private String partTotalDeteriorosCuantiaUnoDos;
    private String partTotalDeteriorosPorcentajeUnoDos;
    private String partTotalDeteriorosCuantiaUnoTres;
    private String partTotalDeteriorosPorcentajeUnoTres;
    private String partRecuperacionesDeteriorosCorteUno;
    private String partRecuperacionesDeteriorosCorteDos;
    private String partRecuperacionesDeteriorosCorteTres;
    private String partRecuperacionesDeteriorosCuantiaUnoDos;
    private String partRecuperacionesDeteriorosPorcentajeUnoDos;
    private String partRecuperacionesDeteriorosCuantiaUnoTres;
    private String partRecuperacionesDeteriorosPorcentajeUnoTres;
    private String partDeteriorosNetosCorteUno;
    private String partDeteriorosNetosCorteDos;
    private String partDeteriorosNetosCorteTres;
    private String partDeteriorosNetosCuantiaUnoDos;
    private String partDeteriorosNetosPorcentajeUnoDos;
    private String partDeteriorosNetosCuantiaUnoTres;
    private String partDeteriorosNetosPorcentajeUnoTres;
    private String partExcedenteNetoAntesCorteUno;
    private String partExcedenteNetoAntesCorteDos;
    private String partExcedenteNetoAntesCorteTres;
    private String partExcedenteNetoAntesCuantiaUnoDos;
    private String partExcedenteNetoAntesPorcentajeUnoDos;
    private String partExcedenteNetoAntesCuantiaUnoTres;
    private String partExcedenteNetoAntesPorcentajeUnoTres;
    private String partImpuestoCorteUno;
    private String partImpuestoCorteDos;
    private String partImpuestoCorteTres;
    private String partImpuestoCuantiaUnoDos;
    private String partImpuestoPorcentajeUnoDos;
    private String partImpuestoCuantiaUnoTres;
    private String partImpuestoPorcentajeUnoTres;
    private String partExcedenteNetoCorteUno;
    private String partExcedenteNetoCorteDos;
    private String partExcedenteNetoCorteTres;
    private String partExcedenteNetoCuantiaUnoDos;
    private String partExcedenteNetoPorcentajeUnoDos;
    private String partExcedenteNetoCuantiaUnoTres;
    private String partExcedenteNetoPorcentajeUnoTres;
    private String partExcedentePerdidasORICorteUno;
    private String partExcedentePerdidasORICorteDos;
    private String partExcedentePerdidasORICorteTres;
    private String partExcedentePerdidasORICuantiaUnoDos;
    private String partExcedentePerdidasORIPorcentajeUnoDos;
    private String partExcedentePerdidasORICuantiaUnoTres;
    private String partExcedentePerdidasORIPorcentajeUnoTres;
    private String partExcedentePerdidasCorteUno;
    private String partExcedentePerdidasCorteDos;
    private String partExcedentePerdidasCorteTres;
    private String partExcedentePerdidasCuantiaUnoDos;
    private String partExcedentePerdidasPorcentajeUnoDos;
    private String partExcedentePerdidasCuantiaUnoTres;
    private String partExcedentePerdidasPorcentajeUnoTres;

    public String getPartInteresesRecibidosCarteraCorteUno() {
        if (partInteresesRecibidosCarteraCorteUno == null) {
            partInteresesRecibidosCarteraCorteUno = "";
        }
        return partInteresesRecibidosCarteraCorteUno;
    }

    public String getPartInteresesRecibidosCarteraCorteDos() {
        if (partInteresesRecibidosCarteraCorteDos == null) {
            partInteresesRecibidosCarteraCorteDos = "";
        }
        return partInteresesRecibidosCarteraCorteDos;
    }

    public String getPartInteresesRecibidosCarteraCorteTres() {
        if (partInteresesRecibidosCarteraCorteTres == null) {
            partInteresesRecibidosCarteraCorteTres = "";
        }
        return partInteresesRecibidosCarteraCorteTres;
    }

    public String getPartInteresesRecibidosCarteraCuantiaUnoDos() {
        if (partInteresesRecibidosCarteraCuantiaUnoDos == null) {
            partInteresesRecibidosCarteraCuantiaUnoDos = "";
        }
        return partInteresesRecibidosCarteraCuantiaUnoDos;
    }

    public String getPartInteresesRecibidosCarteraPorcentajeUnoDos() {
        if (partInteresesRecibidosCarteraPorcentajeUnoDos == null) {
            partInteresesRecibidosCarteraPorcentajeUnoDos = "";
        }
        return partInteresesRecibidosCarteraPorcentajeUnoDos;
    }

    public String getPartInteresesRecibidosCarteraCuantiaUnoTres() {
        if (partInteresesRecibidosCarteraCuantiaUnoTres == null) {
            partInteresesRecibidosCarteraCuantiaUnoTres = "";
        }
        return partInteresesRecibidosCarteraCuantiaUnoTres;
    }

    public String getPartInteresesRecibidosCarteraPorcentajeUnoTres() {
        if (partInteresesRecibidosCarteraPorcentajeUnoTres == null) {
            partInteresesRecibidosCarteraPorcentajeUnoTres = "";
        }
        return partInteresesRecibidosCarteraPorcentajeUnoTres;
    }

    public String getPartInteresesPagadosDepositosCorteUno() {
        if (partInteresesPagadosDepositosCorteUno == null) {
            partInteresesPagadosDepositosCorteUno = "";
        }
        return partInteresesPagadosDepositosCorteUno;
    }

    public String getPartInteresesPagadosDepositosCorteDos() {
        if (partInteresesPagadosDepositosCorteDos == null) {
            partInteresesPagadosDepositosCorteDos = "";
        }
        return partInteresesPagadosDepositosCorteDos;
    }

    public String getPartInteresesPagadosDepositosCorteTres() {
        if (partInteresesPagadosDepositosCorteTres == null) {
            partInteresesPagadosDepositosCorteTres = "";
        }
        return partInteresesPagadosDepositosCorteTres;
    }

    public String getPartInteresesPagadosDepositosCuantiaUnoDos() {
        if (partInteresesPagadosDepositosCuantiaUnoDos == null) {
            partInteresesPagadosDepositosCuantiaUnoDos = "";
        }
        return partInteresesPagadosDepositosCuantiaUnoDos;
    }

    public String getPartInteresesPagadosDepositosPorcentajeUnoDos() {
        if (partInteresesPagadosDepositosPorcentajeUnoDos == null) {
            partInteresesPagadosDepositosPorcentajeUnoDos = "";
        }
        return partInteresesPagadosDepositosPorcentajeUnoDos;
    }

    public String getPartInteresesPagadosDepositosCuantiaUnoTres() {
        if (partInteresesPagadosDepositosCuantiaUnoTres == null) {
            partInteresesPagadosDepositosCuantiaUnoTres = "";
        }
        return partInteresesPagadosDepositosCuantiaUnoTres;
    }

    public String getPartInteresesPagadosDepositosPorcentajeUnoTres() {
        if (partInteresesPagadosDepositosPorcentajeUnoTres == null) {
            partInteresesPagadosDepositosPorcentajeUnoTres = "";
        }
        return partInteresesPagadosDepositosPorcentajeUnoTres;
    }

    public String getPartMargenIntermediacionCorteUno() {
        if (partMargenIntermediacionCorteUno == null) {
            partMargenIntermediacionCorteUno = "";
        }
        return partMargenIntermediacionCorteUno;
    }

    public String getPartMargenIntermediacionCorteDos() {
        if (partMargenIntermediacionCorteDos == null) {
            partMargenIntermediacionCorteDos = "";
        }
        return partMargenIntermediacionCorteDos;
    }

    public String getPartMargenIntermediacionCorteTres() {
        if (partMargenIntermediacionCorteTres == null) {
            partMargenIntermediacionCorteTres = "";
        }
        return partMargenIntermediacionCorteTres;
    }

    public String getPartMargenIntermediacionCuantiaUnoDos() {
        if (partMargenIntermediacionCuantiaUnoDos == null) {
            partMargenIntermediacionCuantiaUnoDos = "";
        }
        return partMargenIntermediacionCuantiaUnoDos;
    }

    public String getPartMargenIntermediacionPorcentajeUnoDos() {
        if (partMargenIntermediacionPorcentajeUnoDos == null) {
            partMargenIntermediacionPorcentajeUnoDos = "";
        }
        return partMargenIntermediacionPorcentajeUnoDos;
    }

    public String getPartMargenIntermediacionCuantiaUnoTres() {
        if (partMargenIntermediacionCuantiaUnoTres == null) {
            partMargenIntermediacionCuantiaUnoTres = "";
        }
        return partMargenIntermediacionCuantiaUnoTres;
    }

    public String getPartMargenIntermediacionPorcentajeUnoTres() {
        if (partMargenIntermediacionPorcentajeUnoTres == null) {
            partMargenIntermediacionPorcentajeUnoTres = "";
        }
        return partMargenIntermediacionPorcentajeUnoTres;
    }

    public String getPartOtrosIngresosCorteUno() {
        if (partOtrosIngresosCorteUno == null) {
            partOtrosIngresosCorteUno = "";
        }
        return partOtrosIngresosCorteUno;
    }

    public String getPartOtrosIngresosCorteDos() {
        if (partOtrosIngresosCorteDos == null) {
            partOtrosIngresosCorteDos = "";
        }
        return partOtrosIngresosCorteDos;
    }

    public String getPartOtrosIngresosCorteTres() {
        if (partOtrosIngresosCorteTres == null) {
            partOtrosIngresosCorteTres = "";
        }
        return partOtrosIngresosCorteTres;
    }

    public String getPartOtrosIngresosCuantiaUnoDos() {
        if (partOtrosIngresosCuantiaUnoDos == null) {
            partOtrosIngresosCuantiaUnoDos = "";
        }
        return partOtrosIngresosCuantiaUnoDos;
    }

    public String getPartOtrosIngresosPorcentajeUnoDos() {
        if (partOtrosIngresosPorcentajeUnoDos == null) {
            partOtrosIngresosPorcentajeUnoDos = "";
        }
        return partOtrosIngresosPorcentajeUnoDos;
    }

    public String getPartOtrosIngresosCuantiaUnoTres() {
        if (partOtrosIngresosCuantiaUnoTres == null) {
            partOtrosIngresosCuantiaUnoTres = "";
        }
        return partOtrosIngresosCuantiaUnoTres;
    }

    public String getPartOtrosIngresosPorcentajeUnoTres() {
        if (partOtrosIngresosPorcentajeUnoTres == null) {
            partOtrosIngresosPorcentajeUnoTres = "";
        }
        return partOtrosIngresosPorcentajeUnoTres;
    }

    public String getPartOtrosEgresosCorteUno() {
        if (partOtrosEgresosCorteUno == null) {
            partOtrosEgresosCorteUno = "";
        }
        return partOtrosEgresosCorteUno;
    }

    public String getPartOtrosEgresosCorteDos() {
        if (partOtrosEgresosCorteDos == null) {
            partOtrosEgresosCorteDos = "";
        }
        return partOtrosEgresosCorteDos;
    }

    public String getPartOtrosEgresosCorteTres() {
        if (partOtrosEgresosCorteTres == null) {
            partOtrosEgresosCorteTres = "";
        }
        return partOtrosEgresosCorteTres;
    }

    public String getPartOtrosEgresosCuantiaUnoDos() {
        if (partOtrosEgresosCuantiaUnoDos == null) {
            partOtrosEgresosCuantiaUnoDos = "";
        }
        return partOtrosEgresosCuantiaUnoDos;
    }

    public String getPartOtrosEgresosPorcentajeUnoDos() {
        if (partOtrosEgresosPorcentajeUnoDos == null) {
            partOtrosEgresosPorcentajeUnoDos = "";
        }
        return partOtrosEgresosPorcentajeUnoDos;
    }

    public String getPartOtrosEgresosCuantiaUnoTres() {
        if (partOtrosEgresosCuantiaUnoTres == null) {
            partOtrosEgresosCuantiaUnoTres = "";
        }
        return partOtrosEgresosCuantiaUnoTres;
    }

    public String getPartOtrosEgresosPorcentajeUnoTres() {
        if (partOtrosEgresosPorcentajeUnoTres == null) {
            partOtrosEgresosPorcentajeUnoTres = "";
        }
        return partOtrosEgresosPorcentajeUnoTres;
    }

    public String getPartMargenFinancieroBrutoCorteUno() {
        if (partMargenFinancieroBrutoCorteUno == null) {
            partMargenFinancieroBrutoCorteUno = "";
        }
        return partMargenFinancieroBrutoCorteUno;
    }

    public String getPartMargenFinancieroBrutoCorteDos() {
        if (partMargenFinancieroBrutoCorteDos == null) {
            partMargenFinancieroBrutoCorteDos = "";
        }
        return partMargenFinancieroBrutoCorteDos;
    }

    public String getPartMargenFinancieroBrutoCorteTres() {
        if (partMargenFinancieroBrutoCorteTres == null) {
            partMargenFinancieroBrutoCorteTres = "";
        }
        return partMargenFinancieroBrutoCorteTres;
    }

    public String getPartMargenFinancieroBrutoCuantiaUnoDos() {
        if (partMargenFinancieroBrutoCuantiaUnoDos == null) {
            partMargenFinancieroBrutoCuantiaUnoDos = "";
        }
        return partMargenFinancieroBrutoCuantiaUnoDos;
    }

    public String getPartMargenFinancieroBrutoPorcentajeUnoDos() {
        if (partMargenFinancieroBrutoPorcentajeUnoDos == null) {
            partMargenFinancieroBrutoPorcentajeUnoDos = "";
        }
        return partMargenFinancieroBrutoPorcentajeUnoDos;
    }

    public String getPartMargenFinancieroBrutoCuantiaUnoTres() {
        if (partMargenFinancieroBrutoCuantiaUnoTres == null) {
            partMargenFinancieroBrutoCuantiaUnoTres = "";
        }
        return partMargenFinancieroBrutoCuantiaUnoTres;
    }

    public String getPartMargenFinancieroBrutoPorcentajeUnoTres() {
        if (partMargenFinancieroBrutoPorcentajeUnoTres == null) {
            partMargenFinancieroBrutoPorcentajeUnoTres = "";
        }
        return partMargenFinancieroBrutoPorcentajeUnoTres;
    }

    public String getPartOtrosIngresosOperacionalesCorteUno() {
        if (partOtrosIngresosOperacionalesCorteUno == null) {
            partOtrosIngresosOperacionalesCorteUno = "";
        }
        return partOtrosIngresosOperacionalesCorteUno;
    }

    public String getPartOtrosIngresosOperacionalesCorteDos() {
        if (partOtrosIngresosOperacionalesCorteDos == null) {
            partOtrosIngresosOperacionalesCorteDos = "";
        }
        return partOtrosIngresosOperacionalesCorteDos;
    }

    public String getPartOtrosIngresosOperacionalesCorteTres() {
        if (partOtrosIngresosOperacionalesCorteTres == null) {
            partOtrosIngresosOperacionalesCorteTres = "";
        }
        return partOtrosIngresosOperacionalesCorteTres;
    }

    public String getPartOtrosIngresosOperacionalesCuantiaUnoDos() {
        if (partOtrosIngresosOperacionalesCuantiaUnoDos == null) {
            partOtrosIngresosOperacionalesCuantiaUnoDos = "";
        }
        return partOtrosIngresosOperacionalesCuantiaUnoDos;
    }

    public String getPartOtrosIngresosOperacionalesPorcentajeUnoDos() {
        if (partOtrosIngresosOperacionalesPorcentajeUnoDos == null) {
            partOtrosIngresosOperacionalesPorcentajeUnoDos = "";
        }
        return partOtrosIngresosOperacionalesPorcentajeUnoDos;
    }

    public String getPartOtrosIngresosOperacionalesCuantiaUnoTres() {
        if (partOtrosIngresosOperacionalesCuantiaUnoTres == null) {
            partOtrosIngresosOperacionalesCuantiaUnoTres = "";
        }
        return partOtrosIngresosOperacionalesCuantiaUnoTres;
    }

    public String getPartOtrosIngresosOperacionalesPorcentajeUnoTres() {
        if (partOtrosIngresosOperacionalesPorcentajeUnoTres == null) {
            partOtrosIngresosOperacionalesPorcentajeUnoTres = "";
        }
        return partOtrosIngresosOperacionalesPorcentajeUnoTres;
    }

    public String getPartMargenBrutoCorteUno() {
        if (partMargenBrutoCorteUno == null) {
            partMargenBrutoCorteUno = "";
        }
        return partMargenBrutoCorteUno;
    }

    public String getPartMargenBrutoCorteDos() {
        if (partMargenBrutoCorteDos == null) {
            partMargenBrutoCorteDos = "";
        }
        return partMargenBrutoCorteDos;
    }

    public String getPartMargenBrutoCorteTres() {
        if (partMargenBrutoCorteTres == null) {
            partMargenBrutoCorteTres = "";
        }
        return partMargenBrutoCorteTres;
    }

    public String getPartMargenBrutoCuantiaUnoDos() {
        if (partMargenBrutoCuantiaUnoDos == null) {
            partMargenBrutoCuantiaUnoDos = "";
        }
        return partMargenBrutoCuantiaUnoDos;
    }

    public String getPartMargenBrutoPorcentajeUnoDos() {
        if (partMargenBrutoPorcentajeUnoDos == null) {
            partMargenBrutoPorcentajeUnoDos = "";
        }
        return partMargenBrutoPorcentajeUnoDos;
    }

    public String getPartMargenBrutoCuantiaUnoTres() {
        if (partMargenBrutoCuantiaUnoTres == null) {
            partMargenBrutoCuantiaUnoTres = "";
        }
        return partMargenBrutoCuantiaUnoTres;
    }

    public String getPartMargenBrutoPorcentajeUnoTres() {
        if (partMargenBrutoPorcentajeUnoTres == null) {
            partMargenBrutoPorcentajeUnoTres = "";
        }
        return partMargenBrutoPorcentajeUnoTres;
    }

    public String getPartGastosPersonalesCorteUno() {
        if (partGastosPersonalesCorteUno == null) {
            partGastosPersonalesCorteUno = "";
        }
        return partGastosPersonalesCorteUno;
    }

    public String getPartGastosPersonalesCorteDos() {
        if (partGastosPersonalesCorteDos == null) {
            partGastosPersonalesCorteDos = "";
        }
        return partGastosPersonalesCorteDos;
    }

    public String getPartGastosPersonalesCorteTres() {
        if (partGastosPersonalesCorteTres == null) {
            partGastosPersonalesCorteTres = "";
        }
        return partGastosPersonalesCorteTres;
    }

    public String getPartGastosPersonalesCuantiaUnoDos() {
        if (partGastosPersonalesCuantiaUnoDos == null) {
            partGastosPersonalesCuantiaUnoDos = "";
        }
        return partGastosPersonalesCuantiaUnoDos;
    }

    public String getPartGastosPersonalesPorcentajeUnoDos() {
        if (partGastosPersonalesPorcentajeUnoDos == null) {
            partGastosPersonalesPorcentajeUnoDos = "";
        }
        return partGastosPersonalesPorcentajeUnoDos;
    }

    public String getPartGastosPersonalesCuantiaUnoTres() {
        if (partGastosPersonalesCuantiaUnoTres == null) {
            partGastosPersonalesCuantiaUnoTres = "";
        }
        return partGastosPersonalesCuantiaUnoTres;
    }

    public String getPartGastosPersonalesPorcentajeUnoTres() {
        if (partGastosPersonalesPorcentajeUnoTres == null) {
            partGastosPersonalesPorcentajeUnoTres = "";
        }
        return partGastosPersonalesPorcentajeUnoTres;
    }

    public String getPartGastosGeneralesCorteUno() {
        if (partGastosGeneralesCorteUno == null) {
            partGastosGeneralesCorteUno = "";
        }
        return partGastosGeneralesCorteUno;
    }

    public String getPartGastosGeneralesCorteDos() {
        if (partGastosGeneralesCorteDos == null) {
            partGastosGeneralesCorteDos = "";
        }
        return partGastosGeneralesCorteDos;
    }

    public String getPartGastosGeneralesCorteTres() {
        if (partGastosGeneralesCorteTres == null) {
            partGastosGeneralesCorteTres = "";
        }
        return partGastosGeneralesCorteTres;
    }

    public String getPartGastosGeneralesCuantiaUnoDos() {
        if (partGastosGeneralesCuantiaUnoDos == null) {
            partGastosGeneralesCuantiaUnoDos = "";
        }
        return partGastosGeneralesCuantiaUnoDos;
    }

    public String getPartGastosGeneralesPorcentajeUnoDos() {
        if (partGastosGeneralesPorcentajeUnoDos == null) {
            partGastosGeneralesPorcentajeUnoDos = "";
        }
        return partGastosGeneralesPorcentajeUnoDos;
    }

    public String getPartGastosGeneralesCuantiaUnoTres() {
        if (partGastosGeneralesCuantiaUnoTres == null) {
            partGastosGeneralesCuantiaUnoTres = "";
        }
        return partGastosGeneralesCuantiaUnoTres;
    }

    public String getPartGastosGeneralesPorcentajeUnoTres() {
        if (partGastosGeneralesPorcentajeUnoTres == null) {
            partGastosGeneralesPorcentajeUnoTres = "";
        }
        return partGastosGeneralesPorcentajeUnoTres;
    }

    public String getPartDepreciacionesAmortizacionesCorteUno() {
        if (partDepreciacionesAmortizacionesCorteUno == null) {
            partDepreciacionesAmortizacionesCorteUno = "";
        }
        return partDepreciacionesAmortizacionesCorteUno;
    }

    public String getPartDepreciacionesAmortizacionesCorteDos() {
        if (partDepreciacionesAmortizacionesCorteDos == null) {
            partDepreciacionesAmortizacionesCorteDos = "";
        }
        return partDepreciacionesAmortizacionesCorteDos;
    }

    public String getPartDepreciacionesAmortizacionesCorteTres() {
        if (partDepreciacionesAmortizacionesCorteTres == null) {
            partDepreciacionesAmortizacionesCorteTres = "";
        }
        return partDepreciacionesAmortizacionesCorteTres;
    }

    public String getPartDepreciacionesAmortizacionesCuantiaUnoDos() {
        if (partDepreciacionesAmortizacionesCuantiaUnoDos == null) {
            partDepreciacionesAmortizacionesCuantiaUnoDos = "";
        }
        return partDepreciacionesAmortizacionesCuantiaUnoDos;
    }

    public String getPartDepreciacionesAmortizacionesPorcentajeUnoDos() {
        if (partDepreciacionesAmortizacionesPorcentajeUnoDos == null) {
            partDepreciacionesAmortizacionesPorcentajeUnoDos = "";
        }
        return partDepreciacionesAmortizacionesPorcentajeUnoDos;
    }

    public String getPartDepreciacionesAmortizacionesCuantiaUnoTres() {
        if (partDepreciacionesAmortizacionesCuantiaUnoTres == null) {
            partDepreciacionesAmortizacionesCuantiaUnoTres = "";
        }
        return partDepreciacionesAmortizacionesCuantiaUnoTres;
    }

    public String getPartDepreciacionesAmortizacionesPorcentajeUnoTres() {
        if (partDepreciacionesAmortizacionesPorcentajeUnoTres == null) {
            partDepreciacionesAmortizacionesPorcentajeUnoTres = "";
        }
        return partDepreciacionesAmortizacionesPorcentajeUnoTres;
    }

    public String getPartEgresosOperacionalesCorteUno() {
        if (partEgresosOperacionalesCorteUno == null) {
            partEgresosOperacionalesCorteUno = "";
        }
        return partEgresosOperacionalesCorteUno;
    }

    public String getPartEgresosOperacionalesCorteDos() {
        if (partEgresosOperacionalesCorteDos == null) {
            partEgresosOperacionalesCorteDos = "";
        }
        return partEgresosOperacionalesCorteDos;
    }

    public String getPartEgresosOperacionalesCorteTres() {
        if (partEgresosOperacionalesCorteTres == null) {
            partEgresosOperacionalesCorteTres = "";
        }
        return partEgresosOperacionalesCorteTres;
    }

    public String getPartEgresosOperacionalesCuantiaUnoDos() {
        if (partEgresosOperacionalesCuantiaUnoDos == null) {
            partEgresosOperacionalesCuantiaUnoDos = "";
        }
        return partEgresosOperacionalesCuantiaUnoDos;
    }

    public String getPartEgresosOperacionalesPorcentajeUnoDos() {
        if (partEgresosOperacionalesPorcentajeUnoDos == null) {
            partEgresosOperacionalesPorcentajeUnoDos = "";
        }
        return partEgresosOperacionalesPorcentajeUnoDos;
    }

    public String getPartEgresosOperacionalesCuantiaUnoTres() {
        if (partEgresosOperacionalesCuantiaUnoTres == null) {
            partEgresosOperacionalesCuantiaUnoTres = "";
        }
        return partEgresosOperacionalesCuantiaUnoTres;
    }

    public String getPartEgresosOperacionalesPorcentajeUnoTres() {
        if (partEgresosOperacionalesPorcentajeUnoTres == null) {
            partEgresosOperacionalesPorcentajeUnoTres = "";
        }
        return partEgresosOperacionalesPorcentajeUnoTres;
    }

    public String getPartTotalGatosAdminCorteUno() {
        if (partTotalGatosAdminCorteUno == null) {
            partTotalGatosAdminCorteUno = "";
        }
        return partTotalGatosAdminCorteUno;
    }

    public String getPartTotalGatosAdminCorteDos() {
        if (partTotalGatosAdminCorteDos == null) {
            partTotalGatosAdminCorteDos = "";
        }
        return partTotalGatosAdminCorteDos;
    }

    public String getPartTotalGatosAdminCorteTres() {
        if (partTotalGatosAdminCorteTres == null) {
            partTotalGatosAdminCorteTres = "";
        }
        return partTotalGatosAdminCorteTres;
    }

    public String getPartTotalGatosAdminCuantiaUnoDos() {
        if (partTotalGatosAdminCuantiaUnoDos == null) {
            partTotalGatosAdminCuantiaUnoDos = "";
        }
        return partTotalGatosAdminCuantiaUnoDos;
    }

    public String getPartTotalGatosAdminPorcentajeUnoDos() {
        if (partTotalGatosAdminPorcentajeUnoDos == null) {
            partTotalGatosAdminPorcentajeUnoDos = "";
        }
        return partTotalGatosAdminPorcentajeUnoDos;
    }

    public String getPartTotalGatosAdminCuantiaUnoTres() {
        if (partTotalGatosAdminCuantiaUnoTres == null) {
            partTotalGatosAdminCuantiaUnoTres = "";
        }
        return partTotalGatosAdminCuantiaUnoTres;
    }

    public String getPartTotalGatosAdminPorcentajeUnoTres() {
        if (partTotalGatosAdminPorcentajeUnoTres == null) {
            partTotalGatosAdminPorcentajeUnoTres = "";
        }
        return partTotalGatosAdminPorcentajeUnoTres;
    }

    public String getPartExcedenteOperacionalAntesCorteUno() {
        if (partExcedenteOperacionalAntesCorteUno == null) {
            partExcedenteOperacionalAntesCorteUno = "";
        }
        return partExcedenteOperacionalAntesCorteUno;
    }

    public String getPartExcedenteOperacionalAntesCorteDos() {
        if (partExcedenteOperacionalAntesCorteDos == null) {
            partExcedenteOperacionalAntesCorteDos = "";
        }
        return partExcedenteOperacionalAntesCorteDos;
    }

    public String getPartExcedenteOperacionalAntesCorteTres() {
        if (partExcedenteOperacionalAntesCorteTres == null) {
            partExcedenteOperacionalAntesCorteTres = "";
        }
        return partExcedenteOperacionalAntesCorteTres;
    }

    public String getPartExcedenteOperacionalAntesCuantiaUnoDos() {
        if (partExcedenteOperacionalAntesCuantiaUnoDos == null) {
            partExcedenteOperacionalAntesCuantiaUnoDos = "";
        }
        return partExcedenteOperacionalAntesCuantiaUnoDos;
    }

    public String getPartExcedenteOperacionalAntesPorcentajeUnoDos() {
        if (partExcedenteOperacionalAntesPorcentajeUnoDos == null) {
            partExcedenteOperacionalAntesPorcentajeUnoDos = "";
        }
        return partExcedenteOperacionalAntesPorcentajeUnoDos;
    }

    public String getPartExcedenteOperacionalAntesCuantiaUnoTres() {
        if (partExcedenteOperacionalAntesCuantiaUnoTres == null) {
            partExcedenteOperacionalAntesCuantiaUnoTres = "";
        }
        return partExcedenteOperacionalAntesCuantiaUnoTres;
    }

    public String getPartExcedenteOperacionalAntesPorcentajeUnoTres() {
        if (partExcedenteOperacionalAntesPorcentajeUnoTres == null) {
            partExcedenteOperacionalAntesPorcentajeUnoTres = "";
        }
        return partExcedenteOperacionalAntesPorcentajeUnoTres;
    }

    public String getPartDeterioroCarteraCorteUno() {
        if (partDeterioroCarteraCorteUno == null) {
            partDeterioroCarteraCorteUno = "";
        }
        return partDeterioroCarteraCorteUno;
    }

    public String getPartDeterioroCarteraCorteDos() {
        if (partDeterioroCarteraCorteDos == null) {
            partDeterioroCarteraCorteDos = "";
        }
        return partDeterioroCarteraCorteDos;
    }

    public String getPartDeterioroCarteraCorteTres() {
        if (partDeterioroCarteraCorteTres == null) {
            partDeterioroCarteraCorteTres = "";
        }
        return partDeterioroCarteraCorteTres;
    }

    public String getPartDeterioroCarteraCuantiaUnoDos() {
        if (partDeterioroCarteraCuantiaUnoDos == null) {
            partDeterioroCarteraCuantiaUnoDos = "";
        }
        return partDeterioroCarteraCuantiaUnoDos;
    }

    public String getPartDeterioroCarteraPorcentajeUnoDos() {
        if (partDeterioroCarteraPorcentajeUnoDos == null) {
            partDeterioroCarteraPorcentajeUnoDos = "";
        }
        return partDeterioroCarteraPorcentajeUnoDos;
    }

    public String getPartDeterioroCarteraCuantiaUnoTres() {
        if (partDeterioroCarteraCuantiaUnoTres == null) {
            partDeterioroCarteraCuantiaUnoTres = "";
        }
        return partDeterioroCarteraCuantiaUnoTres;
    }

    public String getPartDeterioroCarteraPorcentajeUnoTres() {
        if (partDeterioroCarteraPorcentajeUnoTres == null) {
            partDeterioroCarteraPorcentajeUnoTres = "";
        }
        return partDeterioroCarteraPorcentajeUnoTres;
    }

    public String getPartDeterioroCuentasPorCobrarCorteUno() {
        if (partDeterioroCuentasPorCobrarCorteUno == null) {
            partDeterioroCuentasPorCobrarCorteUno = "";
        }
        return partDeterioroCuentasPorCobrarCorteUno;
    }

    public String getPartDeterioroCuentasPorCobrarCorteDos() {
        if (partDeterioroCuentasPorCobrarCorteDos == null) {
            partDeterioroCuentasPorCobrarCorteDos = "";
        }
        return partDeterioroCuentasPorCobrarCorteDos;
    }

    public String getPartDeterioroCuentasPorCobrarCorteTres() {
        if (partDeterioroCuentasPorCobrarCorteTres == null) {
            partDeterioroCuentasPorCobrarCorteTres = "";
        }
        return partDeterioroCuentasPorCobrarCorteTres;
    }

    public String getPartDeterioroCuentasPorCobrarCuantiaUnoDos() {
        if (partDeterioroCuentasPorCobrarCuantiaUnoDos == null) {
            partDeterioroCuentasPorCobrarCuantiaUnoDos = "";
        }
        return partDeterioroCuentasPorCobrarCuantiaUnoDos;
    }

    public String getPartDeterioroCuentasPorCobrarPorcentajeUnoDos() {
        if (partDeterioroCuentasPorCobrarPorcentajeUnoDos == null) {
            partDeterioroCuentasPorCobrarPorcentajeUnoDos = "";
        }
        return partDeterioroCuentasPorCobrarPorcentajeUnoDos;
    }

    public String getPartDeterioroCuentasPorCobrarCuantiaUnoTres() {
        if (partDeterioroCuentasPorCobrarCuantiaUnoTres == null) {
            partDeterioroCuentasPorCobrarCuantiaUnoTres = "";
        }
        return partDeterioroCuentasPorCobrarCuantiaUnoTres;
    }

    public String getPartDeterioroCuentasPorCobrarPorcentajeUnoTres() {
        if (partDeterioroCuentasPorCobrarPorcentajeUnoTres == null) {
            partDeterioroCuentasPorCobrarPorcentajeUnoTres = "";
        }
        return partDeterioroCuentasPorCobrarPorcentajeUnoTres;
    }

    public String getPartDeterioroInversionesCorteUno() {
        if (partDeterioroInversionesCorteUno == null) {
            partDeterioroInversionesCorteUno = "";
        }
        return partDeterioroInversionesCorteUno;
    }

    public String getPartDeterioroInversionesCorteDos() {
        if (partDeterioroInversionesCorteDos == null) {
            partDeterioroInversionesCorteDos = "";
        }
        return partDeterioroInversionesCorteDos;
    }

    public String getPartDeterioroInversionesCorteTres() {
        if (partDeterioroInversionesCorteTres == null) {
            partDeterioroInversionesCorteTres = "";
        }
        return partDeterioroInversionesCorteTres;
    }

    public String getPartDeterioroInversionesCuantiaUnoDos() {
        if (partDeterioroInversionesCuantiaUnoDos == null) {
            partDeterioroInversionesCuantiaUnoDos = "";
        }
        return partDeterioroInversionesCuantiaUnoDos;
    }

    public String getPartDeterioroInversionesPorcentajeUnoDos() {
        if (partDeterioroInversionesPorcentajeUnoDos == null) {
            partDeterioroInversionesPorcentajeUnoDos = "";
        }
        return partDeterioroInversionesPorcentajeUnoDos;
    }

    public String getPartDeterioroInversionesCuantiaUnoTres() {
        if (partDeterioroInversionesCuantiaUnoTres == null) {
            partDeterioroInversionesCuantiaUnoTres = "";
        }
        return partDeterioroInversionesCuantiaUnoTres;
    }

    public String getPartDeterioroInversionesPorcentajeUnoTres() {
        if (partDeterioroInversionesPorcentajeUnoTres == null) {
            partDeterioroInversionesPorcentajeUnoTres = "";
        }
        return partDeterioroInversionesPorcentajeUnoTres;
    }

    public String getPartOtrosDeteriorosCorteUno() {
        if (partOtrosDeteriorosCorteUno == null) {
            partOtrosDeteriorosCorteUno = "";
        }
        return partOtrosDeteriorosCorteUno;
    }

    public String getPartOtrosDeteriorosCorteDos() {
        if (partOtrosDeteriorosCorteDos == null) {
            partOtrosDeteriorosCorteDos = "";
        }
        return partOtrosDeteriorosCorteDos;
    }

    public String getPartOtrosDeteriorosCorteTres() {
        if (partOtrosDeteriorosCorteTres == null) {
            partOtrosDeteriorosCorteTres = "";
        }
        return partOtrosDeteriorosCorteTres;
    }

    public String getPartOtrosDeteriorosCuantiaUnoDos() {
        if (partOtrosDeteriorosCuantiaUnoDos == null) {
            partOtrosDeteriorosCuantiaUnoDos = "";
        }
        return partOtrosDeteriorosCuantiaUnoDos;
    }

    public String getPartOtrosDeteriorosPorcentajeUnoDos() {
        if (partOtrosDeteriorosPorcentajeUnoDos == null) {
            partOtrosDeteriorosPorcentajeUnoDos = "";
        }
        return partOtrosDeteriorosPorcentajeUnoDos;
    }

    public String getPartOtrosDeteriorosCuantiaUnoTres() {
        if (partOtrosDeteriorosCuantiaUnoTres == null) {
            partOtrosDeteriorosCuantiaUnoTres = "";
        }
        return partOtrosDeteriorosCuantiaUnoTres;
    }

    public String getPartOtrosDeteriorosPorcentajeUnoTres() {
        if (partOtrosDeteriorosPorcentajeUnoTres == null) {
            partOtrosDeteriorosPorcentajeUnoTres = "";
        }
        return partOtrosDeteriorosPorcentajeUnoTres;
    }

    public String getPartTotalDeteriorosCorteUno() {
        if (partTotalDeteriorosCorteUno == null) {
            partTotalDeteriorosCorteUno = "";
        }
        return partTotalDeteriorosCorteUno;
    }

    public String getPartTotalDeteriorosCorteDos() {
        if (partTotalDeteriorosCorteDos == null) {
            partTotalDeteriorosCorteDos = "";
        }
        return partTotalDeteriorosCorteDos;
    }

    public String getPartTotalDeteriorosCorteTres() {
        if (partTotalDeteriorosCorteTres == null) {
            partTotalDeteriorosCorteTres = "";
        }
        return partTotalDeteriorosCorteTres;
    }

    public String getPartTotalDeteriorosCuantiaUnoDos() {
        if (partTotalDeteriorosCuantiaUnoDos == null) {
            partTotalDeteriorosCuantiaUnoDos = "";
        }
        return partTotalDeteriorosCuantiaUnoDos;
    }

    public String getPartTotalDeteriorosPorcentajeUnoDos() {
        if (partTotalDeteriorosPorcentajeUnoDos == null) {
            partTotalDeteriorosPorcentajeUnoDos = "";
        }
        return partTotalDeteriorosPorcentajeUnoDos;
    }

    public String getPartTotalDeteriorosCuantiaUnoTres() {
        if (partTotalDeteriorosCuantiaUnoTres == null) {
            partTotalDeteriorosCuantiaUnoTres = "";
        }
        return partTotalDeteriorosCuantiaUnoTres;
    }

    public String getPartTotalDeteriorosPorcentajeUnoTres() {
        if (partTotalDeteriorosPorcentajeUnoTres == null) {
            partTotalDeteriorosPorcentajeUnoTres = "";
        }
        return partTotalDeteriorosPorcentajeUnoTres;
    }

    public String getPartRecuperacionesDeteriorosCorteUno() {
        if (partRecuperacionesDeteriorosCorteUno == null) {
            partRecuperacionesDeteriorosCorteUno = "";
        }
        return partRecuperacionesDeteriorosCorteUno;
    }

    public String getPartRecuperacionesDeteriorosCorteDos() {
        if (partRecuperacionesDeteriorosCorteDos == null) {
            partRecuperacionesDeteriorosCorteDos = "";
        }
        return partRecuperacionesDeteriorosCorteDos;
    }

    public String getPartRecuperacionesDeteriorosCorteTres() {
        if (partRecuperacionesDeteriorosCorteTres == null) {
            partRecuperacionesDeteriorosCorteTres = "";
        }
        return partRecuperacionesDeteriorosCorteTres;
    }

    public String getPartRecuperacionesDeteriorosCuantiaUnoDos() {
        if (partRecuperacionesDeteriorosCuantiaUnoDos == null) {
            partRecuperacionesDeteriorosCuantiaUnoDos = "";
        }
        return partRecuperacionesDeteriorosCuantiaUnoDos;
    }

    public String getPartRecuperacionesDeteriorosPorcentajeUnoDos() {
        if (partRecuperacionesDeteriorosPorcentajeUnoDos == null) {
            partRecuperacionesDeteriorosPorcentajeUnoDos = "";
        }
        return partRecuperacionesDeteriorosPorcentajeUnoDos;
    }

    public String getPartRecuperacionesDeteriorosCuantiaUnoTres() {
        if (partRecuperacionesDeteriorosCuantiaUnoTres == null) {
            partRecuperacionesDeteriorosCuantiaUnoTres = "";
        }
        return partRecuperacionesDeteriorosCuantiaUnoTres;
    }

    public String getPartRecuperacionesDeteriorosPorcentajeUnoTres() {
        if (partRecuperacionesDeteriorosPorcentajeUnoTres == null) {
            partRecuperacionesDeteriorosPorcentajeUnoTres = "";
        }
        return partRecuperacionesDeteriorosPorcentajeUnoTres;
    }

    public String getPartDeteriorosNetosCorteUno() {
        if (partDeteriorosNetosCorteUno == null) {
            partDeteriorosNetosCorteUno = "";
        }
        return partDeteriorosNetosCorteUno;
    }

    public String getPartDeteriorosNetosCorteDos() {
        if (partDeteriorosNetosCorteDos == null) {
            partDeteriorosNetosCorteDos = "";
        }
        return partDeteriorosNetosCorteDos;
    }

    public String getPartDeteriorosNetosCorteTres() {
        if (partDeteriorosNetosCorteTres == null) {
            partDeteriorosNetosCorteTres = "";
        }
        return partDeteriorosNetosCorteTres;
    }

    public String getPartDeteriorosNetosCuantiaUnoDos() {
        if (partDeteriorosNetosCuantiaUnoDos == null) {
            partDeteriorosNetosCuantiaUnoDos = "";
        }
        return partDeteriorosNetosCuantiaUnoDos;
    }

    public String getPartDeteriorosNetosPorcentajeUnoDos() {
        if (partDeteriorosNetosPorcentajeUnoDos == null) {
            partDeteriorosNetosPorcentajeUnoDos = "";
        }
        return partDeteriorosNetosPorcentajeUnoDos;
    }

    public String getPartDeteriorosNetosCuantiaUnoTres() {
        if (partDeteriorosNetosCuantiaUnoTres == null) {
            partDeteriorosNetosCuantiaUnoTres = "";
        }
        return partDeteriorosNetosCuantiaUnoTres;
    }

    public String getPartDeteriorosNetosPorcentajeUnoTres() {
        if (partDeteriorosNetosPorcentajeUnoTres == null) {
            partDeteriorosNetosPorcentajeUnoTres = "";
        }
        return partDeteriorosNetosPorcentajeUnoTres;
    }

    public String getPartExcedenteNetoAntesCorteUno() {
        if (partExcedenteNetoAntesCorteUno == null) {
            partExcedenteNetoAntesCorteUno = "";
        }
        return partExcedenteNetoAntesCorteUno;
    }

    public String getPartExcedenteNetoAntesCorteDos() {
        if (partExcedenteNetoAntesCorteDos == null) {
            partExcedenteNetoAntesCorteDos = "";
        }
        return partExcedenteNetoAntesCorteDos;
    }

    public String getPartExcedenteNetoAntesCorteTres() {
        if (partExcedenteNetoAntesCorteTres == null) {
            partExcedenteNetoAntesCorteTres = "";
        }
        return partExcedenteNetoAntesCorteTres;
    }

    public String getPartExcedenteNetoAntesCuantiaUnoDos() {
        if (partExcedenteNetoAntesCuantiaUnoDos == null) {
            partExcedenteNetoAntesCuantiaUnoDos = "";
        }
        return partExcedenteNetoAntesCuantiaUnoDos;
    }

    public String getPartExcedenteNetoAntesPorcentajeUnoDos() {
        if (partExcedenteNetoAntesPorcentajeUnoDos == null) {
            partExcedenteNetoAntesPorcentajeUnoDos = "";
        }
        return partExcedenteNetoAntesPorcentajeUnoDos;
    }

    public String getPartExcedenteNetoAntesCuantiaUnoTres() {
        if (partExcedenteNetoAntesCuantiaUnoTres == null) {
            partExcedenteNetoAntesCuantiaUnoTres = "";
        }
        return partExcedenteNetoAntesCuantiaUnoTres;
    }

    public String getPartExcedenteNetoAntesPorcentajeUnoTres() {
        if (partExcedenteNetoAntesPorcentajeUnoTres == null) {
            partExcedenteNetoAntesPorcentajeUnoTres = "";
        }
        return partExcedenteNetoAntesPorcentajeUnoTres;
    }

    public String getPartImpuestoCorteUno() {
        if (partImpuestoCorteUno == null) {
            partImpuestoCorteUno = "";
        }
        return partImpuestoCorteUno;
    }

    public String getPartImpuestoCorteDos() {
        if (partImpuestoCorteDos == null) {
            partImpuestoCorteDos = "";
        }
        return partImpuestoCorteDos;
    }

    public String getPartImpuestoCorteTres() {
        if (partImpuestoCorteTres == null) {
            partImpuestoCorteTres = "";
        }
        return partImpuestoCorteTres;
    }

    public String getPartImpuestoCuantiaUnoDos() {
        if (partImpuestoCuantiaUnoDos == null) {
            partImpuestoCuantiaUnoDos = "";
        }
        return partImpuestoCuantiaUnoDos;
    }

    public String getPartImpuestoPorcentajeUnoDos() {
        if (partImpuestoPorcentajeUnoDos == null) {
            partImpuestoPorcentajeUnoDos = "";
        }
        return partImpuestoPorcentajeUnoDos;
    }

    public String getPartImpuestoCuantiaUnoTres() {
        if (partImpuestoCuantiaUnoTres == null) {
            partImpuestoCuantiaUnoTres = "";
        }
        return partImpuestoCuantiaUnoTres;
    }

    public String getPartImpuestoPorcentajeUnoTres() {
        if (partImpuestoPorcentajeUnoTres == null) {
            partImpuestoPorcentajeUnoTres = "";
        }
        return partImpuestoPorcentajeUnoTres;
    }

    public String getPartExcedenteNetoCorteUno() {
        if (partExcedenteNetoCorteUno == null) {
            partExcedenteNetoCorteUno = "";
        }
        return partExcedenteNetoCorteUno;
    }

    public String getPartExcedenteNetoCorteDos() {
        if (partExcedenteNetoCorteDos == null) {
            partExcedenteNetoCorteDos = "";
        }
        return partExcedenteNetoCorteDos;
    }

    public String getPartExcedenteNetoCorteTres() {
        if (partExcedenteNetoCorteTres == null) {
            partExcedenteNetoCorteTres = "";
        }
        return partExcedenteNetoCorteTres;
    }

    public String getPartExcedenteNetoCuantiaUnoDos() {
        if (partExcedenteNetoCuantiaUnoDos == null) {
            partExcedenteNetoCuantiaUnoDos = "";
        }
        return partExcedenteNetoCuantiaUnoDos;
    }

    public String getPartExcedenteNetoPorcentajeUnoDos() {
        if (partExcedenteNetoPorcentajeUnoDos == null) {
            partExcedenteNetoPorcentajeUnoDos = "";
        }
        return partExcedenteNetoPorcentajeUnoDos;
    }

    public String getPartExcedenteNetoCuantiaUnoTres() {
        if (partExcedenteNetoCuantiaUnoTres == null) {
            partExcedenteNetoCuantiaUnoTres = "";
        }
        return partExcedenteNetoCuantiaUnoTres;
    }

    public String getPartExcedenteNetoPorcentajeUnoTres() {
        if (partExcedenteNetoPorcentajeUnoTres == null) {
            partExcedenteNetoPorcentajeUnoTres = "";
        }
        return partExcedenteNetoPorcentajeUnoTres;
    }

    public String getPartExcedentePerdidasORICorteUno() {
        if (partExcedentePerdidasORICorteUno == null) {
            partExcedentePerdidasORICorteUno = "";
        }
        return partExcedentePerdidasORICorteUno;
    }

    public String getPartExcedentePerdidasORICorteDos() {
        if (partExcedentePerdidasORICorteDos == null) {
            partExcedentePerdidasORICorteDos = "";
        }
        return partExcedentePerdidasORICorteDos;
    }

    public String getPartExcedentePerdidasORICorteTres() {
        if (partExcedentePerdidasORICorteTres == null) {
            partExcedentePerdidasORICorteTres = "";
        }
        return partExcedentePerdidasORICorteTres;
    }

    public String getPartExcedentePerdidasORICuantiaUnoDos() {
        if (partExcedentePerdidasORICuantiaUnoDos == null) {
            partExcedentePerdidasORICuantiaUnoDos = "";
        }
        return partExcedentePerdidasORICuantiaUnoDos;
    }

    public String getPartExcedentePerdidasORIPorcentajeUnoDos() {
        if (partExcedentePerdidasORIPorcentajeUnoDos == null) {
            partExcedentePerdidasORIPorcentajeUnoDos = "";
        }
        return partExcedentePerdidasORIPorcentajeUnoDos;
    }

    public String getPartExcedentePerdidasORICuantiaUnoTres() {
        if (partExcedentePerdidasORICuantiaUnoTres == null) {
            partExcedentePerdidasORICuantiaUnoTres = "";
        }
        return partExcedentePerdidasORICuantiaUnoTres;
    }

    public String getPartExcedentePerdidasORIPorcentajeUnoTres() {
        if (partExcedentePerdidasORIPorcentajeUnoTres == null) {
            partExcedentePerdidasORIPorcentajeUnoTres = "";
        }
        return partExcedentePerdidasORIPorcentajeUnoTres;
    }

    public String getPartExcedentePerdidasCorteUno() {
        if (partExcedentePerdidasCorteUno == null) {
            partExcedentePerdidasCorteUno = "";
        }
        return partExcedentePerdidasCorteUno;
    }

    public String getPartExcedentePerdidasCorteDos() {
        if (partExcedentePerdidasCorteDos == null) {
            partExcedentePerdidasCorteDos = "";
        }
        return partExcedentePerdidasCorteDos;
    }

    public String getPartExcedentePerdidasCorteTres() {
        if (partExcedentePerdidasCorteTres == null) {
            partExcedentePerdidasCorteTres = "";
        }
        return partExcedentePerdidasCorteTres;
    }

    public String getPartExcedentePerdidasCuantiaUnoDos() {
        if (partExcedentePerdidasCuantiaUnoDos == null) {
            partExcedentePerdidasCuantiaUnoDos = "";
        }
        return partExcedentePerdidasCuantiaUnoDos;
    }

    public String getPartExcedentePerdidasPorcentajeUnoDos() {
        if (partExcedentePerdidasPorcentajeUnoDos == null) {
            partExcedentePerdidasPorcentajeUnoDos = "";
        }
        return partExcedentePerdidasPorcentajeUnoDos;
    }

    public String getPartExcedentePerdidasCuantiaUnoTres() {
        if (partExcedentePerdidasCuantiaUnoTres == null) {
            partExcedentePerdidasCuantiaUnoTres = "";
        }
        return partExcedentePerdidasCuantiaUnoTres;
    }

    public String getPartExcedentePerdidasPorcentajeUnoTres() {
        if (partExcedentePerdidasPorcentajeUnoTres == null) {
            partExcedentePerdidasPorcentajeUnoTres = "";
        }
        return partExcedentePerdidasPorcentajeUnoTres;
    }

}
