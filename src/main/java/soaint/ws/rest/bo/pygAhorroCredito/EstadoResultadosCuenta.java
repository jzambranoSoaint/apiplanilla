package soaint.ws.rest.bo.pygAhorroCredito;

public class EstadoResultadosCuenta {
    private String labelCorteUno;
    private String labelCorteDos;
    private String labelCorteTres;
    private String labelCorteCuatro;
    private String labelCorteCinco;
    private String labelCorteSeis;
    private String labelCorteSiete;
    private String labelCorteOcho;
    private String labelCorteNueve;
    private String labelCorteDiez;
    private String labelCorteOnce;
    private String labelCorteDoce;
    private String labelCorteTrece;
    private String interesesRecibidosCarteraCorteUno;
    private String interesesRecibidosCarteraCorteDos;
    private String interesesRecibidosCarteraCorteTres;
    private String interesesRecibidosCarteraCorteCuatro;
    private String interesesRecibidosCarteraCorteCinco;
    private String interesesRecibidosCarteraCorteSeis;
    private String interesesRecibidosCarteraCorteSiete;
    private String interesesRecibidosCarteraCorteOcho;
    private String interesesRecibidosCarteraCorteNueve;
    private String interesesRecibidosCarteraCorteDiez;
    private String interesesRecibidosCarteraCorteOnce;
    private String interesesRecibidosCarteraCorteDoce;
    private String interesesRecibidosCarteraCorteTrece;
    private String interesesPagadosDepositosCorteUno;
    private String interesesPagadosDepositosCorteDos;
    private String interesesPagadosDepositosCorteTres;
    private String interesesPagadosDepositosCorteCuatro;
    private String interesesPagadosDepositosCorteCinco;
    private String interesesPagadosDepositosCorteSeis;
    private String interesesPagadosDepositosCorteSiete;
    private String interesesPagadosDepositosCorteOcho;
    private String interesesPagadosDepositosCorteNueve;
    private String interesesPagadosDepositosCorteDiez;
    private String interesesPagadosDepositosCorteOnce;
    private String interesesPagadosDepositosCorteDoce;
    private String interesesPagadosDepositosCorteTrece;
    private String margenIntermediacionCorteUno;
    private String margenIntermediacionCorteDos;
    private String margenIntermediacionCorteTres;
    private String margenIntermediacionCorteCuatro;
    private String margenIntermediacionCorteCinco;
    private String margenIntermediacionCorteSeis;
    private String margenIntermediacionCorteSiete;
    private String margenIntermediacionCorteOcho;
    private String margenIntermediacionCorteNueve;
    private String margenIntermediacionCorteDiez;
    private String margenIntermediacionCorteOnce;
    private String margenIntermediacionCorteDoce;
    private String margenIntermediacionCorteTrece;
    private String otrosIngresosCorteUno;
    private String otrosIngresosCorteDos;
    private String otrosIngresosCorteTres;
    private String otrosIngresosCorteCuatro;
    private String otrosIngresosCorteCinco;
    private String otrosIngresosCorteSeis;
    private String otrosIngresosCorteSiete;
    private String otrosIngresosCorteOcho;
    private String otrosIngresosCorteNueve;
    private String otrosIngresosCorteDiez;
    private String otrosIngresosCorteOnce;
    private String otrosIngresosCorteDoce;
    private String otrosIngresosCorteTrece;
    private String otrosEgresosCorteUno;
    private String otrosEgresosCorteDos;
    private String otrosEgresosCorteTres;
    private String otrosEgresosCorteCuatro;
    private String otrosEgresosCorteCinco;
    private String otrosEgresosCorteSeis;
    private String otrosEgresosCorteSiete;
    private String otrosEgresosCorteOcho;
    private String otrosEgresosCorteNueve;
    private String otrosEgresosCorteDiez;
    private String otrosEgresosCorteOnce;
    private String otrosEgresosCorteDoce;
    private String otrosEgresosCorteTrece;
    private String margenFinancieroBrutoCorteUno;
    private String margenFinancieroBrutoCorteDos;
    private String margenFinancieroBrutoCorteTres;
    private String margenFinancieroBrutoCorteCuatro;
    private String margenFinancieroBrutoCorteCinco;
    private String margenFinancieroBrutoCorteSeis;
    private String margenFinancieroBrutoCorteSiete;
    private String margenFinancieroBrutoCorteOcho;
    private String margenFinancieroBrutoCorteNueve;
    private String margenFinancieroBrutoCorteDiez;
    private String margenFinancieroBrutoCorteOnce;
    private String margenFinancieroBrutoCorteDoce;
    private String margenFinancieroBrutoCorteTrece;
    private String otrosIngresosOperacionalesCorteUno;
    private String otrosIngresosOperacionalesCorteDos;
    private String otrosIngresosOperacionalesCorteTres;
    private String otrosIngresosOperacionalesCorteCuatro;
    private String otrosIngresosOperacionalesCorteCinco;
    private String otrosIngresosOperacionalesCorteSeis;
    private String otrosIngresosOperacionalesCorteSiete;
    private String otrosIngresosOperacionalesCorteOcho;
    private String otrosIngresosOperacionalesCorteNueve;
    private String otrosIngresosOperacionalesCorteDiez;
    private String otrosIngresosOperacionalesCorteOnce;
    private String otrosIngresosOperacionalesCorteDoce;
    private String otrosIngresosOperacionalesCorteTrece;
    private String margenBrutoCorteUno;
    private String margenBrutoCorteDos;
    private String margenBrutoCorteTres;
    private String margenBrutoCorteCuatro;
    private String margenBrutoCorteCinco;
    private String margenBrutoCorteSeis;
    private String margenBrutoCorteSiete;
    private String margenBrutoCorteOcho;
    private String margenBrutoCorteNueve;
    private String margenBrutoCorteDiez;
    private String margenBrutoCorteOnce;
    private String margenBrutoCorteDoce;
    private String margenBrutoCorteTrece;
    private String gastosPersonalesCorteUno;
    private String gastosPersonalesCorteDos;
    private String gastosPersonalesCorteTres;
    private String gastosPersonalesCorteCuatro;
    private String gastosPersonalesCorteCinco;
    private String gastosPersonalesCorteSeis;
    private String gastosPersonalesCorteSiete;
    private String gastosPersonalesCorteOcho;
    private String gastosPersonalesCorteNueve;
    private String gastosPersonalesCorteDiez;
    private String gastosPersonalesCorteOnce;
    private String gastosPersonalesCorteDoce;
    private String gastosPersonalesCorteTrece;
    private String gastosGeneralesCorteUno;
    private String gastosGeneralesCorteDos;
    private String gastosGeneralesCorteTres;
    private String gastosGeneralesCorteCuatro;
    private String gastosGeneralesCorteCinco;
    private String gastosGeneralesCorteSeis;
    private String gastosGeneralesCorteSiete;
    private String gastosGeneralesCorteOcho;
    private String gastosGeneralesCorteNueve;
    private String gastosGeneralesCorteDiez;
    private String gastosGeneralesCorteOnce;
    private String gastosGeneralesCorteDoce;
    private String gastosGeneralesCorteTrece;
    private String depreciacionesAmortizacionesCorteUno;
    private String depreciacionesAmortizacionesCorteDos;
    private String depreciacionesAmortizacionesCorteTres;
    private String depreciacionesAmortizacionesCorteCuatro;
    private String depreciacionesAmortizacionesCorteCinco;
    private String depreciacionesAmortizacionesCorteSeis;
    private String depreciacionesAmortizacionesCorteSiete;
    private String depreciacionesAmortizacionesCorteOcho;
    private String depreciacionesAmortizacionesCorteNueve;
    private String depreciacionesAmortizacionesCorteDiez;
    private String depreciacionesAmortizacionesCorteOnce;
    private String depreciacionesAmortizacionesCorteDoce;
    private String depreciacionesAmortizacionesCorteTrece;
    private String egresosOperacionalesCorteUno;
    private String egresosOperacionalesCorteDos;
    private String egresosOperacionalesCorteTres;
    private String egresosOperacionalesCorteCuatro;
    private String egresosOperacionalesCorteCinco;
    private String egresosOperacionalesCorteSeis;
    private String egresosOperacionalesCorteSiete;
    private String egresosOperacionalesCorteOcho;
    private String egresosOperacionalesCorteNueve;
    private String egresosOperacionalesCorteDiez;
    private String egresosOperacionalesCorteOnce;
    private String egresosOperacionalesCorteDoce;
    private String egresosOperacionalesCorteTrece;
    private String totalGatosAdminCorteUno;
    private String totalGatosAdminCorteDos;
    private String totalGatosAdminCorteTres;
    private String totalGatosAdminCorteCuatro;
    private String totalGatosAdminCorteCinco;
    private String totalGatosAdminCorteSeis;
    private String totalGatosAdminCorteSiete;
    private String totalGatosAdminCorteOcho;
    private String totalGatosAdminCorteNueve;
    private String totalGatosAdminCorteDiez;
    private String totalGatosAdminCorteOnce;
    private String totalGatosAdminCorteDoce;
    private String totalGatosAdminCorteTrece;
    private String excedenteOperacionalAntesCorteUno;
    private String excedenteOperacionalAntesCorteDos;
    private String excedenteOperacionalAntesCorteTres;
    private String excedenteOperacionalAntesCorteCuatro;
    private String excedenteOperacionalAntesCorteCinco;
    private String excedenteOperacionalAntesCorteSeis;
    private String excedenteOperacionalAntesCorteSiete;
    private String excedenteOperacionalAntesCorteOcho;
    private String excedenteOperacionalAntesCorteNueve;
    private String excedenteOperacionalAntesCorteDiez;
    private String excedenteOperacionalAntesCorteOnce;
    private String excedenteOperacionalAntesCorteDoce;
    private String excedenteOperacionalAntesCorteTrece;
    private String deterioroCarteraCorteUno;
    private String deterioroCarteraCorteDos;
    private String deterioroCarteraCorteTres;
    private String deterioroCarteraCorteCuatro;
    private String deterioroCarteraCorteCinco;
    private String deterioroCarteraCorteSeis;
    private String deterioroCarteraCorteSiete;
    private String deterioroCarteraCorteOcho;
    private String deterioroCarteraCorteNueve;
    private String deterioroCarteraCorteDiez;
    private String deterioroCarteraCorteOnce;
    private String deterioroCarteraCorteDoce;
    private String deterioroCarteraCorteTrece;
    private String deterioroCuentasPorCobrarCorteUno;
    private String deterioroCuentasPorCobrarCorteDos;
    private String deterioroCuentasPorCobrarCorteTres;
    private String deterioroCuentasPorCobrarCorteCuatro;
    private String deterioroCuentasPorCobrarCorteCinco;
    private String deterioroCuentasPorCobrarCorteSeis;
    private String deterioroCuentasPorCobrarCorteSiete;
    private String deterioroCuentasPorCobrarCorteOcho;
    private String deterioroCuentasPorCobrarCorteNueve;
    private String deterioroCuentasPorCobrarCorteDiez;
    private String deterioroCuentasPorCobrarCorteOnce;
    private String deterioroCuentasPorCobrarCorteDoce;
    private String deterioroCuentasPorCobrarCorteTrece;
    private String deterioroInversionesCorteUno;
    private String deterioroInversionesCorteDos;
    private String deterioroInversionesCorteTres;
    private String deterioroInversionesCorteCuatro;
    private String deterioroInversionesCorteCinco;
    private String deterioroInversionesCorteSeis;
    private String deterioroInversionesCorteSiete;
    private String deterioroInversionesCorteOcho;
    private String deterioroInversionesCorteNueve;
    private String deterioroInversionesCorteDiez;
    private String deterioroInversionesCorteOnce;
    private String deterioroInversionesCorteDoce;
    private String deterioroInversionesCorteTrece;
    private String otrosDeteriorosCorteUno;
    private String otrosDeteriorosCorteDos;
    private String otrosDeteriorosCorteTres;
    private String otrosDeteriorosCorteCuatro;
    private String otrosDeteriorosCorteCinco;
    private String otrosDeteriorosCorteSeis;
    private String otrosDeteriorosCorteSiete;
    private String otrosDeteriorosCorteOcho;
    private String otrosDeteriorosCorteNueve;
    private String otrosDeteriorosCorteDiez;
    private String otrosDeteriorosCorteOnce;
    private String otrosDeteriorosCorteDoce;
    private String otrosDeteriorosCorteTrece;
    private String totalDeteriorosCorteUno;
    private String totalDeteriorosCorteDos;
    private String totalDeteriorosCorteTres;
    private String totalDeteriorosCorteCuatro;
    private String totalDeteriorosCorteCinco;
    private String totalDeteriorosCorteSeis;
    private String totalDeteriorosCorteSiete;
    private String totalDeteriorosCorteOcho;
    private String totalDeteriorosCorteNueve;
    private String totalDeteriorosCorteDiez;
    private String totalDeteriorosCorteOnce;
    private String totalDeteriorosCorteDoce;
    private String totalDeteriorosCorteTrece;
    private String recuperacionesDeteriorosCorteUno;
    private String recuperacionesDeteriorosCorteDos;
    private String recuperacionesDeteriorosCorteTres;
    private String recuperacionesDeteriorosCorteCuatro;
    private String recuperacionesDeteriorosCorteCinco;
    private String recuperacionesDeteriorosCorteSeis;
    private String recuperacionesDeteriorosCorteSiete;
    private String recuperacionesDeteriorosCorteOcho;
    private String recuperacionesDeteriorosCorteNueve;
    private String recuperacionesDeteriorosCorteDiez;
    private String recuperacionesDeteriorosCorteOnce;
    private String recuperacionesDeteriorosCorteDoce;
    private String recuperacionesDeteriorosCorteTrece;
    private String deteriorosNetosCorteUno;
    private String deteriorosNetosCorteDos;
    private String deteriorosNetosCorteTres;
    private String deteriorosNetosCorteCuatro;
    private String deteriorosNetosCorteCinco;
    private String deteriorosNetosCorteSeis;
    private String deteriorosNetosCorteSiete;
    private String deteriorosNetosCorteOcho;
    private String deteriorosNetosCorteNueve;
    private String deteriorosNetosCorteDiez;
    private String deteriorosNetosCorteOnce;
    private String deteriorosNetosCorteDoce;
    private String deteriorosNetosCorteTrece;
    private String excedenteNetoAntesCorteUno;
    private String excedenteNetoAntesCorteDos;
    private String excedenteNetoAntesCorteTres;
    private String excedenteNetoAntesCorteCuatro;
    private String excedenteNetoAntesCorteCinco;
    private String excedenteNetoAntesCorteSeis;
    private String excedenteNetoAntesCorteSiete;
    private String excedenteNetoAntesCorteOcho;
    private String excedenteNetoAntesCorteNueve;
    private String excedenteNetoAntesCorteDiez;
    private String excedenteNetoAntesCorteOnce;
    private String excedenteNetoAntesCorteDoce;
    private String excedenteNetoAntesCorteTrece;
    private String impuestoCorteUno;
    private String impuestoCorteDos;
    private String impuestoCorteTres;
    private String impuestoCorteCuatro;
    private String impuestoCorteCinco;
    private String impuestoCorteSeis;
    private String impuestoCorteSiete;
    private String impuestoCorteOcho;
    private String impuestoCorteNueve;
    private String impuestoCorteDiez;
    private String impuestoCorteOnce;
    private String impuestoCorteDoce;
    private String impuestoCorteTrece;
    private String excedenteNetoCorteUno;
    private String excedenteNetoCorteDos;
    private String excedenteNetoCorteTres;
    private String excedenteNetoCorteCuatro;
    private String excedenteNetoCorteCinco;
    private String excedenteNetoCorteSeis;
    private String excedenteNetoCorteSiete;
    private String excedenteNetoCorteOcho;
    private String excedenteNetoCorteNueve;
    private String excedenteNetoCorteDiez;
    private String excedenteNetoCorteOnce;
    private String excedenteNetoCorteDoce;
    private String excedenteNetoCorteTrece;
    private String excedentePerdidasORICorteUno;
    private String excedentePerdidasORICorteDos;
    private String excedentePerdidasORICorteTres;
    private String excedentePerdidasORICorteCuatro;
    private String excedentePerdidasORICorteCinco;
    private String excedentePerdidasORICorteSeis;
    private String excedentePerdidasORICorteSiete;
    private String excedentePerdidasORICorteOcho;
    private String excedentePerdidasORICorteNueve;
    private String excedentePerdidasORICorteDiez;
    private String excedentePerdidasORICorteOnce;
    private String excedentePerdidasORICorteDoce;
    private String excedentePerdidasORICorteTrece;
    private String excedentePerdidasCorteUno;
    private String excedentePerdidasCorteDos;
    private String excedentePerdidasCorteTres;
    private String excedentePerdidasCorteCuatro;
    private String excedentePerdidasCorteCinco;
    private String excedentePerdidasCorteSeis;
    private String excedentePerdidasCorteSiete;
    private String excedentePerdidasCorteOcho;
    private String excedentePerdidasCorteNueve;
    private String excedentePerdidasCorteDiez;
    private String excedentePerdidasCorteOnce;
    private String excedentePerdidasCorteDoce;
    private String excedentePerdidasCorteTrece;

    public String getLabelCorteUno() {
        if (labelCorteUno == null) {
            labelCorteUno = "";
        }
        return labelCorteUno;
    }

    public String getLabelCorteDos() {
        if (labelCorteDos == null) {
            labelCorteDos = "";
        }
        return labelCorteDos;
    }

    public String getLabelCorteTres() {
        if (labelCorteTres == null) {
            labelCorteTres = "";
        }
        return labelCorteTres;
    }

    public String getLabelCorteCuatro() {
        if (labelCorteCuatro == null) {
            labelCorteCuatro = "";
        }
        return labelCorteCuatro;
    }

    public String getLabelCorteCinco() {
        if (labelCorteCinco == null) {
            labelCorteCinco = "";
        }
        return labelCorteCinco;
    }

    public String getLabelCorteSeis() {
        if (labelCorteSeis == null) {
            labelCorteSeis = "";
        }
        return labelCorteSeis;
    }

    public String getLabelCorteSiete() {
        if (labelCorteSiete == null) {
            labelCorteSiete = "";
        }
        return labelCorteSiete;
    }

    public String getLabelCorteOcho() {
        if (labelCorteOcho == null) {
            labelCorteOcho = "";
        }
        return labelCorteOcho;
    }

    public String getLabelCorteNueve() {
        if (labelCorteNueve == null) {
            labelCorteNueve = "";
        }
        return labelCorteNueve;
    }

    public String getLabelCorteDiez() {
        if (labelCorteDiez == null) {
            labelCorteDiez = "";
        }
        return labelCorteDiez;
    }

    public String getLabelCorteOnce() {
        if (labelCorteOnce == null) {
            labelCorteOnce = "";
        }
        return labelCorteOnce;
    }

    public String getLabelCorteDoce() {
        if (labelCorteDoce == null) {
            labelCorteDoce = "";
        }
        return labelCorteDoce;
    }

    public String getLabelCorteTrece() {
        if (labelCorteTrece == null) {
            labelCorteTrece = "";
        }
        return labelCorteTrece;
    }

    public String getInteresesRecibidosCarteraCorteUno() {
        if (interesesRecibidosCarteraCorteUno == null) {
            interesesRecibidosCarteraCorteUno = "";
        }
        return interesesRecibidosCarteraCorteUno;
    }

    public String getInteresesRecibidosCarteraCorteDos() {
        if (interesesRecibidosCarteraCorteDos == null) {
            interesesRecibidosCarteraCorteDos = "";
        }
        return interesesRecibidosCarteraCorteDos;
    }

    public String getInteresesRecibidosCarteraCorteTres() {
        if (interesesRecibidosCarteraCorteTres == null) {
            interesesRecibidosCarteraCorteTres = "";
        }
        return interesesRecibidosCarteraCorteTres;
    }

    public String getInteresesRecibidosCarteraCorteCuatro() {
        if (interesesRecibidosCarteraCorteCuatro == null) {
            interesesRecibidosCarteraCorteCuatro = "";
        }
        return interesesRecibidosCarteraCorteCuatro;
    }

    public String getInteresesRecibidosCarteraCorteCinco() {
        if (interesesRecibidosCarteraCorteCinco == null) {
            interesesRecibidosCarteraCorteCinco = "";
        }
        return interesesRecibidosCarteraCorteCinco;
    }

    public String getInteresesRecibidosCarteraCorteSeis() {
        if (interesesRecibidosCarteraCorteSeis == null) {
            interesesRecibidosCarteraCorteSeis = "";
        }
        return interesesRecibidosCarteraCorteSeis;
    }

    public String getInteresesRecibidosCarteraCorteSiete() {
        if (interesesRecibidosCarteraCorteSiete == null) {
            interesesRecibidosCarteraCorteSiete = "";
        }
        return interesesRecibidosCarteraCorteSiete;
    }

    public String getInteresesRecibidosCarteraCorteOcho() {
        if (interesesRecibidosCarteraCorteOcho == null) {
            interesesRecibidosCarteraCorteOcho = "";
        }
        return interesesRecibidosCarteraCorteOcho;
    }

    public String getInteresesRecibidosCarteraCorteNueve() {
        if (interesesRecibidosCarteraCorteNueve == null) {
            interesesRecibidosCarteraCorteNueve = "";
        }
        return interesesRecibidosCarteraCorteNueve;
    }

    public String getInteresesRecibidosCarteraCorteDiez() {
        if (interesesRecibidosCarteraCorteDiez == null) {
            interesesRecibidosCarteraCorteDiez = "";
        }
        return interesesRecibidosCarteraCorteDiez;
    }

    public String getInteresesRecibidosCarteraCorteOnce() {
        if (interesesRecibidosCarteraCorteOnce == null) {
            interesesRecibidosCarteraCorteOnce = "";
        }
        return interesesRecibidosCarteraCorteOnce;
    }

    public String getInteresesRecibidosCarteraCorteDoce() {
        if (interesesRecibidosCarteraCorteDoce == null) {
            interesesRecibidosCarteraCorteDoce = "";
        }
        return interesesRecibidosCarteraCorteDoce;
    }

    public String getInteresesRecibidosCarteraCorteTrece() {
        if (interesesRecibidosCarteraCorteTrece == null) {
            interesesRecibidosCarteraCorteTrece = "";
        }
        return interesesRecibidosCarteraCorteTrece;
    }

    public String getInteresesPagadosDepositosCorteUno() {
        if (interesesPagadosDepositosCorteUno == null) {
            interesesPagadosDepositosCorteUno = "";
        }
        return interesesPagadosDepositosCorteUno;
    }

    public String getInteresesPagadosDepositosCorteDos() {
        if (interesesPagadosDepositosCorteDos == null) {
            interesesPagadosDepositosCorteDos = "";
        }
        return interesesPagadosDepositosCorteDos;
    }

    public String getInteresesPagadosDepositosCorteTres() {
        if (interesesPagadosDepositosCorteTres == null) {
            interesesPagadosDepositosCorteTres = "";
        }
        return interesesPagadosDepositosCorteTres;
    }

    public String getInteresesPagadosDepositosCorteCuatro() {
        if (interesesPagadosDepositosCorteCuatro == null) {
            interesesPagadosDepositosCorteCuatro = "";
        }
        return interesesPagadosDepositosCorteCuatro;
    }

    public String getInteresesPagadosDepositosCorteCinco() {
        if (interesesPagadosDepositosCorteCinco == null) {
            interesesPagadosDepositosCorteCinco = "";
        }
        return interesesPagadosDepositosCorteCinco;
    }

    public String getInteresesPagadosDepositosCorteSeis() {
        if (interesesPagadosDepositosCorteSeis == null) {
            interesesPagadosDepositosCorteSeis = "";
        }
        return interesesPagadosDepositosCorteSeis;
    }

    public String getInteresesPagadosDepositosCorteSiete() {
        if (interesesPagadosDepositosCorteSiete == null) {
            interesesPagadosDepositosCorteSiete = "";
        }
        return interesesPagadosDepositosCorteSiete;
    }

    public String getInteresesPagadosDepositosCorteOcho() {
        if (interesesPagadosDepositosCorteOcho == null) {
            interesesPagadosDepositosCorteOcho = "";
        }
        return interesesPagadosDepositosCorteOcho;
    }

    public String getInteresesPagadosDepositosCorteNueve() {
        if (interesesPagadosDepositosCorteNueve == null) {
            interesesPagadosDepositosCorteNueve = "";
        }
        return interesesPagadosDepositosCorteNueve;
    }

    public String getInteresesPagadosDepositosCorteDiez() {
        if (interesesPagadosDepositosCorteDiez == null) {
            interesesPagadosDepositosCorteDiez = "";
        }
        return interesesPagadosDepositosCorteDiez;
    }

    public String getInteresesPagadosDepositosCorteOnce() {
        if (interesesPagadosDepositosCorteOnce == null) {
            interesesPagadosDepositosCorteOnce = "";
        }
        return interesesPagadosDepositosCorteOnce;
    }

    public String getInteresesPagadosDepositosCorteDoce() {
        if (interesesPagadosDepositosCorteDoce == null) {
            interesesPagadosDepositosCorteDoce = "";
        }
        return interesesPagadosDepositosCorteDoce;
    }

    public String getInteresesPagadosDepositosCorteTrece() {
        if (interesesPagadosDepositosCorteTrece == null) {
            interesesPagadosDepositosCorteTrece = "";
        }
        return interesesPagadosDepositosCorteTrece;
    }

    public String getMargenIntermediacionCorteUno() {
        if (margenIntermediacionCorteUno == null) {
            margenIntermediacionCorteUno = "";
        }
        return margenIntermediacionCorteUno;
    }

    public String getMargenIntermediacionCorteDos() {
        if (margenIntermediacionCorteDos == null) {
            margenIntermediacionCorteDos = "";
        }
        return margenIntermediacionCorteDos;
    }

    public String getMargenIntermediacionCorteTres() {
        if (margenIntermediacionCorteTres == null) {
            margenIntermediacionCorteTres = "";
        }
        return margenIntermediacionCorteTres;
    }

    public String getMargenIntermediacionCorteCuatro() {
        if (margenIntermediacionCorteCuatro == null) {
            margenIntermediacionCorteCuatro = "";
        }
        return margenIntermediacionCorteCuatro;
    }

    public String getMargenIntermediacionCorteCinco() {
        if (margenIntermediacionCorteCinco == null) {
            margenIntermediacionCorteCinco = "";
        }
        return margenIntermediacionCorteCinco;
    }

    public String getMargenIntermediacionCorteSeis() {
        if (margenIntermediacionCorteSeis == null) {
            margenIntermediacionCorteSeis = "";
        }
        return margenIntermediacionCorteSeis;
    }

    public String getMargenIntermediacionCorteSiete() {
        if (margenIntermediacionCorteSiete == null) {
            margenIntermediacionCorteSiete = "";
        }
        return margenIntermediacionCorteSiete;
    }

    public String getMargenIntermediacionCorteOcho() {
        if (margenIntermediacionCorteOcho == null) {
            margenIntermediacionCorteOcho = "";
        }
        return margenIntermediacionCorteOcho;
    }

    public String getMargenIntermediacionCorteNueve() {
        if (margenIntermediacionCorteNueve == null) {
            margenIntermediacionCorteNueve = "";
        }
        return margenIntermediacionCorteNueve;
    }

    public String getMargenIntermediacionCorteDiez() {
        if (margenIntermediacionCorteDiez == null) {
            margenIntermediacionCorteDiez = "";
        }
        return margenIntermediacionCorteDiez;
    }

    public String getMargenIntermediacionCorteOnce() {
        if (margenIntermediacionCorteOnce == null) {
            margenIntermediacionCorteOnce = "";
        }
        return margenIntermediacionCorteOnce;
    }

    public String getMargenIntermediacionCorteDoce() {
        if (margenIntermediacionCorteDoce == null) {
            margenIntermediacionCorteDoce = "";
        }
        return margenIntermediacionCorteDoce;
    }

    public String getMargenIntermediacionCorteTrece() {
        if (margenIntermediacionCorteTrece == null) {
            margenIntermediacionCorteTrece = "";
        }
        return margenIntermediacionCorteTrece;
    }

    public String getOtrosIngresosCorteUno() {
        if (otrosIngresosCorteUno == null) {
            otrosIngresosCorteUno = "";
        }
        return otrosIngresosCorteUno;
    }

    public String getOtrosIngresosCorteDos() {
        if (otrosIngresosCorteDos == null) {
            otrosIngresosCorteDos = "";
        }
        return otrosIngresosCorteDos;
    }

    public String getOtrosIngresosCorteTres() {
        if (otrosIngresosCorteTres == null) {
            otrosIngresosCorteTres = "";
        }
        return otrosIngresosCorteTres;
    }

    public String getOtrosIngresosCorteCuatro() {
        if (otrosIngresosCorteCuatro == null) {
            otrosIngresosCorteCuatro = "";
        }
        return otrosIngresosCorteCuatro;
    }

    public String getOtrosIngresosCorteCinco() {
        if (otrosIngresosCorteCinco == null) {
            otrosIngresosCorteCinco = "";
        }
        return otrosIngresosCorteCinco;
    }

    public String getOtrosIngresosCorteSeis() {
        if (otrosIngresosCorteSeis == null) {
            otrosIngresosCorteSeis = "";
        }
        return otrosIngresosCorteSeis;
    }

    public String getOtrosIngresosCorteSiete() {
        if (otrosIngresosCorteSiete == null) {
            otrosIngresosCorteSiete = "";
        }
        return otrosIngresosCorteSiete;
    }

    public String getOtrosIngresosCorteOcho() {
        if (otrosIngresosCorteOcho == null) {
            otrosIngresosCorteOcho = "";
        }
        return otrosIngresosCorteOcho;
    }

    public String getOtrosIngresosCorteNueve() {
        if (otrosIngresosCorteNueve == null) {
            otrosIngresosCorteNueve = "";
        }
        return otrosIngresosCorteNueve;
    }

    public String getOtrosIngresosCorteDiez() {
        if (otrosIngresosCorteDiez == null) {
            otrosIngresosCorteDiez = "";
        }
        return otrosIngresosCorteDiez;
    }

    public String getOtrosIngresosCorteOnce() {
        if (otrosIngresosCorteOnce == null) {
            otrosIngresosCorteOnce = "";
        }
        return otrosIngresosCorteOnce;
    }

    public String getOtrosIngresosCorteDoce() {
        if (otrosIngresosCorteDoce == null) {
            otrosIngresosCorteDoce = "";
        }
        return otrosIngresosCorteDoce;
    }

    public String getOtrosIngresosCorteTrece() {
        if (otrosIngresosCorteTrece == null) {
            otrosIngresosCorteTrece = "";
        }
        return otrosIngresosCorteTrece;
    }

    public String getOtrosEgresosCorteUno() {
        if (otrosEgresosCorteUno == null) {
            otrosEgresosCorteUno = "";
        }
        return otrosEgresosCorteUno;
    }

    public String getOtrosEgresosCorteDos() {
        if (otrosEgresosCorteDos == null) {
            otrosEgresosCorteDos = "";
        }
        return otrosEgresosCorteDos;
    }

    public String getOtrosEgresosCorteTres() {
        if (otrosEgresosCorteTres == null) {
            otrosEgresosCorteTres = "";
        }
        return otrosEgresosCorteTres;
    }

    public String getOtrosEgresosCorteCuatro() {
        if (otrosEgresosCorteCuatro == null) {
            otrosEgresosCorteCuatro = "";
        }
        return otrosEgresosCorteCuatro;
    }

    public String getOtrosEgresosCorteCinco() {
        if (otrosEgresosCorteCinco == null) {
            otrosEgresosCorteCinco = "";
        }
        return otrosEgresosCorteCinco;
    }

    public String getOtrosEgresosCorteSeis() {
        if (otrosEgresosCorteSeis == null) {
            otrosEgresosCorteSeis = "";
        }
        return otrosEgresosCorteSeis;
    }

    public String getOtrosEgresosCorteSiete() {
        if (otrosEgresosCorteSiete == null) {
            otrosEgresosCorteSiete = "";
        }
        return otrosEgresosCorteSiete;
    }

    public String getOtrosEgresosCorteOcho() {
        if (otrosEgresosCorteOcho == null) {
            otrosEgresosCorteOcho = "";
        }
        return otrosEgresosCorteOcho;
    }

    public String getOtrosEgresosCorteNueve() {
        if (otrosEgresosCorteNueve == null) {
            otrosEgresosCorteNueve = "";
        }
        return otrosEgresosCorteNueve;
    }

    public String getOtrosEgresosCorteDiez() {
        if (otrosEgresosCorteDiez == null) {
            otrosEgresosCorteDiez = "";
        }
        return otrosEgresosCorteDiez;
    }

    public String getOtrosEgresosCorteOnce() {
        if (otrosEgresosCorteOnce == null) {
            otrosEgresosCorteOnce = "";
        }
        return otrosEgresosCorteOnce;
    }

    public String getOtrosEgresosCorteDoce() {
        if (otrosEgresosCorteDoce == null) {
            otrosEgresosCorteDoce = "";
        }
        return otrosEgresosCorteDoce;
    }

    public String getOtrosEgresosCorteTrece() {
        if (otrosEgresosCorteTrece == null) {
            otrosEgresosCorteTrece = "";
        }
        return otrosEgresosCorteTrece;
    }

    public String getMargenFinancieroBrutoCorteUno() {
        if (margenFinancieroBrutoCorteUno == null) {
            margenFinancieroBrutoCorteUno = "";
        }
        return margenFinancieroBrutoCorteUno;
    }

    public String getMargenFinancieroBrutoCorteDos() {
        if (margenFinancieroBrutoCorteDos == null) {
            margenFinancieroBrutoCorteDos = "";
        }
        return margenFinancieroBrutoCorteDos;
    }

    public String getMargenFinancieroBrutoCorteTres() {
        if (margenFinancieroBrutoCorteTres == null) {
            margenFinancieroBrutoCorteTres = "";
        }
        return margenFinancieroBrutoCorteTres;
    }

    public String getMargenFinancieroBrutoCorteCuatro() {
        if (margenFinancieroBrutoCorteCuatro == null) {
            margenFinancieroBrutoCorteCuatro = "";
        }
        return margenFinancieroBrutoCorteCuatro;
    }

    public String getMargenFinancieroBrutoCorteCinco() {
        if (margenFinancieroBrutoCorteCinco == null) {
            margenFinancieroBrutoCorteCinco = "";
        }
        return margenFinancieroBrutoCorteCinco;
    }

    public String getMargenFinancieroBrutoCorteSeis() {
        if (margenFinancieroBrutoCorteSeis == null) {
            margenFinancieroBrutoCorteSeis = "";
        }
        return margenFinancieroBrutoCorteSeis;
    }

    public String getMargenFinancieroBrutoCorteSiete() {
        if (margenFinancieroBrutoCorteSiete == null) {
            margenFinancieroBrutoCorteSiete = "";
        }
        return margenFinancieroBrutoCorteSiete;
    }

    public String getMargenFinancieroBrutoCorteOcho() {
        if (margenFinancieroBrutoCorteOcho == null) {
            margenFinancieroBrutoCorteOcho = "";
        }
        return margenFinancieroBrutoCorteOcho;
    }

    public String getMargenFinancieroBrutoCorteNueve() {
        if (margenFinancieroBrutoCorteNueve == null) {
            margenFinancieroBrutoCorteNueve = "";
        }
        return margenFinancieroBrutoCorteNueve;
    }

    public String getMargenFinancieroBrutoCorteDiez() {
        if (margenFinancieroBrutoCorteDiez == null) {
            margenFinancieroBrutoCorteDiez = "";
        }
        return margenFinancieroBrutoCorteDiez;
    }

    public String getMargenFinancieroBrutoCorteOnce() {
        if (margenFinancieroBrutoCorteOnce == null) {
            margenFinancieroBrutoCorteOnce = "";
        }
        return margenFinancieroBrutoCorteOnce;
    }

    public String getMargenFinancieroBrutoCorteDoce() {
        if (margenFinancieroBrutoCorteDoce == null) {
            margenFinancieroBrutoCorteDoce = "";
        }
        return margenFinancieroBrutoCorteDoce;
    }

    public String getMargenFinancieroBrutoCorteTrece() {
        if (margenFinancieroBrutoCorteTrece == null) {
            margenFinancieroBrutoCorteTrece = "";
        }
        return margenFinancieroBrutoCorteTrece;
    }

    public String getOtrosIngresosOperacionalesCorteUno() {
        if (otrosIngresosOperacionalesCorteUno == null) {
            otrosIngresosOperacionalesCorteUno = "";
        }
        return otrosIngresosOperacionalesCorteUno;
    }

    public String getOtrosIngresosOperacionalesCorteDos() {
        if (otrosIngresosOperacionalesCorteDos == null) {
            otrosIngresosOperacionalesCorteDos = "";
        }
        return otrosIngresosOperacionalesCorteDos;
    }

    public String getOtrosIngresosOperacionalesCorteTres() {
        if (otrosIngresosOperacionalesCorteTres == null) {
            otrosIngresosOperacionalesCorteTres = "";
        }
        return otrosIngresosOperacionalesCorteTres;
    }

    public String getOtrosIngresosOperacionalesCorteCuatro() {
        if (otrosIngresosOperacionalesCorteCuatro == null) {
            otrosIngresosOperacionalesCorteCuatro = "";
        }
        return otrosIngresosOperacionalesCorteCuatro;
    }

    public String getOtrosIngresosOperacionalesCorteCinco() {
        if (otrosIngresosOperacionalesCorteCinco == null) {
            otrosIngresosOperacionalesCorteCinco = "";
        }
        return otrosIngresosOperacionalesCorteCinco;
    }

    public String getOtrosIngresosOperacionalesCorteSeis() {
        if (otrosIngresosOperacionalesCorteSeis == null) {
            otrosIngresosOperacionalesCorteSeis = "";
        }
        return otrosIngresosOperacionalesCorteSeis;
    }

    public String getOtrosIngresosOperacionalesCorteSiete() {
        if (otrosIngresosOperacionalesCorteSiete == null) {
            otrosIngresosOperacionalesCorteSiete = "";
        }
        return otrosIngresosOperacionalesCorteSiete;
    }

    public String getOtrosIngresosOperacionalesCorteOcho() {
        if (otrosIngresosOperacionalesCorteOcho == null) {
            otrosIngresosOperacionalesCorteOcho = "";
        }
        return otrosIngresosOperacionalesCorteOcho;
    }

    public String getOtrosIngresosOperacionalesCorteNueve() {
        if (otrosIngresosOperacionalesCorteNueve == null) {
            otrosIngresosOperacionalesCorteNueve = "";
        }
        return otrosIngresosOperacionalesCorteNueve;
    }

    public String getOtrosIngresosOperacionalesCorteDiez() {
        if (otrosIngresosOperacionalesCorteDiez == null) {
            otrosIngresosOperacionalesCorteDiez = "";
        }
        return otrosIngresosOperacionalesCorteDiez;
    }

    public String getOtrosIngresosOperacionalesCorteOnce() {
        if (otrosIngresosOperacionalesCorteOnce == null) {
            otrosIngresosOperacionalesCorteOnce = "";
        }
        return otrosIngresosOperacionalesCorteOnce;
    }

    public String getOtrosIngresosOperacionalesCorteDoce() {
        if (otrosIngresosOperacionalesCorteDoce == null) {
            otrosIngresosOperacionalesCorteDoce = "";
        }
        return otrosIngresosOperacionalesCorteDoce;
    }

    public String getOtrosIngresosOperacionalesCorteTrece() {
        if (otrosIngresosOperacionalesCorteTrece == null) {
            otrosIngresosOperacionalesCorteTrece = "";
        }
        return otrosIngresosOperacionalesCorteTrece;
    }

    public String getMargenBrutoCorteUno() {
        if (margenBrutoCorteUno == null) {
            margenBrutoCorteUno = "";
        }
        return margenBrutoCorteUno;
    }

    public String getMargenBrutoCorteDos() {
        if (margenBrutoCorteDos == null) {
            margenBrutoCorteDos = "";
        }
        return margenBrutoCorteDos;
    }

    public String getMargenBrutoCorteTres() {
        if (margenBrutoCorteTres == null) {
            margenBrutoCorteTres = "";
        }
        return margenBrutoCorteTres;
    }

    public String getMargenBrutoCorteCuatro() {
        if (margenBrutoCorteCuatro == null) {
            margenBrutoCorteCuatro = "";
        }
        return margenBrutoCorteCuatro;
    }

    public String getMargenBrutoCorteCinco() {
        if (margenBrutoCorteCinco == null) {
            margenBrutoCorteCinco = "";
        }
        return margenBrutoCorteCinco;
    }

    public String getMargenBrutoCorteSeis() {
        if (margenBrutoCorteSeis == null) {
            margenBrutoCorteSeis = "";
        }
        return margenBrutoCorteSeis;
    }

    public String getMargenBrutoCorteSiete() {
        if (margenBrutoCorteSiete == null) {
            margenBrutoCorteSiete = "";
        }
        return margenBrutoCorteSiete;
    }

    public String getMargenBrutoCorteOcho() {
        if (margenBrutoCorteOcho == null) {
            margenBrutoCorteOcho = "";
        }
        return margenBrutoCorteOcho;
    }

    public String getMargenBrutoCorteNueve() {
        if (margenBrutoCorteNueve == null) {
            margenBrutoCorteNueve = "";
        }
        return margenBrutoCorteNueve;
    }

    public String getMargenBrutoCorteDiez() {
        if (margenBrutoCorteDiez == null) {
            margenBrutoCorteDiez = "";
        }
        return margenBrutoCorteDiez;
    }

    public String getMargenBrutoCorteOnce() {
        if (margenBrutoCorteOnce == null) {
            margenBrutoCorteOnce = "";
        }
        return margenBrutoCorteOnce;
    }

    public String getMargenBrutoCorteDoce() {
        if (margenBrutoCorteDoce == null) {
            margenBrutoCorteDoce = "";
        }
        return margenBrutoCorteDoce;
    }

    public String getMargenBrutoCorteTrece() {
        if (margenBrutoCorteTrece == null) {
            margenBrutoCorteTrece = "";
        }
        return margenBrutoCorteTrece;
    }

    public String getGastosPersonalesCorteUno() {
        if (gastosPersonalesCorteUno == null) {
            gastosPersonalesCorteUno = "";
        }
        return gastosPersonalesCorteUno;
    }

    public String getGastosPersonalesCorteDos() {
        if (gastosPersonalesCorteDos == null) {
            gastosPersonalesCorteDos = "";
        }
        return gastosPersonalesCorteDos;
    }

    public String getGastosPersonalesCorteTres() {
        if (gastosPersonalesCorteTres == null) {
            gastosPersonalesCorteTres = "";
        }
        return gastosPersonalesCorteTres;
    }

    public String getGastosPersonalesCorteCuatro() {
        if (gastosPersonalesCorteCuatro == null) {
            gastosPersonalesCorteCuatro = "";
        }
        return gastosPersonalesCorteCuatro;
    }

    public String getGastosPersonalesCorteCinco() {
        if (gastosPersonalesCorteCinco == null) {
            gastosPersonalesCorteCinco = "";
        }
        return gastosPersonalesCorteCinco;
    }

    public String getGastosPersonalesCorteSeis() {
        if (gastosPersonalesCorteSeis == null) {
            gastosPersonalesCorteSeis = "";
        }
        return gastosPersonalesCorteSeis;
    }

    public String getGastosPersonalesCorteSiete() {
        if (gastosPersonalesCorteSiete == null) {
            gastosPersonalesCorteSiete = "";
        }
        return gastosPersonalesCorteSiete;
    }

    public String getGastosPersonalesCorteOcho() {
        if (gastosPersonalesCorteOcho == null) {
            gastosPersonalesCorteOcho = "";
        }
        return gastosPersonalesCorteOcho;
    }

    public String getGastosPersonalesCorteNueve() {
        if (gastosPersonalesCorteNueve == null) {
            gastosPersonalesCorteNueve = "";
        }
        return gastosPersonalesCorteNueve;
    }

    public String getGastosPersonalesCorteDiez() {
        if (gastosPersonalesCorteDiez == null) {
            gastosPersonalesCorteDiez = "";
        }
        return gastosPersonalesCorteDiez;
    }

    public String getGastosPersonalesCorteOnce() {
        if (gastosPersonalesCorteOnce == null) {
            gastosPersonalesCorteOnce = "";
        }
        return gastosPersonalesCorteOnce;
    }

    public String getGastosPersonalesCorteDoce() {
        if (gastosPersonalesCorteDoce == null) {
            gastosPersonalesCorteDoce = "";
        }
        return gastosPersonalesCorteDoce;
    }

    public String getGastosPersonalesCorteTrece() {
        if (gastosPersonalesCorteTrece == null) {
            gastosPersonalesCorteTrece = "";
        }
        return gastosPersonalesCorteTrece;
    }

    public String getGastosGeneralesCorteUno() {
        if (gastosGeneralesCorteUno == null) {
            gastosGeneralesCorteUno = "";
        }
        return gastosGeneralesCorteUno;
    }

    public String getGastosGeneralesCorteDos() {
        if (gastosGeneralesCorteDos == null) {
            gastosGeneralesCorteDos = "";
        }
        return gastosGeneralesCorteDos;
    }

    public String getGastosGeneralesCorteTres() {
        if (gastosGeneralesCorteTres == null) {
            gastosGeneralesCorteTres = "";
        }
        return gastosGeneralesCorteTres;
    }

    public String getGastosGeneralesCorteCuatro() {
        if (gastosGeneralesCorteCuatro == null) {
            gastosGeneralesCorteCuatro = "";
        }
        return gastosGeneralesCorteCuatro;
    }

    public String getGastosGeneralesCorteCinco() {
        if (gastosGeneralesCorteCinco == null) {
            gastosGeneralesCorteCinco = "";
        }
        return gastosGeneralesCorteCinco;
    }

    public String getGastosGeneralesCorteSeis() {
        if (gastosGeneralesCorteSeis == null) {
            gastosGeneralesCorteSeis = "";
        }
        return gastosGeneralesCorteSeis;
    }

    public String getGastosGeneralesCorteSiete() {
        if (gastosGeneralesCorteSiete == null) {
            gastosGeneralesCorteSiete = "";
        }
        return gastosGeneralesCorteSiete;
    }

    public String getGastosGeneralesCorteOcho() {
        if (gastosGeneralesCorteOcho == null) {
            gastosGeneralesCorteOcho = "";
        }
        return gastosGeneralesCorteOcho;
    }

    public String getGastosGeneralesCorteNueve() {
        if (gastosGeneralesCorteNueve == null) {
            gastosGeneralesCorteNueve = "";
        }
        return gastosGeneralesCorteNueve;
    }

    public String getGastosGeneralesCorteDiez() {
        if (gastosGeneralesCorteDiez == null) {
            gastosGeneralesCorteDiez = "";
        }
        return gastosGeneralesCorteDiez;
    }

    public String getGastosGeneralesCorteOnce() {
        if (gastosGeneralesCorteOnce == null) {
            gastosGeneralesCorteOnce = "";
        }
        return gastosGeneralesCorteOnce;
    }

    public String getGastosGeneralesCorteDoce() {
        if (gastosGeneralesCorteDoce == null) {
            gastosGeneralesCorteDoce = "";
        }
        return gastosGeneralesCorteDoce;
    }

    public String getGastosGeneralesCorteTrece() {
        if (gastosGeneralesCorteTrece == null) {
            gastosGeneralesCorteTrece = "";
        }
        return gastosGeneralesCorteTrece;
    }

    public String getDepreciacionesAmortizacionesCorteUno() {
        if (depreciacionesAmortizacionesCorteUno == null) {
            depreciacionesAmortizacionesCorteUno = "";
        }
        return depreciacionesAmortizacionesCorteUno;
    }

    public String getDepreciacionesAmortizacionesCorteDos() {
        if (depreciacionesAmortizacionesCorteDos == null) {
            depreciacionesAmortizacionesCorteDos = "";
        }
        return depreciacionesAmortizacionesCorteDos;
    }

    public String getDepreciacionesAmortizacionesCorteTres() {
        if (depreciacionesAmortizacionesCorteTres == null) {
            depreciacionesAmortizacionesCorteTres = "";
        }
        return depreciacionesAmortizacionesCorteTres;
    }

    public String getDepreciacionesAmortizacionesCorteCuatro() {
        if (depreciacionesAmortizacionesCorteCuatro == null) {
            depreciacionesAmortizacionesCorteCuatro = "";
        }
        return depreciacionesAmortizacionesCorteCuatro;
    }

    public String getDepreciacionesAmortizacionesCorteCinco() {
        if (depreciacionesAmortizacionesCorteCinco == null) {
            depreciacionesAmortizacionesCorteCinco = "";
        }
        return depreciacionesAmortizacionesCorteCinco;
    }

    public String getDepreciacionesAmortizacionesCorteSeis() {
        if (depreciacionesAmortizacionesCorteSeis == null) {
            depreciacionesAmortizacionesCorteSeis = "";
        }
        return depreciacionesAmortizacionesCorteSeis;
    }

    public String getDepreciacionesAmortizacionesCorteSiete() {
        if (depreciacionesAmortizacionesCorteSiete == null) {
            depreciacionesAmortizacionesCorteSiete = "";
        }
        return depreciacionesAmortizacionesCorteSiete;
    }

    public String getDepreciacionesAmortizacionesCorteOcho() {
        if (depreciacionesAmortizacionesCorteOcho == null) {
            depreciacionesAmortizacionesCorteOcho = "";
        }
        return depreciacionesAmortizacionesCorteOcho;
    }

    public String getDepreciacionesAmortizacionesCorteNueve() {
        if (depreciacionesAmortizacionesCorteNueve == null) {
            depreciacionesAmortizacionesCorteNueve = "";
        }
        return depreciacionesAmortizacionesCorteNueve;
    }

    public String getDepreciacionesAmortizacionesCorteDiez() {
        if (depreciacionesAmortizacionesCorteDiez == null) {
            depreciacionesAmortizacionesCorteDiez = "";
        }
        return depreciacionesAmortizacionesCorteDiez;
    }

    public String getDepreciacionesAmortizacionesCorteOnce() {
        if (depreciacionesAmortizacionesCorteOnce == null) {
            depreciacionesAmortizacionesCorteOnce = "";
        }
        return depreciacionesAmortizacionesCorteOnce;
    }

    public String getDepreciacionesAmortizacionesCorteDoce() {
        if (depreciacionesAmortizacionesCorteDoce == null) {
            depreciacionesAmortizacionesCorteDoce = "";
        }
        return depreciacionesAmortizacionesCorteDoce;
    }

    public String getDepreciacionesAmortizacionesCorteTrece() {
        if (depreciacionesAmortizacionesCorteTrece == null) {
            depreciacionesAmortizacionesCorteTrece = "";
        }
        return depreciacionesAmortizacionesCorteTrece;
    }

    public String getEgresosOperacionalesCorteUno() {
        if (egresosOperacionalesCorteUno == null) {
            egresosOperacionalesCorteUno = "";
        }
        return egresosOperacionalesCorteUno;
    }

    public String getEgresosOperacionalesCorteDos() {
        if (egresosOperacionalesCorteDos == null) {
            egresosOperacionalesCorteDos = "";
        }
        return egresosOperacionalesCorteDos;
    }

    public String getEgresosOperacionalesCorteTres() {
        if (egresosOperacionalesCorteTres == null) {
            egresosOperacionalesCorteTres = "";
        }
        return egresosOperacionalesCorteTres;
    }

    public String getEgresosOperacionalesCorteCuatro() {
        if (egresosOperacionalesCorteCuatro == null) {
            egresosOperacionalesCorteCuatro = "";
        }
        return egresosOperacionalesCorteCuatro;
    }

    public String getEgresosOperacionalesCorteCinco() {
        if (egresosOperacionalesCorteCinco == null) {
            egresosOperacionalesCorteCinco = "";
        }
        return egresosOperacionalesCorteCinco;
    }

    public String getEgresosOperacionalesCorteSeis() {
        if (egresosOperacionalesCorteSeis == null) {
            egresosOperacionalesCorteSeis = "";
        }
        return egresosOperacionalesCorteSeis;
    }

    public String getEgresosOperacionalesCorteSiete() {
        if (egresosOperacionalesCorteSiete == null) {
            egresosOperacionalesCorteSiete = "";
        }
        return egresosOperacionalesCorteSiete;
    }

    public String getEgresosOperacionalesCorteOcho() {
        if (egresosOperacionalesCorteOcho == null) {
            egresosOperacionalesCorteOcho = "";
        }
        return egresosOperacionalesCorteOcho;
    }

    public String getEgresosOperacionalesCorteNueve() {
        if (egresosOperacionalesCorteNueve == null) {
            egresosOperacionalesCorteNueve = "";
        }
        return egresosOperacionalesCorteNueve;
    }

    public String getEgresosOperacionalesCorteDiez() {
        if (egresosOperacionalesCorteDiez == null) {
            egresosOperacionalesCorteDiez = "";
        }
        return egresosOperacionalesCorteDiez;
    }

    public String getEgresosOperacionalesCorteOnce() {
        if (egresosOperacionalesCorteOnce == null) {
            egresosOperacionalesCorteOnce = "";
        }
        return egresosOperacionalesCorteOnce;
    }

    public String getEgresosOperacionalesCorteDoce() {
        if (egresosOperacionalesCorteDoce == null) {
            egresosOperacionalesCorteDoce = "";
        }
        return egresosOperacionalesCorteDoce;
    }

    public String getEgresosOperacionalesCorteTrece() {
        if (egresosOperacionalesCorteTrece == null) {
            egresosOperacionalesCorteTrece = "";
        }
        return egresosOperacionalesCorteTrece;
    }

    public String getTotalGatosAdminCorteUno() {
        if (totalGatosAdminCorteUno == null) {
            totalGatosAdminCorteUno = "";
        }
        return totalGatosAdminCorteUno;
    }

    public String getTotalGatosAdminCorteDos() {
        if (totalGatosAdminCorteDos == null) {
            totalGatosAdminCorteDos = "";
        }
        return totalGatosAdminCorteDos;
    }

    public String getTotalGatosAdminCorteTres() {
        if (totalGatosAdminCorteTres == null) {
            totalGatosAdminCorteTres = "";
        }
        return totalGatosAdminCorteTres;
    }

    public String getTotalGatosAdminCorteCuatro() {
        if (totalGatosAdminCorteCuatro == null) {
            totalGatosAdminCorteCuatro = "";
        }
        return totalGatosAdminCorteCuatro;
    }

    public String getTotalGatosAdminCorteCinco() {
        if (totalGatosAdminCorteCinco == null) {
            totalGatosAdminCorteCinco = "";
        }
        return totalGatosAdminCorteCinco;
    }

    public String getTotalGatosAdminCorteSeis() {
        if (totalGatosAdminCorteSeis == null) {
            totalGatosAdminCorteSeis = "";
        }
        return totalGatosAdminCorteSeis;
    }

    public String getTotalGatosAdminCorteSiete() {
        if (totalGatosAdminCorteSiete == null) {
            totalGatosAdminCorteSiete = "";
        }
        return totalGatosAdminCorteSiete;
    }

    public String getTotalGatosAdminCorteOcho() {
        if (totalGatosAdminCorteOcho == null) {
            totalGatosAdminCorteOcho = "";
        }
        return totalGatosAdminCorteOcho;
    }

    public String getTotalGatosAdminCorteNueve() {
        if (totalGatosAdminCorteNueve == null) {
            totalGatosAdminCorteNueve = "";
        }
        return totalGatosAdminCorteNueve;
    }

    public String getTotalGatosAdminCorteDiez() {
        if (totalGatosAdminCorteDiez == null) {
            totalGatosAdminCorteDiez = "";
        }
        return totalGatosAdminCorteDiez;
    }

    public String getTotalGatosAdminCorteOnce() {
        if (totalGatosAdminCorteOnce == null) {
            totalGatosAdminCorteOnce = "";
        }
        return totalGatosAdminCorteOnce;
    }

    public String getTotalGatosAdminCorteDoce() {
        if (totalGatosAdminCorteDoce == null) {
            totalGatosAdminCorteDoce = "";
        }
        return totalGatosAdminCorteDoce;
    }

    public String getTotalGatosAdminCorteTrece() {
        if (totalGatosAdminCorteTrece == null) {
            totalGatosAdminCorteTrece = "";
        }
        return totalGatosAdminCorteTrece;
    }

    public String getExcedenteOperacionalAntesCorteUno() {
        if (excedenteOperacionalAntesCorteUno == null) {
            excedenteOperacionalAntesCorteUno = "";
        }
        return excedenteOperacionalAntesCorteUno;
    }

    public String getExcedenteOperacionalAntesCorteDos() {
        if (excedenteOperacionalAntesCorteDos == null) {
            excedenteOperacionalAntesCorteDos = "";
        }
        return excedenteOperacionalAntesCorteDos;
    }

    public String getExcedenteOperacionalAntesCorteTres() {
        if (excedenteOperacionalAntesCorteTres == null) {
            excedenteOperacionalAntesCorteTres = "";
        }
        return excedenteOperacionalAntesCorteTres;
    }

    public String getExcedenteOperacionalAntesCorteCuatro() {
        if (excedenteOperacionalAntesCorteCuatro == null) {
            excedenteOperacionalAntesCorteCuatro = "";
        }
        return excedenteOperacionalAntesCorteCuatro;
    }

    public String getExcedenteOperacionalAntesCorteCinco() {
        if (excedenteOperacionalAntesCorteCinco == null) {
            excedenteOperacionalAntesCorteCinco = "";
        }
        return excedenteOperacionalAntesCorteCinco;
    }

    public String getExcedenteOperacionalAntesCorteSeis() {
        if (excedenteOperacionalAntesCorteSeis == null) {
            excedenteOperacionalAntesCorteSeis = "";
        }
        return excedenteOperacionalAntesCorteSeis;
    }

    public String getExcedenteOperacionalAntesCorteSiete() {
        if (excedenteOperacionalAntesCorteSiete == null) {
            excedenteOperacionalAntesCorteSiete = "";
        }
        return excedenteOperacionalAntesCorteSiete;
    }

    public String getExcedenteOperacionalAntesCorteOcho() {
        if (excedenteOperacionalAntesCorteOcho == null) {
            excedenteOperacionalAntesCorteOcho = "";
        }
        return excedenteOperacionalAntesCorteOcho;
    }

    public String getExcedenteOperacionalAntesCorteNueve() {
        if (excedenteOperacionalAntesCorteNueve == null) {
            excedenteOperacionalAntesCorteNueve = "";
        }
        return excedenteOperacionalAntesCorteNueve;
    }

    public String getExcedenteOperacionalAntesCorteDiez() {
        if (excedenteOperacionalAntesCorteDiez == null) {
            excedenteOperacionalAntesCorteDiez = "";
        }
        return excedenteOperacionalAntesCorteDiez;
    }

    public String getExcedenteOperacionalAntesCorteOnce() {
        if (excedenteOperacionalAntesCorteOnce == null) {
            excedenteOperacionalAntesCorteOnce = "";
        }
        return excedenteOperacionalAntesCorteOnce;
    }

    public String getExcedenteOperacionalAntesCorteDoce() {
        if (excedenteOperacionalAntesCorteDoce == null) {
            excedenteOperacionalAntesCorteDoce = "";
        }
        return excedenteOperacionalAntesCorteDoce;
    }

    public String getExcedenteOperacionalAntesCorteTrece() {
        if (excedenteOperacionalAntesCorteTrece == null) {
            excedenteOperacionalAntesCorteTrece = "";
        }
        return excedenteOperacionalAntesCorteTrece;
    }

    public String getDeterioroCarteraCorteUno() {
        if (deterioroCarteraCorteUno == null) {
            deterioroCarteraCorteUno = "";
        }
        return deterioroCarteraCorteUno;
    }

    public String getDeterioroCarteraCorteDos() {
        if (deterioroCarteraCorteDos == null) {
            deterioroCarteraCorteDos = "";
        }
        return deterioroCarteraCorteDos;
    }

    public String getDeterioroCarteraCorteTres() {
        if (deterioroCarteraCorteTres == null) {
            deterioroCarteraCorteTres = "";
        }
        return deterioroCarteraCorteTres;
    }

    public String getDeterioroCarteraCorteCuatro() {
        if (deterioroCarteraCorteCuatro == null) {
            deterioroCarteraCorteCuatro = "";
        }
        return deterioroCarteraCorteCuatro;
    }

    public String getDeterioroCarteraCorteCinco() {
        if (deterioroCarteraCorteCinco == null) {
            deterioroCarteraCorteCinco = "";
        }
        return deterioroCarteraCorteCinco;
    }

    public String getDeterioroCarteraCorteSeis() {
        if (deterioroCarteraCorteSeis == null) {
            deterioroCarteraCorteSeis = "";
        }
        return deterioroCarteraCorteSeis;
    }

    public String getDeterioroCarteraCorteSiete() {
        if (deterioroCarteraCorteSiete == null) {
            deterioroCarteraCorteSiete = "";
        }
        return deterioroCarteraCorteSiete;
    }

    public String getDeterioroCarteraCorteOcho() {
        if (deterioroCarteraCorteOcho == null) {
            deterioroCarteraCorteOcho = "";
        }
        return deterioroCarteraCorteOcho;
    }

    public String getDeterioroCarteraCorteNueve() {
        if (deterioroCarteraCorteNueve == null) {
            deterioroCarteraCorteNueve = "";
        }
        return deterioroCarteraCorteNueve;
    }

    public String getDeterioroCarteraCorteDiez() {
        if (deterioroCarteraCorteDiez == null) {
            deterioroCarteraCorteDiez = "";
        }
        return deterioroCarteraCorteDiez;
    }

    public String getDeterioroCarteraCorteOnce() {
        if (deterioroCarteraCorteOnce == null) {
            deterioroCarteraCorteOnce = "";
        }
        return deterioroCarteraCorteOnce;
    }

    public String getDeterioroCarteraCorteDoce() {
        if (deterioroCarteraCorteDoce == null) {
            deterioroCarteraCorteDoce = "";
        }
        return deterioroCarteraCorteDoce;
    }

    public String getDeterioroCarteraCorteTrece() {
        if (deterioroCarteraCorteTrece == null) {
            deterioroCarteraCorteTrece = "";
        }
        return deterioroCarteraCorteTrece;
    }

    public String getDeterioroCuentasPorCobrarCorteUno() {
        if (deterioroCuentasPorCobrarCorteUno == null) {
            deterioroCuentasPorCobrarCorteUno = "";
        }
        return deterioroCuentasPorCobrarCorteUno;
    }

    public String getDeterioroCuentasPorCobrarCorteDos() {
        if (deterioroCuentasPorCobrarCorteDos == null) {
            deterioroCuentasPorCobrarCorteDos = "";
        }
        return deterioroCuentasPorCobrarCorteDos;
    }

    public String getDeterioroCuentasPorCobrarCorteTres() {
        if (deterioroCuentasPorCobrarCorteTres == null) {
            deterioroCuentasPorCobrarCorteTres = "";
        }
        return deterioroCuentasPorCobrarCorteTres;
    }

    public String getDeterioroCuentasPorCobrarCorteCuatro() {
        if (deterioroCuentasPorCobrarCorteCuatro == null) {
            deterioroCuentasPorCobrarCorteCuatro = "";
        }
        return deterioroCuentasPorCobrarCorteCuatro;
    }

    public String getDeterioroCuentasPorCobrarCorteCinco() {
        if (deterioroCuentasPorCobrarCorteCinco == null) {
            deterioroCuentasPorCobrarCorteCinco = "";
        }
        return deterioroCuentasPorCobrarCorteCinco;
    }

    public String getDeterioroCuentasPorCobrarCorteSeis() {
        if (deterioroCuentasPorCobrarCorteSeis == null) {
            deterioroCuentasPorCobrarCorteSeis = "";
        }
        return deterioroCuentasPorCobrarCorteSeis;
    }

    public String getDeterioroCuentasPorCobrarCorteSiete() {
        if (deterioroCuentasPorCobrarCorteSiete == null) {
            deterioroCuentasPorCobrarCorteSiete = "";
        }
        return deterioroCuentasPorCobrarCorteSiete;
    }

    public String getDeterioroCuentasPorCobrarCorteOcho() {
        if (deterioroCuentasPorCobrarCorteOcho == null) {
            deterioroCuentasPorCobrarCorteOcho = "";
        }
        return deterioroCuentasPorCobrarCorteOcho;
    }

    public String getDeterioroCuentasPorCobrarCorteNueve() {
        if (deterioroCuentasPorCobrarCorteNueve == null) {
            deterioroCuentasPorCobrarCorteNueve = "";
        }
        return deterioroCuentasPorCobrarCorteNueve;
    }

    public String getDeterioroCuentasPorCobrarCorteDiez() {
        if (deterioroCuentasPorCobrarCorteDiez == null) {
            deterioroCuentasPorCobrarCorteDiez = "";
        }
        return deterioroCuentasPorCobrarCorteDiez;
    }

    public String getDeterioroCuentasPorCobrarCorteOnce() {
        if (deterioroCuentasPorCobrarCorteOnce == null) {
            deterioroCuentasPorCobrarCorteOnce = "";
        }
        return deterioroCuentasPorCobrarCorteOnce;
    }

    public String getDeterioroCuentasPorCobrarCorteDoce() {
        if (deterioroCuentasPorCobrarCorteDoce == null) {
            deterioroCuentasPorCobrarCorteDoce = "";
        }
        return deterioroCuentasPorCobrarCorteDoce;
    }

    public String getDeterioroCuentasPorCobrarCorteTrece() {
        if (deterioroCuentasPorCobrarCorteTrece == null) {
            deterioroCuentasPorCobrarCorteTrece = "";
        }
        return deterioroCuentasPorCobrarCorteTrece;
    }

    public String getDeterioroInversionesCorteUno() {
        if (deterioroInversionesCorteUno == null) {
            deterioroInversionesCorteUno = "";
        }
        return deterioroInversionesCorteUno;
    }

    public String getDeterioroInversionesCorteDos() {
        if (deterioroInversionesCorteDos == null) {
            deterioroInversionesCorteDos = "";
        }
        return deterioroInversionesCorteDos;
    }

    public String getDeterioroInversionesCorteTres() {
        if (deterioroInversionesCorteTres == null) {
            deterioroInversionesCorteTres = "";
        }
        return deterioroInversionesCorteTres;
    }

    public String getDeterioroInversionesCorteCuatro() {
        if (deterioroInversionesCorteCuatro == null) {
            deterioroInversionesCorteCuatro = "";
        }
        return deterioroInversionesCorteCuatro;
    }

    public String getDeterioroInversionesCorteCinco() {
        if (deterioroInversionesCorteCinco == null) {
            deterioroInversionesCorteCinco = "";
        }
        return deterioroInversionesCorteCinco;
    }

    public String getDeterioroInversionesCorteSeis() {
        if (deterioroInversionesCorteSeis == null) {
            deterioroInversionesCorteSeis = "";
        }
        return deterioroInversionesCorteSeis;
    }

    public String getDeterioroInversionesCorteSiete() {
        if (deterioroInversionesCorteSiete == null) {
            deterioroInversionesCorteSiete = "";
        }
        return deterioroInversionesCorteSiete;
    }

    public String getDeterioroInversionesCorteOcho() {
        if (deterioroInversionesCorteOcho == null) {
            deterioroInversionesCorteOcho = "";
        }
        return deterioroInversionesCorteOcho;
    }

    public String getDeterioroInversionesCorteNueve() {
        if (deterioroInversionesCorteNueve == null) {
            deterioroInversionesCorteNueve = "";
        }
        return deterioroInversionesCorteNueve;
    }

    public String getDeterioroInversionesCorteDiez() {
        if (deterioroInversionesCorteDiez == null) {
            deterioroInversionesCorteDiez = "";
        }
        return deterioroInversionesCorteDiez;
    }

    public String getDeterioroInversionesCorteOnce() {
        if (deterioroInversionesCorteOnce == null) {
            deterioroInversionesCorteOnce = "";
        }
        return deterioroInversionesCorteOnce;
    }

    public String getDeterioroInversionesCorteDoce() {
        if (deterioroInversionesCorteDoce == null) {
            deterioroInversionesCorteDoce = "";
        }
        return deterioroInversionesCorteDoce;
    }

    public String getDeterioroInversionesCorteTrece() {
        if (deterioroInversionesCorteTrece == null) {
            deterioroInversionesCorteTrece = "";
        }
        return deterioroInversionesCorteTrece;
    }

    public String getOtrosDeteriorosCorteUno() {
        if (otrosDeteriorosCorteUno == null) {
            otrosDeteriorosCorteUno = "";
        }
        return otrosDeteriorosCorteUno;
    }

    public String getOtrosDeteriorosCorteDos() {
        if (otrosDeteriorosCorteDos == null) {
            otrosDeteriorosCorteDos = "";
        }
        return otrosDeteriorosCorteDos;
    }

    public String getOtrosDeteriorosCorteTres() {
        if (otrosDeteriorosCorteTres == null) {
            otrosDeteriorosCorteTres = "";
        }
        return otrosDeteriorosCorteTres;
    }

    public String getOtrosDeteriorosCorteCuatro() {
        if (otrosDeteriorosCorteCuatro == null) {
            otrosDeteriorosCorteCuatro = "";
        }
        return otrosDeteriorosCorteCuatro;
    }

    public String getOtrosDeteriorosCorteCinco() {
        if (otrosDeteriorosCorteCinco == null) {
            otrosDeteriorosCorteCinco = "";
        }
        return otrosDeteriorosCorteCinco;
    }

    public String getOtrosDeteriorosCorteSeis() {
        if (otrosDeteriorosCorteSeis == null) {
            otrosDeteriorosCorteSeis = "";
        }
        return otrosDeteriorosCorteSeis;
    }

    public String getOtrosDeteriorosCorteSiete() {
        if (otrosDeteriorosCorteSiete == null) {
            otrosDeteriorosCorteSiete = "";
        }
        return otrosDeteriorosCorteSiete;
    }

    public String getOtrosDeteriorosCorteOcho() {
        if (otrosDeteriorosCorteOcho == null) {
            otrosDeteriorosCorteOcho = "";
        }
        return otrosDeteriorosCorteOcho;
    }

    public String getOtrosDeteriorosCorteNueve() {
        if (otrosDeteriorosCorteNueve == null) {
            otrosDeteriorosCorteNueve = "";
        }
        return otrosDeteriorosCorteNueve;
    }

    public String getOtrosDeteriorosCorteDiez() {
        if (otrosDeteriorosCorteDiez == null) {
            otrosDeteriorosCorteDiez = "";
        }
        return otrosDeteriorosCorteDiez;
    }

    public String getOtrosDeteriorosCorteOnce() {
        if (otrosDeteriorosCorteOnce == null) {
            otrosDeteriorosCorteOnce = "";
        }
        return otrosDeteriorosCorteOnce;
    }

    public String getOtrosDeteriorosCorteDoce() {
        if (otrosDeteriorosCorteDoce == null) {
            otrosDeteriorosCorteDoce = "";
        }
        return otrosDeteriorosCorteDoce;
    }

    public String getOtrosDeteriorosCorteTrece() {
        if (otrosDeteriorosCorteTrece == null) {
            otrosDeteriorosCorteTrece = "";
        }
        return otrosDeteriorosCorteTrece;
    }

    public String getTotalDeteriorosCorteUno() {
        if (totalDeteriorosCorteUno == null) {
            totalDeteriorosCorteUno = "";
        }
        return totalDeteriorosCorteUno;
    }

    public String getTotalDeteriorosCorteDos() {
        if (totalDeteriorosCorteDos == null) {
            totalDeteriorosCorteDos = "";
        }
        return totalDeteriorosCorteDos;
    }

    public String getTotalDeteriorosCorteTres() {
        if (totalDeteriorosCorteTres == null) {
            totalDeteriorosCorteTres = "";
        }
        return totalDeteriorosCorteTres;
    }

    public String getTotalDeteriorosCorteCuatro() {
        if (totalDeteriorosCorteCuatro == null) {
            totalDeteriorosCorteCuatro = "";
        }
        return totalDeteriorosCorteCuatro;
    }

    public String getTotalDeteriorosCorteCinco() {
        if (totalDeteriorosCorteCinco == null) {
            totalDeteriorosCorteCinco = "";
        }
        return totalDeteriorosCorteCinco;
    }

    public String getTotalDeteriorosCorteSeis() {
        if (totalDeteriorosCorteSeis == null) {
            totalDeteriorosCorteSeis = "";
        }
        return totalDeteriorosCorteSeis;
    }

    public String getTotalDeteriorosCorteSiete() {
        if (totalDeteriorosCorteSiete == null) {
            totalDeteriorosCorteSiete = "";
        }
        return totalDeteriorosCorteSiete;
    }

    public String getTotalDeteriorosCorteOcho() {
        if (totalDeteriorosCorteOcho == null) {
            totalDeteriorosCorteOcho = "";
        }
        return totalDeteriorosCorteOcho;
    }

    public String getTotalDeteriorosCorteNueve() {
        if (totalDeteriorosCorteNueve == null) {
            totalDeteriorosCorteNueve = "";
        }
        return totalDeteriorosCorteNueve;
    }

    public String getTotalDeteriorosCorteDiez() {
        if (totalDeteriorosCorteDiez == null) {
            totalDeteriorosCorteDiez = "";
        }
        return totalDeteriorosCorteDiez;
    }

    public String getTotalDeteriorosCorteOnce() {
        if (totalDeteriorosCorteOnce == null) {
            totalDeteriorosCorteOnce = "";
        }
        return totalDeteriorosCorteOnce;
    }

    public String getTotalDeteriorosCorteDoce() {
        if (totalDeteriorosCorteDoce == null) {
            totalDeteriorosCorteDoce = "";
        }
        return totalDeteriorosCorteDoce;
    }

    public String getTotalDeteriorosCorteTrece() {
        if (totalDeteriorosCorteTrece == null) {
            totalDeteriorosCorteTrece = "";
        }
        return totalDeteriorosCorteTrece;
    }

    public String getRecuperacionesDeteriorosCorteUno() {
        if (recuperacionesDeteriorosCorteUno == null) {
            recuperacionesDeteriorosCorteUno = "";
        }
        return recuperacionesDeteriorosCorteUno;
    }

    public String getRecuperacionesDeteriorosCorteDos() {
        if (recuperacionesDeteriorosCorteDos == null) {
            recuperacionesDeteriorosCorteDos = "";
        }
        return recuperacionesDeteriorosCorteDos;
    }

    public String getRecuperacionesDeteriorosCorteTres() {
        if (recuperacionesDeteriorosCorteTres == null) {
            recuperacionesDeteriorosCorteTres = "";
        }
        return recuperacionesDeteriorosCorteTres;
    }

    public String getRecuperacionesDeteriorosCorteCuatro() {
        if (recuperacionesDeteriorosCorteCuatro == null) {
            recuperacionesDeteriorosCorteCuatro = "";
        }
        return recuperacionesDeteriorosCorteCuatro;
    }

    public String getRecuperacionesDeteriorosCorteCinco() {
        if (recuperacionesDeteriorosCorteCinco == null) {
            recuperacionesDeteriorosCorteCinco = "";
        }
        return recuperacionesDeteriorosCorteCinco;
    }

    public String getRecuperacionesDeteriorosCorteSeis() {
        if (recuperacionesDeteriorosCorteSeis == null) {
            recuperacionesDeteriorosCorteSeis = "";
        }
        return recuperacionesDeteriorosCorteSeis;
    }

    public String getRecuperacionesDeteriorosCorteSiete() {
        if (recuperacionesDeteriorosCorteSiete == null) {
            recuperacionesDeteriorosCorteSiete = "";
        }
        return recuperacionesDeteriorosCorteSiete;
    }

    public String getRecuperacionesDeteriorosCorteOcho() {
        if (recuperacionesDeteriorosCorteOcho == null) {
            recuperacionesDeteriorosCorteOcho = "";
        }
        return recuperacionesDeteriorosCorteOcho;
    }

    public String getRecuperacionesDeteriorosCorteNueve() {
        if (recuperacionesDeteriorosCorteNueve == null) {
            recuperacionesDeteriorosCorteNueve = "";
        }
        return recuperacionesDeteriorosCorteNueve;
    }

    public String getRecuperacionesDeteriorosCorteDiez() {
        if (recuperacionesDeteriorosCorteDiez == null) {
            recuperacionesDeteriorosCorteDiez = "";
        }
        return recuperacionesDeteriorosCorteDiez;
    }

    public String getRecuperacionesDeteriorosCorteOnce() {
        if (recuperacionesDeteriorosCorteOnce == null) {
            recuperacionesDeteriorosCorteOnce = "";
        }
        return recuperacionesDeteriorosCorteOnce;
    }

    public String getRecuperacionesDeteriorosCorteDoce() {
        if (recuperacionesDeteriorosCorteDoce == null) {
            recuperacionesDeteriorosCorteDoce = "";
        }
        return recuperacionesDeteriorosCorteDoce;
    }

    public String getRecuperacionesDeteriorosCorteTrece() {
        if (recuperacionesDeteriorosCorteTrece == null) {
            recuperacionesDeteriorosCorteTrece = "";
        }
        return recuperacionesDeteriorosCorteTrece;
    }

    public String getDeteriorosNetosCorteUno() {
        if (deteriorosNetosCorteUno == null) {
            deteriorosNetosCorteUno = "";
        }
        return deteriorosNetosCorteUno;
    }

    public String getDeteriorosNetosCorteDos() {
        if (deteriorosNetosCorteDos == null) {
            deteriorosNetosCorteDos = "";
        }
        return deteriorosNetosCorteDos;
    }

    public String getDeteriorosNetosCorteTres() {
        if (deteriorosNetosCorteTres == null) {
            deteriorosNetosCorteTres = "";
        }
        return deteriorosNetosCorteTres;
    }

    public String getDeteriorosNetosCorteCuatro() {
        if (deteriorosNetosCorteCuatro == null) {
            deteriorosNetosCorteCuatro = "";
        }
        return deteriorosNetosCorteCuatro;
    }

    public String getDeteriorosNetosCorteCinco() {
        if (deteriorosNetosCorteCinco == null) {
            deteriorosNetosCorteCinco = "";
        }
        return deteriorosNetosCorteCinco;
    }

    public String getDeteriorosNetosCorteSeis() {
        if (deteriorosNetosCorteSeis == null) {
            deteriorosNetosCorteSeis = "";
        }
        return deteriorosNetosCorteSeis;
    }

    public String getDeteriorosNetosCorteSiete() {
        if (deteriorosNetosCorteSiete == null) {
            deteriorosNetosCorteSiete = "";
        }
        return deteriorosNetosCorteSiete;
    }

    public String getDeteriorosNetosCorteOcho() {
        if (deteriorosNetosCorteOcho == null) {
            deteriorosNetosCorteOcho = "";
        }
        return deteriorosNetosCorteOcho;
    }

    public String getDeteriorosNetosCorteNueve() {
        if (deteriorosNetosCorteNueve == null) {
            deteriorosNetosCorteNueve = "";
        }
        return deteriorosNetosCorteNueve;
    }

    public String getDeteriorosNetosCorteDiez() {
        if (deteriorosNetosCorteDiez == null) {
            deteriorosNetosCorteDiez = "";
        }
        return deteriorosNetosCorteDiez;
    }

    public String getDeteriorosNetosCorteOnce() {
        if (deteriorosNetosCorteOnce == null) {
            deteriorosNetosCorteOnce = "";
        }
        return deteriorosNetosCorteOnce;
    }

    public String getDeteriorosNetosCorteDoce() {
        if (deteriorosNetosCorteDoce == null) {
            deteriorosNetosCorteDoce = "";
        }
        return deteriorosNetosCorteDoce;
    }

    public String getDeteriorosNetosCorteTrece() {
        if (deteriorosNetosCorteTrece == null) {
            deteriorosNetosCorteTrece = "";
        }
        return deteriorosNetosCorteTrece;
    }

    public String getExcedenteNetoAntesCorteUno() {
        if (excedenteNetoAntesCorteUno == null) {
            excedenteNetoAntesCorteUno = "";
        }
        return excedenteNetoAntesCorteUno;
    }

    public String getExcedenteNetoAntesCorteDos() {
        if (excedenteNetoAntesCorteDos == null) {
            excedenteNetoAntesCorteDos = "";
        }
        return excedenteNetoAntesCorteDos;
    }

    public String getExcedenteNetoAntesCorteTres() {
        if (excedenteNetoAntesCorteTres == null) {
            excedenteNetoAntesCorteTres = "";
        }
        return excedenteNetoAntesCorteTres;
    }

    public String getExcedenteNetoAntesCorteCuatro() {
        if (excedenteNetoAntesCorteCuatro == null) {
            excedenteNetoAntesCorteCuatro = "";
        }
        return excedenteNetoAntesCorteCuatro;
    }

    public String getExcedenteNetoAntesCorteCinco() {
        if (excedenteNetoAntesCorteCinco == null) {
            excedenteNetoAntesCorteCinco = "";
        }
        return excedenteNetoAntesCorteCinco;
    }

    public String getExcedenteNetoAntesCorteSeis() {
        if (excedenteNetoAntesCorteSeis == null) {
            excedenteNetoAntesCorteSeis = "";
        }
        return excedenteNetoAntesCorteSeis;
    }

    public String getExcedenteNetoAntesCorteSiete() {
        if (excedenteNetoAntesCorteSiete == null) {
            excedenteNetoAntesCorteSiete = "";
        }
        return excedenteNetoAntesCorteSiete;
    }

    public String getExcedenteNetoAntesCorteOcho() {
        if (excedenteNetoAntesCorteOcho == null) {
            excedenteNetoAntesCorteOcho = "";
        }
        return excedenteNetoAntesCorteOcho;
    }

    public String getExcedenteNetoAntesCorteNueve() {
        if (excedenteNetoAntesCorteNueve == null) {
            excedenteNetoAntesCorteNueve = "";
        }
        return excedenteNetoAntesCorteNueve;
    }

    public String getExcedenteNetoAntesCorteDiez() {
        if (excedenteNetoAntesCorteDiez == null) {
            excedenteNetoAntesCorteDiez = "";
        }
        return excedenteNetoAntesCorteDiez;
    }

    public String getExcedenteNetoAntesCorteOnce() {
        if (excedenteNetoAntesCorteOnce == null) {
            excedenteNetoAntesCorteOnce = "";
        }
        return excedenteNetoAntesCorteOnce;
    }

    public String getExcedenteNetoAntesCorteDoce() {
        if (excedenteNetoAntesCorteDoce == null) {
            excedenteNetoAntesCorteDoce = "";
        }
        return excedenteNetoAntesCorteDoce;
    }

    public String getExcedenteNetoAntesCorteTrece() {
        if (excedenteNetoAntesCorteTrece == null) {
            excedenteNetoAntesCorteTrece = "";
        }
        return excedenteNetoAntesCorteTrece;
    }

    public String getImpuestoCorteUno() {
        if (impuestoCorteUno == null) {
            impuestoCorteUno = "";
        }
        return impuestoCorteUno;
    }

    public String getImpuestoCorteDos() {
        if (impuestoCorteDos == null) {
            impuestoCorteDos = "";
        }
        return impuestoCorteDos;
    }

    public String getImpuestoCorteTres() {
        if (impuestoCorteTres == null) {
            impuestoCorteTres = "";
        }
        return impuestoCorteTres;
    }

    public String getImpuestoCorteCuatro() {
        if (impuestoCorteCuatro == null) {
            impuestoCorteCuatro = "";
        }
        return impuestoCorteCuatro;
    }

    public String getImpuestoCorteCinco() {
        if (impuestoCorteCinco == null) {
            impuestoCorteCinco = "";
        }
        return impuestoCorteCinco;
    }

    public String getImpuestoCorteSeis() {
        if (impuestoCorteSeis == null) {
            impuestoCorteSeis = "";
        }
        return impuestoCorteSeis;
    }

    public String getImpuestoCorteSiete() {
        if (impuestoCorteSiete == null) {
            impuestoCorteSiete = "";
        }
        return impuestoCorteSiete;
    }

    public String getImpuestoCorteOcho() {
        if (impuestoCorteOcho == null) {
            impuestoCorteOcho = "";
        }
        return impuestoCorteOcho;
    }

    public String getImpuestoCorteNueve() {
        if (impuestoCorteNueve == null) {
            impuestoCorteNueve = "";
        }
        return impuestoCorteNueve;
    }

    public String getImpuestoCorteDiez() {
        if (impuestoCorteDiez == null) {
            impuestoCorteDiez = "";
        }
        return impuestoCorteDiez;
    }

    public String getImpuestoCorteOnce() {
        if (impuestoCorteOnce == null) {
            impuestoCorteOnce = "";
        }
        return impuestoCorteOnce;
    }

    public String getImpuestoCorteDoce() {
        if (impuestoCorteDoce == null) {
            impuestoCorteDoce = "";
        }
        return impuestoCorteDoce;
    }

    public String getImpuestoCorteTrece() {
        if (impuestoCorteTrece == null) {
            impuestoCorteTrece = "";
        }
        return impuestoCorteTrece;
    }

    public String getExcedenteNetoCorteUno() {
        if (excedenteNetoCorteUno == null) {
            excedenteNetoCorteUno = "";
        }
        return excedenteNetoCorteUno;
    }

    public String getExcedenteNetoCorteDos() {
        if (excedenteNetoCorteDos == null) {
            excedenteNetoCorteDos = "";
        }
        return excedenteNetoCorteDos;
    }

    public String getExcedenteNetoCorteTres() {
        if (excedenteNetoCorteTres == null) {
            excedenteNetoCorteTres = "";
        }
        return excedenteNetoCorteTres;
    }

    public String getExcedenteNetoCorteCuatro() {
        if (excedenteNetoCorteCuatro == null) {
            excedenteNetoCorteCuatro = "";
        }
        return excedenteNetoCorteCuatro;
    }

    public String getExcedenteNetoCorteCinco() {
        if (excedenteNetoCorteCinco == null) {
            excedenteNetoCorteCinco = "";
        }
        return excedenteNetoCorteCinco;
    }

    public String getExcedenteNetoCorteSeis() {
        if (excedenteNetoCorteSeis == null) {
            excedenteNetoCorteSeis = "";
        }
        return excedenteNetoCorteSeis;
    }

    public String getExcedenteNetoCorteSiete() {
        if (excedenteNetoCorteSiete == null) {
            excedenteNetoCorteSiete = "";
        }
        return excedenteNetoCorteSiete;
    }

    public String getExcedenteNetoCorteOcho() {
        if (excedenteNetoCorteOcho == null) {
            excedenteNetoCorteOcho = "";
        }
        return excedenteNetoCorteOcho;
    }

    public String getExcedenteNetoCorteNueve() {
        if (excedenteNetoCorteNueve == null) {
            excedenteNetoCorteNueve = "";
        }
        return excedenteNetoCorteNueve;
    }

    public String getExcedenteNetoCorteDiez() {
        if (excedenteNetoCorteDiez == null) {
            excedenteNetoCorteDiez = "";
        }
        return excedenteNetoCorteDiez;
    }

    public String getExcedenteNetoCorteOnce() {
        if (excedenteNetoCorteOnce == null) {
            excedenteNetoCorteOnce = "";
        }
        return excedenteNetoCorteOnce;
    }

    public String getExcedenteNetoCorteDoce() {
        if (excedenteNetoCorteDoce == null) {
            excedenteNetoCorteDoce = "";
        }
        return excedenteNetoCorteDoce;
    }

    public String getExcedenteNetoCorteTrece() {
        if (excedenteNetoCorteTrece == null) {
            excedenteNetoCorteTrece = "";
        }
        return excedenteNetoCorteTrece;
    }

    public String getExcedentePerdidasORICorteUno() {
        if (excedentePerdidasORICorteUno == null) {
            excedentePerdidasORICorteUno = "";
        }
        return excedentePerdidasORICorteUno;
    }

    public String getExcedentePerdidasORICorteDos() {
        if (excedentePerdidasORICorteDos == null) {
            excedentePerdidasORICorteDos = "";
        }
        return excedentePerdidasORICorteDos;
    }

    public String getExcedentePerdidasORICorteTres() {
        if (excedentePerdidasORICorteTres == null) {
            excedentePerdidasORICorteTres = "";
        }
        return excedentePerdidasORICorteTres;
    }

    public String getExcedentePerdidasORICorteCuatro() {
        if (excedentePerdidasORICorteCuatro == null) {
            excedentePerdidasORICorteCuatro = "";
        }
        return excedentePerdidasORICorteCuatro;
    }

    public String getExcedentePerdidasORICorteCinco() {
        if (excedentePerdidasORICorteCinco == null) {
            excedentePerdidasORICorteCinco = "";
        }
        return excedentePerdidasORICorteCinco;
    }

    public String getExcedentePerdidasORICorteSeis() {
        if (excedentePerdidasORICorteSeis == null) {
            excedentePerdidasORICorteSeis = "";
        }
        return excedentePerdidasORICorteSeis;
    }

    public String getExcedentePerdidasORICorteSiete() {
        if (excedentePerdidasORICorteSiete == null) {
            excedentePerdidasORICorteSiete = "";
        }
        return excedentePerdidasORICorteSiete;
    }

    public String getExcedentePerdidasORICorteOcho() {
        if (excedentePerdidasORICorteOcho == null) {
            excedentePerdidasORICorteOcho = "";
        }
        return excedentePerdidasORICorteOcho;
    }

    public String getExcedentePerdidasORICorteNueve() {
        if (excedentePerdidasORICorteNueve == null) {
            excedentePerdidasORICorteNueve = "";
        }
        return excedentePerdidasORICorteNueve;
    }

    public String getExcedentePerdidasORICorteDiez() {
        if (excedentePerdidasORICorteDiez == null) {
            excedentePerdidasORICorteDiez = "";
        }
        return excedentePerdidasORICorteDiez;
    }

    public String getExcedentePerdidasORICorteOnce() {
        if (excedentePerdidasORICorteOnce == null) {
            excedentePerdidasORICorteOnce = "";
        }
        return excedentePerdidasORICorteOnce;
    }

    public String getExcedentePerdidasORICorteDoce() {
        if (excedentePerdidasORICorteDoce == null) {
            excedentePerdidasORICorteDoce = "";
        }
        return excedentePerdidasORICorteDoce;
    }

    public String getExcedentePerdidasORICorteTrece() {
        if (excedentePerdidasORICorteTrece == null) {
            excedentePerdidasORICorteTrece = "";
        }
        return excedentePerdidasORICorteTrece;
    }

    public String getExcedentePerdidasCorteUno() {
        if (excedentePerdidasCorteUno == null) {
            excedentePerdidasCorteUno = "";
        }
        return excedentePerdidasCorteUno;
    }

    public String getExcedentePerdidasCorteDos() {
        if (excedentePerdidasCorteDos == null) {
            excedentePerdidasCorteDos = "";
        }
        return excedentePerdidasCorteDos;
    }

    public String getExcedentePerdidasCorteTres() {
        if (excedentePerdidasCorteTres == null) {
            excedentePerdidasCorteTres = "";
        }
        return excedentePerdidasCorteTres;
    }

    public String getExcedentePerdidasCorteCuatro() {
        if (excedentePerdidasCorteCuatro == null) {
            excedentePerdidasCorteCuatro = "";
        }
        return excedentePerdidasCorteCuatro;
    }

    public String getExcedentePerdidasCorteCinco() {
        if (excedentePerdidasCorteCinco == null) {
            excedentePerdidasCorteCinco = "";
        }
        return excedentePerdidasCorteCinco;
    }

    public String getExcedentePerdidasCorteSeis() {
        if (excedentePerdidasCorteSeis == null) {
            excedentePerdidasCorteSeis = "";
        }
        return excedentePerdidasCorteSeis;
    }

    public String getExcedentePerdidasCorteSiete() {
        if (excedentePerdidasCorteSiete == null) {
            excedentePerdidasCorteSiete = "";
        }
        return excedentePerdidasCorteSiete;
    }

    public String getExcedentePerdidasCorteOcho() {
        if (excedentePerdidasCorteOcho == null) {
            excedentePerdidasCorteOcho = "";
        }
        return excedentePerdidasCorteOcho;
    }

    public String getExcedentePerdidasCorteNueve() {
        if (excedentePerdidasCorteNueve == null) {
            excedentePerdidasCorteNueve = "";
        }
        return excedentePerdidasCorteNueve;
    }

    public String getExcedentePerdidasCorteDiez() {
        if (excedentePerdidasCorteDiez == null) {
            excedentePerdidasCorteDiez = "";
        }
        return excedentePerdidasCorteDiez;
    }

    public String getExcedentePerdidasCorteOnce() {
        if (excedentePerdidasCorteOnce == null) {
            excedentePerdidasCorteOnce = "";
        }
        return excedentePerdidasCorteOnce;
    }

    public String getExcedentePerdidasCorteDoce() {
        if (excedentePerdidasCorteDoce == null) {
            excedentePerdidasCorteDoce = "";
        }
        return excedentePerdidasCorteDoce;
    }

    public String getExcedentePerdidasCorteTrece() {
        if (excedentePerdidasCorteTrece == null) {
            excedentePerdidasCorteTrece = "";
        }
        return excedentePerdidasCorteTrece;
    }

}
