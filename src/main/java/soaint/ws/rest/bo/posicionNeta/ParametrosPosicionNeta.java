package soaint.ws.rest.bo.posicionNeta;

import java.util.ArrayList;

public class ParametrosPosicionNeta {
    private ArrayList<TablaInforme> tablaInforme;
    private InformePosicionNeta informePosicionNeta;

    public ArrayList<TablaInforme> getTablaInforme() {
        if (tablaInforme == null) {
            tablaInforme = new ArrayList<TablaInforme>();
        }
        return tablaInforme;
    }

    public void setTablaInforme(ArrayList<TablaInforme> tablaInforme) {
        this.tablaInforme = tablaInforme;
    }

    public InformePosicionNeta getInformePosicionNeta() {
        if (informePosicionNeta == null) {
            informePosicionNeta = new InformePosicionNeta();
        }
        return informePosicionNeta;
    }

    public void setInformePosicionNeta(InformePosicionNeta informePosicionNeta) {
        this.informePosicionNeta = informePosicionNeta;
    }
}
