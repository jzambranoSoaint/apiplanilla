package soaint.ws.rest.bo.posicionNeta;

public class InformePosicionNeta {
    private String entidad;
    private String nombreEntidad;
    private String fechaCorte;
    private String seguroDeposito;
    private String cobertura;
    private String grandesCaptaciones;
    private String grandesPromCaptaciones;
    private String grandesCartera;
    private String grandesPromColocaciones;
    private String grandesAportes;
    private String grandesNeto;
    private String privilegiadosCaptaciones;
    private String privilegiadosPromCaptaciones;
    private String privilegiadosCartera;
    private String privilegiadosPromColocaciones;
    private String privilegiadosAportes;
    private String privilegiadosNeto;
    private String totalCaptaciones;
    private String totalPromCaptaciones;
    private String totalCartera;
    private String totalPromColocaciones;
    private String totalAportes;
    private String totalNeto;
    private String brechaTotalPrivil;

    public String getEntidad() {
        if (entidad == null) {
            entidad = "";
        }
        return entidad;
    }

    public String getNombreEntidad() {
        if (nombreEntidad == null) {
            nombreEntidad = "";
        }
        return nombreEntidad;
    }

    public String getFechaCorte() {
        if (fechaCorte == null) {
            fechaCorte = "";
        }
        return fechaCorte;
    }

    public String getSeguroDeposito() {
        if (seguroDeposito == null) {
            seguroDeposito = "";
        }
        return seguroDeposito;
    }

    public String getCobertura() {
        if (cobertura == null) {
            cobertura = "";
        }
        return cobertura;
    }

    public String getGrandesCaptaciones() {
        if (grandesCaptaciones == null) {
            grandesCaptaciones = "";
        }
        return grandesCaptaciones;
    }

    public String getGrandesPromCaptaciones() {
        if (grandesPromCaptaciones == null) {
            grandesPromCaptaciones = "";
        }
        return grandesPromCaptaciones;
    }

    public String getGrandesCartera() {
        if (grandesCartera == null) {
            grandesCartera = "";
        }
        return grandesCartera;
    }

    public String getGrandesPromColocaciones() {
        if (grandesPromColocaciones == null) {
            grandesPromColocaciones = "";
        }
        return grandesPromColocaciones;
    }

    public String getGrandesAportes() {
        if (grandesAportes == null) {
            grandesAportes = "";
        }
        return grandesAportes;
    }

    public String getGrandesNeto() {
        if (grandesNeto == null) {
            grandesNeto = "";
        }
        return grandesNeto;
    }

    public String getPrivilegiadosCaptaciones() {
        if (privilegiadosCaptaciones == null) {
            privilegiadosCaptaciones = "";
        }
        return privilegiadosCaptaciones;
    }

    public String getPrivilegiadosPromCaptaciones() {
        if (privilegiadosPromCaptaciones == null) {
            privilegiadosPromCaptaciones = "";
        }
        return privilegiadosPromCaptaciones;
    }

    public String getPrivilegiadosCartera() {
        if (privilegiadosCartera == null) {
            privilegiadosCartera = "";
        }
        return privilegiadosCartera;
    }

    public String getPrivilegiadosPromColocaciones() {
        if (privilegiadosPromColocaciones == null) {
            privilegiadosPromColocaciones = "";
        }
        return privilegiadosPromColocaciones;
    }

    public String getPrivilegiadosAportes() {
        if (privilegiadosAportes == null) {
            privilegiadosAportes = "";
        }
        return privilegiadosAportes;
    }

    public String getPrivilegiadosNeto() {
        if (privilegiadosNeto == null) {
            privilegiadosNeto = "";
        }
        return privilegiadosNeto;
    }

    public String getTotalCaptaciones() {
        if (totalCaptaciones == null) {
            totalCaptaciones = "";
        }
        return totalCaptaciones;
    }

    public String getTotalPromCaptaciones() {
        if (totalPromCaptaciones == null) {
            totalPromCaptaciones = "";
        }
        return totalPromCaptaciones;
    }

    public String getTotalCartera() {
        if (totalCartera == null) {
            totalCartera = "";
        }
        return totalCartera;
    }

    public String getTotalPromColocaciones() {
        if (totalPromColocaciones == null) {
            totalPromColocaciones = "";
        }
        return totalPromColocaciones;
    }

    public String getTotalAportes() {
        if (totalAportes == null) {
            totalAportes = "";
        }
        return totalAportes;
    }

    public String getTotalNeto() {
        if (totalNeto == null) {
            totalNeto = "";
        }
        return totalNeto;
    }

    public String getBrechaTotalPrivil() {
        if (brechaTotalPrivil == null) {
            brechaTotalPrivil = "";
        }
        return brechaTotalPrivil;
    }

}
