package soaint.ws.rest.bo.posicionNeta;

public class TablaInforme {
    private String nroRenglon;
    private String tipoIdentificacion;
    private String nroIdentificacion;
    private String primerApellido;
    private String segundoApellido;
    private String nombres;
    private String totalCaptaciones;
    private String tasaInteresPromCaptaciones;
    private String totalColocaciones;
    private String tasaInteresPromColocaciones;
    private String maxCalificacionCartera;
    private String brechaPasivaActiva;
    private String totalAportes;
    private String brechaTotal;

    public String getNroRenglon() {
        if (nroRenglon == null) {
            nroRenglon = "";
        }
        return nroRenglon;
    }

    public String getTipoIdentificacion() {
        if (tipoIdentificacion == null) {
            tipoIdentificacion = "";
        }
        return tipoIdentificacion;
    }

    public String getNroIdentificacion() {
        if (nroIdentificacion == null) {
            nroIdentificacion = "";
        }
        return nroIdentificacion;
    }

    public String getPrimerApellido() {
        if (primerApellido == null) {
            primerApellido = "";
        }
        return primerApellido;
    }

    public String getSegundoApellido() {
        if (segundoApellido == null) {
            segundoApellido = "";
        }
        return segundoApellido;
    }

    public String getNombres() {
        if (nombres == null) {
            nombres = "";
        }
        return nombres;
    }

    public String getTotalCaptaciones() {
        if (totalCaptaciones == null) {
            totalCaptaciones = "";
        }
        return totalCaptaciones;
    }

    public String getTasaInteresPromCaptaciones() {
        if (tasaInteresPromCaptaciones == null) {
            tasaInteresPromCaptaciones = "";
        }
        return tasaInteresPromCaptaciones;
    }

    public String getTotalColocaciones() {
        if (totalColocaciones == null) {
            totalColocaciones = "";
        }
        return totalColocaciones;
    }

    public String getTasaInteresPromColocaciones() {
        if (tasaInteresPromColocaciones == null) {
            tasaInteresPromColocaciones = "";
        }
        return tasaInteresPromColocaciones;
    }

    public String getMaxCalificacionCartera() {
        if (maxCalificacionCartera == null) {
            maxCalificacionCartera = "";
        }
        return maxCalificacionCartera;
    }

    public String getBrechaPasivaActiva() {
        if (brechaPasivaActiva == null) {
            brechaPasivaActiva = "";
        }
        return brechaPasivaActiva;
    }

    public String getTotalAportes() {
        if (totalAportes == null) {
            totalAportes = "";
        }
        return totalAportes;
    }

    public String getBrechaTotal() {
        if (brechaTotal == null) {
            brechaTotal = "";
        }
        return brechaTotal;
    }

    public void setNroRenglon(String nroRenglon) {
        this.nroRenglon = nroRenglon;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public void setNroIdentificacion(String nroIdentificacion) {
        this.nroIdentificacion = nroIdentificacion;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public void setTotalCaptaciones(String totalCaptaciones) {
        this.totalCaptaciones = totalCaptaciones;
    }

    public void setTasaInteresPromCaptaciones(String tasaInteresPromCaptaciones) {
        this.tasaInteresPromCaptaciones = tasaInteresPromCaptaciones;
    }

    public void setTotalColocaciones(String totalColocaciones) {
        this.totalColocaciones = totalColocaciones;
    }

    public void setTasaInteresPromColocaciones(String tasaInteresPromColocaciones) {
        this.tasaInteresPromColocaciones = tasaInteresPromColocaciones;
    }

    public void setMaxCalificacionCartera(String maxCalificacionCartera) {
        this.maxCalificacionCartera = maxCalificacionCartera;
    }

    public void setBrechaPasivaActiva(String brechaPasivaActiva) {
        this.brechaPasivaActiva = brechaPasivaActiva;
    }

    public void setTotalAportes(String totalAportes) {
        this.totalAportes = totalAportes;
    }

    public void setBrechaTotal(String brechaTotal) {
        this.brechaTotal = brechaTotal;
    }
}
