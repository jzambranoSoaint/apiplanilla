package soaint.ws.rest.bo.balanceAhorroCredito;

public class ParametrosBalanceVariacion {
    private BalanceVariacion balanceVariacion;

    public BalanceVariacion getBalanceVariacion() {
        if (balanceVariacion == null) {
            balanceVariacion = new BalanceVariacion();
        }
        return balanceVariacion;
    }

    public void setBalanceVariacion(BalanceVariacion balanceVariacion) {
        this.balanceVariacion = balanceVariacion;
    }
}
