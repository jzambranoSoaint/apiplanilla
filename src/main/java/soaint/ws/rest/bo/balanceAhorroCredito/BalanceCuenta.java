package soaint.ws.rest.bo.balanceAhorroCredito;

public class BalanceCuenta {
    private String labelCorteUno;
    private String labelCorteDos;
    private String labelCorteTres;
    private String labelCorteCuatro;
    private String labelCorteCinco;
    private String labelCorteSeis;
    private String labelCorteSiete;
    private String labelCorteOcho;
    private String labelCorteNueve;
    private String labelCorteDiez;
    private String labelCorteOnce;
    private String labelCorteDoce;
    private String labelCorteTrece;
    private String activoCorteUno;
    private String activoCorteDos;
    private String activoCorteTres;
    private String activoCorteCuatro;
    private String activoCorteCinco;
    private String activoCorteSeis;
    private String activoCorteSiete;
    private String activoCorteOcho;
    private String activoCorteNueve;
    private String activoCorteDiez;
    private String activoCorteOnce;
    private String activoCorteDoce;
    private String activoCorteTrece;
    private String efectivoCorteUno;
    private String efectivoCorteDos;
    private String efectivoCorteTres;
    private String efectivoCorteCuatro;
    private String efectivoCorteCinco;
    private String efectivoCorteSeis;
    private String efectivoCorteSiete;
    private String efectivoCorteOcho;
    private String efectivoCorteNueve;
    private String efectivoCorteDiez;
    private String efectivoCorteOnce;
    private String efectivoCorteDoce;
    private String efectivoCorteTrece;
    private String inversionesCorteUno;
    private String inversionesCorteDos;
    private String inversionesCorteTres;
    private String inversionesCorteCuatro;
    private String inversionesCorteCinco;
    private String inversionesCorteSeis;
    private String inversionesCorteSiete;
    private String inversionesCorteOcho;
    private String inversionesCorteNueve;
    private String inversionesCorteDiez;
    private String inversionesCorteOnce;
    private String inversionesCorteDoce;
    private String inversionesCorteTrece;
    private String fondoLiquidezCorteUno;
    private String fondoLiquidezCorteDos;
    private String fondoLiquidezCorteTres;
    private String fondoLiquidezCorteCuatro;
    private String fondoLiquidezCorteCinco;
    private String fondoLiquidezCorteSeis;
    private String fondoLiquidezCorteSiete;
    private String fondoLiquidezCorteOcho;
    private String fondoLiquidezCorteNueve;
    private String fondoLiquidezCorteDiez;
    private String fondoLiquidezCorteOnce;
    private String fondoLiquidezCorteDoce;
    private String fondoLiquidezCorteTrece;
    private String inventariosCorteUno;
    private String inventariosCorteDos;
    private String inventariosCorteTres;
    private String inventariosCorteCuatro;
    private String inventariosCorteCinco;
    private String inventariosCorteSeis;
    private String inventariosCorteSiete;
    private String inventariosCorteOcho;
    private String inventariosCorteNueve;
    private String inventariosCorteDiez;
    private String inventariosCorteOnce;
    private String inventariosCorteDoce;
    private String inventariosCorteTrece;
    private String carteraBrutaCorteUno;
    private String carteraBrutaCorteDos;
    private String carteraBrutaCorteTres;
    private String carteraBrutaCorteCuatro;
    private String carteraBrutaCorteCinco;
    private String carteraBrutaCorteSeis;
    private String carteraBrutaCorteSiete;
    private String carteraBrutaCorteOcho;
    private String carteraBrutaCorteNueve;
    private String carteraBrutaCorteDiez;
    private String carteraBrutaCorteOnce;
    private String carteraBrutaCorteDoce;
    private String carteraBrutaCorteTrece;
    private String carteraVencidaCorteUno;
    private String carteraVencidaCorteDos;
    private String carteraVencidaCorteTres;
    private String carteraVencidaCorteCuatro;
    private String carteraVencidaCorteCinco;
    private String carteraVencidaCorteSeis;
    private String carteraVencidaCorteSiete;
    private String carteraVencidaCorteOcho;
    private String carteraVencidaCorteNueve;
    private String carteraVencidaCorteDiez;
    private String carteraVencidaCorteOnce;
    private String carteraVencidaCorteDoce;
    private String carteraVencidaCorteTrece;
    private String carteraImproductivaCorteUno;
    private String carteraImproductivaCorteDos;
    private String carteraImproductivaCorteTres;
    private String carteraImproductivaCorteCuatro;
    private String carteraImproductivaCorteCinco;
    private String carteraImproductivaCorteSeis;
    private String carteraImproductivaCorteSiete;
    private String carteraImproductivaCorteOcho;
    private String carteraImproductivaCorteNueve;
    private String carteraImproductivaCorteDiez;
    private String carteraImproductivaCorteOnce;
    private String carteraImproductivaCorteDoce;
    private String carteraImproductivaCorteTrece;
    private String carteraBrutaConLibranzaCorteUno;
    private String carteraBrutaConLibranzaCorteDos;
    private String carteraBrutaConLibranzaCorteTres;
    private String carteraBrutaConLibranzaCorteCuatro;
    private String carteraBrutaConLibranzaCorteCinco;
    private String carteraBrutaConLibranzaCorteSeis;
    private String carteraBrutaConLibranzaCorteSiete;
    private String carteraBrutaConLibranzaCorteOcho;
    private String carteraBrutaConLibranzaCorteNueve;
    private String carteraBrutaConLibranzaCorteDiez;
    private String carteraBrutaConLibranzaCorteOnce;
    private String carteraBrutaConLibranzaCorteDoce;
    private String carteraBrutaConLibranzaCorteTrece;
    private String carteraBrutaSinLibranzaCorteUno;
    private String carteraBrutaSinLibranzaCorteDos;
    private String carteraBrutaSinLibranzaCorteTres;
    private String carteraBrutaSinLibranzaCorteCuatro;
    private String carteraBrutaSinLibranzaCorteCinco;
    private String carteraBrutaSinLibranzaCorteSeis;
    private String carteraBrutaSinLibranzaCorteSiete;
    private String carteraBrutaSinLibranzaCorteOcho;
    private String carteraBrutaSinLibranzaCorteNueve;
    private String carteraBrutaSinLibranzaCorteDiez;
    private String carteraBrutaSinLibranzaCorteOnce;
    private String carteraBrutaSinLibranzaCorteDoce;
    private String carteraBrutaSinLibranzaCorteTrece;
    private String deterioroCorteUno;
    private String deterioroCorteDos;
    private String deterioroCorteTres;
    private String deterioroCorteCuatro;
    private String deterioroCorteCinco;
    private String deterioroCorteSeis;
    private String deterioroCorteSiete;
    private String deterioroCorteOcho;
    private String deterioroCorteNueve;
    private String deterioroCorteDiez;
    private String deterioroCorteOnce;
    private String deterioroCorteDoce;
    private String deterioroCorteTrece;
    private String cuentasCobrarCorteUno;
    private String cuentasCobrarCorteDos;
    private String cuentasCobrarCorteTres;
    private String cuentasCobrarCorteCuatro;
    private String cuentasCobrarCorteCinco;
    private String cuentasCobrarCorteSeis;
    private String cuentasCobrarCorteSiete;
    private String cuentasCobrarCorteOcho;
    private String cuentasCobrarCorteNueve;
    private String cuentasCobrarCorteDiez;
    private String cuentasCobrarCorteOnce;
    private String cuentasCobrarCorteDoce;
    private String cuentasCobrarCorteTrece;
    private String activosMaterialesCorteUno;
    private String activosMaterialesCorteDos;
    private String activosMaterialesCorteTres;
    private String activosMaterialesCorteCuatro;
    private String activosMaterialesCorteCinco;
    private String activosMaterialesCorteSeis;
    private String activosMaterialesCorteSiete;
    private String activosMaterialesCorteOcho;
    private String activosMaterialesCorteNueve;
    private String activosMaterialesCorteDiez;
    private String activosMaterialesCorteOnce;
    private String activosMaterialesCorteDoce;
    private String activosMaterialesCorteTrece;
    private String conveniosPorCobrarCorteUno;
    private String conveniosPorCobrarCorteDos;
    private String conveniosPorCobrarCorteTres;
    private String conveniosPorCobrarCorteCuatro;
    private String conveniosPorCobrarCorteCinco;
    private String conveniosPorCobrarCorteSeis;
    private String conveniosPorCobrarCorteSiete;
    private String conveniosPorCobrarCorteOcho;
    private String conveniosPorCobrarCorteNueve;
    private String conveniosPorCobrarCorteDiez;
    private String conveniosPorCobrarCorteOnce;
    private String conveniosPorCobrarCorteDoce;
    private String conveniosPorCobrarCorteTrece;
    private String deterioroConveniosCorteUno;
    private String deterioroConveniosCorteDos;
    private String deterioroConveniosCorteTres;
    private String deterioroConveniosCorteCuatro;
    private String deterioroConveniosCorteCinco;
    private String deterioroConveniosCorteSeis;
    private String deterioroConveniosCorteSiete;
    private String deterioroConveniosCorteOcho;
    private String deterioroConveniosCorteNueve;
    private String deterioroConveniosCorteDiez;
    private String deterioroConveniosCorteOnce;
    private String deterioroConveniosCorteDoce;
    private String deterioroConveniosCorteTrece;
    private String activosNoCorrientesCorteUno;
    private String activosNoCorrientesCorteDos;
    private String activosNoCorrientesCorteTres;
    private String activosNoCorrientesCorteCuatro;
    private String activosNoCorrientesCorteCinco;
    private String activosNoCorrientesCorteSeis;
    private String activosNoCorrientesCorteSiete;
    private String activosNoCorrientesCorteOcho;
    private String activosNoCorrientesCorteNueve;
    private String activosNoCorrientesCorteDiez;
    private String activosNoCorrientesCorteOnce;
    private String activosNoCorrientesCorteDoce;
    private String activosNoCorrientesCorteTrece;
    private String otrosActivosCorteUno;
    private String otrosActivosCorteDos;
    private String otrosActivosCorteTres;
    private String otrosActivosCorteCuatro;
    private String otrosActivosCorteCinco;
    private String otrosActivosCorteSeis;
    private String otrosActivosCorteSiete;
    private String otrosActivosCorteOcho;
    private String otrosActivosCorteNueve;
    private String otrosActivosCorteDiez;
    private String otrosActivosCorteOnce;
    private String otrosActivosCorteDoce;
    private String otrosActivosCorteTrece;
    private String totalActivosCorteUno;
    private String totalActivosCorteDos;
    private String totalActivosCorteTres;
    private String totalActivosCorteCuatro;
    private String totalActivosCorteCinco;
    private String totalActivosCorteSeis;
    private String totalActivosCorteSiete;
    private String totalActivosCorteOcho;
    private String totalActivosCorteNueve;
    private String totalActivosCorteDiez;
    private String totalActivosCorteOnce;
    private String totalActivosCorteDoce;
    private String totalActivosCorteTrece;
    private String pasivosCorteUno;
    private String pasivosCorteDos;
    private String pasivosCorteTres;
    private String pasivosCorteCuatro;
    private String pasivosCorteCinco;
    private String pasivosCorteSeis;
    private String pasivosCorteSiete;
    private String pasivosCorteOcho;
    private String pasivosCorteNueve;
    private String pasivosCorteDiez;
    private String pasivosCorteOnce;
    private String pasivosCorteDoce;
    private String pasivosCorteTrece;
    private String depositosCorteUno;
    private String depositosCorteDos;
    private String depositosCorteTres;
    private String depositosCorteCuatro;
    private String depositosCorteCinco;
    private String depositosCorteSeis;
    private String depositosCorteSiete;
    private String depositosCorteOcho;
    private String depositosCorteNueve;
    private String depositosCorteDiez;
    private String depositosCorteOnce;
    private String depositosCorteDoce;
    private String depositosCorteTrece;
    private String depositosAhorroCorteUno;
    private String depositosAhorroCorteDos;
    private String depositosAhorroCorteTres;
    private String depositosAhorroCorteCuatro;
    private String depositosAhorroCorteCinco;
    private String depositosAhorroCorteSeis;
    private String depositosAhorroCorteSiete;
    private String depositosAhorroCorteOcho;
    private String depositosAhorroCorteNueve;
    private String depositosAhorroCorteDiez;
    private String depositosAhorroCorteOnce;
    private String depositosAhorroCorteDoce;
    private String depositosAhorroCorteTrece;
    private String cdatCorteUno;
    private String cdatCorteDos;
    private String cdatCorteTres;
    private String cdatCorteCuatro;
    private String cdatCorteCinco;
    private String cdatCorteSeis;
    private String cdatCorteSiete;
    private String cdatCorteOcho;
    private String cdatCorteNueve;
    private String cdatCorteDiez;
    private String cdatCorteOnce;
    private String cdatCorteDoce;
    private String cdatCorteTrece;
    private String ahorroContractualCorteUno;
    private String ahorroContractualCorteDos;
    private String ahorroContractualCorteTres;
    private String ahorroContractualCorteCuatro;
    private String ahorroContractualCorteCinco;
    private String ahorroContractualCorteSeis;
    private String ahorroContractualCorteSiete;
    private String ahorroContractualCorteOcho;
    private String ahorroContractualCorteNueve;
    private String ahorroContractualCorteDiez;
    private String ahorroContractualCorteOnce;
    private String ahorroContractualCorteDoce;
    private String ahorroContractualCorteTrece;
    private String ahorroPermanenteCorteUno;
    private String ahorroPermanenteCorteDos;
    private String ahorroPermanenteCorteTres;
    private String ahorroPermanenteCorteCuatro;
    private String ahorroPermanenteCorteCinco;
    private String ahorroPermanenteCorteSeis;
    private String ahorroPermanenteCorteSiete;
    private String ahorroPermanenteCorteOcho;
    private String ahorroPermanenteCorteNueve;
    private String ahorroPermanenteCorteDiez;
    private String ahorroPermanenteCorteOnce;
    private String ahorroPermanenteCorteDoce;
    private String ahorroPermanenteCorteTrece;
    private String tituloInversionCorteUno;
    private String tituloInversionCorteDos;
    private String tituloInversionCorteTres;
    private String tituloInversionCorteCuatro;
    private String tituloInversionCorteCinco;
    private String tituloInversionCorteSeis;
    private String tituloInversionCorteSiete;
    private String tituloInversionCorteOcho;
    private String tituloInversionCorteNueve;
    private String tituloInversionCorteDiez;
    private String tituloInversionCorteOnce;
    private String tituloInversionCorteDoce;
    private String tituloInversionCorteTrece;
    private String creditosBancosCorteUno;
    private String creditosBancosCorteDos;
    private String creditosBancosCorteTres;
    private String creditosBancosCorteCuatro;
    private String creditosBancosCorteCinco;
    private String creditosBancosCorteSeis;
    private String creditosBancosCorteSiete;
    private String creditosBancosCorteOcho;
    private String creditosBancosCorteNueve;
    private String creditosBancosCorteDiez;
    private String creditosBancosCorteOnce;
    private String creditosBancosCorteDoce;
    private String creditosBancosCorteTrece;
    private String cuentasPorPagarCorteUno;
    private String cuentasPorPagarCorteDos;
    private String cuentasPorPagarCorteTres;
    private String cuentasPorPagarCorteCuatro;
    private String cuentasPorPagarCorteCinco;
    private String cuentasPorPagarCorteSeis;
    private String cuentasPorPagarCorteSiete;
    private String cuentasPorPagarCorteOcho;
    private String cuentasPorPagarCorteNueve;
    private String cuentasPorPagarCorteDiez;
    private String cuentasPorPagarCorteOnce;
    private String cuentasPorPagarCorteDoce;
    private String cuentasPorPagarCorteTrece;
    private String fondosSocialesCorteUno;
    private String fondosSocialesCorteDos;
    private String fondosSocialesCorteTres;
    private String fondosSocialesCorteCuatro;
    private String fondosSocialesCorteCinco;
    private String fondosSocialesCorteSeis;
    private String fondosSocialesCorteSiete;
    private String fondosSocialesCorteOcho;
    private String fondosSocialesCorteNueve;
    private String fondosSocialesCorteDiez;
    private String fondosSocialesCorteOnce;
    private String fondosSocialesCorteDoce;
    private String fondosSocialesCorteTrece;
    private String provisionesCorteUno;
    private String provisionesCorteDos;
    private String provisionesCorteTres;
    private String provisionesCorteCuatro;
    private String provisionesCorteCinco;
    private String provisionesCorteSeis;
    private String provisionesCorteSiete;
    private String provisionesCorteOcho;
    private String provisionesCorteNueve;
    private String provisionesCorteDiez;
    private String provisionesCorteOnce;
    private String provisionesCorteDoce;
    private String provisionesCorteTrece;
    private String aportesSocialesCorteUno;
    private String aportesSocialesCorteDos;
    private String aportesSocialesCorteTres;
    private String aportesSocialesCorteCuatro;
    private String aportesSocialesCorteCinco;
    private String aportesSocialesCorteSeis;
    private String aportesSocialesCorteSiete;
    private String aportesSocialesCorteOcho;
    private String aportesSocialesCorteNueve;
    private String aportesSocialesCorteDiez;
    private String aportesSocialesCorteOnce;
    private String aportesSocialesCorteDoce;
    private String aportesSocialesCorteTrece;
    private String totalPasivosCorteUno;
    private String totalPasivosCorteDos;
    private String totalPasivosCorteTres;
    private String totalPasivosCorteCuatro;
    private String totalPasivosCorteCinco;
    private String totalPasivosCorteSeis;
    private String totalPasivosCorteSiete;
    private String totalPasivosCorteOcho;
    private String totalPasivosCorteNueve;
    private String totalPasivosCorteDiez;
    private String totalPasivosCorteOnce;
    private String totalPasivosCorteDoce;
    private String totalPasivosCorteTrece;
    private String patrimonioCorteUno;
    private String patrimonioCorteDos;
    private String patrimonioCorteTres;
    private String patrimonioCorteCuatro;
    private String patrimonioCorteCinco;
    private String patrimonioCorteSeis;
    private String patrimonioCorteSiete;
    private String patrimonioCorteOcho;
    private String patrimonioCorteNueve;
    private String patrimonioCorteDiez;
    private String patrimonioCorteOnce;
    private String patrimonioCorteDoce;
    private String patrimonioCorteTrece;
    private String capitalSocialCorteUno;
    private String capitalSocialCorteDos;
    private String capitalSocialCorteTres;
    private String capitalSocialCorteCuatro;
    private String capitalSocialCorteCinco;
    private String capitalSocialCorteSeis;
    private String capitalSocialCorteSiete;
    private String capitalSocialCorteOcho;
    private String capitalSocialCorteNueve;
    private String capitalSocialCorteDiez;
    private String capitalSocialCorteOnce;
    private String capitalSocialCorteDoce;
    private String capitalSocialCorteTrece;
    private String reservasCorteUno;
    private String reservasCorteDos;
    private String reservasCorteTres;
    private String reservasCorteCuatro;
    private String reservasCorteCinco;
    private String reservasCorteSeis;
    private String reservasCorteSiete;
    private String reservasCorteOcho;
    private String reservasCorteNueve;
    private String reservasCorteDiez;
    private String reservasCorteOnce;
    private String reservasCorteDoce;
    private String reservasCorteTrece;
    private String fondosDestinacionCorteUno;
    private String fondosDestinacionCorteDos;
    private String fondosDestinacionCorteTres;
    private String fondosDestinacionCorteCuatro;
    private String fondosDestinacionCorteCinco;
    private String fondosDestinacionCorteSeis;
    private String fondosDestinacionCorteSiete;
    private String fondosDestinacionCorteOcho;
    private String fondosDestinacionCorteNueve;
    private String fondosDestinacionCorteDiez;
    private String fondosDestinacionCorteOnce;
    private String fondosDestinacionCorteDoce;
    private String fondosDestinacionCorteTrece;
    private String superavitCorteUno;
    private String superavitCorteDos;
    private String superavitCorteTres;
    private String superavitCorteCuatro;
    private String superavitCorteCinco;
    private String superavitCorteSeis;
    private String superavitCorteSiete;
    private String superavitCorteOcho;
    private String superavitCorteNueve;
    private String superavitCorteDiez;
    private String superavitCorteOnce;
    private String superavitCorteDoce;
    private String superavitCorteTrece;
    private String resultadosEjeAnterioresCorteUno;
    private String resultadosEjeAnterioresCorteDos;
    private String resultadosEjeAnterioresCorteTres;
    private String resultadosEjeAnterioresCorteCuatro;
    private String resultadosEjeAnterioresCorteCinco;
    private String resultadosEjeAnterioresCorteSeis;
    private String resultadosEjeAnterioresCorteSiete;
    private String resultadosEjeAnterioresCorteOcho;
    private String resultadosEjeAnterioresCorteNueve;
    private String resultadosEjeAnterioresCorteDiez;
    private String resultadosEjeAnterioresCorteOnce;
    private String resultadosEjeAnterioresCorteDoce;
    private String resultadosEjeAnterioresCorteTrece;
    private String resultadosEjercicioCorteUno;
    private String resultadosEjercicioCorteDos;
    private String resultadosEjercicioCorteTres;
    private String resultadosEjercicioCorteCuatro;
    private String resultadosEjercicioCorteCinco;
    private String resultadosEjercicioCorteSeis;
    private String resultadosEjercicioCorteSiete;
    private String resultadosEjercicioCorteOcho;
    private String resultadosEjercicioCorteNueve;
    private String resultadosEjercicioCorteDiez;
    private String resultadosEjercicioCorteOnce;
    private String resultadosEjercicioCorteDoce;
    private String resultadosEjercicioCorteTrece;
    private String resultadosOriCorteUno;
    private String resultadosOriCorteDos;
    private String resultadosOriCorteTres;
    private String resultadosOriCorteCuatro;
    private String resultadosOriCorteCinco;
    private String resultadosOriCorteSeis;
    private String resultadosOriCorteSiete;
    private String resultadosOriCorteOcho;
    private String resultadosOriCorteNueve;
    private String resultadosOriCorteDiez;
    private String resultadosOriCorteOnce;
    private String resultadosOriCorteDoce;
    private String resultadosOriCorteTrece;
    private String totalPatrimonioCorteUno;
    private String totalPatrimonioCorteDos;
    private String totalPatrimonioCorteTres;
    private String totalPatrimonioCorteCuatro;
    private String totalPatrimonioCorteCinco;
    private String totalPatrimonioCorteSeis;
    private String totalPatrimonioCorteSiete;
    private String totalPatrimonioCorteOcho;
    private String totalPatrimonioCorteNueve;
    private String totalPatrimonioCorteDiez;
    private String totalPatrimonioCorteOnce;
    private String totalPatrimonioCorteDoce;
    private String totalPatrimonioCorteTrece;

    public String getLabelCorteUno() {
        if (labelCorteUno == null) {
            labelCorteUno = "";
        }
        return labelCorteUno;
    }

    public String getLabelCorteDos() {
        if (labelCorteDos == null) {
            labelCorteDos = "";
        }
        return labelCorteDos;
    }

    public String getLabelCorteTres() {
        if (labelCorteTres == null) {
            labelCorteTres = "";
        }
        return labelCorteTres;
    }

    public String getLabelCorteCuatro() {
        if (labelCorteCuatro == null) {
            labelCorteCuatro = "";
        }
        return labelCorteCuatro;
    }

    public String getLabelCorteCinco() {
        if (labelCorteCinco == null) {
            labelCorteCinco = "";
        }
        return labelCorteCinco;
    }

    public String getLabelCorteSeis() {
        if (labelCorteSeis == null) {
            labelCorteSeis = "";
        }
        return labelCorteSeis;
    }

    public String getLabelCorteSiete() {
        if (labelCorteSiete == null) {
            labelCorteSiete = "";
        }
        return labelCorteSiete;
    }

    public String getLabelCorteOcho() {
        if (labelCorteOcho == null) {
            labelCorteOcho = "";
        }
        return labelCorteOcho;
    }

    public String getLabelCorteNueve() {
        if (labelCorteNueve == null) {
            labelCorteNueve = "";
        }
        return labelCorteNueve;
    }

    public String getLabelCorteDiez() {
        if (labelCorteDiez == null) {
            labelCorteDiez = "";
        }
        return labelCorteDiez;
    }

    public String getLabelCorteOnce() {
        if (labelCorteOnce == null) {
            labelCorteOnce = "";
        }
        return labelCorteOnce;
    }

    public String getLabelCorteDoce() {
        if (labelCorteDoce == null) {
            labelCorteDoce = "";
        }
        return labelCorteDoce;
    }

    public String getLabelCorteTrece() {
        if (labelCorteTrece == null) {
            labelCorteTrece = "";
        }
        return labelCorteTrece;
    }

    public String getActivoCorteUno() {
        if (activoCorteUno == null) {
            activoCorteUno = "";
        }
        return activoCorteUno;
    }

    public String getActivoCorteDos() {
        if (activoCorteDos == null) {
            activoCorteDos = "";
        }
        return activoCorteDos;
    }

    public String getActivoCorteTres() {
        if (activoCorteTres == null) {
            activoCorteTres = "";
        }
        return activoCorteTres;
    }

    public String getActivoCorteCuatro() {
        if (activoCorteCuatro == null) {
            activoCorteCuatro = "";
        }
        return activoCorteCuatro;
    }

    public String getActivoCorteCinco() {
        if (activoCorteCinco == null) {
            activoCorteCinco = "";
        }
        return activoCorteCinco;
    }

    public String getActivoCorteSeis() {
        if (activoCorteSeis == null) {
            activoCorteSeis = "";
        }
        return activoCorteSeis;
    }

    public String getActivoCorteSiete() {
        if (activoCorteSiete == null) {
            activoCorteSiete = "";
        }
        return activoCorteSiete;
    }

    public String getActivoCorteOcho() {
        if (activoCorteOcho == null) {
            activoCorteOcho = "";
        }
        return activoCorteOcho;
    }

    public String getActivoCorteNueve() {
        if (activoCorteNueve == null) {
            activoCorteNueve = "";
        }
        return activoCorteNueve;
    }

    public String getActivoCorteDiez() {
        if (activoCorteDiez == null) {
            activoCorteDiez = "";
        }
        return activoCorteDiez;
    }

    public String getActivoCorteOnce() {
        if (activoCorteOnce == null) {
            activoCorteOnce = "";
        }
        return activoCorteOnce;
    }

    public String getActivoCorteDoce() {
        if (activoCorteDoce == null) {
            activoCorteDoce = "";
        }
        return activoCorteDoce;
    }

    public String getActivoCorteTrece() {
        if (activoCorteTrece == null) {
            activoCorteTrece = "";
        }
        return activoCorteTrece;
    }

    public String getEfectivoCorteUno() {
        if (efectivoCorteUno == null) {
            efectivoCorteUno = "";
        }
        return efectivoCorteUno;
    }

    public String getEfectivoCorteDos() {
        if (efectivoCorteDos == null) {
            efectivoCorteDos = "";
        }
        return efectivoCorteDos;
    }

    public String getEfectivoCorteTres() {
        if (efectivoCorteTres == null) {
            efectivoCorteTres = "";
        }
        return efectivoCorteTres;
    }

    public String getEfectivoCorteCuatro() {
        if (efectivoCorteCuatro == null) {
            efectivoCorteCuatro = "";
        }
        return efectivoCorteCuatro;
    }

    public String getEfectivoCorteCinco() {
        if (efectivoCorteCinco == null) {
            efectivoCorteCinco = "";
        }
        return efectivoCorteCinco;
    }

    public String getEfectivoCorteSeis() {
        if (efectivoCorteSeis == null) {
            efectivoCorteSeis = "";
        }
        return efectivoCorteSeis;
    }

    public String getEfectivoCorteSiete() {
        if (efectivoCorteSiete == null) {
            efectivoCorteSiete = "";
        }
        return efectivoCorteSiete;
    }

    public String getEfectivoCorteOcho() {
        if (efectivoCorteOcho == null) {
            efectivoCorteOcho = "";
        }
        return efectivoCorteOcho;
    }

    public String getEfectivoCorteNueve() {
        if (efectivoCorteNueve == null) {
            efectivoCorteNueve = "";
        }
        return efectivoCorteNueve;
    }

    public String getEfectivoCorteDiez() {
        if (efectivoCorteDiez == null) {
            efectivoCorteDiez = "";
        }
        return efectivoCorteDiez;
    }

    public String getEfectivoCorteOnce() {
        if (efectivoCorteOnce == null) {
            efectivoCorteOnce = "";
        }
        return efectivoCorteOnce;
    }

    public String getEfectivoCorteDoce() {
        if (efectivoCorteDoce == null) {
            efectivoCorteDoce = "";
        }
        return efectivoCorteDoce;
    }

    public String getEfectivoCorteTrece() {
        if (efectivoCorteTrece == null) {
            efectivoCorteTrece = "";
        }
        return efectivoCorteTrece;
    }

    public String getInversionesCorteUno() {
        if (inversionesCorteUno == null) {
            inversionesCorteUno = "";
        }
        return inversionesCorteUno;
    }

    public String getInversionesCorteDos() {
        if (inversionesCorteDos == null) {
            inversionesCorteDos = "";
        }
        return inversionesCorteDos;
    }

    public String getInversionesCorteTres() {
        if (inversionesCorteTres == null) {
            inversionesCorteTres = "";
        }
        return inversionesCorteTres;
    }

    public String getInversionesCorteCuatro() {
        if (inversionesCorteCuatro == null) {
            inversionesCorteCuatro = "";
        }
        return inversionesCorteCuatro;
    }

    public String getInversionesCorteCinco() {
        if (inversionesCorteCinco == null) {
            inversionesCorteCinco = "";
        }
        return inversionesCorteCinco;
    }

    public String getInversionesCorteSeis() {
        if (inversionesCorteSeis == null) {
            inversionesCorteSeis = "";
        }
        return inversionesCorteSeis;
    }

    public String getInversionesCorteSiete() {
        if (inversionesCorteSiete == null) {
            inversionesCorteSiete = "";
        }
        return inversionesCorteSiete;
    }

    public String getInversionesCorteOcho() {
        if (inversionesCorteOcho == null) {
            inversionesCorteOcho = "";
        }
        return inversionesCorteOcho;
    }

    public String getInversionesCorteNueve() {
        if (inversionesCorteNueve == null) {
            inversionesCorteNueve = "";
        }
        return inversionesCorteNueve;
    }

    public String getInversionesCorteDiez() {
        if (inversionesCorteDiez == null) {
            inversionesCorteDiez = "";
        }
        return inversionesCorteDiez;
    }

    public String getInversionesCorteOnce() {
        if (inversionesCorteOnce == null) {
            inversionesCorteOnce = "";
        }
        return inversionesCorteOnce;
    }

    public String getInversionesCorteDoce() {
        if (inversionesCorteDoce == null) {
            inversionesCorteDoce = "";
        }
        return inversionesCorteDoce;
    }

    public String getInversionesCorteTrece() {
        if (inversionesCorteTrece == null) {
            inversionesCorteTrece = "";
        }
        return inversionesCorteTrece;
    }

    public String getFondoLiquidezCorteUno() {
        if (fondoLiquidezCorteUno == null) {
            fondoLiquidezCorteUno = "";
        }
        return fondoLiquidezCorteUno;
    }

    public String getFondoLiquidezCorteDos() {
        if (fondoLiquidezCorteDos == null) {
            fondoLiquidezCorteDos = "";
        }
        return fondoLiquidezCorteDos;
    }

    public String getFondoLiquidezCorteTres() {
        if (fondoLiquidezCorteTres == null) {
            fondoLiquidezCorteTres = "";
        }
        return fondoLiquidezCorteTres;
    }

    public String getFondoLiquidezCorteCuatro() {
        if (fondoLiquidezCorteCuatro == null) {
            fondoLiquidezCorteCuatro = "";
        }
        return fondoLiquidezCorteCuatro;
    }

    public String getFondoLiquidezCorteCinco() {
        if (fondoLiquidezCorteCinco == null) {
            fondoLiquidezCorteCinco = "";
        }
        return fondoLiquidezCorteCinco;
    }

    public String getFondoLiquidezCorteSeis() {
        if (fondoLiquidezCorteSeis == null) {
            fondoLiquidezCorteSeis = "";
        }
        return fondoLiquidezCorteSeis;
    }

    public String getFondoLiquidezCorteSiete() {
        if (fondoLiquidezCorteSiete == null) {
            fondoLiquidezCorteSiete = "";
        }
        return fondoLiquidezCorteSiete;
    }

    public String getFondoLiquidezCorteOcho() {
        if (fondoLiquidezCorteOcho == null) {
            fondoLiquidezCorteOcho = "";
        }
        return fondoLiquidezCorteOcho;
    }

    public String getFondoLiquidezCorteNueve() {
        if (fondoLiquidezCorteNueve == null) {
            fondoLiquidezCorteNueve = "";
        }
        return fondoLiquidezCorteNueve;
    }

    public String getFondoLiquidezCorteDiez() {
        if (fondoLiquidezCorteDiez == null) {
            fondoLiquidezCorteDiez = "";
        }
        return fondoLiquidezCorteDiez;
    }

    public String getFondoLiquidezCorteOnce() {
        if (fondoLiquidezCorteOnce == null) {
            fondoLiquidezCorteOnce = "";
        }
        return fondoLiquidezCorteOnce;
    }

    public String getFondoLiquidezCorteDoce() {
        if (fondoLiquidezCorteDoce == null) {
            fondoLiquidezCorteDoce = "";
        }
        return fondoLiquidezCorteDoce;
    }

    public String getFondoLiquidezCorteTrece() {
        if (fondoLiquidezCorteTrece == null) {
            fondoLiquidezCorteTrece = "";
        }
        return fondoLiquidezCorteTrece;
    }

    public String getInventariosCorteUno() {
        if (inventariosCorteUno == null) {
            inventariosCorteUno = "";
        }
        return inventariosCorteUno;
    }

    public String getInventariosCorteDos() {
        if (inventariosCorteDos == null) {
            inventariosCorteDos = "";
        }
        return inventariosCorteDos;
    }

    public String getInventariosCorteTres() {
        if (inventariosCorteTres == null) {
            inventariosCorteTres = "";
        }
        return inventariosCorteTres;
    }

    public String getInventariosCorteCuatro() {
        if (inventariosCorteCuatro == null) {
            inventariosCorteCuatro = "";
        }
        return inventariosCorteCuatro;
    }

    public String getInventariosCorteCinco() {
        if (inventariosCorteCinco == null) {
            inventariosCorteCinco = "";
        }
        return inventariosCorteCinco;
    }

    public String getInventariosCorteSeis() {
        if (inventariosCorteSeis == null) {
            inventariosCorteSeis = "";
        }
        return inventariosCorteSeis;
    }

    public String getInventariosCorteSiete() {
        if (inventariosCorteSiete == null) {
            inventariosCorteSiete = "";
        }
        return inventariosCorteSiete;
    }

    public String getInventariosCorteOcho() {
        if (inventariosCorteOcho == null) {
            inventariosCorteOcho = "";
        }
        return inventariosCorteOcho;
    }

    public String getInventariosCorteNueve() {
        if (inventariosCorteNueve == null) {
            inventariosCorteNueve = "";
        }
        return inventariosCorteNueve;
    }

    public String getInventariosCorteDiez() {
        if (inventariosCorteDiez == null) {
            inventariosCorteDiez = "";
        }
        return inventariosCorteDiez;
    }

    public String getInventariosCorteOnce() {
        if (inventariosCorteOnce == null) {
            inventariosCorteOnce = "";
        }
        return inventariosCorteOnce;
    }

    public String getInventariosCorteDoce() {
        if (inventariosCorteDoce == null) {
            inventariosCorteDoce = "";
        }
        return inventariosCorteDoce;
    }

    public String getInventariosCorteTrece() {
        if (inventariosCorteTrece == null) {
            inventariosCorteTrece = "";
        }
        return inventariosCorteTrece;
    }

    public String getCarteraBrutaCorteUno() {
        if (carteraBrutaCorteUno == null) {
            carteraBrutaCorteUno = "";
        }
        return carteraBrutaCorteUno;
    }

    public String getCarteraBrutaCorteDos() {
        if (carteraBrutaCorteDos == null) {
            carteraBrutaCorteDos = "";
        }
        return carteraBrutaCorteDos;
    }

    public String getCarteraBrutaCorteTres() {
        if (carteraBrutaCorteTres == null) {
            carteraBrutaCorteTres = "";
        }
        return carteraBrutaCorteTres;
    }

    public String getCarteraBrutaCorteCuatro() {
        if (carteraBrutaCorteCuatro == null) {
            carteraBrutaCorteCuatro = "";
        }
        return carteraBrutaCorteCuatro;
    }

    public String getCarteraBrutaCorteCinco() {
        if (carteraBrutaCorteCinco == null) {
            carteraBrutaCorteCinco = "";
        }
        return carteraBrutaCorteCinco;
    }

    public String getCarteraBrutaCorteSeis() {
        if (carteraBrutaCorteSeis == null) {
            carteraBrutaCorteSeis = "";
        }
        return carteraBrutaCorteSeis;
    }

    public String getCarteraBrutaCorteSiete() {
        if (carteraBrutaCorteSiete == null) {
            carteraBrutaCorteSiete = "";
        }
        return carteraBrutaCorteSiete;
    }

    public String getCarteraBrutaCorteOcho() {
        if (carteraBrutaCorteOcho == null) {
            carteraBrutaCorteOcho = "";
        }
        return carteraBrutaCorteOcho;
    }

    public String getCarteraBrutaCorteNueve() {
        if (carteraBrutaCorteNueve == null) {
            carteraBrutaCorteNueve = "";
        }
        return carteraBrutaCorteNueve;
    }

    public String getCarteraBrutaCorteDiez() {
        if (carteraBrutaCorteDiez == null) {
            carteraBrutaCorteDiez = "";
        }
        return carteraBrutaCorteDiez;
    }

    public String getCarteraBrutaCorteOnce() {
        if (carteraBrutaCorteOnce == null) {
            carteraBrutaCorteOnce = "";
        }
        return carteraBrutaCorteOnce;
    }

    public String getCarteraBrutaCorteDoce() {
        if (carteraBrutaCorteDoce == null) {
            carteraBrutaCorteDoce = "";
        }
        return carteraBrutaCorteDoce;
    }

    public String getCarteraBrutaCorteTrece() {
        if (carteraBrutaCorteTrece == null) {
            carteraBrutaCorteTrece = "";
        }
        return carteraBrutaCorteTrece;
    }

    public String getCarteraVencidaCorteUno() {
        if (carteraVencidaCorteUno == null) {
            carteraVencidaCorteUno = "";
        }
        return carteraVencidaCorteUno;
    }

    public String getCarteraVencidaCorteDos() {
        if (carteraVencidaCorteDos == null) {
            carteraVencidaCorteDos = "";
        }
        return carteraVencidaCorteDos;
    }

    public String getCarteraVencidaCorteTres() {
        if (carteraVencidaCorteTres == null) {
            carteraVencidaCorteTres = "";
        }
        return carteraVencidaCorteTres;
    }

    public String getCarteraVencidaCorteCuatro() {
        if (carteraVencidaCorteCuatro == null) {
            carteraVencidaCorteCuatro = "";
        }
        return carteraVencidaCorteCuatro;
    }

    public String getCarteraVencidaCorteCinco() {
        if (carteraVencidaCorteCinco == null) {
            carteraVencidaCorteCinco = "";
        }
        return carteraVencidaCorteCinco;
    }

    public String getCarteraVencidaCorteSeis() {
        if (carteraVencidaCorteSeis == null) {
            carteraVencidaCorteSeis = "";
        }
        return carteraVencidaCorteSeis;
    }

    public String getCarteraVencidaCorteSiete() {
        if (carteraVencidaCorteSiete == null) {
            carteraVencidaCorteSiete = "";
        }
        return carteraVencidaCorteSiete;
    }

    public String getCarteraVencidaCorteOcho() {
        if (carteraVencidaCorteOcho == null) {
            carteraVencidaCorteOcho = "";
        }
        return carteraVencidaCorteOcho;
    }

    public String getCarteraVencidaCorteNueve() {
        if (carteraVencidaCorteNueve == null) {
            carteraVencidaCorteNueve = "";
        }
        return carteraVencidaCorteNueve;
    }

    public String getCarteraVencidaCorteDiez() {
        if (carteraVencidaCorteDiez == null) {
            carteraVencidaCorteDiez = "";
        }
        return carteraVencidaCorteDiez;
    }

    public String getCarteraVencidaCorteOnce() {
        if (carteraVencidaCorteOnce == null) {
            carteraVencidaCorteOnce = "";
        }
        return carteraVencidaCorteOnce;
    }

    public String getCarteraVencidaCorteDoce() {
        if (carteraVencidaCorteDoce == null) {
            carteraVencidaCorteDoce = "";
        }
        return carteraVencidaCorteDoce;
    }

    public String getCarteraVencidaCorteTrece() {
        if (carteraVencidaCorteTrece == null) {
            carteraVencidaCorteTrece = "";
        }
        return carteraVencidaCorteTrece;
    }

    public String getCarteraImproductivaCorteUno() {
        if (carteraImproductivaCorteUno == null) {
            carteraImproductivaCorteUno = "";
        }
        return carteraImproductivaCorteUno;
    }

    public String getCarteraImproductivaCorteDos() {
        if (carteraImproductivaCorteDos == null) {
            carteraImproductivaCorteDos = "";
        }
        return carteraImproductivaCorteDos;
    }

    public String getCarteraImproductivaCorteTres() {
        if (carteraImproductivaCorteTres == null) {
            carteraImproductivaCorteTres = "";
        }
        return carteraImproductivaCorteTres;
    }

    public String getCarteraImproductivaCorteCuatro() {
        if (carteraImproductivaCorteCuatro == null) {
            carteraImproductivaCorteCuatro = "";
        }
        return carteraImproductivaCorteCuatro;
    }

    public String getCarteraImproductivaCorteCinco() {
        if (carteraImproductivaCorteCinco == null) {
            carteraImproductivaCorteCinco = "";
        }
        return carteraImproductivaCorteCinco;
    }

    public String getCarteraImproductivaCorteSeis() {
        if (carteraImproductivaCorteSeis == null) {
            carteraImproductivaCorteSeis = "";
        }
        return carteraImproductivaCorteSeis;
    }

    public String getCarteraImproductivaCorteSiete() {
        if (carteraImproductivaCorteSiete == null) {
            carteraImproductivaCorteSiete = "";
        }
        return carteraImproductivaCorteSiete;
    }

    public String getCarteraImproductivaCorteOcho() {
        if (carteraImproductivaCorteOcho == null) {
            carteraImproductivaCorteOcho = "";
        }
        return carteraImproductivaCorteOcho;
    }

    public String getCarteraImproductivaCorteNueve() {
        if (carteraImproductivaCorteNueve == null) {
            carteraImproductivaCorteNueve = "";
        }
        return carteraImproductivaCorteNueve;
    }

    public String getCarteraImproductivaCorteDiez() {
        if (carteraImproductivaCorteDiez == null) {
            carteraImproductivaCorteDiez = "";
        }
        return carteraImproductivaCorteDiez;
    }

    public String getCarteraImproductivaCorteOnce() {
        if (carteraImproductivaCorteOnce == null) {
            carteraImproductivaCorteOnce = "";
        }
        return carteraImproductivaCorteOnce;
    }

    public String getCarteraImproductivaCorteDoce() {
        if (carteraImproductivaCorteDoce == null) {
            carteraImproductivaCorteDoce = "";
        }
        return carteraImproductivaCorteDoce;
    }

    public String getCarteraImproductivaCorteTrece() {
        if (carteraImproductivaCorteTrece == null) {
            carteraImproductivaCorteTrece = "";
        }
        return carteraImproductivaCorteTrece;
    }

    public String getCarteraBrutaConLibranzaCorteUno() {
        if (carteraBrutaConLibranzaCorteUno == null) {
            carteraBrutaConLibranzaCorteUno = "";
        }
        return carteraBrutaConLibranzaCorteUno;
    }

    public String getCarteraBrutaConLibranzaCorteDos() {
        if (carteraBrutaConLibranzaCorteDos == null) {
            carteraBrutaConLibranzaCorteDos = "";
        }
        return carteraBrutaConLibranzaCorteDos;
    }

    public String getCarteraBrutaConLibranzaCorteTres() {
        if (carteraBrutaConLibranzaCorteTres == null) {
            carteraBrutaConLibranzaCorteTres = "";
        }
        return carteraBrutaConLibranzaCorteTres;
    }

    public String getCarteraBrutaConLibranzaCorteCuatro() {
        if (carteraBrutaConLibranzaCorteCuatro == null) {
            carteraBrutaConLibranzaCorteCuatro = "";
        }
        return carteraBrutaConLibranzaCorteCuatro;
    }

    public String getCarteraBrutaConLibranzaCorteCinco() {
        if (carteraBrutaConLibranzaCorteCinco == null) {
            carteraBrutaConLibranzaCorteCinco = "";
        }
        return carteraBrutaConLibranzaCorteCinco;
    }

    public String getCarteraBrutaConLibranzaCorteSeis() {
        if (carteraBrutaConLibranzaCorteSeis == null) {
            carteraBrutaConLibranzaCorteSeis = "";
        }
        return carteraBrutaConLibranzaCorteSeis;
    }

    public String getCarteraBrutaConLibranzaCorteSiete() {
        if (carteraBrutaConLibranzaCorteSiete == null) {
            carteraBrutaConLibranzaCorteSiete = "";
        }
        return carteraBrutaConLibranzaCorteSiete;
    }

    public String getCarteraBrutaConLibranzaCorteOcho() {
        if (carteraBrutaConLibranzaCorteOcho == null) {
            carteraBrutaConLibranzaCorteOcho = "";
        }
        return carteraBrutaConLibranzaCorteOcho;
    }

    public String getCarteraBrutaConLibranzaCorteNueve() {
        if (carteraBrutaConLibranzaCorteNueve == null) {
            carteraBrutaConLibranzaCorteNueve = "";
        }
        return carteraBrutaConLibranzaCorteNueve;
    }

    public String getCarteraBrutaConLibranzaCorteDiez() {
        if (carteraBrutaConLibranzaCorteDiez == null) {
            carteraBrutaConLibranzaCorteDiez = "";
        }
        return carteraBrutaConLibranzaCorteDiez;
    }

    public String getCarteraBrutaConLibranzaCorteOnce() {
        if (carteraBrutaConLibranzaCorteOnce == null) {
            carteraBrutaConLibranzaCorteOnce = "";
        }
        return carteraBrutaConLibranzaCorteOnce;
    }

    public String getCarteraBrutaConLibranzaCorteDoce() {
        if (carteraBrutaConLibranzaCorteDoce == null) {
            carteraBrutaConLibranzaCorteDoce = "";
        }
        return carteraBrutaConLibranzaCorteDoce;
    }

    public String getCarteraBrutaConLibranzaCorteTrece() {
        if (carteraBrutaConLibranzaCorteTrece == null) {
            carteraBrutaConLibranzaCorteTrece = "";
        }
        return carteraBrutaConLibranzaCorteTrece;
    }

    public String getCarteraBrutaSinLibranzaCorteUno() {
        if (carteraBrutaSinLibranzaCorteUno == null) {
            carteraBrutaSinLibranzaCorteUno = "";
        }
        return carteraBrutaSinLibranzaCorteUno;
    }

    public String getCarteraBrutaSinLibranzaCorteDos() {
        if (carteraBrutaSinLibranzaCorteDos == null) {
            carteraBrutaSinLibranzaCorteDos = "";
        }
        return carteraBrutaSinLibranzaCorteDos;
    }

    public String getCarteraBrutaSinLibranzaCorteTres() {
        if (carteraBrutaSinLibranzaCorteTres == null) {
            carteraBrutaSinLibranzaCorteTres = "";
        }
        return carteraBrutaSinLibranzaCorteTres;
    }

    public String getCarteraBrutaSinLibranzaCorteCuatro() {
        if (carteraBrutaSinLibranzaCorteCuatro == null) {
            carteraBrutaSinLibranzaCorteCuatro = "";
        }
        return carteraBrutaSinLibranzaCorteCuatro;
    }

    public String getCarteraBrutaSinLibranzaCorteCinco() {
        if (carteraBrutaSinLibranzaCorteCinco == null) {
            carteraBrutaSinLibranzaCorteCinco = "";
        }
        return carteraBrutaSinLibranzaCorteCinco;
    }

    public String getCarteraBrutaSinLibranzaCorteSeis() {
        if (carteraBrutaSinLibranzaCorteSeis == null) {
            carteraBrutaSinLibranzaCorteSeis = "";
        }
        return carteraBrutaSinLibranzaCorteSeis;
    }

    public String getCarteraBrutaSinLibranzaCorteSiete() {
        if (carteraBrutaSinLibranzaCorteSiete == null) {
            carteraBrutaSinLibranzaCorteSiete = "";
        }
        return carteraBrutaSinLibranzaCorteSiete;
    }

    public String getCarteraBrutaSinLibranzaCorteOcho() {
        if (carteraBrutaSinLibranzaCorteOcho == null) {
            carteraBrutaSinLibranzaCorteOcho = "";
        }
        return carteraBrutaSinLibranzaCorteOcho;
    }

    public String getCarteraBrutaSinLibranzaCorteNueve() {
        if (carteraBrutaSinLibranzaCorteNueve == null) {
            carteraBrutaSinLibranzaCorteNueve = "";
        }
        return carteraBrutaSinLibranzaCorteNueve;
    }

    public String getCarteraBrutaSinLibranzaCorteDiez() {
        if (carteraBrutaSinLibranzaCorteDiez == null) {
            carteraBrutaSinLibranzaCorteDiez = "";
        }
        return carteraBrutaSinLibranzaCorteDiez;
    }

    public String getCarteraBrutaSinLibranzaCorteOnce() {
        if (carteraBrutaSinLibranzaCorteOnce == null) {
            carteraBrutaSinLibranzaCorteOnce = "";
        }
        return carteraBrutaSinLibranzaCorteOnce;
    }

    public String getCarteraBrutaSinLibranzaCorteDoce() {
        if (carteraBrutaSinLibranzaCorteDoce == null) {
            carteraBrutaSinLibranzaCorteDoce = "";
        }
        return carteraBrutaSinLibranzaCorteDoce;
    }

    public String getCarteraBrutaSinLibranzaCorteTrece() {
        if (carteraBrutaSinLibranzaCorteTrece == null) {
            carteraBrutaSinLibranzaCorteTrece = "";
        }
        return carteraBrutaSinLibranzaCorteTrece;
    }

    public String getDeterioroCorteUno() {
        if (deterioroCorteUno == null) {
            deterioroCorteUno = "";
        }
        return deterioroCorteUno;
    }

    public String getDeterioroCorteDos() {
        if (deterioroCorteDos == null) {
            deterioroCorteDos = "";
        }
        return deterioroCorteDos;
    }

    public String getDeterioroCorteTres() {
        if (deterioroCorteTres == null) {
            deterioroCorteTres = "";
        }
        return deterioroCorteTres;
    }

    public String getDeterioroCorteCuatro() {
        if (deterioroCorteCuatro == null) {
            deterioroCorteCuatro = "";
        }
        return deterioroCorteCuatro;
    }

    public String getDeterioroCorteCinco() {
        if (deterioroCorteCinco == null) {
            deterioroCorteCinco = "";
        }
        return deterioroCorteCinco;
    }

    public String getDeterioroCorteSeis() {
        if (deterioroCorteSeis == null) {
            deterioroCorteSeis = "";
        }
        return deterioroCorteSeis;
    }

    public String getDeterioroCorteSiete() {
        if (deterioroCorteSiete == null) {
            deterioroCorteSiete = "";
        }
        return deterioroCorteSiete;
    }

    public String getDeterioroCorteOcho() {
        if (deterioroCorteOcho == null) {
            deterioroCorteOcho = "";
        }
        return deterioroCorteOcho;
    }

    public String getDeterioroCorteNueve() {
        if (deterioroCorteNueve == null) {
            deterioroCorteNueve = "";
        }
        return deterioroCorteNueve;
    }

    public String getDeterioroCorteDiez() {
        if (deterioroCorteDiez == null) {
            deterioroCorteDiez = "";
        }
        return deterioroCorteDiez;
    }

    public String getDeterioroCorteOnce() {
        if (deterioroCorteOnce == null) {
            deterioroCorteOnce = "";
        }
        return deterioroCorteOnce;
    }

    public String getDeterioroCorteDoce() {
        if (deterioroCorteDoce == null) {
            deterioroCorteDoce = "";
        }
        return deterioroCorteDoce;
    }

    public String getDeterioroCorteTrece() {
        if (deterioroCorteTrece == null) {
            deterioroCorteTrece = "";
        }
        return deterioroCorteTrece;
    }

    public String getCuentasCobrarCorteUno() {
        if (cuentasCobrarCorteUno == null) {
            cuentasCobrarCorteUno = "";
        }
        return cuentasCobrarCorteUno;
    }

    public String getCuentasCobrarCorteDos() {
        if (cuentasCobrarCorteDos == null) {
            cuentasCobrarCorteDos = "";
        }
        return cuentasCobrarCorteDos;
    }

    public String getCuentasCobrarCorteTres() {
        if (cuentasCobrarCorteTres == null) {
            cuentasCobrarCorteTres = "";
        }
        return cuentasCobrarCorteTres;
    }

    public String getCuentasCobrarCorteCuatro() {
        if (cuentasCobrarCorteCuatro == null) {
            cuentasCobrarCorteCuatro = "";
        }
        return cuentasCobrarCorteCuatro;
    }

    public String getCuentasCobrarCorteCinco() {
        if (cuentasCobrarCorteCinco == null) {
            cuentasCobrarCorteCinco = "";
        }
        return cuentasCobrarCorteCinco;
    }

    public String getCuentasCobrarCorteSeis() {
        if (cuentasCobrarCorteSeis == null) {
            cuentasCobrarCorteSeis = "";
        }
        return cuentasCobrarCorteSeis;
    }

    public String getCuentasCobrarCorteSiete() {
        if (cuentasCobrarCorteSiete == null) {
            cuentasCobrarCorteSiete = "";
        }
        return cuentasCobrarCorteSiete;
    }

    public String getCuentasCobrarCorteOcho() {
        if (cuentasCobrarCorteOcho == null) {
            cuentasCobrarCorteOcho = "";
        }
        return cuentasCobrarCorteOcho;
    }

    public String getCuentasCobrarCorteNueve() {
        if (cuentasCobrarCorteNueve == null) {
            cuentasCobrarCorteNueve = "";
        }
        return cuentasCobrarCorteNueve;
    }

    public String getCuentasCobrarCorteDiez() {
        if (cuentasCobrarCorteDiez == null) {
            cuentasCobrarCorteDiez = "";
        }
        return cuentasCobrarCorteDiez;
    }

    public String getCuentasCobrarCorteOnce() {
        if (cuentasCobrarCorteOnce == null) {
            cuentasCobrarCorteOnce = "";
        }
        return cuentasCobrarCorteOnce;
    }

    public String getCuentasCobrarCorteDoce() {
        if (cuentasCobrarCorteDoce == null) {
            cuentasCobrarCorteDoce = "";
        }
        return cuentasCobrarCorteDoce;
    }

    public String getCuentasCobrarCorteTrece() {
        if (cuentasCobrarCorteTrece == null) {
            cuentasCobrarCorteTrece = "";
        }
        return cuentasCobrarCorteTrece;
    }

    public String getActivosMaterialesCorteUno() {
        if (activosMaterialesCorteUno == null) {
            activosMaterialesCorteUno = "";
        }
        return activosMaterialesCorteUno;
    }

    public String getActivosMaterialesCorteDos() {
        if (activosMaterialesCorteDos == null) {
            activosMaterialesCorteDos = "";
        }
        return activosMaterialesCorteDos;
    }

    public String getActivosMaterialesCorteTres() {
        if (activosMaterialesCorteTres == null) {
            activosMaterialesCorteTres = "";
        }
        return activosMaterialesCorteTres;
    }

    public String getActivosMaterialesCorteCuatro() {
        if (activosMaterialesCorteCuatro == null) {
            activosMaterialesCorteCuatro = "";
        }
        return activosMaterialesCorteCuatro;
    }

    public String getActivosMaterialesCorteCinco() {
        if (activosMaterialesCorteCinco == null) {
            activosMaterialesCorteCinco = "";
        }
        return activosMaterialesCorteCinco;
    }

    public String getActivosMaterialesCorteSeis() {
        if (activosMaterialesCorteSeis == null) {
            activosMaterialesCorteSeis = "";
        }
        return activosMaterialesCorteSeis;
    }

    public String getActivosMaterialesCorteSiete() {
        if (activosMaterialesCorteSiete == null) {
            activosMaterialesCorteSiete = "";
        }
        return activosMaterialesCorteSiete;
    }

    public String getActivosMaterialesCorteOcho() {
        if (activosMaterialesCorteOcho == null) {
            activosMaterialesCorteOcho = "";
        }
        return activosMaterialesCorteOcho;
    }

    public String getActivosMaterialesCorteNueve() {
        if (activosMaterialesCorteNueve == null) {
            activosMaterialesCorteNueve = "";
        }
        return activosMaterialesCorteNueve;
    }

    public String getActivosMaterialesCorteDiez() {
        if (activosMaterialesCorteDiez == null) {
            activosMaterialesCorteDiez = "";
        }
        return activosMaterialesCorteDiez;
    }

    public String getActivosMaterialesCorteOnce() {
        if (activosMaterialesCorteOnce == null) {
            activosMaterialesCorteOnce = "";
        }
        return activosMaterialesCorteOnce;
    }

    public String getActivosMaterialesCorteDoce() {
        if (activosMaterialesCorteDoce == null) {
            activosMaterialesCorteDoce = "";
        }
        return activosMaterialesCorteDoce;
    }

    public String getActivosMaterialesCorteTrece() {
        if (activosMaterialesCorteTrece == null) {
            activosMaterialesCorteTrece = "";
        }
        return activosMaterialesCorteTrece;
    }

    public String getConveniosPorCobrarCorteUno() {
        if (conveniosPorCobrarCorteUno == null) {
            conveniosPorCobrarCorteUno = "";
        }
        return conveniosPorCobrarCorteUno;
    }

    public String getConveniosPorCobrarCorteDos() {
        if (conveniosPorCobrarCorteDos == null) {
            conveniosPorCobrarCorteDos = "";
        }
        return conveniosPorCobrarCorteDos;
    }

    public String getConveniosPorCobrarCorteTres() {
        if (conveniosPorCobrarCorteTres == null) {
            conveniosPorCobrarCorteTres = "";
        }
        return conveniosPorCobrarCorteTres;
    }

    public String getConveniosPorCobrarCorteCuatro() {
        if (conveniosPorCobrarCorteCuatro == null) {
            conveniosPorCobrarCorteCuatro = "";
        }
        return conveniosPorCobrarCorteCuatro;
    }

    public String getConveniosPorCobrarCorteCinco() {
        if (conveniosPorCobrarCorteCinco == null) {
            conveniosPorCobrarCorteCinco = "";
        }
        return conveniosPorCobrarCorteCinco;
    }

    public String getConveniosPorCobrarCorteSeis() {
        if (conveniosPorCobrarCorteSeis == null) {
            conveniosPorCobrarCorteSeis = "";
        }
        return conveniosPorCobrarCorteSeis;
    }

    public String getConveniosPorCobrarCorteSiete() {
        if (conveniosPorCobrarCorteSiete == null) {
            conveniosPorCobrarCorteSiete = "";
        }
        return conveniosPorCobrarCorteSiete;
    }

    public String getConveniosPorCobrarCorteOcho() {
        if (conveniosPorCobrarCorteOcho == null) {
            conveniosPorCobrarCorteOcho = "";
        }
        return conveniosPorCobrarCorteOcho;
    }

    public String getConveniosPorCobrarCorteNueve() {
        if (conveniosPorCobrarCorteNueve == null) {
            conveniosPorCobrarCorteNueve = "";
        }
        return conveniosPorCobrarCorteNueve;
    }

    public String getConveniosPorCobrarCorteDiez() {
        if (conveniosPorCobrarCorteDiez == null) {
            conveniosPorCobrarCorteDiez = "";
        }
        return conveniosPorCobrarCorteDiez;
    }

    public String getConveniosPorCobrarCorteOnce() {
        if (conveniosPorCobrarCorteOnce == null) {
            conveniosPorCobrarCorteOnce = "";
        }
        return conveniosPorCobrarCorteOnce;
    }

    public String getConveniosPorCobrarCorteDoce() {
        if (conveniosPorCobrarCorteDoce == null) {
            conveniosPorCobrarCorteDoce = "";
        }
        return conveniosPorCobrarCorteDoce;
    }

    public String getConveniosPorCobrarCorteTrece() {
        if (conveniosPorCobrarCorteTrece == null) {
            conveniosPorCobrarCorteTrece = "";
        }
        return conveniosPorCobrarCorteTrece;
    }

    public String getDeterioroConveniosCorteUno() {
        if (deterioroConveniosCorteUno == null) {
            deterioroConveniosCorteUno = "";
        }
        return deterioroConveniosCorteUno;
    }

    public String getDeterioroConveniosCorteDos() {
        if (deterioroConveniosCorteDos == null) {
            deterioroConveniosCorteDos = "";
        }
        return deterioroConveniosCorteDos;
    }

    public String getDeterioroConveniosCorteTres() {
        if (deterioroConveniosCorteTres == null) {
            deterioroConveniosCorteTres = "";
        }
        return deterioroConveniosCorteTres;
    }

    public String getDeterioroConveniosCorteCuatro() {
        if (deterioroConveniosCorteCuatro == null) {
            deterioroConveniosCorteCuatro = "";
        }
        return deterioroConveniosCorteCuatro;
    }

    public String getDeterioroConveniosCorteCinco() {
        if (deterioroConveniosCorteCinco == null) {
            deterioroConveniosCorteCinco = "";
        }
        return deterioroConveniosCorteCinco;
    }

    public String getDeterioroConveniosCorteSeis() {
        if (deterioroConveniosCorteSeis == null) {
            deterioroConveniosCorteSeis = "";
        }
        return deterioroConveniosCorteSeis;
    }

    public String getDeterioroConveniosCorteSiete() {
        if (deterioroConveniosCorteSiete == null) {
            deterioroConveniosCorteSiete = "";
        }
        return deterioroConveniosCorteSiete;
    }

    public String getDeterioroConveniosCorteOcho() {
        if (deterioroConveniosCorteOcho == null) {
            deterioroConveniosCorteOcho = "";
        }
        return deterioroConveniosCorteOcho;
    }

    public String getDeterioroConveniosCorteNueve() {
        if (deterioroConveniosCorteNueve == null) {
            deterioroConveniosCorteNueve = "";
        }
        return deterioroConveniosCorteNueve;
    }

    public String getDeterioroConveniosCorteDiez() {
        if (deterioroConveniosCorteDiez == null) {
            deterioroConveniosCorteDiez = "";
        }
        return deterioroConveniosCorteDiez;
    }

    public String getDeterioroConveniosCorteOnce() {
        if (deterioroConveniosCorteOnce == null) {
            deterioroConveniosCorteOnce = "";
        }
        return deterioroConveniosCorteOnce;
    }

    public String getDeterioroConveniosCorteDoce() {
        if (deterioroConveniosCorteDoce == null) {
            deterioroConveniosCorteDoce = "";
        }
        return deterioroConveniosCorteDoce;
    }

    public String getDeterioroConveniosCorteTrece() {
        if (deterioroConveniosCorteTrece == null) {
            deterioroConveniosCorteTrece = "";
        }
        return deterioroConveniosCorteTrece;
    }

    public String getActivosNoCorrientesCorteUno() {
        if (activosNoCorrientesCorteUno == null) {
            activosNoCorrientesCorteUno = "";
        }
        return activosNoCorrientesCorteUno;
    }

    public String getActivosNoCorrientesCorteDos() {
        if (activosNoCorrientesCorteDos == null) {
            activosNoCorrientesCorteDos = "";
        }
        return activosNoCorrientesCorteDos;
    }

    public String getActivosNoCorrientesCorteTres() {
        if (activosNoCorrientesCorteTres == null) {
            activosNoCorrientesCorteTres = "";
        }
        return activosNoCorrientesCorteTres;
    }

    public String getActivosNoCorrientesCorteCuatro() {
        if (activosNoCorrientesCorteCuatro == null) {
            activosNoCorrientesCorteCuatro = "";
        }
        return activosNoCorrientesCorteCuatro;
    }

    public String getActivosNoCorrientesCorteCinco() {
        if (activosNoCorrientesCorteCinco == null) {
            activosNoCorrientesCorteCinco = "";
        }
        return activosNoCorrientesCorteCinco;
    }

    public String getActivosNoCorrientesCorteSeis() {
        if (activosNoCorrientesCorteSeis == null) {
            activosNoCorrientesCorteSeis = "";
        }
        return activosNoCorrientesCorteSeis;
    }

    public String getActivosNoCorrientesCorteSiete() {
        if (activosNoCorrientesCorteSiete == null) {
            activosNoCorrientesCorteSiete = "";
        }
        return activosNoCorrientesCorteSiete;
    }

    public String getActivosNoCorrientesCorteOcho() {
        if (activosNoCorrientesCorteOcho == null) {
            activosNoCorrientesCorteOcho = "";
        }
        return activosNoCorrientesCorteOcho;
    }

    public String getActivosNoCorrientesCorteNueve() {
        if (activosNoCorrientesCorteNueve == null) {
            activosNoCorrientesCorteNueve = "";
        }
        return activosNoCorrientesCorteNueve;
    }

    public String getActivosNoCorrientesCorteDiez() {
        if (activosNoCorrientesCorteDiez == null) {
            activosNoCorrientesCorteDiez = "";
        }
        return activosNoCorrientesCorteDiez;
    }

    public String getActivosNoCorrientesCorteOnce() {
        if (activosNoCorrientesCorteOnce == null) {
            activosNoCorrientesCorteOnce = "";
        }
        return activosNoCorrientesCorteOnce;
    }

    public String getActivosNoCorrientesCorteDoce() {
        if (activosNoCorrientesCorteDoce == null) {
            activosNoCorrientesCorteDoce = "";
        }
        return activosNoCorrientesCorteDoce;
    }

    public String getActivosNoCorrientesCorteTrece() {
        if (activosNoCorrientesCorteTrece == null) {
            activosNoCorrientesCorteTrece = "";
        }
        return activosNoCorrientesCorteTrece;
    }

    public String getOtrosActivosCorteUno() {
        if (otrosActivosCorteUno == null) {
            otrosActivosCorteUno = "";
        }
        return otrosActivosCorteUno;
    }

    public String getOtrosActivosCorteDos() {
        if (otrosActivosCorteDos == null) {
            otrosActivosCorteDos = "";
        }
        return otrosActivosCorteDos;
    }

    public String getOtrosActivosCorteTres() {
        if (otrosActivosCorteTres == null) {
            otrosActivosCorteTres = "";
        }
        return otrosActivosCorteTres;
    }

    public String getOtrosActivosCorteCuatro() {
        if (otrosActivosCorteCuatro == null) {
            otrosActivosCorteCuatro = "";
        }
        return otrosActivosCorteCuatro;
    }

    public String getOtrosActivosCorteCinco() {
        if (otrosActivosCorteCinco == null) {
            otrosActivosCorteCinco = "";
        }
        return otrosActivosCorteCinco;
    }

    public String getOtrosActivosCorteSeis() {
        if (otrosActivosCorteSeis == null) {
            otrosActivosCorteSeis = "";
        }
        return otrosActivosCorteSeis;
    }

    public String getOtrosActivosCorteSiete() {
        if (otrosActivosCorteSiete == null) {
            otrosActivosCorteSiete = "";
        }
        return otrosActivosCorteSiete;
    }

    public String getOtrosActivosCorteOcho() {
        if (otrosActivosCorteOcho == null) {
            otrosActivosCorteOcho = "";
        }
        return otrosActivosCorteOcho;
    }

    public String getOtrosActivosCorteNueve() {
        if (otrosActivosCorteNueve == null) {
            otrosActivosCorteNueve = "";
        }
        return otrosActivosCorteNueve;
    }

    public String getOtrosActivosCorteDiez() {
        if (otrosActivosCorteDiez == null) {
            otrosActivosCorteDiez = "";
        }
        return otrosActivosCorteDiez;
    }

    public String getOtrosActivosCorteOnce() {
        if (otrosActivosCorteOnce == null) {
            otrosActivosCorteOnce = "";
        }
        return otrosActivosCorteOnce;
    }

    public String getOtrosActivosCorteDoce() {
        if (otrosActivosCorteDoce == null) {
            otrosActivosCorteDoce = "";
        }
        return otrosActivosCorteDoce;
    }

    public String getOtrosActivosCorteTrece() {
        if (otrosActivosCorteTrece == null) {
            otrosActivosCorteTrece = "";
        }
        return otrosActivosCorteTrece;
    }

    public String getTotalActivosCorteUno() {
        if (totalActivosCorteUno == null) {
            totalActivosCorteUno = "";
        }
        return totalActivosCorteUno;
    }

    public String getTotalActivosCorteDos() {
        if (totalActivosCorteDos == null) {
            totalActivosCorteDos = "";
        }
        return totalActivosCorteDos;
    }

    public String getTotalActivosCorteTres() {
        if (totalActivosCorteTres == null) {
            totalActivosCorteTres = "";
        }
        return totalActivosCorteTres;
    }

    public String getTotalActivosCorteCuatro() {
        if (totalActivosCorteCuatro == null) {
            totalActivosCorteCuatro = "";
        }
        return totalActivosCorteCuatro;
    }

    public String getTotalActivosCorteCinco() {
        if (totalActivosCorteCinco == null) {
            totalActivosCorteCinco = "";
        }
        return totalActivosCorteCinco;
    }

    public String getTotalActivosCorteSeis() {
        if (totalActivosCorteSeis == null) {
            totalActivosCorteSeis = "";
        }
        return totalActivosCorteSeis;
    }

    public String getTotalActivosCorteSiete() {
        if (totalActivosCorteSiete == null) {
            totalActivosCorteSiete = "";
        }
        return totalActivosCorteSiete;
    }

    public String getTotalActivosCorteOcho() {
        if (totalActivosCorteOcho == null) {
            totalActivosCorteOcho = "";
        }
        return totalActivosCorteOcho;
    }

    public String getTotalActivosCorteNueve() {
        if (totalActivosCorteNueve == null) {
            totalActivosCorteNueve = "";
        }
        return totalActivosCorteNueve;
    }

    public String getTotalActivosCorteDiez() {
        if (totalActivosCorteDiez == null) {
            totalActivosCorteDiez = "";
        }
        return totalActivosCorteDiez;
    }

    public String getTotalActivosCorteOnce() {
        if (totalActivosCorteOnce == null) {
            totalActivosCorteOnce = "";
        }
        return totalActivosCorteOnce;
    }

    public String getTotalActivosCorteDoce() {
        if (totalActivosCorteDoce == null) {
            totalActivosCorteDoce = "";
        }
        return totalActivosCorteDoce;
    }

    public String getTotalActivosCorteTrece() {
        if (totalActivosCorteTrece == null) {
            totalActivosCorteTrece = "";
        }
        return totalActivosCorteTrece;
    }

    public String getPasivosCorteUno() {
        if (pasivosCorteUno == null) {
            pasivosCorteUno = "";
        }
        return pasivosCorteUno;
    }

    public String getPasivosCorteDos() {
        if (pasivosCorteDos == null) {
            pasivosCorteDos = "";
        }
        return pasivosCorteDos;
    }

    public String getPasivosCorteTres() {
        if (pasivosCorteTres == null) {
            pasivosCorteTres = "";
        }
        return pasivosCorteTres;
    }

    public String getPasivosCorteCuatro() {
        if (pasivosCorteCuatro == null) {
            pasivosCorteCuatro = "";
        }
        return pasivosCorteCuatro;
    }

    public String getPasivosCorteCinco() {
        if (pasivosCorteCinco == null) {
            pasivosCorteCinco = "";
        }
        return pasivosCorteCinco;
    }

    public String getPasivosCorteSeis() {
        if (pasivosCorteSeis == null) {
            pasivosCorteSeis = "";
        }
        return pasivosCorteSeis;
    }

    public String getPasivosCorteSiete() {
        if (pasivosCorteSiete == null) {
            pasivosCorteSiete = "";
        }
        return pasivosCorteSiete;
    }

    public String getPasivosCorteOcho() {
        if (pasivosCorteOcho == null) {
            pasivosCorteOcho = "";
        }
        return pasivosCorteOcho;
    }

    public String getPasivosCorteNueve() {
        if (pasivosCorteNueve == null) {
            pasivosCorteNueve = "";
        }
        return pasivosCorteNueve;
    }

    public String getPasivosCorteDiez() {
        if (pasivosCorteDiez == null) {
            pasivosCorteDiez = "";
        }
        return pasivosCorteDiez;
    }

    public String getPasivosCorteOnce() {
        if (pasivosCorteOnce == null) {
            pasivosCorteOnce = "";
        }
        return pasivosCorteOnce;
    }

    public String getPasivosCorteDoce() {
        if (pasivosCorteDoce == null) {
            pasivosCorteDoce = "";
        }
        return pasivosCorteDoce;
    }

    public String getPasivosCorteTrece() {
        if (pasivosCorteTrece == null) {
            pasivosCorteTrece = "";
        }
        return pasivosCorteTrece;
    }

    public String getDepositosCorteUno() {
        if (depositosCorteUno == null) {
            depositosCorteUno = "";
        }
        return depositosCorteUno;
    }

    public String getDepositosCorteDos() {
        if (depositosCorteDos == null) {
            depositosCorteDos = "";
        }
        return depositosCorteDos;
    }

    public String getDepositosCorteTres() {
        if (depositosCorteTres == null) {
            depositosCorteTres = "";
        }
        return depositosCorteTres;
    }

    public String getDepositosCorteCuatro() {
        if (depositosCorteCuatro == null) {
            depositosCorteCuatro = "";
        }
        return depositosCorteCuatro;
    }

    public String getDepositosCorteCinco() {
        if (depositosCorteCinco == null) {
            depositosCorteCinco = "";
        }
        return depositosCorteCinco;
    }

    public String getDepositosCorteSeis() {
        if (depositosCorteSeis == null) {
            depositosCorteSeis = "";
        }
        return depositosCorteSeis;
    }

    public String getDepositosCorteSiete() {
        if (depositosCorteSiete == null) {
            depositosCorteSiete = "";
        }
        return depositosCorteSiete;
    }

    public String getDepositosCorteOcho() {
        if (depositosCorteOcho == null) {
            depositosCorteOcho = "";
        }
        return depositosCorteOcho;
    }

    public String getDepositosCorteNueve() {
        if (depositosCorteNueve == null) {
            depositosCorteNueve = "";
        }
        return depositosCorteNueve;
    }

    public String getDepositosCorteDiez() {
        if (depositosCorteDiez == null) {
            depositosCorteDiez = "";
        }
        return depositosCorteDiez;
    }

    public String getDepositosCorteOnce() {
        if (depositosCorteOnce == null) {
            depositosCorteOnce = "";
        }
        return depositosCorteOnce;
    }

    public String getDepositosCorteDoce() {
        if (depositosCorteDoce == null) {
            depositosCorteDoce = "";
        }
        return depositosCorteDoce;
    }

    public String getDepositosCorteTrece() {
        if (depositosCorteTrece == null) {
            depositosCorteTrece = "";
        }
        return depositosCorteTrece;
    }

    public String getDepositosAhorroCorteUno() {
        if (depositosAhorroCorteUno == null) {
            depositosAhorroCorteUno = "";
        }
        return depositosAhorroCorteUno;
    }

    public String getDepositosAhorroCorteDos() {
        if (depositosAhorroCorteDos == null) {
            depositosAhorroCorteDos = "";
        }
        return depositosAhorroCorteDos;
    }

    public String getDepositosAhorroCorteTres() {
        if (depositosAhorroCorteTres == null) {
            depositosAhorroCorteTres = "";
        }
        return depositosAhorroCorteTres;
    }

    public String getDepositosAhorroCorteCuatro() {
        if (depositosAhorroCorteCuatro == null) {
            depositosAhorroCorteCuatro = "";
        }
        return depositosAhorroCorteCuatro;
    }

    public String getDepositosAhorroCorteCinco() {
        if (depositosAhorroCorteCinco == null) {
            depositosAhorroCorteCinco = "";
        }
        return depositosAhorroCorteCinco;
    }

    public String getDepositosAhorroCorteSeis() {
        if (depositosAhorroCorteSeis == null) {
            depositosAhorroCorteSeis = "";
        }
        return depositosAhorroCorteSeis;
    }

    public String getDepositosAhorroCorteSiete() {
        if (depositosAhorroCorteSiete == null) {
            depositosAhorroCorteSiete = "";
        }
        return depositosAhorroCorteSiete;
    }

    public String getDepositosAhorroCorteOcho() {
        if (depositosAhorroCorteOcho == null) {
            depositosAhorroCorteOcho = "";
        }
        return depositosAhorroCorteOcho;
    }

    public String getDepositosAhorroCorteNueve() {
        if (depositosAhorroCorteNueve == null) {
            depositosAhorroCorteNueve = "";
        }
        return depositosAhorroCorteNueve;
    }

    public String getDepositosAhorroCorteDiez() {
        if (depositosAhorroCorteDiez == null) {
            depositosAhorroCorteDiez = "";
        }
        return depositosAhorroCorteDiez;
    }

    public String getDepositosAhorroCorteOnce() {
        if (depositosAhorroCorteOnce == null) {
            depositosAhorroCorteOnce = "";
        }
        return depositosAhorroCorteOnce;
    }

    public String getDepositosAhorroCorteDoce() {
        if (depositosAhorroCorteDoce == null) {
            depositosAhorroCorteDoce = "";
        }
        return depositosAhorroCorteDoce;
    }

    public String getDepositosAhorroCorteTrece() {
        if (depositosAhorroCorteTrece == null) {
            depositosAhorroCorteTrece = "";
        }
        return depositosAhorroCorteTrece;
    }

    public String getCdatCorteUno() {
        if (cdatCorteUno == null) {
            cdatCorteUno = "";
        }
        return cdatCorteUno;
    }

    public String getCdatCorteDos() {
        if (cdatCorteDos == null) {
            cdatCorteDos = "";
        }
        return cdatCorteDos;
    }

    public String getCdatCorteTres() {
        if (cdatCorteTres == null) {
            cdatCorteTres = "";
        }
        return cdatCorteTres;
    }

    public String getCdatCorteCuatro() {
        if (cdatCorteCuatro == null) {
            cdatCorteCuatro = "";
        }
        return cdatCorteCuatro;
    }

    public String getCdatCorteCinco() {
        if (cdatCorteCinco == null) {
            cdatCorteCinco = "";
        }
        return cdatCorteCinco;
    }

    public String getCdatCorteSeis() {
        if (cdatCorteSeis == null) {
            cdatCorteSeis = "";
        }
        return cdatCorteSeis;
    }

    public String getCdatCorteSiete() {
        if (cdatCorteSiete == null) {
            cdatCorteSiete = "";
        }
        return cdatCorteSiete;
    }

    public String getCdatCorteOcho() {
        if (cdatCorteOcho == null) {
            cdatCorteOcho = "";
        }
        return cdatCorteOcho;
    }

    public String getCdatCorteNueve() {
        if (cdatCorteNueve == null) {
            cdatCorteNueve = "";
        }
        return cdatCorteNueve;
    }

    public String getCdatCorteDiez() {
        if (cdatCorteDiez == null) {
            cdatCorteDiez = "";
        }
        return cdatCorteDiez;
    }

    public String getCdatCorteOnce() {
        if (cdatCorteOnce == null) {
            cdatCorteOnce = "";
        }
        return cdatCorteOnce;
    }

    public String getCdatCorteDoce() {
        if (cdatCorteDoce == null) {
            cdatCorteDoce = "";
        }
        return cdatCorteDoce;
    }

    public String getCdatCorteTrece() {
        if (cdatCorteTrece == null) {
            cdatCorteTrece = "";
        }
        return cdatCorteTrece;
    }

    public String getAhorroContractualCorteUno() {
        if (ahorroContractualCorteUno == null) {
            ahorroContractualCorteUno = "";
        }
        return ahorroContractualCorteUno;
    }

    public String getAhorroContractualCorteDos() {
        if (ahorroContractualCorteDos == null) {
            ahorroContractualCorteDos = "";
        }
        return ahorroContractualCorteDos;
    }

    public String getAhorroContractualCorteTres() {
        if (ahorroContractualCorteTres == null) {
            ahorroContractualCorteTres = "";
        }
        return ahorroContractualCorteTres;
    }

    public String getAhorroContractualCorteCuatro() {
        if (ahorroContractualCorteCuatro == null) {
            ahorroContractualCorteCuatro = "";
        }
        return ahorroContractualCorteCuatro;
    }

    public String getAhorroContractualCorteCinco() {
        if (ahorroContractualCorteCinco == null) {
            ahorroContractualCorteCinco = "";
        }
        return ahorroContractualCorteCinco;
    }

    public String getAhorroContractualCorteSeis() {
        if (ahorroContractualCorteSeis == null) {
            ahorroContractualCorteSeis = "";
        }
        return ahorroContractualCorteSeis;
    }

    public String getAhorroContractualCorteSiete() {
        if (ahorroContractualCorteSiete == null) {
            ahorroContractualCorteSiete = "";
        }
        return ahorroContractualCorteSiete;
    }

    public String getAhorroContractualCorteOcho() {
        if (ahorroContractualCorteOcho == null) {
            ahorroContractualCorteOcho = "";
        }
        return ahorroContractualCorteOcho;
    }

    public String getAhorroContractualCorteNueve() {
        if (ahorroContractualCorteNueve == null) {
            ahorroContractualCorteNueve = "";
        }
        return ahorroContractualCorteNueve;
    }

    public String getAhorroContractualCorteDiez() {
        if (ahorroContractualCorteDiez == null) {
            ahorroContractualCorteDiez = "";
        }
        return ahorroContractualCorteDiez;
    }

    public String getAhorroContractualCorteOnce() {
        if (ahorroContractualCorteOnce == null) {
            ahorroContractualCorteOnce = "";
        }
        return ahorroContractualCorteOnce;
    }

    public String getAhorroContractualCorteDoce() {
        if (ahorroContractualCorteDoce == null) {
            ahorroContractualCorteDoce = "";
        }
        return ahorroContractualCorteDoce;
    }

    public String getAhorroContractualCorteTrece() {
        if (ahorroContractualCorteTrece == null) {
            ahorroContractualCorteTrece = "";
        }
        return ahorroContractualCorteTrece;
    }

    public String getAhorroPermanenteCorteUno() {
        if (ahorroPermanenteCorteUno == null) {
            ahorroPermanenteCorteUno = "";
        }
        return ahorroPermanenteCorteUno;
    }

    public String getAhorroPermanenteCorteDos() {
        if (ahorroPermanenteCorteDos == null) {
            ahorroPermanenteCorteDos = "";
        }
        return ahorroPermanenteCorteDos;
    }

    public String getAhorroPermanenteCorteTres() {
        if (ahorroPermanenteCorteTres == null) {
            ahorroPermanenteCorteTres = "";
        }
        return ahorroPermanenteCorteTres;
    }

    public String getAhorroPermanenteCorteCuatro() {
        if (ahorroPermanenteCorteCuatro == null) {
            ahorroPermanenteCorteCuatro = "";
        }
        return ahorroPermanenteCorteCuatro;
    }

    public String getAhorroPermanenteCorteCinco() {
        if (ahorroPermanenteCorteCinco == null) {
            ahorroPermanenteCorteCinco = "";
        }
        return ahorroPermanenteCorteCinco;
    }

    public String getAhorroPermanenteCorteSeis() {
        if (ahorroPermanenteCorteSeis == null) {
            ahorroPermanenteCorteSeis = "";
        }
        return ahorroPermanenteCorteSeis;
    }

    public String getAhorroPermanenteCorteSiete() {
        if (ahorroPermanenteCorteSiete == null) {
            ahorroPermanenteCorteSiete = "";
        }
        return ahorroPermanenteCorteSiete;
    }

    public String getAhorroPermanenteCorteOcho() {
        if (ahorroPermanenteCorteOcho == null) {
            ahorroPermanenteCorteOcho = "";
        }
        return ahorroPermanenteCorteOcho;
    }

    public String getAhorroPermanenteCorteNueve() {
        if (ahorroPermanenteCorteNueve == null) {
            ahorroPermanenteCorteNueve = "";
        }
        return ahorroPermanenteCorteNueve;
    }

    public String getAhorroPermanenteCorteDiez() {
        if (ahorroPermanenteCorteDiez == null) {
            ahorroPermanenteCorteDiez = "";
        }
        return ahorroPermanenteCorteDiez;
    }

    public String getAhorroPermanenteCorteOnce() {
        if (ahorroPermanenteCorteOnce == null) {
            ahorroPermanenteCorteOnce = "";
        }
        return ahorroPermanenteCorteOnce;
    }

    public String getAhorroPermanenteCorteDoce() {
        if (ahorroPermanenteCorteDoce == null) {
            ahorroPermanenteCorteDoce = "";
        }
        return ahorroPermanenteCorteDoce;
    }

    public String getAhorroPermanenteCorteTrece() {
        if (ahorroPermanenteCorteTrece == null) {
            ahorroPermanenteCorteTrece = "";
        }
        return ahorroPermanenteCorteTrece;
    }

    public String getTituloInversionCorteUno() {
        if (tituloInversionCorteUno == null) {
            tituloInversionCorteUno = "";
        }
        return tituloInversionCorteUno;
    }

    public String getTituloInversionCorteDos() {
        if (tituloInversionCorteDos == null) {
            tituloInversionCorteDos = "";
        }
        return tituloInversionCorteDos;
    }

    public String getTituloInversionCorteTres() {
        if (tituloInversionCorteTres == null) {
            tituloInversionCorteTres = "";
        }
        return tituloInversionCorteTres;
    }

    public String getTituloInversionCorteCuatro() {
        if (tituloInversionCorteCuatro == null) {
            tituloInversionCorteCuatro = "";
        }
        return tituloInversionCorteCuatro;
    }

    public String getTituloInversionCorteCinco() {
        if (tituloInversionCorteCinco == null) {
            tituloInversionCorteCinco = "";
        }
        return tituloInversionCorteCinco;
    }

    public String getTituloInversionCorteSeis() {
        if (tituloInversionCorteSeis == null) {
            tituloInversionCorteSeis = "";
        }
        return tituloInversionCorteSeis;
    }

    public String getTituloInversionCorteSiete() {
        if (tituloInversionCorteSiete == null) {
            tituloInversionCorteSiete = "";
        }
        return tituloInversionCorteSiete;
    }

    public String getTituloInversionCorteOcho() {
        if (tituloInversionCorteOcho == null) {
            tituloInversionCorteOcho = "";
        }
        return tituloInversionCorteOcho;
    }

    public String getTituloInversionCorteNueve() {
        if (tituloInversionCorteNueve == null) {
            tituloInversionCorteNueve = "";
        }
        return tituloInversionCorteNueve;
    }

    public String getTituloInversionCorteDiez() {
        if (tituloInversionCorteDiez == null) {
            tituloInversionCorteDiez = "";
        }
        return tituloInversionCorteDiez;
    }

    public String getTituloInversionCorteOnce() {
        if (tituloInversionCorteOnce == null) {
            tituloInversionCorteOnce = "";
        }
        return tituloInversionCorteOnce;
    }

    public String getTituloInversionCorteDoce() {
        if (tituloInversionCorteDoce == null) {
            tituloInversionCorteDoce = "";
        }
        return tituloInversionCorteDoce;
    }

    public String getTituloInversionCorteTrece() {
        if (tituloInversionCorteTrece == null) {
            tituloInversionCorteTrece = "";
        }
        return tituloInversionCorteTrece;
    }

    public String getCreditosBancosCorteUno() {
        if (creditosBancosCorteUno == null) {
            creditosBancosCorteUno = "";
        }
        return creditosBancosCorteUno;
    }

    public String getCreditosBancosCorteDos() {
        if (creditosBancosCorteDos == null) {
            creditosBancosCorteDos = "";
        }
        return creditosBancosCorteDos;
    }

    public String getCreditosBancosCorteTres() {
        if (creditosBancosCorteTres == null) {
            creditosBancosCorteTres = "";
        }
        return creditosBancosCorteTres;
    }

    public String getCreditosBancosCorteCuatro() {
        if (creditosBancosCorteCuatro == null) {
            creditosBancosCorteCuatro = "";
        }
        return creditosBancosCorteCuatro;
    }

    public String getCreditosBancosCorteCinco() {
        if (creditosBancosCorteCinco == null) {
            creditosBancosCorteCinco = "";
        }
        return creditosBancosCorteCinco;
    }

    public String getCreditosBancosCorteSeis() {
        if (creditosBancosCorteSeis == null) {
            creditosBancosCorteSeis = "";
        }
        return creditosBancosCorteSeis;
    }

    public String getCreditosBancosCorteSiete() {
        if (creditosBancosCorteSiete == null) {
            creditosBancosCorteSiete = "";
        }
        return creditosBancosCorteSiete;
    }

    public String getCreditosBancosCorteOcho() {
        if (creditosBancosCorteOcho == null) {
            creditosBancosCorteOcho = "";
        }
        return creditosBancosCorteOcho;
    }

    public String getCreditosBancosCorteNueve() {
        if (creditosBancosCorteNueve == null) {
            creditosBancosCorteNueve = "";
        }
        return creditosBancosCorteNueve;
    }

    public String getCreditosBancosCorteDiez() {
        if (creditosBancosCorteDiez == null) {
            creditosBancosCorteDiez = "";
        }
        return creditosBancosCorteDiez;
    }

    public String getCreditosBancosCorteOnce() {
        if (creditosBancosCorteOnce == null) {
            creditosBancosCorteOnce = "";
        }
        return creditosBancosCorteOnce;
    }

    public String getCreditosBancosCorteDoce() {
        if (creditosBancosCorteDoce == null) {
            creditosBancosCorteDoce = "";
        }
        return creditosBancosCorteDoce;
    }

    public String getCreditosBancosCorteTrece() {
        if (creditosBancosCorteTrece == null) {
            creditosBancosCorteTrece = "";
        }
        return creditosBancosCorteTrece;
    }

    public String getCuentasPorPagarCorteUno() {
        if (cuentasPorPagarCorteUno == null) {
            cuentasPorPagarCorteUno = "";
        }
        return cuentasPorPagarCorteUno;
    }

    public String getCuentasPorPagarCorteDos() {
        if (cuentasPorPagarCorteDos == null) {
            cuentasPorPagarCorteDos = "";
        }
        return cuentasPorPagarCorteDos;
    }

    public String getCuentasPorPagarCorteTres() {
        if (cuentasPorPagarCorteTres == null) {
            cuentasPorPagarCorteTres = "";
        }
        return cuentasPorPagarCorteTres;
    }

    public String getCuentasPorPagarCorteCuatro() {
        if (cuentasPorPagarCorteCuatro == null) {
            cuentasPorPagarCorteCuatro = "";
        }
        return cuentasPorPagarCorteCuatro;
    }

    public String getCuentasPorPagarCorteCinco() {
        if (cuentasPorPagarCorteCinco == null) {
            cuentasPorPagarCorteCinco = "";
        }
        return cuentasPorPagarCorteCinco;
    }

    public String getCuentasPorPagarCorteSeis() {
        if (cuentasPorPagarCorteSeis == null) {
            cuentasPorPagarCorteSeis = "";
        }
        return cuentasPorPagarCorteSeis;
    }

    public String getCuentasPorPagarCorteSiete() {
        if (cuentasPorPagarCorteSiete == null) {
            cuentasPorPagarCorteSiete = "";
        }
        return cuentasPorPagarCorteSiete;
    }

    public String getCuentasPorPagarCorteOcho() {
        if (cuentasPorPagarCorteOcho == null) {
            cuentasPorPagarCorteOcho = "";
        }
        return cuentasPorPagarCorteOcho;
    }

    public String getCuentasPorPagarCorteNueve() {
        if (cuentasPorPagarCorteNueve == null) {
            cuentasPorPagarCorteNueve = "";
        }
        return cuentasPorPagarCorteNueve;
    }

    public String getCuentasPorPagarCorteDiez() {
        if (cuentasPorPagarCorteDiez == null) {
            cuentasPorPagarCorteDiez = "";
        }
        return cuentasPorPagarCorteDiez;
    }

    public String getCuentasPorPagarCorteOnce() {
        if (cuentasPorPagarCorteOnce == null) {
            cuentasPorPagarCorteOnce = "";
        }
        return cuentasPorPagarCorteOnce;
    }

    public String getCuentasPorPagarCorteDoce() {
        if (cuentasPorPagarCorteDoce == null) {
            cuentasPorPagarCorteDoce = "";
        }
        return cuentasPorPagarCorteDoce;
    }

    public String getCuentasPorPagarCorteTrece() {
        if (cuentasPorPagarCorteTrece == null) {
            cuentasPorPagarCorteTrece = "";
        }
        return cuentasPorPagarCorteTrece;
    }

    public String getFondosSocialesCorteUno() {
        if (fondosSocialesCorteUno == null) {
            fondosSocialesCorteUno = "";
        }
        return fondosSocialesCorteUno;
    }

    public String getFondosSocialesCorteDos() {
        if (fondosSocialesCorteDos == null) {
            fondosSocialesCorteDos = "";
        }
        return fondosSocialesCorteDos;
    }

    public String getFondosSocialesCorteTres() {
        if (fondosSocialesCorteTres == null) {
            fondosSocialesCorteTres = "";
        }
        return fondosSocialesCorteTres;
    }

    public String getFondosSocialesCorteCuatro() {
        if (fondosSocialesCorteCuatro == null) {
            fondosSocialesCorteCuatro = "";
        }
        return fondosSocialesCorteCuatro;
    }

    public String getFondosSocialesCorteCinco() {
        if (fondosSocialesCorteCinco == null) {
            fondosSocialesCorteCinco = "";
        }
        return fondosSocialesCorteCinco;
    }

    public String getFondosSocialesCorteSeis() {
        if (fondosSocialesCorteSeis == null) {
            fondosSocialesCorteSeis = "";
        }
        return fondosSocialesCorteSeis;
    }

    public String getFondosSocialesCorteSiete() {
        if (fondosSocialesCorteSiete == null) {
            fondosSocialesCorteSiete = "";
        }
        return fondosSocialesCorteSiete;
    }

    public String getFondosSocialesCorteOcho() {
        if (fondosSocialesCorteOcho == null) {
            fondosSocialesCorteOcho = "";
        }
        return fondosSocialesCorteOcho;
    }

    public String getFondosSocialesCorteNueve() {
        if (fondosSocialesCorteNueve == null) {
            fondosSocialesCorteNueve = "";
        }
        return fondosSocialesCorteNueve;
    }

    public String getFondosSocialesCorteDiez() {
        if (fondosSocialesCorteDiez == null) {
            fondosSocialesCorteDiez = "";
        }
        return fondosSocialesCorteDiez;
    }

    public String getFondosSocialesCorteOnce() {
        if (fondosSocialesCorteOnce == null) {
            fondosSocialesCorteOnce = "";
        }
        return fondosSocialesCorteOnce;
    }

    public String getFondosSocialesCorteDoce() {
        if (fondosSocialesCorteDoce == null) {
            fondosSocialesCorteDoce = "";
        }
        return fondosSocialesCorteDoce;
    }

    public String getFondosSocialesCorteTrece() {
        if (fondosSocialesCorteTrece == null) {
            fondosSocialesCorteTrece = "";
        }
        return fondosSocialesCorteTrece;
    }

    public String getProvisionesCorteUno() {
        if (provisionesCorteUno == null) {
            provisionesCorteUno = "";
        }
        return provisionesCorteUno;
    }

    public String getProvisionesCorteDos() {
        if (provisionesCorteDos == null) {
            provisionesCorteDos = "";
        }
        return provisionesCorteDos;
    }

    public String getProvisionesCorteTres() {
        if (provisionesCorteTres == null) {
            provisionesCorteTres = "";
        }
        return provisionesCorteTres;
    }

    public String getProvisionesCorteCuatro() {
        if (provisionesCorteCuatro == null) {
            provisionesCorteCuatro = "";
        }
        return provisionesCorteCuatro;
    }

    public String getProvisionesCorteCinco() {
        if (provisionesCorteCinco == null) {
            provisionesCorteCinco = "";
        }
        return provisionesCorteCinco;
    }

    public String getProvisionesCorteSeis() {
        if (provisionesCorteSeis == null) {
            provisionesCorteSeis = "";
        }
        return provisionesCorteSeis;
    }

    public String getProvisionesCorteSiete() {
        if (provisionesCorteSiete == null) {
            provisionesCorteSiete = "";
        }
        return provisionesCorteSiete;
    }

    public String getProvisionesCorteOcho() {
        if (provisionesCorteOcho == null) {
            provisionesCorteOcho = "";
        }
        return provisionesCorteOcho;
    }

    public String getProvisionesCorteNueve() {
        if (provisionesCorteNueve == null) {
            provisionesCorteNueve = "";
        }
        return provisionesCorteNueve;
    }

    public String getProvisionesCorteDiez() {
        if (provisionesCorteDiez == null) {
            provisionesCorteDiez = "";
        }
        return provisionesCorteDiez;
    }

    public String getProvisionesCorteOnce() {
        if (provisionesCorteOnce == null) {
            provisionesCorteOnce = "";
        }
        return provisionesCorteOnce;
    }

    public String getProvisionesCorteDoce() {
        if (provisionesCorteDoce == null) {
            provisionesCorteDoce = "";
        }
        return provisionesCorteDoce;
    }

    public String getProvisionesCorteTrece() {
        if (provisionesCorteTrece == null) {
            provisionesCorteTrece = "";
        }
        return provisionesCorteTrece;
    }

    public String getAportesSocialesCorteUno() {
        if (aportesSocialesCorteUno == null) {
            aportesSocialesCorteUno = "";
        }
        return aportesSocialesCorteUno;
    }

    public String getAportesSocialesCorteDos() {
        if (aportesSocialesCorteDos == null) {
            aportesSocialesCorteDos = "";
        }
        return aportesSocialesCorteDos;
    }

    public String getAportesSocialesCorteTres() {
        if (aportesSocialesCorteTres == null) {
            aportesSocialesCorteTres = "";
        }
        return aportesSocialesCorteTres;
    }

    public String getAportesSocialesCorteCuatro() {
        if (aportesSocialesCorteCuatro == null) {
            aportesSocialesCorteCuatro = "";
        }
        return aportesSocialesCorteCuatro;
    }

    public String getAportesSocialesCorteCinco() {
        if (aportesSocialesCorteCinco == null) {
            aportesSocialesCorteCinco = "";
        }
        return aportesSocialesCorteCinco;
    }

    public String getAportesSocialesCorteSeis() {
        if (aportesSocialesCorteSeis == null) {
            aportesSocialesCorteSeis = "";
        }
        return aportesSocialesCorteSeis;
    }

    public String getAportesSocialesCorteSiete() {
        if (aportesSocialesCorteSiete == null) {
            aportesSocialesCorteSiete = "";
        }
        return aportesSocialesCorteSiete;
    }

    public String getAportesSocialesCorteOcho() {
        if (aportesSocialesCorteOcho == null) {
            aportesSocialesCorteOcho = "";
        }
        return aportesSocialesCorteOcho;
    }

    public String getAportesSocialesCorteNueve() {
        if (aportesSocialesCorteNueve == null) {
            aportesSocialesCorteNueve = "";
        }
        return aportesSocialesCorteNueve;
    }

    public String getAportesSocialesCorteDiez() {
        if (aportesSocialesCorteDiez == null) {
            aportesSocialesCorteDiez = "";
        }
        return aportesSocialesCorteDiez;
    }

    public String getAportesSocialesCorteOnce() {
        if (aportesSocialesCorteOnce == null) {
            aportesSocialesCorteOnce = "";
        }
        return aportesSocialesCorteOnce;
    }

    public String getAportesSocialesCorteDoce() {
        if (aportesSocialesCorteDoce == null) {
            aportesSocialesCorteDoce = "";
        }
        return aportesSocialesCorteDoce;
    }

    public String getAportesSocialesCorteTrece() {
        if (aportesSocialesCorteTrece == null) {
            aportesSocialesCorteTrece = "";
        }
        return aportesSocialesCorteTrece;
    }

    public String getTotalPasivosCorteUno() {
        if (totalPasivosCorteUno == null) {
            totalPasivosCorteUno = "";
        }
        return totalPasivosCorteUno;
    }

    public String getTotalPasivosCorteDos() {
        if (totalPasivosCorteDos == null) {
            totalPasivosCorteDos = "";
        }
        return totalPasivosCorteDos;
    }

    public String getTotalPasivosCorteTres() {
        if (totalPasivosCorteTres == null) {
            totalPasivosCorteTres = "";
        }
        return totalPasivosCorteTres;
    }

    public String getTotalPasivosCorteCuatro() {
        if (totalPasivosCorteCuatro == null) {
            totalPasivosCorteCuatro = "";
        }
        return totalPasivosCorteCuatro;
    }

    public String getTotalPasivosCorteCinco() {
        if (totalPasivosCorteCinco == null) {
            totalPasivosCorteCinco = "";
        }
        return totalPasivosCorteCinco;
    }

    public String getTotalPasivosCorteSeis() {
        if (totalPasivosCorteSeis == null) {
            totalPasivosCorteSeis = "";
        }
        return totalPasivosCorteSeis;
    }

    public String getTotalPasivosCorteSiete() {
        if (totalPasivosCorteSiete == null) {
            totalPasivosCorteSiete = "";
        }
        return totalPasivosCorteSiete;
    }

    public String getTotalPasivosCorteOcho() {
        if (totalPasivosCorteOcho == null) {
            totalPasivosCorteOcho = "";
        }
        return totalPasivosCorteOcho;
    }

    public String getTotalPasivosCorteNueve() {
        if (totalPasivosCorteNueve == null) {
            totalPasivosCorteNueve = "";
        }
        return totalPasivosCorteNueve;
    }

    public String getTotalPasivosCorteDiez() {
        if (totalPasivosCorteDiez == null) {
            totalPasivosCorteDiez = "";
        }
        return totalPasivosCorteDiez;
    }

    public String getTotalPasivosCorteOnce() {
        if (totalPasivosCorteOnce == null) {
            totalPasivosCorteOnce = "";
        }
        return totalPasivosCorteOnce;
    }

    public String getTotalPasivosCorteDoce() {
        if (totalPasivosCorteDoce == null) {
            totalPasivosCorteDoce = "";
        }
        return totalPasivosCorteDoce;
    }

    public String getTotalPasivosCorteTrece() {
        if (totalPasivosCorteTrece == null) {
            totalPasivosCorteTrece = "";
        }
        return totalPasivosCorteTrece;
    }

    public String getPatrimonioCorteUno() {
        if (patrimonioCorteUno == null) {
            patrimonioCorteUno = "";
        }
        return patrimonioCorteUno;
    }

    public String getPatrimonioCorteDos() {
        if (patrimonioCorteDos == null) {
            patrimonioCorteDos = "";
        }
        return patrimonioCorteDos;
    }

    public String getPatrimonioCorteTres() {
        if (patrimonioCorteTres == null) {
            patrimonioCorteTres = "";
        }
        return patrimonioCorteTres;
    }

    public String getPatrimonioCorteCuatro() {
        if (patrimonioCorteCuatro == null) {
            patrimonioCorteCuatro = "";
        }
        return patrimonioCorteCuatro;
    }

    public String getPatrimonioCorteCinco() {
        if (patrimonioCorteCinco == null) {
            patrimonioCorteCinco = "";
        }
        return patrimonioCorteCinco;
    }

    public String getPatrimonioCorteSeis() {
        if (patrimonioCorteSeis == null) {
            patrimonioCorteSeis = "";
        }
        return patrimonioCorteSeis;
    }

    public String getPatrimonioCorteSiete() {
        if (patrimonioCorteSiete == null) {
            patrimonioCorteSiete = "";
        }
        return patrimonioCorteSiete;
    }

    public String getPatrimonioCorteOcho() {
        if (patrimonioCorteOcho == null) {
            patrimonioCorteOcho = "";
        }
        return patrimonioCorteOcho;
    }

    public String getPatrimonioCorteNueve() {
        if (patrimonioCorteNueve == null) {
            patrimonioCorteNueve = "";
        }
        return patrimonioCorteNueve;
    }

    public String getPatrimonioCorteDiez() {
        if (patrimonioCorteDiez == null) {
            patrimonioCorteDiez = "";
        }
        return patrimonioCorteDiez;
    }

    public String getPatrimonioCorteOnce() {
        if (patrimonioCorteOnce == null) {
            patrimonioCorteOnce = "";
        }
        return patrimonioCorteOnce;
    }

    public String getPatrimonioCorteDoce() {
        if (patrimonioCorteDoce == null) {
            patrimonioCorteDoce = "";
        }
        return patrimonioCorteDoce;
    }

    public String getPatrimonioCorteTrece() {
        if (patrimonioCorteTrece == null) {
            patrimonioCorteTrece = "";
        }
        return patrimonioCorteTrece;
    }

    public String getCapitalSocialCorteUno() {
        if (capitalSocialCorteUno == null) {
            capitalSocialCorteUno = "";
        }
        return capitalSocialCorteUno;
    }

    public String getCapitalSocialCorteDos() {
        if (capitalSocialCorteDos == null) {
            capitalSocialCorteDos = "";
        }
        return capitalSocialCorteDos;
    }

    public String getCapitalSocialCorteTres() {
        if (capitalSocialCorteTres == null) {
            capitalSocialCorteTres = "";
        }
        return capitalSocialCorteTres;
    }

    public String getCapitalSocialCorteCuatro() {
        if (capitalSocialCorteCuatro == null) {
            capitalSocialCorteCuatro = "";
        }
        return capitalSocialCorteCuatro;
    }

    public String getCapitalSocialCorteCinco() {
        if (capitalSocialCorteCinco == null) {
            capitalSocialCorteCinco = "";
        }
        return capitalSocialCorteCinco;
    }

    public String getCapitalSocialCorteSeis() {
        if (capitalSocialCorteSeis == null) {
            capitalSocialCorteSeis = "";
        }
        return capitalSocialCorteSeis;
    }

    public String getCapitalSocialCorteSiete() {
        if (capitalSocialCorteSiete == null) {
            capitalSocialCorteSiete = "";
        }
        return capitalSocialCorteSiete;
    }

    public String getCapitalSocialCorteOcho() {
        if (capitalSocialCorteOcho == null) {
            capitalSocialCorteOcho = "";
        }
        return capitalSocialCorteOcho;
    }

    public String getCapitalSocialCorteNueve() {
        if (capitalSocialCorteNueve == null) {
            capitalSocialCorteNueve = "";
        }
        return capitalSocialCorteNueve;
    }

    public String getCapitalSocialCorteDiez() {
        if (capitalSocialCorteDiez == null) {
            capitalSocialCorteDiez = "";
        }
        return capitalSocialCorteDiez;
    }

    public String getCapitalSocialCorteOnce() {
        if (capitalSocialCorteOnce == null) {
            capitalSocialCorteOnce = "";
        }
        return capitalSocialCorteOnce;
    }

    public String getCapitalSocialCorteDoce() {
        if (capitalSocialCorteDoce == null) {
            capitalSocialCorteDoce = "";
        }
        return capitalSocialCorteDoce;
    }

    public String getCapitalSocialCorteTrece() {
        if (capitalSocialCorteTrece == null) {
            capitalSocialCorteTrece = "";
        }
        return capitalSocialCorteTrece;
    }

    public String getReservasCorteUno() {
        if (reservasCorteUno == null) {
            reservasCorteUno = "";
        }
        return reservasCorteUno;
    }

    public String getReservasCorteDos() {
        if (reservasCorteDos == null) {
            reservasCorteDos = "";
        }
        return reservasCorteDos;
    }

    public String getReservasCorteTres() {
        if (reservasCorteTres == null) {
            reservasCorteTres = "";
        }
        return reservasCorteTres;
    }

    public String getReservasCorteCuatro() {
        if (reservasCorteCuatro == null) {
            reservasCorteCuatro = "";
        }
        return reservasCorteCuatro;
    }

    public String getReservasCorteCinco() {
        if (reservasCorteCinco == null) {
            reservasCorteCinco = "";
        }
        return reservasCorteCinco;
    }

    public String getReservasCorteSeis() {
        if (reservasCorteSeis == null) {
            reservasCorteSeis = "";
        }
        return reservasCorteSeis;
    }

    public String getReservasCorteSiete() {
        if (reservasCorteSiete == null) {
            reservasCorteSiete = "";
        }
        return reservasCorteSiete;
    }

    public String getReservasCorteOcho() {
        if (reservasCorteOcho == null) {
            reservasCorteOcho = "";
        }
        return reservasCorteOcho;
    }

    public String getReservasCorteNueve() {
        if (reservasCorteNueve == null) {
            reservasCorteNueve = "";
        }
        return reservasCorteNueve;
    }

    public String getReservasCorteDiez() {
        if (reservasCorteDiez == null) {
            reservasCorteDiez = "";
        }
        return reservasCorteDiez;
    }

    public String getReservasCorteOnce() {
        if (reservasCorteOnce == null) {
            reservasCorteOnce = "";
        }
        return reservasCorteOnce;
    }

    public String getReservasCorteDoce() {
        if (reservasCorteDoce == null) {
            reservasCorteDoce = "";
        }
        return reservasCorteDoce;
    }

    public String getReservasCorteTrece() {
        if (reservasCorteTrece == null) {
            reservasCorteTrece = "";
        }
        return reservasCorteTrece;
    }

    public String getFondosDestinacionCorteUno() {
        if (fondosDestinacionCorteUno == null) {
            fondosDestinacionCorteUno = "";
        }
        return fondosDestinacionCorteUno;
    }

    public String getFondosDestinacionCorteDos() {
        if (fondosDestinacionCorteDos == null) {
            fondosDestinacionCorteDos = "";
        }
        return fondosDestinacionCorteDos;
    }

    public String getFondosDestinacionCorteTres() {
        if (fondosDestinacionCorteTres == null) {
            fondosDestinacionCorteTres = "";
        }
        return fondosDestinacionCorteTres;
    }

    public String getFondosDestinacionCorteCuatro() {
        if (fondosDestinacionCorteCuatro == null) {
            fondosDestinacionCorteCuatro = "";
        }
        return fondosDestinacionCorteCuatro;
    }

    public String getFondosDestinacionCorteCinco() {
        if (fondosDestinacionCorteCinco == null) {
            fondosDestinacionCorteCinco = "";
        }
        return fondosDestinacionCorteCinco;
    }

    public String getFondosDestinacionCorteSeis() {
        if (fondosDestinacionCorteSeis == null) {
            fondosDestinacionCorteSeis = "";
        }
        return fondosDestinacionCorteSeis;
    }

    public String getFondosDestinacionCorteSiete() {
        if (fondosDestinacionCorteSiete == null) {
            fondosDestinacionCorteSiete = "";
        }
        return fondosDestinacionCorteSiete;
    }

    public String getFondosDestinacionCorteOcho() {
        if (fondosDestinacionCorteOcho == null) {
            fondosDestinacionCorteOcho = "";
        }
        return fondosDestinacionCorteOcho;
    }

    public String getFondosDestinacionCorteNueve() {
        if (fondosDestinacionCorteNueve == null) {
            fondosDestinacionCorteNueve = "";
        }
        return fondosDestinacionCorteNueve;
    }

    public String getFondosDestinacionCorteDiez() {
        if (fondosDestinacionCorteDiez == null) {
            fondosDestinacionCorteDiez = "";
        }
        return fondosDestinacionCorteDiez;
    }

    public String getFondosDestinacionCorteOnce() {
        if (fondosDestinacionCorteOnce == null) {
            fondosDestinacionCorteOnce = "";
        }
        return fondosDestinacionCorteOnce;
    }

    public String getFondosDestinacionCorteDoce() {
        if (fondosDestinacionCorteDoce == null) {
            fondosDestinacionCorteDoce = "";
        }
        return fondosDestinacionCorteDoce;
    }

    public String getFondosDestinacionCorteTrece() {
        if (fondosDestinacionCorteTrece == null) {
            fondosDestinacionCorteTrece = "";
        }
        return fondosDestinacionCorteTrece;
    }

    public String getSuperavitCorteUno() {
        if (superavitCorteUno == null) {
            superavitCorteUno = "";
        }
        return superavitCorteUno;
    }

    public String getSuperavitCorteDos() {
        if (superavitCorteDos == null) {
            superavitCorteDos = "";
        }
        return superavitCorteDos;
    }

    public String getSuperavitCorteTres() {
        if (superavitCorteTres == null) {
            superavitCorteTres = "";
        }
        return superavitCorteTres;
    }

    public String getSuperavitCorteCuatro() {
        if (superavitCorteCuatro == null) {
            superavitCorteCuatro = "";
        }
        return superavitCorteCuatro;
    }

    public String getSuperavitCorteCinco() {
        if (superavitCorteCinco == null) {
            superavitCorteCinco = "";
        }
        return superavitCorteCinco;
    }

    public String getSuperavitCorteSeis() {
        if (superavitCorteSeis == null) {
            superavitCorteSeis = "";
        }
        return superavitCorteSeis;
    }

    public String getSuperavitCorteSiete() {
        if (superavitCorteSiete == null) {
            superavitCorteSiete = "";
        }
        return superavitCorteSiete;
    }

    public String getSuperavitCorteOcho() {
        if (superavitCorteOcho == null) {
            superavitCorteOcho = "";
        }
        return superavitCorteOcho;
    }

    public String getSuperavitCorteNueve() {
        if (superavitCorteNueve == null) {
            superavitCorteNueve = "";
        }
        return superavitCorteNueve;
    }

    public String getSuperavitCorteDiez() {
        if (superavitCorteDiez == null) {
            superavitCorteDiez = "";
        }
        return superavitCorteDiez;
    }

    public String getSuperavitCorteOnce() {
        if (superavitCorteOnce == null) {
            superavitCorteOnce = "";
        }
        return superavitCorteOnce;
    }

    public String getSuperavitCorteDoce() {
        if (superavitCorteDoce == null) {
            superavitCorteDoce = "";
        }
        return superavitCorteDoce;
    }

    public String getSuperavitCorteTrece() {
        if (superavitCorteTrece == null) {
            superavitCorteTrece = "";
        }
        return superavitCorteTrece;
    }

    public String getResultadosEjeAnterioresCorteUno() {
        if (resultadosEjeAnterioresCorteUno == null) {
            resultadosEjeAnterioresCorteUno = "";
        }
        return resultadosEjeAnterioresCorteUno;
    }

    public String getResultadosEjeAnterioresCorteDos() {
        if (resultadosEjeAnterioresCorteDos == null) {
            resultadosEjeAnterioresCorteDos = "";
        }
        return resultadosEjeAnterioresCorteDos;
    }

    public String getResultadosEjeAnterioresCorteTres() {
        if (resultadosEjeAnterioresCorteTres == null) {
            resultadosEjeAnterioresCorteTres = "";
        }
        return resultadosEjeAnterioresCorteTres;
    }

    public String getResultadosEjeAnterioresCorteCuatro() {
        if (resultadosEjeAnterioresCorteCuatro == null) {
            resultadosEjeAnterioresCorteCuatro = "";
        }
        return resultadosEjeAnterioresCorteCuatro;
    }

    public String getResultadosEjeAnterioresCorteCinco() {
        if (resultadosEjeAnterioresCorteCinco == null) {
            resultadosEjeAnterioresCorteCinco = "";
        }
        return resultadosEjeAnterioresCorteCinco;
    }

    public String getResultadosEjeAnterioresCorteSeis() {
        if (resultadosEjeAnterioresCorteSeis == null) {
            resultadosEjeAnterioresCorteSeis = "";
        }
        return resultadosEjeAnterioresCorteSeis;
    }

    public String getResultadosEjeAnterioresCorteSiete() {
        if (resultadosEjeAnterioresCorteSiete == null) {
            resultadosEjeAnterioresCorteSiete = "";
        }
        return resultadosEjeAnterioresCorteSiete;
    }

    public String getResultadosEjeAnterioresCorteOcho() {
        if (resultadosEjeAnterioresCorteOcho == null) {
            resultadosEjeAnterioresCorteOcho = "";
        }
        return resultadosEjeAnterioresCorteOcho;
    }

    public String getResultadosEjeAnterioresCorteNueve() {
        if (resultadosEjeAnterioresCorteNueve == null) {
            resultadosEjeAnterioresCorteNueve = "";
        }
        return resultadosEjeAnterioresCorteNueve;
    }

    public String getResultadosEjeAnterioresCorteDiez() {
        if (resultadosEjeAnterioresCorteDiez == null) {
            resultadosEjeAnterioresCorteDiez = "";
        }
        return resultadosEjeAnterioresCorteDiez;
    }

    public String getResultadosEjeAnterioresCorteOnce() {
        if (resultadosEjeAnterioresCorteOnce == null) {
            resultadosEjeAnterioresCorteOnce = "";
        }
        return resultadosEjeAnterioresCorteOnce;
    }

    public String getResultadosEjeAnterioresCorteDoce() {
        if (resultadosEjeAnterioresCorteDoce == null) {
            resultadosEjeAnterioresCorteDoce = "";
        }
        return resultadosEjeAnterioresCorteDoce;
    }

    public String getResultadosEjeAnterioresCorteTrece() {
        if (resultadosEjeAnterioresCorteTrece == null) {
            resultadosEjeAnterioresCorteTrece = "";
        }
        return resultadosEjeAnterioresCorteTrece;
    }

    public String getResultadosEjercicioCorteUno() {
        if (resultadosEjercicioCorteUno == null) {
            resultadosEjercicioCorteUno = "";
        }
        return resultadosEjercicioCorteUno;
    }

    public String getResultadosEjercicioCorteDos() {
        if (resultadosEjercicioCorteDos == null) {
            resultadosEjercicioCorteDos = "";
        }
        return resultadosEjercicioCorteDos;
    }

    public String getResultadosEjercicioCorteTres() {
        if (resultadosEjercicioCorteTres == null) {
            resultadosEjercicioCorteTres = "";
        }
        return resultadosEjercicioCorteTres;
    }

    public String getResultadosEjercicioCorteCuatro() {
        if (resultadosEjercicioCorteCuatro == null) {
            resultadosEjercicioCorteCuatro = "";
        }
        return resultadosEjercicioCorteCuatro;
    }

    public String getResultadosEjercicioCorteCinco() {
        if (resultadosEjercicioCorteCinco == null) {
            resultadosEjercicioCorteCinco = "";
        }
        return resultadosEjercicioCorteCinco;
    }

    public String getResultadosEjercicioCorteSeis() {
        if (resultadosEjercicioCorteSeis == null) {
            resultadosEjercicioCorteSeis = "";
        }
        return resultadosEjercicioCorteSeis;
    }

    public String getResultadosEjercicioCorteSiete() {
        if (resultadosEjercicioCorteSiete == null) {
            resultadosEjercicioCorteSiete = "";
        }
        return resultadosEjercicioCorteSiete;
    }

    public String getResultadosEjercicioCorteOcho() {
        if (resultadosEjercicioCorteOcho == null) {
            resultadosEjercicioCorteOcho = "";
        }
        return resultadosEjercicioCorteOcho;
    }

    public String getResultadosEjercicioCorteNueve() {
        if (resultadosEjercicioCorteNueve == null) {
            resultadosEjercicioCorteNueve = "";
        }
        return resultadosEjercicioCorteNueve;
    }

    public String getResultadosEjercicioCorteDiez() {
        if (resultadosEjercicioCorteDiez == null) {
            resultadosEjercicioCorteDiez = "";
        }
        return resultadosEjercicioCorteDiez;
    }

    public String getResultadosEjercicioCorteOnce() {
        if (resultadosEjercicioCorteOnce == null) {
            resultadosEjercicioCorteOnce = "";
        }
        return resultadosEjercicioCorteOnce;
    }

    public String getResultadosEjercicioCorteDoce() {
        if (resultadosEjercicioCorteDoce == null) {
            resultadosEjercicioCorteDoce = "";
        }
        return resultadosEjercicioCorteDoce;
    }

    public String getResultadosEjercicioCorteTrece() {
        if (resultadosEjercicioCorteTrece == null) {
            resultadosEjercicioCorteTrece = "";
        }
        return resultadosEjercicioCorteTrece;
    }

    public String getResultadosOriCorteUno() {
        if (resultadosOriCorteUno == null) {
            resultadosOriCorteUno = "";
        }
        return resultadosOriCorteUno;
    }

    public String getResultadosOriCorteDos() {
        if (resultadosOriCorteDos == null) {
            resultadosOriCorteDos = "";
        }
        return resultadosOriCorteDos;
    }

    public String getResultadosOriCorteTres() {
        if (resultadosOriCorteTres == null) {
            resultadosOriCorteTres = "";
        }
        return resultadosOriCorteTres;
    }

    public String getResultadosOriCorteCuatro() {
        if (resultadosOriCorteCuatro == null) {
            resultadosOriCorteCuatro = "";
        }
        return resultadosOriCorteCuatro;
    }

    public String getResultadosOriCorteCinco() {
        if (resultadosOriCorteCinco == null) {
            resultadosOriCorteCinco = "";
        }
        return resultadosOriCorteCinco;
    }

    public String getResultadosOriCorteSeis() {
        if (resultadosOriCorteSeis == null) {
            resultadosOriCorteSeis = "";
        }
        return resultadosOriCorteSeis;
    }

    public String getResultadosOriCorteSiete() {
        if (resultadosOriCorteSiete == null) {
            resultadosOriCorteSiete = "";
        }
        return resultadosOriCorteSiete;
    }

    public String getResultadosOriCorteOcho() {
        if (resultadosOriCorteOcho == null) {
            resultadosOriCorteOcho = "";
        }
        return resultadosOriCorteOcho;
    }

    public String getResultadosOriCorteNueve() {
        if (resultadosOriCorteNueve == null) {
            resultadosOriCorteNueve = "";
        }
        return resultadosOriCorteNueve;
    }

    public String getResultadosOriCorteDiez() {
        if (resultadosOriCorteDiez == null) {
            resultadosOriCorteDiez = "";
        }
        return resultadosOriCorteDiez;
    }

    public String getResultadosOriCorteOnce() {
        if (resultadosOriCorteOnce == null) {
            resultadosOriCorteOnce = "";
        }
        return resultadosOriCorteOnce;
    }

    public String getResultadosOriCorteDoce() {
        if (resultadosOriCorteDoce == null) {
            resultadosOriCorteDoce = "";
        }
        return resultadosOriCorteDoce;
    }

    public String getResultadosOriCorteTrece() {
        if (resultadosOriCorteTrece == null) {
            resultadosOriCorteTrece = "";
        }
        return resultadosOriCorteTrece;
    }

    public String getTotalPatrimonioCorteUno() {
        if (totalPatrimonioCorteUno == null) {
            totalPatrimonioCorteUno = "";
        }
        return totalPatrimonioCorteUno;
    }

    public String getTotalPatrimonioCorteDos() {
        if (totalPatrimonioCorteDos == null) {
            totalPatrimonioCorteDos = "";
        }
        return totalPatrimonioCorteDos;
    }

    public String getTotalPatrimonioCorteTres() {
        if (totalPatrimonioCorteTres == null) {
            totalPatrimonioCorteTres = "";
        }
        return totalPatrimonioCorteTres;
    }

    public String getTotalPatrimonioCorteCuatro() {
        if (totalPatrimonioCorteCuatro == null) {
            totalPatrimonioCorteCuatro = "";
        }
        return totalPatrimonioCorteCuatro;
    }

    public String getTotalPatrimonioCorteCinco() {
        if (totalPatrimonioCorteCinco == null) {
            totalPatrimonioCorteCinco = "";
        }
        return totalPatrimonioCorteCinco;
    }

    public String getTotalPatrimonioCorteSeis() {
        if (totalPatrimonioCorteSeis == null) {
            totalPatrimonioCorteSeis = "";
        }
        return totalPatrimonioCorteSeis;
    }

    public String getTotalPatrimonioCorteSiete() {
        if (totalPatrimonioCorteSiete == null) {
            totalPatrimonioCorteSiete = "";
        }
        return totalPatrimonioCorteSiete;
    }

    public String getTotalPatrimonioCorteOcho() {
        if (totalPatrimonioCorteOcho == null) {
            totalPatrimonioCorteOcho = "";
        }
        return totalPatrimonioCorteOcho;
    }

    public String getTotalPatrimonioCorteNueve() {
        if (totalPatrimonioCorteNueve == null) {
            totalPatrimonioCorteNueve = "";
        }
        return totalPatrimonioCorteNueve;
    }

    public String getTotalPatrimonioCorteDiez() {
        if (totalPatrimonioCorteDiez == null) {
            totalPatrimonioCorteDiez = "";
        }
        return totalPatrimonioCorteDiez;
    }

    public String getTotalPatrimonioCorteOnce() {
        if (totalPatrimonioCorteOnce == null) {
            totalPatrimonioCorteOnce = "";
        }
        return totalPatrimonioCorteOnce;
    }

    public String getTotalPatrimonioCorteDoce() {
        if (totalPatrimonioCorteDoce == null) {
            totalPatrimonioCorteDoce = "";
        }
        return totalPatrimonioCorteDoce;
    }

    public String getTotalPatrimonioCorteTrece() {
        if (totalPatrimonioCorteTrece == null) {
            totalPatrimonioCorteTrece = "";
        }
        return totalPatrimonioCorteTrece;
    }

}
