package soaint.ws.rest.bo.balanceAhorroCredito;

public class BalanceVariacion {
    private String partActivoCorteUno;
    private String partActivoCorteDos;
    private String partActivoCorteTres;
    private String partActivoCuantiaUnoDos;
    private String partActivoPorcentajeUnoDos;
    private String partActivoCuantiaUnoTres;
    private String partActivoPorcentajeUnoTres;
    private String partEfectivoCorteUno;
    private String partEfectivoCorteDos;
    private String partEfectivoCorteTres;
    private String partEfectivoCuantiaUnoDos;
    private String partEfectivoPorcentajeUnoDos;
    private String partEfectivoCuantiaUnoTres;
    private String partEfectivoPorcentajeUnoTres;
    private String partInversionesCorteUno;
    private String partInversionesCorteDos;
    private String partInversionesCorteTres;
    private String partInversionesCuantiaUnoDos;
    private String partInversionesPorcentajeUnoDos;
    private String partInversionesCuantiaUnoTres;
    private String partInversionesPorcentajeUnoTres;
    private String partFondoLiquidezCorteUno;
    private String partFondoLiquidezCorteDos;
    private String partFondoLiquidezCorteTres;
    private String partFondoLiquidezCuantiaUnoDos;
    private String partFondoLiquidezPorcentajeUnoDos;
    private String partFondoLiquidezCuantiaUnoTres;
    private String partFondoLiquidezPorcentajeUnoTres;
    private String partInventariosCorteUno;
    private String partInventariosCorteDos;
    private String partInventariosCorteTres;
    private String partInventariosCuantiaUnoDos;
    private String partInventariosPorcentajeUnoDos;
    private String partInventariosCuantiaUnoTres;
    private String partInventariosPorcentajeUnoTres;
    private String partCarteraBrutaCorteUno;
    private String partCarteraBrutaCorteDos;
    private String partCarteraBrutaCorteTres;
    private String partCarteraBrutaCuantiaUnoDos;
    private String partCarteraBrutaPorcentajeUnoDos;
    private String partCarteraBrutaCuantiaUnoTres;
    private String partCarteraBrutaPorcentajeUnoTres;
    private String partCarteraVencidaCorteUno;
    private String partCarteraVencidaCorteDos;
    private String partCarteraVencidaCorteTres;
    private String partCarteraVencidaCuantiaUnoDos;
    private String partCarteraVencidaPorcentajeUnoDos;
    private String partCarteraVencidaCuantiaUnoTres;
    private String partCarteraVencidaPorcentajeUnoTres;
    private String partCarteraImproductivaCorteUno;
    private String partCarteraImproductivaCorteDos;
    private String partCarteraImproductivaCorteTres;
    private String partCarteraImproductivaCuantiaUnoDos;
    private String partCarteraImproductivaPorcentajeUnoDos;
    private String partCarteraImproductivaCuantiaUnoTres;
    private String partCarteraImproductivaPorcentajeUnoTres;
    private String partCarteraBrutaConLibranzaCorteUno;
    private String partCarteraBrutaConLibranzaCorteDos;
    private String partCarteraBrutaConLibranzaCorteTres;
    private String partCarteraBrutaConLibranzaCuantiaUnoDos;
    private String partCarteraBrutaConLibranzaPorcentajeUnoDos;
    private String partCarteraBrutaConLibranzaCuantiaUnoTres;
    private String partCarteraBrutaConLibranzaPorcentajeUnoTres;
    private String partCarteraBrutaSinLibranzaCorteUno;
    private String partCarteraBrutaSinLibranzaCorteDos;
    private String partCarteraBrutaSinLibranzaCorteTres;
    private String partCarteraBrutaSinLibranzaCuantiaUnoDos;
    private String partCarteraBrutaSinLibranzaPorcentajeUnoDos;
    private String partCarteraBrutaSinLibranzaCuantiaUnoTres;
    private String partCarteraBrutaSinLibranzaPorcentajeUnoTres;
    private String partDeterioroCorteUno;
    private String partDeterioroCorteDos;
    private String partDeterioroCorteTres;
    private String partDeterioroCuantiaUnoDos;
    private String partDeterioroPorcentajeUnoDos;
    private String partDeterioroCuantiaUnoTres;
    private String partDeterioroPorcentajeUnoTres;
    private String partCuentasCobrarCorteUno;
    private String partCuentasCobrarCorteDos;
    private String partCuentasCobrarCorteTres;
    private String partCuentasCobrarCuantiaUnoDos;
    private String partCuentasCobrarPorcentajeUnoDos;
    private String partCuentasCobrarCuantiaUnoTres;
    private String partCuentasCobrarPorcentajeUnoTres;
    private String partActivosMaterialesCorteUno;
    private String partActivosMaterialesCorteDos;
    private String partActivosMaterialesCorteTres;
    private String partActivosMaterialesCuantiaUnoDos;
    private String partActivosMaterialesPorcentajeUnoDos;
    private String partActivosMaterialesCuantiaUnoTres;
    private String partActivosMaterialesPorcentajeUnoTres;
    private String partConveniosPorCobraCorteUno;
    private String partConveniosPorCobraCorteDos;
    private String partConveniosPorCobraCorteTres;
    private String partConveniosPorCobraCuantiaUnoDos;
    private String partConveniosPorCobraPorcentajeUnoDos;
    private String partConveniosPorCobraCuantiaUnoTres;
    private String partConveniosPorCobraPorcentajeUnoTres;
    private String partDeterioroConveniosCorteUno;
    private String partDeterioroConveniosCorteDos;
    private String partDeterioroConveniosCorteTres;
    private String partDeterioroConveniosCuantiaUnoDos;
    private String partDeterioroConveniosPorcentajeUnoDos;
    private String partDeterioroConveniosCuantiaUnoTres;
    private String partDeterioroConveniosPorcentajeUnoTres;
    private String partActivosNoCorrientesCorteUno;
    private String partActivosNoCorrientesCorteDos;
    private String partActivosNoCorrientesCorteTres;
    private String partActivosNoCorrientesCuantiaUnoDos;
    private String partActivosNoCorrientesPorcentajeUnoDos;
    private String partActivosNoCorrientesCuantiaUnoTres;
    private String partActivosNoCorrientesPorcentajeUnoTres;
    private String partOtrosActivosCorteUno;
    private String partOtrosActivosCorteDos;
    private String partOtrosActivosCorteTres;
    private String partOtrosActivosCuantiaUnoDos;
    private String partOtrosActivosPorcentajeUnoDos;
    private String partOtrosActivosCuantiaUnoTres;
    private String partOtrosActivosPorcentajeUnoTres;
    private String partTotalActivosCorteUno;
    private String partTotalActivosCorteDos;
    private String partTotalActivosCorteTres;
    private String partTotalActivosCuantiaUnoDos;
    private String partTotalActivosPorcentajeUnoDos;
    private String partTotalActivosCuantiaUnoTres;
    private String partTotalActivosPorcentajeUnoTres;
    private String partPasivosCorteUno;
    private String partPasivosCorteDos;
    private String partPasivosCorteTres;
    private String partPasivosCuantiaUnoDos;
    private String partPasivosPorcentajeUnoDos;
    private String partPasivosCuantiaUnoTres;
    private String partPasivosPorcentajeUnoTres;
    private String partDepositosCorteUno;
    private String partDepositosCorteDos;
    private String partDepositosCorteTres;
    private String partDepositosCuantiaUnoDos;
    private String partDepositosPorcentajeUnoDos;
    private String partDepositosCuantiaUnoTres;
    private String partDepositosPorcentajeUnoTres;
    private String partDepositosAhorroCorteUno;
    private String partDepositosAhorroCorteDos;
    private String partDepositosAhorroCorteTres;
    private String partDepositosAhorroCuantiaUnoDos;
    private String partDepositosAhorroPorcentajeUnoDos;
    private String partDepositosAhorroCuantiaUnoTres;
    private String partDepositosAhorroPorcentajeUnoTres;
    private String partCdatCorteUno;
    private String partCdatCorteDos;
    private String partCdatCorteTres;
    private String partCdatCuantiaUnoDos;
    private String partCdatPorcentajeUnoDos;
    private String partCdatCuantiaUnoTres;
    private String partCdatPorcentajeUnoTres;
    private String partAhorroContractualCorteUno;
    private String partAhorroContractualCorteDos;
    private String partAhorroContractualCorteTres;
    private String partAhorroContractualCuantiaUnoDos;
    private String partAhorroContractualPorcentajeUnoDos;
    private String partAhorroContractualCuantiaUnoTres;
    private String partAhorroContractualPorcentajeUnoTres;
    private String partAhorroPermanenteCorteUno;
    private String partAhorroPermanenteCorteDos;
    private String partAhorroPermanenteCorteTres;
    private String partAhorroPermanenteCuantiaUnoDos;
    private String partAhorroPermanentePorcentajeUnoDos;
    private String partAhorroPermanenteCuantiaUnoTres;
    private String partAhorroPermanentePorcentajeUnoTres;
    private String partTituloInversionCorteUno;
    private String partTituloInversionCorteDos;
    private String partTituloInversionCorteTres;
    private String partTituloInversionCuantiaUnoDos;
    private String partTituloInversionPorcentajeUnoDos;
    private String partTituloInversionCuantiaUnoTres;
    private String partTituloInversionPorcentajeUnoTres;
    private String partCreditosBancosCorteUno;
    private String partCreditosBancosCorteDos;
    private String partCreditosBancosCorteTres;
    private String partCreditosBancosCuantiaUnoDos;
    private String partCreditosBancosPorcentajeUnoDos;
    private String partCreditosBancosCuantiaUnoTres;
    private String partCreditosBancosPorcentajeUnoTres;
    private String partCuentasPorPagarCorteUno;
    private String partCuentasPorPagarCorteDos;
    private String partCuentasPorPagarCorteTres;
    private String partCuentasPorPagarCuantiaUnoDos;
    private String partCuentasPorPagarPorcentajeUnoDos;
    private String partCuentasPorPagarCuantiaUnoTres;
    private String partCuentasPorPagarPorcentajeUnoTres;
    private String partFondosSocialesCorteUno;
    private String partFondosSocialesCorteDos;
    private String partFondosSocialesCorteTres;
    private String partFondosSocialesCuantiaUnoDos;
    private String partFondosSocialesPorcentajeUnoDos;
    private String partFondosSocialesCuantiaUnoTres;
    private String partFondosSocialesPorcentajeUnoTres;
    private String partProvisionesCorteUno;
    private String partProvisionesCorteDos;
    private String partProvisionesCorteTres;
    private String partProvisionesCuantiaUnoDos;
    private String partProvisionesPorcentajeUnoDos;
    private String partProvisionesCuantiaUnoTres;
    private String partProvisionesPorcentajeUnoTres;
    private String partAportesSocialesCorteUno;
    private String partAportesSocialesCorteDos;
    private String partAportesSocialesCorteTres;
    private String partAportesSocialesCuantiaUnoDos;
    private String partAportesSocialesPorcentajeUnoDos;
    private String partAportesSocialesCuantiaUnoTres;
    private String partAportesSocialesPorcentajeUnoTres;
    private String partTotalPasivosCorteUno;
    private String partTotalPasivosCorteDos;
    private String partTotalPasivosCorteTres;
    private String partTotalPasivosCuantiaUnoDos;
    private String partTotalPasivosPorcentajeUnoDos;
    private String partTotalPasivosCuantiaUnoTres;
    private String partTotalPasivosPorcentajeUnoTres;
    private String partPatrimonioCorteUno;
    private String partPatrimonioCorteDos;
    private String partPatrimonioCorteTres;
    private String partPatrimonioCuantiaUnoDos;
    private String partPatrimonioPorcentajeUnoDos;
    private String partPatrimonioCuantiaUnoTres;
    private String partPatrimonioPorcentajeUnoTres;
    private String partCapitalSocialCorteUno;
    private String partCapitalSocialCorteDos;
    private String partCapitalSocialCorteTres;
    private String partCapitalSocialCuantiaUnoDos;
    private String partCapitalSocialPorcentajeUnoDos;
    private String partCapitalSocialCuantiaUnoTres;
    private String partCapitalSocialPorcentajeUnoTres;
    private String partReservasCorteUno;
    private String partReservasCorteDos;
    private String partReservasCorteTres;
    private String partReservasCuantiaUnoDos;
    private String partReservasPorcentajeUnoDos;
    private String partReservasCuantiaUnoTres;
    private String partReservasPorcentajeUnoTres;
    private String partFondosDestinacionCorteUno;
    private String partFondosDestinacionCorteDos;
    private String partFondosDestinacionCorteTres;
    private String partFondosDestinacionCuantiaUnoDos;
    private String partFondosDestinacionPorcentajeUnoDos;
    private String partFondosDestinacionCuantiaUnoTres;
    private String partFondosDestinacionPorcentajeUnoTres;
    private String partSuperavitCorteUno;
    private String partSuperavitCorteDos;
    private String partSuperavitCorteTres;
    private String partSuperavitCuantiaUnoDos;
    private String partSuperavitPorcentajeUnoDos;
    private String partSuperavitCuantiaUnoTres;
    private String partSuperavitPorcentajeUnoTres;
    private String partResultadosEjeAnterioresCorteUno;
    private String partResultadosEjeAnterioresCorteDos;
    private String partResultadosEjeAnterioresCorteTres;
    private String partResultadosEjeAnterioresCuantiaUnoDos;
    private String partResultadosEjeAnterioresPorcentajeUnoDos;
    private String partResultadosEjeAnterioresCuantiaUnoTres;
    private String partResultadosEjeAnterioresPorcentajeUnoTres;
    private String partResultadosEjercicioCorteUno;
    private String partResultadosEjercicioCorteDos;
    private String partResultadosEjercicioCorteTres;
    private String partResultadosEjercicioCuantiaUnoDos;
    private String partResultadosEjercicioPorcentajeUnoDos;
    private String partResultadosEjercicioCuantiaUnoTres;
    private String partResultadosEjercicioPorcentajeUnoTres;
    private String partResultadosOriCorteUno;
    private String partResultadosOriCorteDos;
    private String partResultadosOriCorteTres;
    private String partResultadosOriCuantiaUnoDos;
    private String partResultadosOriPorcentajeUnoDos;
    private String partResultadosOriCuantiaUnoTres;
    private String partResultadosOriPorcentajeUnoTres;
    private String partTotalPatrimonioCorteUno;
    private String partTotalPatrimonioCorteDos;
    private String partTotalPatrimonioCorteTres;
    private String partTotalPatrimonioCuantiaUnoDos;
    private String partTotalPatrimonioPorcentajeUnoDos;
    private String partTotalPatrimonioCuantiaUnoTres;
    private String partTotalPatrimonioPorcentajeUnoTres;

    public String getPartActivoCorteUno() {
        if (partActivoCorteUno == null) {
            partActivoCorteUno = "";
        }
        return partActivoCorteUno;
    }

    public String getPartActivoCorteDos() {
        if (partActivoCorteDos == null) {
            partActivoCorteDos = "";
        }
        return partActivoCorteDos;
    }

    public String getPartActivoCorteTres() {
        if (partActivoCorteTres == null) {
            partActivoCorteTres = "";
        }
        return partActivoCorteTres;
    }

    public String getPartActivoCuantiaUnoDos() {
        if (partActivoCuantiaUnoDos == null) {
            partActivoCuantiaUnoDos = "";
        }
        return partActivoCuantiaUnoDos;
    }

    public String getPartActivoPorcentajeUnoDos() {
        if (partActivoPorcentajeUnoDos == null) {
            partActivoPorcentajeUnoDos = "";
        }
        return partActivoPorcentajeUnoDos;
    }

    public String getPartActivoCuantiaUnoTres() {
        if (partActivoCuantiaUnoTres == null) {
            partActivoCuantiaUnoTres = "";
        }
        return partActivoCuantiaUnoTres;
    }

    public String getPartActivoPorcentajeUnoTres() {
        if (partActivoPorcentajeUnoTres == null) {
            partActivoPorcentajeUnoTres = "";
        }
        return partActivoPorcentajeUnoTres;
    }

    public String getPartEfectivoCorteUno() {
        if (partEfectivoCorteUno == null) {
            partEfectivoCorteUno = "";
        }
        return partEfectivoCorteUno;
    }

    public String getPartEfectivoCorteDos() {
        if (partEfectivoCorteDos == null) {
            partEfectivoCorteDos = "";
        }
        return partEfectivoCorteDos;
    }

    public String getPartEfectivoCorteTres() {
        if (partEfectivoCorteTres == null) {
            partEfectivoCorteTres = "";
        }
        return partEfectivoCorteTres;
    }

    public String getPartEfectivoCuantiaUnoDos() {
        if (partEfectivoCuantiaUnoDos == null) {
            partEfectivoCuantiaUnoDos = "";
        }
        return partEfectivoCuantiaUnoDos;
    }

    public String getPartEfectivoPorcentajeUnoDos() {
        if (partEfectivoPorcentajeUnoDos == null) {
            partEfectivoPorcentajeUnoDos = "";
        }
        return partEfectivoPorcentajeUnoDos;
    }

    public String getPartEfectivoCuantiaUnoTres() {
        if (partEfectivoCuantiaUnoTres == null) {
            partEfectivoCuantiaUnoTres = "";
        }
        return partEfectivoCuantiaUnoTres;
    }

    public String getPartEfectivoPorcentajeUnoTres() {
        if (partEfectivoPorcentajeUnoTres == null) {
            partEfectivoPorcentajeUnoTres = "";
        }
        return partEfectivoPorcentajeUnoTres;
    }

    public String getPartInversionesCorteUno() {
        if (partInversionesCorteUno == null) {
            partInversionesCorteUno = "";
        }
        return partInversionesCorteUno;
    }

    public String getPartInversionesCorteDos() {
        if (partInversionesCorteDos == null) {
            partInversionesCorteDos = "";
        }
        return partInversionesCorteDos;
    }

    public String getPartInversionesCorteTres() {
        if (partInversionesCorteTres == null) {
            partInversionesCorteTres = "";
        }
        return partInversionesCorteTres;
    }

    public String getPartInversionesCuantiaUnoDos() {
        if (partInversionesCuantiaUnoDos == null) {
            partInversionesCuantiaUnoDos = "";
        }
        return partInversionesCuantiaUnoDos;
    }

    public String getPartInversionesPorcentajeUnoDos() {
        if (partInversionesPorcentajeUnoDos == null) {
            partInversionesPorcentajeUnoDos = "";
        }
        return partInversionesPorcentajeUnoDos;
    }

    public String getPartInversionesCuantiaUnoTres() {
        if (partInversionesCuantiaUnoTres == null) {
            partInversionesCuantiaUnoTres = "";
        }
        return partInversionesCuantiaUnoTres;
    }

    public String getPartInversionesPorcentajeUnoTres() {
        if (partInversionesPorcentajeUnoTres == null) {
            partInversionesPorcentajeUnoTres = "";
        }
        return partInversionesPorcentajeUnoTres;
    }

    public String getPartFondoLiquidezCorteUno() {
        if (partFondoLiquidezCorteUno == null) {
            partFondoLiquidezCorteUno = "";
        }
        return partFondoLiquidezCorteUno;
    }

    public String getPartFondoLiquidezCorteDos() {
        if (partFondoLiquidezCorteDos == null) {
            partFondoLiquidezCorteDos = "";
        }
        return partFondoLiquidezCorteDos;
    }

    public String getPartFondoLiquidezCorteTres() {
        if (partFondoLiquidezCorteTres == null) {
            partFondoLiquidezCorteTres = "";
        }
        return partFondoLiquidezCorteTres;
    }

    public String getPartFondoLiquidezCuantiaUnoDos() {
        if (partFondoLiquidezCuantiaUnoDos == null) {
            partFondoLiquidezCuantiaUnoDos = "";
        }
        return partFondoLiquidezCuantiaUnoDos;
    }

    public String getPartFondoLiquidezPorcentajeUnoDos() {
        if (partFondoLiquidezPorcentajeUnoDos == null) {
            partFondoLiquidezPorcentajeUnoDos = "";
        }
        return partFondoLiquidezPorcentajeUnoDos;
    }

    public String getPartFondoLiquidezCuantiaUnoTres() {
        if (partFondoLiquidezCuantiaUnoTres == null) {
            partFondoLiquidezCuantiaUnoTres = "";
        }
        return partFondoLiquidezCuantiaUnoTres;
    }

    public String getPartFondoLiquidezPorcentajeUnoTres() {
        if (partFondoLiquidezPorcentajeUnoTres == null) {
            partFondoLiquidezPorcentajeUnoTres = "";
        }
        return partFondoLiquidezPorcentajeUnoTres;
    }

    public String getPartInventariosCorteUno() {
        if (partInventariosCorteUno == null) {
            partInventariosCorteUno = "";
        }
        return partInventariosCorteUno;
    }

    public String getPartInventariosCorteDos() {
        if (partInventariosCorteDos == null) {
            partInventariosCorteDos = "";
        }
        return partInventariosCorteDos;
    }

    public String getPartInventariosCorteTres() {
        if (partInventariosCorteTres == null) {
            partInventariosCorteTres = "";
        }
        return partInventariosCorteTres;
    }

    public String getPartInventariosCuantiaUnoDos() {
        if (partInventariosCuantiaUnoDos == null) {
            partInventariosCuantiaUnoDos = "";
        }
        return partInventariosCuantiaUnoDos;
    }

    public String getPartInventariosPorcentajeUnoDos() {
        if (partInventariosPorcentajeUnoDos == null) {
            partInventariosPorcentajeUnoDos = "";
        }
        return partInventariosPorcentajeUnoDos;
    }

    public String getPartInventariosCuantiaUnoTres() {
        if (partInventariosCuantiaUnoTres == null) {
            partInventariosCuantiaUnoTres = "";
        }
        return partInventariosCuantiaUnoTres;
    }

    public String getPartInventariosPorcentajeUnoTres() {
        if (partInventariosPorcentajeUnoTres == null) {
            partInventariosPorcentajeUnoTres = "";
        }
        return partInventariosPorcentajeUnoTres;
    }

    public String getPartCarteraBrutaCorteUno() {
        if (partCarteraBrutaCorteUno == null) {
            partCarteraBrutaCorteUno = "";
        }
        return partCarteraBrutaCorteUno;
    }

    public String getPartCarteraBrutaCorteDos() {
        if (partCarteraBrutaCorteDos == null) {
            partCarteraBrutaCorteDos = "";
        }
        return partCarteraBrutaCorteDos;
    }

    public String getPartCarteraBrutaCorteTres() {
        if (partCarteraBrutaCorteTres == null) {
            partCarteraBrutaCorteTres = "";
        }
        return partCarteraBrutaCorteTres;
    }

    public String getPartCarteraBrutaCuantiaUnoDos() {
        if (partCarteraBrutaCuantiaUnoDos == null) {
            partCarteraBrutaCuantiaUnoDos = "";
        }
        return partCarteraBrutaCuantiaUnoDos;
    }

    public String getPartCarteraBrutaPorcentajeUnoDos() {
        if (partCarteraBrutaPorcentajeUnoDos == null) {
            partCarteraBrutaPorcentajeUnoDos = "";
        }
        return partCarteraBrutaPorcentajeUnoDos;
    }

    public String getPartCarteraBrutaCuantiaUnoTres() {
        if (partCarteraBrutaCuantiaUnoTres == null) {
            partCarteraBrutaCuantiaUnoTres = "";
        }
        return partCarteraBrutaCuantiaUnoTres;
    }

    public String getPartCarteraBrutaPorcentajeUnoTres() {
        if (partCarteraBrutaPorcentajeUnoTres == null) {
            partCarteraBrutaPorcentajeUnoTres = "";
        }
        return partCarteraBrutaPorcentajeUnoTres;
    }

    public String getPartCarteraVencidaCorteUno() {
        if (partCarteraVencidaCorteUno == null) {
            partCarteraVencidaCorteUno = "";
        }
        return partCarteraVencidaCorteUno;
    }

    public String getPartCarteraVencidaCorteDos() {
        if (partCarteraVencidaCorteDos == null) {
            partCarteraVencidaCorteDos = "";
        }
        return partCarteraVencidaCorteDos;
    }

    public String getPartCarteraVencidaCorteTres() {
        if (partCarteraVencidaCorteTres == null) {
            partCarteraVencidaCorteTres = "";
        }
        return partCarteraVencidaCorteTres;
    }

    public String getPartCarteraVencidaCuantiaUnoDos() {
        if (partCarteraVencidaCuantiaUnoDos == null) {
            partCarteraVencidaCuantiaUnoDos = "";
        }
        return partCarteraVencidaCuantiaUnoDos;
    }

    public String getPartCarteraVencidaPorcentajeUnoDos() {
        if (partCarteraVencidaPorcentajeUnoDos == null) {
            partCarteraVencidaPorcentajeUnoDos = "";
        }
        return partCarteraVencidaPorcentajeUnoDos;
    }

    public String getPartCarteraVencidaCuantiaUnoTres() {
        if (partCarteraVencidaCuantiaUnoTres == null) {
            partCarteraVencidaCuantiaUnoTres = "";
        }
        return partCarteraVencidaCuantiaUnoTres;
    }

    public String getPartCarteraVencidaPorcentajeUnoTres() {
        if (partCarteraVencidaPorcentajeUnoTres == null) {
            partCarteraVencidaPorcentajeUnoTres = "";
        }
        return partCarteraVencidaPorcentajeUnoTres;
    }

    public String getPartCarteraImproductivaCorteUno() {
        if (partCarteraImproductivaCorteUno == null) {
            partCarteraImproductivaCorteUno = "";
        }
        return partCarteraImproductivaCorteUno;
    }

    public String getPartCarteraImproductivaCorteDos() {
        if (partCarteraImproductivaCorteDos == null) {
            partCarteraImproductivaCorteDos = "";
        }
        return partCarteraImproductivaCorteDos;
    }

    public String getPartCarteraImproductivaCorteTres() {
        if (partCarteraImproductivaCorteTres == null) {
            partCarteraImproductivaCorteTres = "";
        }
        return partCarteraImproductivaCorteTres;
    }

    public String getPartCarteraImproductivaCuantiaUnoDos() {
        if (partCarteraImproductivaCuantiaUnoDos == null) {
            partCarteraImproductivaCuantiaUnoDos = "";
        }
        return partCarteraImproductivaCuantiaUnoDos;
    }

    public String getPartCarteraImproductivaPorcentajeUnoDos() {
        if (partCarteraImproductivaPorcentajeUnoDos == null) {
            partCarteraImproductivaPorcentajeUnoDos = "";
        }
        return partCarteraImproductivaPorcentajeUnoDos;
    }

    public String getPartCarteraImproductivaCuantiaUnoTres() {
        if (partCarteraImproductivaCuantiaUnoTres == null) {
            partCarteraImproductivaCuantiaUnoTres = "";
        }
        return partCarteraImproductivaCuantiaUnoTres;
    }

    public String getPartCarteraImproductivaPorcentajeUnoTres() {
        if (partCarteraImproductivaPorcentajeUnoTres == null) {
            partCarteraImproductivaPorcentajeUnoTres = "";
        }
        return partCarteraImproductivaPorcentajeUnoTres;
    }

    public String getPartCarteraBrutaConLibranzaCorteUno() {
        if (partCarteraBrutaConLibranzaCorteUno == null) {
            partCarteraBrutaConLibranzaCorteUno = "";
        }
        return partCarteraBrutaConLibranzaCorteUno;
    }

    public String getPartCarteraBrutaConLibranzaCorteDos() {
        if (partCarteraBrutaConLibranzaCorteDos == null) {
            partCarteraBrutaConLibranzaCorteDos = "";
        }
        return partCarteraBrutaConLibranzaCorteDos;
    }

    public String getPartCarteraBrutaConLibranzaCorteTres() {
        if (partCarteraBrutaConLibranzaCorteTres == null) {
            partCarteraBrutaConLibranzaCorteTres = "";
        }
        return partCarteraBrutaConLibranzaCorteTres;
    }

    public String getPartCarteraBrutaConLibranzaCuantiaUnoDos() {
        if (partCarteraBrutaConLibranzaCuantiaUnoDos == null) {
            partCarteraBrutaConLibranzaCuantiaUnoDos = "";
        }
        return partCarteraBrutaConLibranzaCuantiaUnoDos;
    }

    public String getPartCarteraBrutaConLibranzaPorcentajeUnoDos() {
        if (partCarteraBrutaConLibranzaPorcentajeUnoDos == null) {
            partCarteraBrutaConLibranzaPorcentajeUnoDos = "";
        }
        return partCarteraBrutaConLibranzaPorcentajeUnoDos;
    }

    public String getPartCarteraBrutaConLibranzaCuantiaUnoTres() {
        if (partCarteraBrutaConLibranzaCuantiaUnoTres == null) {
            partCarteraBrutaConLibranzaCuantiaUnoTres = "";
        }
        return partCarteraBrutaConLibranzaCuantiaUnoTres;
    }

    public String getPartCarteraBrutaConLibranzaPorcentajeUnoTres() {
        if (partCarteraBrutaConLibranzaPorcentajeUnoTres == null) {
            partCarteraBrutaConLibranzaPorcentajeUnoTres = "";
        }
        return partCarteraBrutaConLibranzaPorcentajeUnoTres;
    }

    public String getPartCarteraBrutaSinLibranzaCorteUno() {
        if (partCarteraBrutaSinLibranzaCorteUno == null) {
            partCarteraBrutaSinLibranzaCorteUno = "";
        }
        return partCarteraBrutaSinLibranzaCorteUno;
    }

    public String getPartCarteraBrutaSinLibranzaCorteDos() {
        if (partCarteraBrutaSinLibranzaCorteDos == null) {
            partCarteraBrutaSinLibranzaCorteDos = "";
        }
        return partCarteraBrutaSinLibranzaCorteDos;
    }

    public String getPartCarteraBrutaSinLibranzaCorteTres() {
        if (partCarteraBrutaSinLibranzaCorteTres == null) {
            partCarteraBrutaSinLibranzaCorteTres = "";
        }
        return partCarteraBrutaSinLibranzaCorteTres;
    }

    public String getPartCarteraBrutaSinLibranzaCuantiaUnoDos() {
        if (partCarteraBrutaSinLibranzaCuantiaUnoDos == null) {
            partCarteraBrutaSinLibranzaCuantiaUnoDos = "";
        }
        return partCarteraBrutaSinLibranzaCuantiaUnoDos;
    }

    public String getPartCarteraBrutaSinLibranzaPorcentajeUnoDos() {
        if (partCarteraBrutaSinLibranzaPorcentajeUnoDos == null) {
            partCarteraBrutaSinLibranzaPorcentajeUnoDos = "";
        }
        return partCarteraBrutaSinLibranzaPorcentajeUnoDos;
    }

    public String getPartCarteraBrutaSinLibranzaCuantiaUnoTres() {
        if (partCarteraBrutaSinLibranzaCuantiaUnoTres == null) {
            partCarteraBrutaSinLibranzaCuantiaUnoTres = "";
        }
        return partCarteraBrutaSinLibranzaCuantiaUnoTres;
    }

    public String getPartCarteraBrutaSinLibranzaPorcentajeUnoTres() {
        if (partCarteraBrutaSinLibranzaPorcentajeUnoTres == null) {
            partCarteraBrutaSinLibranzaPorcentajeUnoTres = "";
        }
        return partCarteraBrutaSinLibranzaPorcentajeUnoTres;
    }

    public String getPartDeterioroCorteUno() {
        if (partDeterioroCorteUno == null) {
            partDeterioroCorteUno = "";
        }
        return partDeterioroCorteUno;
    }

    public String getPartDeterioroCorteDos() {
        if (partDeterioroCorteDos == null) {
            partDeterioroCorteDos = "";
        }
        return partDeterioroCorteDos;
    }

    public String getPartDeterioroCorteTres() {
        if (partDeterioroCorteTres == null) {
            partDeterioroCorteTres = "";
        }
        return partDeterioroCorteTres;
    }

    public String getPartDeterioroCuantiaUnoDos() {
        if (partDeterioroCuantiaUnoDos == null) {
            partDeterioroCuantiaUnoDos = "";
        }
        return partDeterioroCuantiaUnoDos;
    }

    public String getPartDeterioroPorcentajeUnoDos() {
        if (partDeterioroPorcentajeUnoDos == null) {
            partDeterioroPorcentajeUnoDos = "";
        }
        return partDeterioroPorcentajeUnoDos;
    }

    public String getPartDeterioroCuantiaUnoTres() {
        if (partDeterioroCuantiaUnoTres == null) {
            partDeterioroCuantiaUnoTres = "";
        }
        return partDeterioroCuantiaUnoTres;
    }

    public String getPartDeterioroPorcentajeUnoTres() {
        if (partDeterioroPorcentajeUnoTres == null) {
            partDeterioroPorcentajeUnoTres = "";
        }
        return partDeterioroPorcentajeUnoTres;
    }

    public String getPartCuentasCobrarCorteUno() {
        if (partCuentasCobrarCorteUno == null) {
            partCuentasCobrarCorteUno = "";
        }
        return partCuentasCobrarCorteUno;
    }

    public String getPartCuentasCobrarCorteDos() {
        if (partCuentasCobrarCorteDos == null) {
            partCuentasCobrarCorteDos = "";
        }
        return partCuentasCobrarCorteDos;
    }

    public String getPartCuentasCobrarCorteTres() {
        if (partCuentasCobrarCorteTres == null) {
            partCuentasCobrarCorteTres = "";
        }
        return partCuentasCobrarCorteTres;
    }

    public String getPartCuentasCobrarCuantiaUnoDos() {
        if (partCuentasCobrarCuantiaUnoDos == null) {
            partCuentasCobrarCuantiaUnoDos = "";
        }
        return partCuentasCobrarCuantiaUnoDos;
    }

    public String getPartCuentasCobrarPorcentajeUnoDos() {
        if (partCuentasCobrarPorcentajeUnoDos == null) {
            partCuentasCobrarPorcentajeUnoDos = "";
        }
        return partCuentasCobrarPorcentajeUnoDos;
    }

    public String getPartCuentasCobrarCuantiaUnoTres() {
        if (partCuentasCobrarCuantiaUnoTres == null) {
            partCuentasCobrarCuantiaUnoTres = "";
        }
        return partCuentasCobrarCuantiaUnoTres;
    }

    public String getPartCuentasCobrarPorcentajeUnoTres() {
        if (partCuentasCobrarPorcentajeUnoTres == null) {
            partCuentasCobrarPorcentajeUnoTres = "";
        }
        return partCuentasCobrarPorcentajeUnoTres;
    }

    public String getPartActivosMaterialesCorteUno() {
        if (partActivosMaterialesCorteUno == null) {
            partActivosMaterialesCorteUno = "";
        }
        return partActivosMaterialesCorteUno;
    }

    public String getPartActivosMaterialesCorteDos() {
        if (partActivosMaterialesCorteDos == null) {
            partActivosMaterialesCorteDos = "";
        }
        return partActivosMaterialesCorteDos;
    }

    public String getPartActivosMaterialesCorteTres() {
        if (partActivosMaterialesCorteTres == null) {
            partActivosMaterialesCorteTres = "";
        }
        return partActivosMaterialesCorteTres;
    }

    public String getPartActivosMaterialesCuantiaUnoDos() {
        if (partActivosMaterialesCuantiaUnoDos == null) {
            partActivosMaterialesCuantiaUnoDos = "";
        }
        return partActivosMaterialesCuantiaUnoDos;
    }

    public String getPartActivosMaterialesPorcentajeUnoDos() {
        if (partActivosMaterialesPorcentajeUnoDos == null) {
            partActivosMaterialesPorcentajeUnoDos = "";
        }
        return partActivosMaterialesPorcentajeUnoDos;
    }

    public String getPartActivosMaterialesCuantiaUnoTres() {
        if (partActivosMaterialesCuantiaUnoTres == null) {
            partActivosMaterialesCuantiaUnoTres = "";
        }
        return partActivosMaterialesCuantiaUnoTres;
    }

    public String getPartActivosMaterialesPorcentajeUnoTres() {
        if (partActivosMaterialesPorcentajeUnoTres == null) {
            partActivosMaterialesPorcentajeUnoTres = "";
        }
        return partActivosMaterialesPorcentajeUnoTres;
    }

    public String getPartConveniosPorCobraCorteUno() {
        if (partConveniosPorCobraCorteUno == null) {
            partConveniosPorCobraCorteUno = "";
        }
        return partConveniosPorCobraCorteUno;
    }

    public String getPartConveniosPorCobraCorteDos() {
        if (partConveniosPorCobraCorteDos == null) {
            partConveniosPorCobraCorteDos = "";
        }
        return partConveniosPorCobraCorteDos;
    }

    public String getPartConveniosPorCobraCorteTres() {
        if (partConveniosPorCobraCorteTres == null) {
            partConveniosPorCobraCorteTres = "";
        }
        return partConveniosPorCobraCorteTres;
    }

    public String getPartConveniosPorCobraCuantiaUnoDos() {
        if (partConveniosPorCobraCuantiaUnoDos == null) {
            partConveniosPorCobraCuantiaUnoDos = "";
        }
        return partConveniosPorCobraCuantiaUnoDos;
    }

    public String getPartConveniosPorCobraPorcentajeUnoDos() {
        if (partConveniosPorCobraPorcentajeUnoDos == null) {
            partConveniosPorCobraPorcentajeUnoDos = "";
        }
        return partConveniosPorCobraPorcentajeUnoDos;
    }

    public String getPartConveniosPorCobraCuantiaUnoTres() {
        if (partConveniosPorCobraCuantiaUnoTres == null) {
            partConveniosPorCobraCuantiaUnoTres = "";
        }
        return partConveniosPorCobraCuantiaUnoTres;
    }

    public String getPartConveniosPorCobraPorcentajeUnoTres() {
        if (partConveniosPorCobraPorcentajeUnoTres == null) {
            partConveniosPorCobraPorcentajeUnoTres = "";
        }
        return partConveniosPorCobraPorcentajeUnoTres;
    }

    public String getPartDeterioroConveniosCorteUno() {
        if (partDeterioroConveniosCorteUno == null) {
            partDeterioroConveniosCorteUno = "";
        }
        return partDeterioroConveniosCorteUno;
    }

    public String getPartDeterioroConveniosCorteDos() {
        if (partDeterioroConveniosCorteDos == null) {
            partDeterioroConveniosCorteDos = "";
        }
        return partDeterioroConveniosCorteDos;
    }

    public String getPartDeterioroConveniosCorteTres() {
        if (partDeterioroConveniosCorteTres == null) {
            partDeterioroConveniosCorteTres = "";
        }
        return partDeterioroConveniosCorteTres;
    }

    public String getPartDeterioroConveniosCuantiaUnoDos() {
        if (partDeterioroConveniosCuantiaUnoDos == null) {
            partDeterioroConveniosCuantiaUnoDos = "";
        }
        return partDeterioroConveniosCuantiaUnoDos;
    }

    public String getPartDeterioroConveniosPorcentajeUnoDos() {
        if (partDeterioroConveniosPorcentajeUnoDos == null) {
            partDeterioroConveniosPorcentajeUnoDos = "";
        }
        return partDeterioroConveniosPorcentajeUnoDos;
    }

    public String getPartDeterioroConveniosCuantiaUnoTres() {
        if (partDeterioroConveniosCuantiaUnoTres == null) {
            partDeterioroConveniosCuantiaUnoTres = "";
        }
        return partDeterioroConveniosCuantiaUnoTres;
    }

    public String getPartDeterioroConveniosPorcentajeUnoTres() {
        if (partDeterioroConveniosPorcentajeUnoTres == null) {
            partDeterioroConveniosPorcentajeUnoTres = "";
        }
        return partDeterioroConveniosPorcentajeUnoTres;
    }

    public String getPartActivosNoCorrientesCorteUno() {
        if (partActivosNoCorrientesCorteUno == null) {
            partActivosNoCorrientesCorteUno = "";
        }
        return partActivosNoCorrientesCorteUno;
    }

    public String getPartActivosNoCorrientesCorteDos() {
        if (partActivosNoCorrientesCorteDos == null) {
            partActivosNoCorrientesCorteDos = "";
        }
        return partActivosNoCorrientesCorteDos;
    }

    public String getPartActivosNoCorrientesCorteTres() {
        if (partActivosNoCorrientesCorteTres == null) {
            partActivosNoCorrientesCorteTres = "";
        }
        return partActivosNoCorrientesCorteTres;
    }

    public String getPartActivosNoCorrientesCuantiaUnoDos() {
        if (partActivosNoCorrientesCuantiaUnoDos == null) {
            partActivosNoCorrientesCuantiaUnoDos = "";
        }
        return partActivosNoCorrientesCuantiaUnoDos;
    }

    public String getPartActivosNoCorrientesPorcentajeUnoDos() {
        if (partActivosNoCorrientesPorcentajeUnoDos == null) {
            partActivosNoCorrientesPorcentajeUnoDos = "";
        }
        return partActivosNoCorrientesPorcentajeUnoDos;
    }

    public String getPartActivosNoCorrientesCuantiaUnoTres() {
        if (partActivosNoCorrientesCuantiaUnoTres == null) {
            partActivosNoCorrientesCuantiaUnoTres = "";
        }
        return partActivosNoCorrientesCuantiaUnoTres;
    }

    public String getPartActivosNoCorrientesPorcentajeUnoTres() {
        if (partActivosNoCorrientesPorcentajeUnoTres == null) {
            partActivosNoCorrientesPorcentajeUnoTres = "";
        }
        return partActivosNoCorrientesPorcentajeUnoTres;
    }

    public String getPartOtrosActivosCorteUno() {
        if (partOtrosActivosCorteUno == null) {
            partOtrosActivosCorteUno = "";
        }
        return partOtrosActivosCorteUno;
    }

    public String getPartOtrosActivosCorteDos() {
        if (partOtrosActivosCorteDos == null) {
            partOtrosActivosCorteDos = "";
        }
        return partOtrosActivosCorteDos;
    }

    public String getPartOtrosActivosCorteTres() {
        if (partOtrosActivosCorteTres == null) {
            partOtrosActivosCorteTres = "";
        }
        return partOtrosActivosCorteTres;
    }

    public String getPartOtrosActivosCuantiaUnoDos() {
        if (partOtrosActivosCuantiaUnoDos == null) {
            partOtrosActivosCuantiaUnoDos = "";
        }
        return partOtrosActivosCuantiaUnoDos;
    }

    public String getPartOtrosActivosPorcentajeUnoDos() {
        if (partOtrosActivosPorcentajeUnoDos == null) {
            partOtrosActivosPorcentajeUnoDos = "";
        }
        return partOtrosActivosPorcentajeUnoDos;
    }

    public String getPartOtrosActivosCuantiaUnoTres() {
        if (partOtrosActivosCuantiaUnoTres == null) {
            partOtrosActivosCuantiaUnoTres = "";
        }
        return partOtrosActivosCuantiaUnoTres;
    }

    public String getPartOtrosActivosPorcentajeUnoTres() {
        if (partOtrosActivosPorcentajeUnoTres == null) {
            partOtrosActivosPorcentajeUnoTres = "";
        }
        return partOtrosActivosPorcentajeUnoTres;
    }

    public String getPartTotalActivosCorteUno() {
        if (partTotalActivosCorteUno == null) {
            partTotalActivosCorteUno = "";
        }
        return partTotalActivosCorteUno;
    }

    public String getPartTotalActivosCorteDos() {
        if (partTotalActivosCorteDos == null) {
            partTotalActivosCorteDos = "";
        }
        return partTotalActivosCorteDos;
    }

    public String getPartTotalActivosCorteTres() {
        if (partTotalActivosCorteTres == null) {
            partTotalActivosCorteTres = "";
        }
        return partTotalActivosCorteTres;
    }

    public String getPartTotalActivosCuantiaUnoDos() {
        if (partTotalActivosCuantiaUnoDos == null) {
            partTotalActivosCuantiaUnoDos = "";
        }
        return partTotalActivosCuantiaUnoDos;
    }

    public String getPartTotalActivosPorcentajeUnoDos() {
        if (partTotalActivosPorcentajeUnoDos == null) {
            partTotalActivosPorcentajeUnoDos = "";
        }
        return partTotalActivosPorcentajeUnoDos;
    }

    public String getPartTotalActivosCuantiaUnoTres() {
        if (partTotalActivosCuantiaUnoTres == null) {
            partTotalActivosCuantiaUnoTres = "";
        }
        return partTotalActivosCuantiaUnoTres;
    }

    public String getPartTotalActivosPorcentajeUnoTres() {
        if (partTotalActivosPorcentajeUnoTres == null) {
            partTotalActivosPorcentajeUnoTres = "";
        }
        return partTotalActivosPorcentajeUnoTres;
    }

    public String getPartPasivosCorteUno() {
        if (partPasivosCorteUno == null) {
            partPasivosCorteUno = "";
        }
        return partPasivosCorteUno;
    }

    public String getPartPasivosCorteDos() {
        if (partPasivosCorteDos == null) {
            partPasivosCorteDos = "";
        }
        return partPasivosCorteDos;
    }

    public String getPartPasivosCorteTres() {
        if (partPasivosCorteTres == null) {
            partPasivosCorteTres = "";
        }
        return partPasivosCorteTres;
    }

    public String getPartPasivosCuantiaUnoDos() {
        if (partPasivosCuantiaUnoDos == null) {
            partPasivosCuantiaUnoDos = "";
        }
        return partPasivosCuantiaUnoDos;
    }

    public String getPartPasivosPorcentajeUnoDos() {
        if (partPasivosPorcentajeUnoDos == null) {
            partPasivosPorcentajeUnoDos = "";
        }
        return partPasivosPorcentajeUnoDos;
    }

    public String getPartPasivosCuantiaUnoTres() {
        if (partPasivosCuantiaUnoTres == null) {
            partPasivosCuantiaUnoTres = "";
        }
        return partPasivosCuantiaUnoTres;
    }

    public String getPartPasivosPorcentajeUnoTres() {
        if (partPasivosPorcentajeUnoTres == null) {
            partPasivosPorcentajeUnoTres = "";
        }
        return partPasivosPorcentajeUnoTres;
    }

    public String getPartDepositosCorteUno() {
        if (partDepositosCorteUno == null) {
            partDepositosCorteUno = "";
        }
        return partDepositosCorteUno;
    }

    public String getPartDepositosCorteDos() {
        if (partDepositosCorteDos == null) {
            partDepositosCorteDos = "";
        }
        return partDepositosCorteDos;
    }

    public String getPartDepositosCorteTres() {
        if (partDepositosCorteTres == null) {
            partDepositosCorteTres = "";
        }
        return partDepositosCorteTres;
    }

    public String getPartDepositosCuantiaUnoDos() {
        if (partDepositosCuantiaUnoDos == null) {
            partDepositosCuantiaUnoDos = "";
        }
        return partDepositosCuantiaUnoDos;
    }

    public String getPartDepositosPorcentajeUnoDos() {
        if (partDepositosPorcentajeUnoDos == null) {
            partDepositosPorcentajeUnoDos = "";
        }
        return partDepositosPorcentajeUnoDos;
    }

    public String getPartDepositosCuantiaUnoTres() {
        if (partDepositosCuantiaUnoTres == null) {
            partDepositosCuantiaUnoTres = "";
        }
        return partDepositosCuantiaUnoTres;
    }

    public String getPartDepositosPorcentajeUnoTres() {
        if (partDepositosPorcentajeUnoTres == null) {
            partDepositosPorcentajeUnoTres = "";
        }
        return partDepositosPorcentajeUnoTres;
    }

    public String getPartDepositosAhorroCorteUno() {
        if (partDepositosAhorroCorteUno == null) {
            partDepositosAhorroCorteUno = "";
        }
        return partDepositosAhorroCorteUno;
    }

    public String getPartDepositosAhorroCorteDos() {
        if (partDepositosAhorroCorteDos == null) {
            partDepositosAhorroCorteDos = "";
        }
        return partDepositosAhorroCorteDos;
    }

    public String getPartDepositosAhorroCorteTres() {
        if (partDepositosAhorroCorteTres == null) {
            partDepositosAhorroCorteTres = "";
        }
        return partDepositosAhorroCorteTres;
    }

    public String getPartDepositosAhorroCuantiaUnoDos() {
        if (partDepositosAhorroCuantiaUnoDos == null) {
            partDepositosAhorroCuantiaUnoDos = "";
        }
        return partDepositosAhorroCuantiaUnoDos;
    }

    public String getPartDepositosAhorroPorcentajeUnoDos() {
        if (partDepositosAhorroPorcentajeUnoDos == null) {
            partDepositosAhorroPorcentajeUnoDos = "";
        }
        return partDepositosAhorroPorcentajeUnoDos;
    }

    public String getPartDepositosAhorroCuantiaUnoTres() {
        if (partDepositosAhorroCuantiaUnoTres == null) {
            partDepositosAhorroCuantiaUnoTres = "";
        }
        return partDepositosAhorroCuantiaUnoTres;
    }

    public String getPartDepositosAhorroPorcentajeUnoTres() {
        if (partDepositosAhorroPorcentajeUnoTres == null) {
            partDepositosAhorroPorcentajeUnoTres = "";
        }
        return partDepositosAhorroPorcentajeUnoTres;
    }

    public String getPartCdatCorteUno() {
        if (partCdatCorteUno == null) {
            partCdatCorteUno = "";
        }
        return partCdatCorteUno;
    }

    public String getPartCdatCorteDos() {
        if (partCdatCorteDos == null) {
            partCdatCorteDos = "";
        }
        return partCdatCorteDos;
    }

    public String getPartCdatCorteTres() {
        if (partCdatCorteTres == null) {
            partCdatCorteTres = "";
        }
        return partCdatCorteTres;
    }

    public String getPartCdatCuantiaUnoDos() {
        if (partCdatCuantiaUnoDos == null) {
            partCdatCuantiaUnoDos = "";
        }
        return partCdatCuantiaUnoDos;
    }

    public String getPartCdatPorcentajeUnoDos() {
        if (partCdatPorcentajeUnoDos == null) {
            partCdatPorcentajeUnoDos = "";
        }
        return partCdatPorcentajeUnoDos;
    }

    public String getPartCdatCuantiaUnoTres() {
        if (partCdatCuantiaUnoTres == null) {
            partCdatCuantiaUnoTres = "";
        }
        return partCdatCuantiaUnoTres;
    }

    public String getPartCdatPorcentajeUnoTres() {
        if (partCdatPorcentajeUnoTres == null) {
            partCdatPorcentajeUnoTres = "";
        }
        return partCdatPorcentajeUnoTres;
    }

    public String getPartAhorroContractualCorteUno() {
        if (partAhorroContractualCorteUno == null) {
            partAhorroContractualCorteUno = "";
        }
        return partAhorroContractualCorteUno;
    }

    public String getPartAhorroContractualCorteDos() {
        if (partAhorroContractualCorteDos == null) {
            partAhorroContractualCorteDos = "";
        }
        return partAhorroContractualCorteDos;
    }

    public String getPartAhorroContractualCorteTres() {
        if (partAhorroContractualCorteTres == null) {
            partAhorroContractualCorteTres = "";
        }
        return partAhorroContractualCorteTres;
    }

    public String getPartAhorroContractualCuantiaUnoDos() {
        if (partAhorroContractualCuantiaUnoDos == null) {
            partAhorroContractualCuantiaUnoDos = "";
        }
        return partAhorroContractualCuantiaUnoDos;
    }

    public String getPartAhorroContractualPorcentajeUnoDos() {
        if (partAhorroContractualPorcentajeUnoDos == null) {
            partAhorroContractualPorcentajeUnoDos = "";
        }
        return partAhorroContractualPorcentajeUnoDos;
    }

    public String getPartAhorroContractualCuantiaUnoTres() {
        if (partAhorroContractualCuantiaUnoTres == null) {
            partAhorroContractualCuantiaUnoTres = "";
        }
        return partAhorroContractualCuantiaUnoTres;
    }

    public String getPartAhorroContractualPorcentajeUnoTres() {
        if (partAhorroContractualPorcentajeUnoTres == null) {
            partAhorroContractualPorcentajeUnoTres = "";
        }
        return partAhorroContractualPorcentajeUnoTres;
    }

    public String getPartAhorroPermanenteCorteUno() {
        if (partAhorroPermanenteCorteUno == null) {
            partAhorroPermanenteCorteUno = "";
        }
        return partAhorroPermanenteCorteUno;
    }

    public String getPartAhorroPermanenteCorteDos() {
        if (partAhorroPermanenteCorteDos == null) {
            partAhorroPermanenteCorteDos = "";
        }
        return partAhorroPermanenteCorteDos;
    }

    public String getPartAhorroPermanenteCorteTres() {
        if (partAhorroPermanenteCorteTres == null) {
            partAhorroPermanenteCorteTres = "";
        }
        return partAhorroPermanenteCorteTres;
    }

    public String getPartAhorroPermanenteCuantiaUnoDos() {
        if (partAhorroPermanenteCuantiaUnoDos == null) {
            partAhorroPermanenteCuantiaUnoDos = "";
        }
        return partAhorroPermanenteCuantiaUnoDos;
    }

    public String getPartAhorroPermanentePorcentajeUnoDos() {
        if (partAhorroPermanentePorcentajeUnoDos == null) {
            partAhorroPermanentePorcentajeUnoDos = "";
        }
        return partAhorroPermanentePorcentajeUnoDos;
    }

    public String getPartAhorroPermanenteCuantiaUnoTres() {
        if (partAhorroPermanenteCuantiaUnoTres == null) {
            partAhorroPermanenteCuantiaUnoTres = "";
        }
        return partAhorroPermanenteCuantiaUnoTres;
    }

    public String getPartAhorroPermanentePorcentajeUnoTres() {
        if (partAhorroPermanentePorcentajeUnoTres == null) {
            partAhorroPermanentePorcentajeUnoTres = "";
        }
        return partAhorroPermanentePorcentajeUnoTres;
    }

    public String getPartTituloInversionCorteUno() {
        if (partTituloInversionCorteUno == null) {
            partTituloInversionCorteUno = "";
        }
        return partTituloInversionCorteUno;
    }

    public String getPartTituloInversionCorteDos() {
        if (partTituloInversionCorteDos == null) {
            partTituloInversionCorteDos = "";
        }
        return partTituloInversionCorteDos;
    }

    public String getPartTituloInversionCorteTres() {
        if (partTituloInversionCorteTres == null) {
            partTituloInversionCorteTres = "";
        }
        return partTituloInversionCorteTres;
    }

    public String getPartTituloInversionCuantiaUnoDos() {
        if (partTituloInversionCuantiaUnoDos == null) {
            partTituloInversionCuantiaUnoDos = "";
        }
        return partTituloInversionCuantiaUnoDos;
    }

    public String getPartTituloInversionPorcentajeUnoDos() {
        if (partTituloInversionPorcentajeUnoDos == null) {
            partTituloInversionPorcentajeUnoDos = "";
        }
        return partTituloInversionPorcentajeUnoDos;
    }

    public String getPartTituloInversionCuantiaUnoTres() {
        if (partTituloInversionCuantiaUnoTres == null) {
            partTituloInversionCuantiaUnoTres = "";
        }
        return partTituloInversionCuantiaUnoTres;
    }

    public String getPartTituloInversionPorcentajeUnoTres() {
        if (partTituloInversionPorcentajeUnoTres == null) {
            partTituloInversionPorcentajeUnoTres = "";
        }
        return partTituloInversionPorcentajeUnoTres;
    }

    public String getPartCreditosBancosCorteUno() {
        if (partCreditosBancosCorteUno == null) {
            partCreditosBancosCorteUno = "";
        }
        return partCreditosBancosCorteUno;
    }

    public String getPartCreditosBancosCorteDos() {
        if (partCreditosBancosCorteDos == null) {
            partCreditosBancosCorteDos = "";
        }
        return partCreditosBancosCorteDos;
    }

    public String getPartCreditosBancosCorteTres() {
        if (partCreditosBancosCorteTres == null) {
            partCreditosBancosCorteTres = "";
        }
        return partCreditosBancosCorteTres;
    }

    public String getPartCreditosBancosCuantiaUnoDos() {
        if (partCreditosBancosCuantiaUnoDos == null) {
            partCreditosBancosCuantiaUnoDos = "";
        }
        return partCreditosBancosCuantiaUnoDos;
    }

    public String getPartCreditosBancosPorcentajeUnoDos() {
        if (partCreditosBancosPorcentajeUnoDos == null) {
            partCreditosBancosPorcentajeUnoDos = "";
        }
        return partCreditosBancosPorcentajeUnoDos;
    }

    public String getPartCreditosBancosCuantiaUnoTres() {
        if (partCreditosBancosCuantiaUnoTres == null) {
            partCreditosBancosCuantiaUnoTres = "";
        }
        return partCreditosBancosCuantiaUnoTres;
    }

    public String getPartCreditosBancosPorcentajeUnoTres() {
        if (partCreditosBancosPorcentajeUnoTres == null) {
            partCreditosBancosPorcentajeUnoTres = "";
        }
        return partCreditosBancosPorcentajeUnoTres;
    }

    public String getPartCuentasPorPagarCorteUno() {
        if (partCuentasPorPagarCorteUno == null) {
            partCuentasPorPagarCorteUno = "";
        }
        return partCuentasPorPagarCorteUno;
    }

    public String getPartCuentasPorPagarCorteDos() {
        if (partCuentasPorPagarCorteDos == null) {
            partCuentasPorPagarCorteDos = "";
        }
        return partCuentasPorPagarCorteDos;
    }

    public String getPartCuentasPorPagarCorteTres() {
        if (partCuentasPorPagarCorteTres == null) {
            partCuentasPorPagarCorteTres = "";
        }
        return partCuentasPorPagarCorteTres;
    }

    public String getPartCuentasPorPagarCuantiaUnoDos() {
        if (partCuentasPorPagarCuantiaUnoDos == null) {
            partCuentasPorPagarCuantiaUnoDos = "";
        }
        return partCuentasPorPagarCuantiaUnoDos;
    }

    public String getPartCuentasPorPagarPorcentajeUnoDos() {
        if (partCuentasPorPagarPorcentajeUnoDos == null) {
            partCuentasPorPagarPorcentajeUnoDos = "";
        }
        return partCuentasPorPagarPorcentajeUnoDos;
    }

    public String getPartCuentasPorPagarCuantiaUnoTres() {
        if (partCuentasPorPagarCuantiaUnoTres == null) {
            partCuentasPorPagarCuantiaUnoTres = "";
        }
        return partCuentasPorPagarCuantiaUnoTres;
    }

    public String getPartCuentasPorPagarPorcentajeUnoTres() {
        if (partCuentasPorPagarPorcentajeUnoTres == null) {
            partCuentasPorPagarPorcentajeUnoTres = "";
        }
        return partCuentasPorPagarPorcentajeUnoTres;
    }

    public String getPartFondosSocialesCorteUno() {
        if (partFondosSocialesCorteUno == null) {
            partFondosSocialesCorteUno = "";
        }
        return partFondosSocialesCorteUno;
    }

    public String getPartFondosSocialesCorteDos() {
        if (partFondosSocialesCorteDos == null) {
            partFondosSocialesCorteDos = "";
        }
        return partFondosSocialesCorteDos;
    }

    public String getPartFondosSocialesCorteTres() {
        if (partFondosSocialesCorteTres == null) {
            partFondosSocialesCorteTres = "";
        }
        return partFondosSocialesCorteTres;
    }

    public String getPartFondosSocialesCuantiaUnoDos() {
        if (partFondosSocialesCuantiaUnoDos == null) {
            partFondosSocialesCuantiaUnoDos = "";
        }
        return partFondosSocialesCuantiaUnoDos;
    }

    public String getPartFondosSocialesPorcentajeUnoDos() {
        if (partFondosSocialesPorcentajeUnoDos == null) {
            partFondosSocialesPorcentajeUnoDos = "";
        }
        return partFondosSocialesPorcentajeUnoDos;
    }

    public String getPartFondosSocialesCuantiaUnoTres() {
        if (partFondosSocialesCuantiaUnoTres == null) {
            partFondosSocialesCuantiaUnoTres = "";
        }
        return partFondosSocialesCuantiaUnoTres;
    }

    public String getPartFondosSocialesPorcentajeUnoTres() {
        if (partFondosSocialesPorcentajeUnoTres == null) {
            partFondosSocialesPorcentajeUnoTres = "";
        }
        return partFondosSocialesPorcentajeUnoTres;
    }

    public String getPartProvisionesCorteUno() {
        if (partProvisionesCorteUno == null) {
            partProvisionesCorteUno = "";
        }
        return partProvisionesCorteUno;
    }

    public String getPartProvisionesCorteDos() {
        if (partProvisionesCorteDos == null) {
            partProvisionesCorteDos = "";
        }
        return partProvisionesCorteDos;
    }

    public String getPartProvisionesCorteTres() {
        if (partProvisionesCorteTres == null) {
            partProvisionesCorteTres = "";
        }
        return partProvisionesCorteTres;
    }

    public String getPartProvisionesCuantiaUnoDos() {
        if (partProvisionesCuantiaUnoDos == null) {
            partProvisionesCuantiaUnoDos = "";
        }
        return partProvisionesCuantiaUnoDos;
    }

    public String getPartProvisionesPorcentajeUnoDos() {
        if (partProvisionesPorcentajeUnoDos == null) {
            partProvisionesPorcentajeUnoDos = "";
        }
        return partProvisionesPorcentajeUnoDos;
    }

    public String getPartProvisionesCuantiaUnoTres() {
        if (partProvisionesCuantiaUnoTres == null) {
            partProvisionesCuantiaUnoTres = "";
        }
        return partProvisionesCuantiaUnoTres;
    }

    public String getPartProvisionesPorcentajeUnoTres() {
        if (partProvisionesPorcentajeUnoTres == null) {
            partProvisionesPorcentajeUnoTres = "";
        }
        return partProvisionesPorcentajeUnoTres;
    }

    public String getPartAportesSocialesCorteUno() {
        if (partAportesSocialesCorteUno == null) {
            partAportesSocialesCorteUno = "";
        }
        return partAportesSocialesCorteUno;
    }

    public String getPartAportesSocialesCorteDos() {
        if (partAportesSocialesCorteDos == null) {
            partAportesSocialesCorteDos = "";
        }
        return partAportesSocialesCorteDos;
    }

    public String getPartAportesSocialesCorteTres() {
        if (partAportesSocialesCorteTres == null) {
            partAportesSocialesCorteTres = "";
        }
        return partAportesSocialesCorteTres;
    }

    public String getPartAportesSocialesCuantiaUnoDos() {
        if (partAportesSocialesCuantiaUnoDos == null) {
            partAportesSocialesCuantiaUnoDos = "";
        }
        return partAportesSocialesCuantiaUnoDos;
    }

    public String getPartAportesSocialesPorcentajeUnoDos() {
        if (partAportesSocialesPorcentajeUnoDos == null) {
            partAportesSocialesPorcentajeUnoDos = "";
        }
        return partAportesSocialesPorcentajeUnoDos;
    }

    public String getPartAportesSocialesCuantiaUnoTres() {
        if (partAportesSocialesCuantiaUnoTres == null) {
            partAportesSocialesCuantiaUnoTres = "";
        }
        return partAportesSocialesCuantiaUnoTres;
    }

    public String getPartAportesSocialesPorcentajeUnoTres() {
        if (partAportesSocialesPorcentajeUnoTres == null) {
            partAportesSocialesPorcentajeUnoTres = "";
        }
        return partAportesSocialesPorcentajeUnoTres;
    }

    public String getPartTotalPasivosCorteUno() {
        if (partTotalPasivosCorteUno == null) {
            partTotalPasivosCorteUno = "";
        }
        return partTotalPasivosCorteUno;
    }

    public String getPartTotalPasivosCorteDos() {
        if (partTotalPasivosCorteDos == null) {
            partTotalPasivosCorteDos = "";
        }
        return partTotalPasivosCorteDos;
    }

    public String getPartTotalPasivosCorteTres() {
        if (partTotalPasivosCorteTres == null) {
            partTotalPasivosCorteTres = "";
        }
        return partTotalPasivosCorteTres;
    }

    public String getPartTotalPasivosCuantiaUnoDos() {
        if (partTotalPasivosCuantiaUnoDos == null) {
            partTotalPasivosCuantiaUnoDos = "";
        }
        return partTotalPasivosCuantiaUnoDos;
    }

    public String getPartTotalPasivosPorcentajeUnoDos() {
        if (partTotalPasivosPorcentajeUnoDos == null) {
            partTotalPasivosPorcentajeUnoDos = "";
        }
        return partTotalPasivosPorcentajeUnoDos;
    }

    public String getPartTotalPasivosCuantiaUnoTres() {
        if (partTotalPasivosCuantiaUnoTres == null) {
            partTotalPasivosCuantiaUnoTres = "";
        }
        return partTotalPasivosCuantiaUnoTres;
    }

    public String getPartTotalPasivosPorcentajeUnoTres() {
        if (partTotalPasivosPorcentajeUnoTres == null) {
            partTotalPasivosPorcentajeUnoTres = "";
        }
        return partTotalPasivosPorcentajeUnoTres;
    }

    public String getPartPatrimonioCorteUno() {
        if (partPatrimonioCorteUno == null) {
            partPatrimonioCorteUno = "";
        }
        return partPatrimonioCorteUno;
    }

    public String getPartPatrimonioCorteDos() {
        if (partPatrimonioCorteDos == null) {
            partPatrimonioCorteDos = "";
        }
        return partPatrimonioCorteDos;
    }

    public String getPartPatrimonioCorteTres() {
        if (partPatrimonioCorteTres == null) {
            partPatrimonioCorteTres = "";
        }
        return partPatrimonioCorteTres;
    }

    public String getPartPatrimonioCuantiaUnoDos() {
        if (partPatrimonioCuantiaUnoDos == null) {
            partPatrimonioCuantiaUnoDos = "";
        }
        return partPatrimonioCuantiaUnoDos;
    }

    public String getPartPatrimonioPorcentajeUnoDos() {
        if (partPatrimonioPorcentajeUnoDos == null) {
            partPatrimonioPorcentajeUnoDos = "";
        }
        return partPatrimonioPorcentajeUnoDos;
    }

    public String getPartPatrimonioCuantiaUnoTres() {
        if (partPatrimonioCuantiaUnoTres == null) {
            partPatrimonioCuantiaUnoTres = "";
        }
        return partPatrimonioCuantiaUnoTres;
    }

    public String getPartPatrimonioPorcentajeUnoTres() {
        if (partPatrimonioPorcentajeUnoTres == null) {
            partPatrimonioPorcentajeUnoTres = "";
        }
        return partPatrimonioPorcentajeUnoTres;
    }

    public String getPartCapitalSocialCorteUno() {
        if (partCapitalSocialCorteUno == null) {
            partCapitalSocialCorteUno = "";
        }
        return partCapitalSocialCorteUno;
    }

    public String getPartCapitalSocialCorteDos() {
        if (partCapitalSocialCorteDos == null) {
            partCapitalSocialCorteDos = "";
        }
        return partCapitalSocialCorteDos;
    }

    public String getPartCapitalSocialCorteTres() {
        if (partCapitalSocialCorteTres == null) {
            partCapitalSocialCorteTres = "";
        }
        return partCapitalSocialCorteTres;
    }

    public String getPartCapitalSocialCuantiaUnoDos() {
        if (partCapitalSocialCuantiaUnoDos == null) {
            partCapitalSocialCuantiaUnoDos = "";
        }
        return partCapitalSocialCuantiaUnoDos;
    }

    public String getPartCapitalSocialPorcentajeUnoDos() {
        if (partCapitalSocialPorcentajeUnoDos == null) {
            partCapitalSocialPorcentajeUnoDos = "";
        }
        return partCapitalSocialPorcentajeUnoDos;
    }

    public String getPartCapitalSocialCuantiaUnoTres() {
        if (partCapitalSocialCuantiaUnoTres == null) {
            partCapitalSocialCuantiaUnoTres = "";
        }
        return partCapitalSocialCuantiaUnoTres;
    }

    public String getPartCapitalSocialPorcentajeUnoTres() {
        if (partCapitalSocialPorcentajeUnoTres == null) {
            partCapitalSocialPorcentajeUnoTres = "";
        }
        return partCapitalSocialPorcentajeUnoTres;
    }

    public String getPartReservasCorteUno() {
        if (partReservasCorteUno == null) {
            partReservasCorteUno = "";
        }
        return partReservasCorteUno;
    }

    public String getPartReservasCorteDos() {
        if (partReservasCorteDos == null) {
            partReservasCorteDos = "";
        }
        return partReservasCorteDos;
    }

    public String getPartReservasCorteTres() {
        if (partReservasCorteTres == null) {
            partReservasCorteTres = "";
        }
        return partReservasCorteTres;
    }

    public String getPartReservasCuantiaUnoDos() {
        if (partReservasCuantiaUnoDos == null) {
            partReservasCuantiaUnoDos = "";
        }
        return partReservasCuantiaUnoDos;
    }

    public String getPartReservasPorcentajeUnoDos() {
        if (partReservasPorcentajeUnoDos == null) {
            partReservasPorcentajeUnoDos = "";
        }
        return partReservasPorcentajeUnoDos;
    }

    public String getPartReservasCuantiaUnoTres() {
        if (partReservasCuantiaUnoTres == null) {
            partReservasCuantiaUnoTres = "";
        }
        return partReservasCuantiaUnoTres;
    }

    public String getPartReservasPorcentajeUnoTres() {
        if (partReservasPorcentajeUnoTres == null) {
            partReservasPorcentajeUnoTres = "";
        }
        return partReservasPorcentajeUnoTres;
    }

    public String getPartFondosDestinacionCorteUno() {
        if (partFondosDestinacionCorteUno == null) {
            partFondosDestinacionCorteUno = "";
        }
        return partFondosDestinacionCorteUno;
    }

    public String getPartFondosDestinacionCorteDos() {
        if (partFondosDestinacionCorteDos == null) {
            partFondosDestinacionCorteDos = "";
        }
        return partFondosDestinacionCorteDos;
    }

    public String getPartFondosDestinacionCorteTres() {
        if (partFondosDestinacionCorteTres == null) {
            partFondosDestinacionCorteTres = "";
        }
        return partFondosDestinacionCorteTres;
    }

    public String getPartFondosDestinacionCuantiaUnoDos() {
        if (partFondosDestinacionCuantiaUnoDos == null) {
            partFondosDestinacionCuantiaUnoDos = "";
        }
        return partFondosDestinacionCuantiaUnoDos;
    }

    public String getPartFondosDestinacionPorcentajeUnoDos() {
        if (partFondosDestinacionPorcentajeUnoDos == null) {
            partFondosDestinacionPorcentajeUnoDos = "";
        }
        return partFondosDestinacionPorcentajeUnoDos;
    }

    public String getPartFondosDestinacionCuantiaUnoTres() {
        if (partFondosDestinacionCuantiaUnoTres == null) {
            partFondosDestinacionCuantiaUnoTres = "";
        }
        return partFondosDestinacionCuantiaUnoTres;
    }

    public String getPartFondosDestinacionPorcentajeUnoTres() {
        if (partFondosDestinacionPorcentajeUnoTres == null) {
            partFondosDestinacionPorcentajeUnoTres = "";
        }
        return partFondosDestinacionPorcentajeUnoTres;
    }

    public String getPartSuperavitCorteUno() {
        if (partSuperavitCorteUno == null) {
            partSuperavitCorteUno = "";
        }
        return partSuperavitCorteUno;
    }

    public String getPartSuperavitCorteDos() {
        if (partSuperavitCorteDos == null) {
            partSuperavitCorteDos = "";
        }
        return partSuperavitCorteDos;
    }

    public String getPartSuperavitCorteTres() {
        if (partSuperavitCorteTres == null) {
            partSuperavitCorteTres = "";
        }
        return partSuperavitCorteTres;
    }

    public String getPartSuperavitCuantiaUnoDos() {
        if (partSuperavitCuantiaUnoDos == null) {
            partSuperavitCuantiaUnoDos = "";
        }
        return partSuperavitCuantiaUnoDos;
    }

    public String getPartSuperavitPorcentajeUnoDos() {
        if (partSuperavitPorcentajeUnoDos == null) {
            partSuperavitPorcentajeUnoDos = "";
        }
        return partSuperavitPorcentajeUnoDos;
    }

    public String getPartSuperavitCuantiaUnoTres() {
        if (partSuperavitCuantiaUnoTres == null) {
            partSuperavitCuantiaUnoTres = "";
        }
        return partSuperavitCuantiaUnoTres;
    }

    public String getPartSuperavitPorcentajeUnoTres() {
        if (partSuperavitPorcentajeUnoTres == null) {
            partSuperavitPorcentajeUnoTres = "";
        }
        return partSuperavitPorcentajeUnoTres;
    }

    public String getPartResultadosEjeAnterioresCorteUno() {
        if (partResultadosEjeAnterioresCorteUno == null) {
            partResultadosEjeAnterioresCorteUno = "";
        }
        return partResultadosEjeAnterioresCorteUno;
    }

    public String getPartResultadosEjeAnterioresCorteDos() {
        if (partResultadosEjeAnterioresCorteDos == null) {
            partResultadosEjeAnterioresCorteDos = "";
        }
        return partResultadosEjeAnterioresCorteDos;
    }

    public String getPartResultadosEjeAnterioresCorteTres() {
        if (partResultadosEjeAnterioresCorteTres == null) {
            partResultadosEjeAnterioresCorteTres = "";
        }
        return partResultadosEjeAnterioresCorteTres;
    }

    public String getPartResultadosEjeAnterioresCuantiaUnoDos() {
        if (partResultadosEjeAnterioresCuantiaUnoDos == null) {
            partResultadosEjeAnterioresCuantiaUnoDos = "";
        }
        return partResultadosEjeAnterioresCuantiaUnoDos;
    }

    public String getPartResultadosEjeAnterioresPorcentajeUnoDos() {
        if (partResultadosEjeAnterioresPorcentajeUnoDos == null) {
            partResultadosEjeAnterioresPorcentajeUnoDos = "";
        }
        return partResultadosEjeAnterioresPorcentajeUnoDos;
    }

    public String getPartResultadosEjeAnterioresCuantiaUnoTres() {
        if (partResultadosEjeAnterioresCuantiaUnoTres == null) {
            partResultadosEjeAnterioresCuantiaUnoTres = "";
        }
        return partResultadosEjeAnterioresCuantiaUnoTres;
    }

    public String getPartResultadosEjeAnterioresPorcentajeUnoTres() {
        if (partResultadosEjeAnterioresPorcentajeUnoTres == null) {
            partResultadosEjeAnterioresPorcentajeUnoTres = "";
        }
        return partResultadosEjeAnterioresPorcentajeUnoTres;
    }

    public String getPartResultadosEjercicioCorteUno() {
        if (partResultadosEjercicioCorteUno == null) {
            partResultadosEjercicioCorteUno = "";
        }
        return partResultadosEjercicioCorteUno;
    }

    public String getPartResultadosEjercicioCorteDos() {
        if (partResultadosEjercicioCorteDos == null) {
            partResultadosEjercicioCorteDos = "";
        }
        return partResultadosEjercicioCorteDos;
    }

    public String getPartResultadosEjercicioCorteTres() {
        if (partResultadosEjercicioCorteTres == null) {
            partResultadosEjercicioCorteTres = "";
        }
        return partResultadosEjercicioCorteTres;
    }

    public String getPartResultadosEjercicioCuantiaUnoDos() {
        if (partResultadosEjercicioCuantiaUnoDos == null) {
            partResultadosEjercicioCuantiaUnoDos = "";
        }
        return partResultadosEjercicioCuantiaUnoDos;
    }

    public String getPartResultadosEjercicioPorcentajeUnoDos() {
        if (partResultadosEjercicioPorcentajeUnoDos == null) {
            partResultadosEjercicioPorcentajeUnoDos = "";
        }
        return partResultadosEjercicioPorcentajeUnoDos;
    }

    public String getPartResultadosEjercicioCuantiaUnoTres() {
        if (partResultadosEjercicioCuantiaUnoTres == null) {
            partResultadosEjercicioCuantiaUnoTres = "";
        }
        return partResultadosEjercicioCuantiaUnoTres;
    }

    public String getPartResultadosEjercicioPorcentajeUnoTres() {
        if (partResultadosEjercicioPorcentajeUnoTres == null) {
            partResultadosEjercicioPorcentajeUnoTres = "";
        }
        return partResultadosEjercicioPorcentajeUnoTres;
    }

    public String getPartResultadosOriCorteUno() {
        if (partResultadosOriCorteUno == null) {
            partResultadosOriCorteUno = "";
        }
        return partResultadosOriCorteUno;
    }

    public String getPartResultadosOriCorteDos() {
        if (partResultadosOriCorteDos == null) {
            partResultadosOriCorteDos = "";
        }
        return partResultadosOriCorteDos;
    }

    public String getPartResultadosOriCorteTres() {
        if (partResultadosOriCorteTres == null) {
            partResultadosOriCorteTres = "";
        }
        return partResultadosOriCorteTres;
    }

    public String getPartResultadosOriCuantiaUnoDos() {
        if (partResultadosOriCuantiaUnoDos == null) {
            partResultadosOriCuantiaUnoDos = "";
        }
        return partResultadosOriCuantiaUnoDos;
    }

    public String getPartResultadosOriPorcentajeUnoDos() {
        if (partResultadosOriPorcentajeUnoDos == null) {
            partResultadosOriPorcentajeUnoDos = "";
        }
        return partResultadosOriPorcentajeUnoDos;
    }

    public String getPartResultadosOriCuantiaUnoTres() {
        if (partResultadosOriCuantiaUnoTres == null) {
            partResultadosOriCuantiaUnoTres = "";
        }
        return partResultadosOriCuantiaUnoTres;
    }

    public String getPartResultadosOriPorcentajeUnoTres() {
        if (partResultadosOriPorcentajeUnoTres == null) {
            partResultadosOriPorcentajeUnoTres = "";
        }
        return partResultadosOriPorcentajeUnoTres;
    }

    public String getPartTotalPatrimonioCorteUno() {
        if (partTotalPatrimonioCorteUno == null) {
            partTotalPatrimonioCorteUno = "";
        }
        return partTotalPatrimonioCorteUno;
    }

    public String getPartTotalPatrimonioCorteDos() {
        if (partTotalPatrimonioCorteDos == null) {
            partTotalPatrimonioCorteDos = "";
        }
        return partTotalPatrimonioCorteDos;
    }

    public String getPartTotalPatrimonioCorteTres() {
        if (partTotalPatrimonioCorteTres == null) {
            partTotalPatrimonioCorteTres = "";
        }
        return partTotalPatrimonioCorteTres;
    }

    public String getPartTotalPatrimonioCuantiaUnoDos() {
        if (partTotalPatrimonioCuantiaUnoDos == null) {
            partTotalPatrimonioCuantiaUnoDos = "";
        }
        return partTotalPatrimonioCuantiaUnoDos;
    }

    public String getPartTotalPatrimonioPorcentajeUnoDos() {
        if (partTotalPatrimonioPorcentajeUnoDos == null) {
            partTotalPatrimonioPorcentajeUnoDos = "";
        }
        return partTotalPatrimonioPorcentajeUnoDos;
    }

    public String getPartTotalPatrimonioCuantiaUnoTres() {
        if (partTotalPatrimonioCuantiaUnoTres == null) {
            partTotalPatrimonioCuantiaUnoTres = "";
        }
        return partTotalPatrimonioCuantiaUnoTres;
    }

    public String getPartTotalPatrimonioPorcentajeUnoTres() {
        if (partTotalPatrimonioPorcentajeUnoTres == null) {
            partTotalPatrimonioPorcentajeUnoTres = "";
        }
        return partTotalPatrimonioPorcentajeUnoTres;
    }

}
