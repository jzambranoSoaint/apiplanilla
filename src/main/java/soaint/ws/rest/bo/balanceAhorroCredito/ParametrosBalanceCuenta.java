package soaint.ws.rest.bo.balanceAhorroCredito;

public class ParametrosBalanceCuenta {
    private BalanceCuenta balanceCuenta;

    public BalanceCuenta getBalanceCuenta() {
        if (balanceCuenta == null) {
            balanceCuenta = new BalanceCuenta();
        }
        return balanceCuenta;
    }

    public void setBalanceCuenta(BalanceCuenta balanceCuenta) {
        this.balanceCuenta = balanceCuenta;
    }
}
