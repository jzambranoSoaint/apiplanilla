package soaint.ws.rest.bo.matAyC;

public class ParametrosOtrosIndicadores {
    private OtrosIndicadores otrosIndicadores;

    public OtrosIndicadores getOtrosIndicadores() {
        if (otrosIndicadores == null) {
            otrosIndicadores = new OtrosIndicadores();
        }
        return otrosIndicadores;
    }

    public void setOtrosIndicadores(OtrosIndicadores otrosIndicadores) {
        this.otrosIndicadores = otrosIndicadores;
    }
}
