package soaint.ws.rest.bo.matAyC;

public class ModeloAlertaTemprana {
    private String labelCorteUno;
    private String labelCorteDos;
    private String labelCorteTres;
    private String labelCorteCuatro;
    private String labelCorteCinco;
    private String labelCorteSeis;
    private String labelCorteSiete;
    private String labelCorteOcho;
    private String labelCorteNueve;
    private String labelCorteDiez;
    private String labelCorteOnce;
    private String labelCorteDoce;
    private String labelCorteTrece;
    private String probabilidadQuiebraCorteUno;
    private String probabilidadQuiebraCorteDos;
    private String probabilidadQuiebraCorteTres;
    private String probabilidadQuiebraCorteCuatro;
    private String probabilidadQuiebraCorteCinco;
    private String probabilidadQuiebraCorteSeis;
    private String probabilidadQuiebraCorteSiete;
    private String probabilidadQuiebraCorteOcho;
    private String probabilidadQuiebraCorteNueve;
    private String probabilidadQuiebraCorteDiez;
    private String probabilidadQuiebraCorteOnce;
    private String probabilidadQuiebraCorteDoce;
    private String probabilidadQuiebraCorteTrece;
    private String sufucienciaMargenCorteUno;
    private String sufucienciaMargenCorteDos;
    private String sufucienciaMargenCorteTres;
    private String sufucienciaMargenCorteCuatro;
    private String sufucienciaMargenCorteCinco;
    private String sufucienciaMargenCorteSeis;
    private String sufucienciaMargenCorteSiete;
    private String sufucienciaMargenCorteOcho;
    private String sufucienciaMargenCorteNueve;
    private String sufucienciaMargenCorteDiez;
    private String sufucienciaMargenCorteOnce;
    private String sufucienciaMargCorteDocene;
    private String sufucienciaMargenCorteTrece;
    private String depositosCarteraCorteUno;
    private String depositosCarteraCorteDos;
    private String depositosCarteraCorteTres;
    private String depositosCarteraCorteCuatro;
    private String depositosCarteraCorteCinco;
    private String depositosCarteraCorteSeis;
    private String depositosCarteraCorteSiete;
    private String depositosCarteraCorteOcho;
    private String depositosCarteraCorteNueve;
    private String depositosCarteraCorteDiez;
    private String depositosCarteraCorteOnce;
    private String depositosCarteraCorteDoce;
    private String depositosCarteraCorteTrece;
    private String margenTotalCorteUno;
    private String margenTotalCorteDos;
    private String margenTotalCorteTres;
    private String margenTotalCorteCuatro;
    private String margenTotalCorteCinco;
    private String margenTotalCorteSeis;
    private String margenTotalCorteSiete;
    private String margenTotalCorteOcho;
    private String margenTotalCorteNueve;
    private String margenTotalCorteDiez;
    private String margenTotalCorteOnce;
    private String margenTotalCorteDoce;
    private String margenTotalCorteTrece;
    private String promedioCalCameCorteUno;
    private String promedioCalCameCorteDos;
    private String promedioCalCameCorteTres;
    private String promedioCalCameCorteCuatro;
    private String promedioCalCameCorteCinco;
    private String promedioCalCameCorteSeis;
    private String promedioCalCameCorteSiete;
    private String promedioCalCameCorteOcho;
    private String promedioCalCameCorteNueve;
    private String promedioCalCameCorteDiez;
    private String promedioCalCameCorteOnce;
    private String promedioCalCameCorteDoce;
    private String promedioCalCameCorteTrece;
    private String indCameMargenSolvenciaCorteUno;
    private String indCameMargenSolvenciaCorteDos;
    private String indCameMargenSolvenciaCorteTres;
    private String indCameMargenSolvenciaCorteCuatro;
    private String indCameMargenSolvenciaCorteCinco;
    private String indCameMargenSolvenciaCorteSeis;
    private String indCameMargenSolvenciaCorteSiete;
    private String indCameMargenSolvenciaCorteOcho;
    private String indCameMargenSolvenciaCorteNueve;
    private String indCameMargenSolvenciaCorteDiez;
    private String indCameMargenSolvenciaCorteOnce;
    private String indCameMargenSolvenciaCorteDoce;
    private String indCameMargenSolvenciaCorteTrece;
    private String indCameCarteraVencidaCorteUno;
    private String indCameCarteraVencidaCorteDos;
    private String indCameCarteraVencidaCorteTres;
    private String indCameCarteraVencidaCorteCuatro;
    private String indCameCarteraVencidaCorteCinco;
    private String indCameCarteraVencidaCorteSeis;
    private String indCameCarteraVencidaCorteSiete;
    private String indCameCarteraVencidaCorteOcho;
    private String indCameCarteraVencidaCorteNueve;
    private String indCameCarteraVencidaCorteDiez;
    private String indCameCarteraVencidaCorteOnce;
    private String indCameCarteraVencidaCorteDoce;
    private String indCameCarteraVencidaCorteTrece;
    private String indCameMargenTotalCorteUno;
    private String indCameMargenTotalCorteDos;
    private String indCameMargenTotalCorteTres;
    private String indCameMargenTotalCorteCuatro;
    private String indCameMargenTotalCorteCinco;
    private String indCameMargenTotalCorteSeis;
    private String indCameMargenTotalCorteSiete;
    private String indCameMargenTotalCorteOcho;
    private String indCameMargenTotalCorteNueve;
    private String indCameMargenTotalCorteDiez;
    private String indCameMargenTotalCorteOnce;
    private String indCameMargenTotalCorteDoce;
    private String indCameMargenTotalCorteTrece;
    private String indCameActivoPasivoCorteUno;
    private String indCameActivoPasivoCorteDos;
    private String indCameActivoPasivoCorteTres;
    private String indCameActivoPasivoCorteCuatro;
    private String indCameActivoPasivoCorteCinco;
    private String indCameActivoPasivoCorteSeis;
    private String indCameActivoPasivoCorteSiete;
    private String indCameActivoPasivoCorteOcho;
    private String indCameActivoPasivoCorteNueve;
    private String indCameActivoPasivoCorteDiez;
    private String indCameActivoPasivoCorteOnce;
    private String indCameActivoPasivoCorteDoce;
    private String indCameActivoPasivoCorteTrece;
    private String indCameCostoAgenciaCorteUno;
    private String indCameCostoAgenciaCorteDos;
    private String indCameCostoAgenciaCorteTres;
    private String indCameCostoAgenciaCorteCuatro;
    private String indCameCostoAgenciaCorteCinco;
    private String indCameCostoAgenciaCorteSeis;
    private String indCameCostoAgenciaCorteSiete;
    private String indCameCostoAgenciaCorteOcho;
    private String indCameCostoAgenciaCorteNueve;
    private String indCameCostoAgenciaCorteDiez;
    private String indCameCostoAgenciaCorteOnce;
    private String indCameCostoAgenciaCorteDoce;
    private String indCameCostoAgenciaCorteTrece;
    private String indCameDepositosCarteraCorteUno;
    private String indCameDepositosCarteraCorteDos;
    private String indCameDepositosCarteraCorteTres;
    private String indCameDepositosCarteraCorteCuatro;
    private String indCameDepositosCarteraCorteCinco;
    private String indCameDepositosCarteraCorteSeis;
    private String indCameDepositosCarteraCorteSiete;
    private String indCameDepositosCarteraCorteOcho;
    private String indCameDepositosCarteraCorteNueve;
    private String indCameDepositosCarteraCorteDiez;
    private String indCameDepositosCarteraCorteOnce;
    private String indCameDepositosCarteraCorteDoce;
    private String indCameDepositosCarteraCorteTrece;
    private String indCameCarteraActivosCorteUno;
    private String indCameCarteraActivosCorteDos;
    private String indCameCarteraActivosCorteTres;
    private String indCameCarteraActivosCorteCuatro;
    private String indCameCarteraActivosCorteCinco;
    private String indCameCarteraActivosCorteSeis;
    private String indCameCarteraActivosCorteSiete;
    private String indCameCarteraActivosCorteOcho;
    private String indCameCarteraActivosCorteNueve;
    private String indCameCarteraActivosCorteDiez;
    private String indCameCarteraActivosCorteOnce;
    private String indCameCarteraActivosCorteDoce;
    private String indCameCarteraActivosCorteTrece;
    private String indCameActivoFijoCorteUno;
    private String indCameActivoFijoCorteDos;
    private String indCameActivoFijoCorteTres;
    private String indCameActivoFijoCorteCuatro;
    private String indCameActivoFijoCorteCinco;
    private String indCameActivoFijoCorteSeis;
    private String indCameActivoFijoCorteSiete;
    private String indCameActivoFijoCorteOcho;
    private String indCameActivoFijoCorteNueve;
    private String indCameActivoFijoCorteDiez;
    private String indCameActivoFijoCorteOnce;
    private String indCameActivoFijoCorteDoce;
    private String indCameActivoFijoCorteTrece;
    private String indCameSuficienciaCorteUno;
    private String indCameSuficienciaCorteDos;
    private String indCameSuficienciaCorteTres;
    private String indCameSuficienciaCorteCuatro;
    private String indCameSuficienciaCorteCinco;
    private String indCameSuficienciaCorteSeis;
    private String indCameSuficienciaCorteSiete;
    private String indCameSuficienciaCorteOcho;
    private String indCameSuficienciaCorteNueve;
    private String indCameSuficienciaCorteDiez;
    private String indCameSuficienciaCorteOnce;
    private String indCameSuficienciaCorteDoce;
    private String indCameSuficienciaCorteTrece;
    private String indCameMargenSolvenciaMinCorteUno;
    private String indCameMargenSolvenciaMinCorteDos;
    private String indCameMargenSolvenciaMinCorteTres;
    private String indCameMargenSolvenciaMinCorteCuatro;
    private String indCameMargenSolvenciaMinCorteCinco;
    private String indCameMargenSolvenciaMinCorteSeis;
    private String indCameMargenSolvenciaMinCorteSiete;
    private String indCameMargenSolvenciaMinCorteOcho;
    private String indCameMargenSolvenciaMinCorteNueve;
    private String indCameMargenSolvenciaMinCorteDiez;
    private String indCameMargenSolvenciaMinCorteOnce;
    private String indCameMargenSolvenciaMinCorteDoce;
    private String indCameMargenSolvenciaMinCorteTrece;
    private String calCameMargenSolvenciaCorteUno;
    private String calCameMargenSolvenciaCorteDos;
    private String calCameMargenSolvenciaCorteTres;
    private String calCameMargenSolvenciaCorteCuatro;
    private String calCameMargenSolvenciaCorteCinco;
    private String calCameMargenSolvenciaCorteSeis;
    private String calCameMargenSolvenciaCorteSiete;
    private String calCameMargenSolvenciaCorteOcho;
    private String calCameMargenSolvenciaCorteNueve;
    private String calCameMargenSolvenciaCorteDiez;
    private String calCameMargenSolvenciaCorteOnce;
    private String calCameMargenSolvenciaCorteDoce;
    private String calCameMargenSolvenciaCorteTrece;
    private String calCameCarteraVencidaCorteUno;
    private String calCameCarteraVencidaCorteDos;
    private String calCameCarteraVencidaCorteTres;
    private String calCameCarteraVencidaCorteCuatro;
    private String calCameCarteraVencidaCorteCinco;
    private String calCameCarteraVencidaCorteSeis;
    private String calCameCarteraVencidaCorteSiete;
    private String calCameCarteraVencidaCorteOcho;
    private String calCameCarteraVencidaCorteNueve;
    private String calCameCarteraVencidaCorteDiez;
    private String calCameCarteraVencidaCorteOnce;
    private String calCameCarteraVencidaCorteDoce;
    private String calCameCarteraVencidaCorteTrece;
    private String calCameMargenTotalCorteUno;
    private String calCameMargenTotalCorteDos;
    private String calCameMargenTotalCorteTres;
    private String calCameMargenTotalCorteCuatro;
    private String calCameMargenTotalCorteCinco;
    private String calCameMargenTotalCorteSeis;
    private String calCameMargenTotalCorteSiete;
    private String calCameMargenTotalCorteOcho;
    private String calCameMargenTotalCorteNueve;
    private String calCameMargenTotalCorteDiez;
    private String calCameMargenTotalCorteOnce;
    private String calCameMargenTotalCorteDoce;
    private String calCameMargenTotalCorteTrece;
    private String calCameActivoPasivoCorteUno;
    private String calCameActivoPasivoCorteDos;
    private String calCameActivoPasivoCorteTres;
    private String calCameActivoPasivoCorteCuatro;
    private String calCameActivoPasivoCorteCinco;
    private String calCameActivoPasivoCorteSeis;
    private String calCameActivoPasivoCorteSiete;
    private String calCameActivoPasivoCorteOcho;
    private String calCameActivoPasivoCorteNueve;
    private String calCameActivoPasivoCorteDiez;
    private String calCameActivoPasivoCorteOnce;
    private String calCameActivoPasivoCorteDoce;
    private String calCameActivoPasivoCorteTrece;
    private String calCameCostoAgenciaCorteUno;
    private String calCameCostoAgenciaCorteDos;
    private String calCameCostoAgenciaCorteTres;
    private String calCameCostoAgenciaCorteCuatro;
    private String calCameCostoAgenciaCorteCinco;
    private String calCameCostoAgenciaCorteSeis;
    private String calCameCostoAgenciaCorteSiete;
    private String calCameCostoAgenciaCorteOcho;
    private String calCameCostoAgenciaCorteNueve;
    private String calCameCostoAgenciaCorteDiez;
    private String calCameCostoAgenciaCorteOnce;
    private String calCameCostoAgenciaCorteDoce;
    private String calCameCostoAgenciaCorteTrece;
    private String calCameDepositosCarteraCorteUno;
    private String calCameDepositosCarteraCorteDos;
    private String calCameDepositosCarteraCorteTres;
    private String calCameDepositosCarteraCorteCuatro;
    private String calCameDepositosCarteraCorteCinco;
    private String calCameDepositosCarteraCorteSeis;
    private String calCameDepositosCarteraCorteSiete;
    private String calCameDepositosCarteraCorteOcho;
    private String calCameDepositosCarteraCorteNueve;
    private String calCameDepositosCarteraCorteDiez;
    private String calCameDepositosCarteraCorteOnce;
    private String calCameDepositosCarteraCorteDoce;
    private String calCameDepositosCarteraCorteTrece;
    private String calCameCarteraActivosCorteUno;
    private String calCameCarteraActivosCorteDos;
    private String calCameCarteraActivosCorteTres;
    private String calCameCarteraActivosCorteCuatro;
    private String calCameCarteraActivosCorteCinco;
    private String calCameCarteraActivosCorteSeis;
    private String calCameCarteraActivosCorteSiete;
    private String calCameCarteraActivosCorteOcho;
    private String calCameCarteraActivosCorteNueve;
    private String calCameCarteraActivosCorteDiez;
    private String calCameCarteraActivosCorteOnce;
    private String calCameCarteraActivosCorteDoce;
    private String calCameCarteraActivosCorteTrece;
    private String calCameActivoFijoCorteUno;
    private String calCameActivoFijoCorteDos;
    private String calCameActivoFijoCorteTres;
    private String calCameActivoFijoCorteCuatro;
    private String calCameActivoFijoCorteCinco;
    private String calCameActivoFijoCorteSeis;
    private String calCameActivoFijoCorteSiete;
    private String calCameActivoFijoCorteOcho;
    private String calCameActivoFijoCorteNueve;
    private String calCameActivoFijoCorteDiez;
    private String calCameActivoFijoCorteOnce;
    private String calCameActivoFijoCorteDoce;
    private String calCameActivoFijoCorteTrece;
    private String calCameSuficienciaCorteUno;
    private String calCameSuficienciaCorteDos;
    private String calCameSuficienciaCorteTres;
    private String calCameSuficienciaCorteCuatro;
    private String calCameSuficienciaCorteCinco;
    private String calCameSuficienciaCorteSeis;
    private String calCameSuficienciaCorteSiete;
    private String calCameSuficienciaCorteOcho;
    private String calCameSuficienciaCorteNueve;
    private String calCameSuficienciaCorteDiez;
    private String calCameSuficienciaCorteOnce;
    private String calCameSuficienciaCorteDoce;
    private String calCameSuficienciaCorteTrece;

    public String getLabelCorteUno() {
        if (labelCorteUno == null) {
            labelCorteUno = "";
        }
        return labelCorteUno;
    }

    public String getLabelCorteDos() {
        if (labelCorteDos == null) {
            labelCorteDos = "";
        }
        return labelCorteDos;
    }

    public String getLabelCorteTres() {
        if (labelCorteTres == null) {
            labelCorteTres = "";
        }
        return labelCorteTres;
    }

    public String getLabelCorteCuatro() {
        if (labelCorteCuatro == null) {
            labelCorteCuatro = "";
        }
        return labelCorteCuatro;
    }

    public String getLabelCorteCinco() {
        if (labelCorteCinco == null) {
            labelCorteCinco = "";
        }
        return labelCorteCinco;
    }

    public String getLabelCorteSeis() {
        if (labelCorteSeis == null) {
            labelCorteSeis = "";
        }
        return labelCorteSeis;
    }

    public String getLabelCorteSiete() {
        if (labelCorteSiete == null) {
            labelCorteSiete = "";
        }
        return labelCorteSiete;
    }

    public String getLabelCorteOcho() {
        if (labelCorteOcho == null) {
            labelCorteOcho = "";
        }
        return labelCorteOcho;
    }

    public String getLabelCorteNueve() {
        if (labelCorteNueve == null) {
            labelCorteNueve = "";
        }
        return labelCorteNueve;
    }

    public String getLabelCorteDiez() {
        if (labelCorteDiez == null) {
            labelCorteDiez = "";
        }
        return labelCorteDiez;
    }

    public String getLabelCorteOnce() {
        if (labelCorteOnce == null) {
            labelCorteOnce = "";
        }
        return labelCorteOnce;
    }

    public String getLabelCorteDoce() {
        if (labelCorteDoce == null) {
            labelCorteDoce = "";
        }
        return labelCorteDoce;
    }

    public String getLabelCorteTrece() {
        if (labelCorteTrece == null) {
            labelCorteTrece = "";
        }
        return labelCorteTrece;
    }

    public String getProbabilidadQuiebraCorteUno() {
        if (probabilidadQuiebraCorteUno == null) {
            probabilidadQuiebraCorteUno = "";
        }
        return probabilidadQuiebraCorteUno;
    }

    public String getProbabilidadQuiebraCorteDos() {
        if (probabilidadQuiebraCorteDos == null) {
            probabilidadQuiebraCorteDos = "";
        }
        return probabilidadQuiebraCorteDos;
    }

    public String getProbabilidadQuiebraCorteTres() {
        if (probabilidadQuiebraCorteTres == null) {
            probabilidadQuiebraCorteTres = "";
        }
        return probabilidadQuiebraCorteTres;
    }

    public String getProbabilidadQuiebraCorteCuatro() {
        if (probabilidadQuiebraCorteCuatro == null) {
            probabilidadQuiebraCorteCuatro = "";
        }
        return probabilidadQuiebraCorteCuatro;
    }

    public String getProbabilidadQuiebraCorteCinco() {
        if (probabilidadQuiebraCorteCinco == null) {
            probabilidadQuiebraCorteCinco = "";
        }
        return probabilidadQuiebraCorteCinco;
    }

    public String getProbabilidadQuiebraCorteSeis() {
        if (probabilidadQuiebraCorteSeis == null) {
            probabilidadQuiebraCorteSeis = "";
        }
        return probabilidadQuiebraCorteSeis;
    }

    public String getProbabilidadQuiebraCorteSiete() {
        if (probabilidadQuiebraCorteSiete == null) {
            probabilidadQuiebraCorteSiete = "";
        }
        return probabilidadQuiebraCorteSiete;
    }

    public String getProbabilidadQuiebraCorteOcho() {
        if (probabilidadQuiebraCorteOcho == null) {
            probabilidadQuiebraCorteOcho = "";
        }
        return probabilidadQuiebraCorteOcho;
    }

    public String getProbabilidadQuiebraCorteNueve() {
        if (probabilidadQuiebraCorteNueve == null) {
            probabilidadQuiebraCorteNueve = "";
        }
        return probabilidadQuiebraCorteNueve;
    }

    public String getProbabilidadQuiebraCorteDiez() {
        if (probabilidadQuiebraCorteDiez == null) {
            probabilidadQuiebraCorteDiez = "";
        }
        return probabilidadQuiebraCorteDiez;
    }

    public String getProbabilidadQuiebraCorteOnce() {
        if (probabilidadQuiebraCorteOnce == null) {
            probabilidadQuiebraCorteOnce = "";
        }
        return probabilidadQuiebraCorteOnce;
    }

    public String getProbabilidadQuiebraCorteDoce() {
        if (probabilidadQuiebraCorteDoce == null) {
            probabilidadQuiebraCorteDoce = "";
        }
        return probabilidadQuiebraCorteDoce;
    }

    public String getProbabilidadQuiebraCorteTrece() {
        if (probabilidadQuiebraCorteTrece == null) {
            probabilidadQuiebraCorteTrece = "";
        }
        return probabilidadQuiebraCorteTrece;
    }

    public String getSufucienciaMargenCorteUno() {
        if (sufucienciaMargenCorteUno == null) {
            sufucienciaMargenCorteUno = "";
        }
        return sufucienciaMargenCorteUno;
    }

    public String getSufucienciaMargenCorteDos() {
        if (sufucienciaMargenCorteDos == null) {
            sufucienciaMargenCorteDos = "";
        }
        return sufucienciaMargenCorteDos;
    }

    public String getSufucienciaMargenCorteTres() {
        if (sufucienciaMargenCorteTres == null) {
            sufucienciaMargenCorteTres = "";
        }
        return sufucienciaMargenCorteTres;
    }

    public String getSufucienciaMargenCorteCuatro() {
        if (sufucienciaMargenCorteCuatro == null) {
            sufucienciaMargenCorteCuatro = "";
        }
        return sufucienciaMargenCorteCuatro;
    }

    public String getSufucienciaMargenCorteCinco() {
        if (sufucienciaMargenCorteCinco == null) {
            sufucienciaMargenCorteCinco = "";
        }
        return sufucienciaMargenCorteCinco;
    }

    public String getSufucienciaMargenCorteSeis() {
        if (sufucienciaMargenCorteSeis == null) {
            sufucienciaMargenCorteSeis = "";
        }
        return sufucienciaMargenCorteSeis;
    }

    public String getSufucienciaMargenCorteSiete() {
        if (sufucienciaMargenCorteSiete == null) {
            sufucienciaMargenCorteSiete = "";
        }
        return sufucienciaMargenCorteSiete;
    }

    public String getSufucienciaMargenCorteOcho() {
        if (sufucienciaMargenCorteOcho == null) {
            sufucienciaMargenCorteOcho = "";
        }
        return sufucienciaMargenCorteOcho;
    }

    public String getSufucienciaMargenCorteNueve() {
        if (sufucienciaMargenCorteNueve == null) {
            sufucienciaMargenCorteNueve = "";
        }
        return sufucienciaMargenCorteNueve;
    }

    public String getSufucienciaMargenCorteDiez() {
        if (sufucienciaMargenCorteDiez == null) {
            sufucienciaMargenCorteDiez = "";
        }
        return sufucienciaMargenCorteDiez;
    }

    public String getSufucienciaMargenCorteOnce() {
        if (sufucienciaMargenCorteOnce == null) {
            sufucienciaMargenCorteOnce = "";
        }
        return sufucienciaMargenCorteOnce;
    }

    public String getSufucienciaMargCorteDocene() {
        if (sufucienciaMargCorteDocene == null) {
            sufucienciaMargCorteDocene = "";
        }
        return sufucienciaMargCorteDocene;
    }

    public String getSufucienciaMargenCorteTrece() {
        if (sufucienciaMargenCorteTrece == null) {
            sufucienciaMargenCorteTrece = "";
        }
        return sufucienciaMargenCorteTrece;
    }

    public String getDepositosCarteraCorteUno() {
        if (depositosCarteraCorteUno == null) {
            depositosCarteraCorteUno = "";
        }
        return depositosCarteraCorteUno;
    }

    public String getDepositosCarteraCorteDos() {
        if (depositosCarteraCorteDos == null) {
            depositosCarteraCorteDos = "";
        }
        return depositosCarteraCorteDos;
    }

    public String getDepositosCarteraCorteTres() {
        if (depositosCarteraCorteTres == null) {
            depositosCarteraCorteTres = "";
        }
        return depositosCarteraCorteTres;
    }

    public String getDepositosCarteraCorteCuatro() {
        if (depositosCarteraCorteCuatro == null) {
            depositosCarteraCorteCuatro = "";
        }
        return depositosCarteraCorteCuatro;
    }

    public String getDepositosCarteraCorteCinco() {
        if (depositosCarteraCorteCinco == null) {
            depositosCarteraCorteCinco = "";
        }
        return depositosCarteraCorteCinco;
    }

    public String getDepositosCarteraCorteSeis() {
        if (depositosCarteraCorteSeis == null) {
            depositosCarteraCorteSeis = "";
        }
        return depositosCarteraCorteSeis;
    }

    public String getDepositosCarteraCorteSiete() {
        if (depositosCarteraCorteSiete == null) {
            depositosCarteraCorteSiete = "";
        }
        return depositosCarteraCorteSiete;
    }

    public String getDepositosCarteraCorteOcho() {
        if (depositosCarteraCorteOcho == null) {
            depositosCarteraCorteOcho = "";
        }
        return depositosCarteraCorteOcho;
    }

    public String getDepositosCarteraCorteNueve() {
        if (depositosCarteraCorteNueve == null) {
            depositosCarteraCorteNueve = "";
        }
        return depositosCarteraCorteNueve;
    }

    public String getDepositosCarteraCorteDiez() {
        if (depositosCarteraCorteDiez == null) {
            depositosCarteraCorteDiez = "";
        }
        return depositosCarteraCorteDiez;
    }

    public String getDepositosCarteraCorteOnce() {
        if (depositosCarteraCorteOnce == null) {
            depositosCarteraCorteOnce = "";
        }
        return depositosCarteraCorteOnce;
    }

    public String getDepositosCarteraCorteDoce() {
        if (depositosCarteraCorteDoce == null) {
            depositosCarteraCorteDoce = "";
        }
        return depositosCarteraCorteDoce;
    }

    public String getDepositosCarteraCorteTrece() {
        if (depositosCarteraCorteTrece == null) {
            depositosCarteraCorteTrece = "";
        }
        return depositosCarteraCorteTrece;
    }

    public String getMargenTotalCorteUno() {
        if (margenTotalCorteUno == null) {
            margenTotalCorteUno = "";
        }
        return margenTotalCorteUno;
    }

    public String getMargenTotalCorteDos() {
        if (margenTotalCorteDos == null) {
            margenTotalCorteDos = "";
        }
        return margenTotalCorteDos;
    }

    public String getMargenTotalCorteTres() {
        if (margenTotalCorteTres == null) {
            margenTotalCorteTres = "";
        }
        return margenTotalCorteTres;
    }

    public String getMargenTotalCorteCuatro() {
        if (margenTotalCorteCuatro == null) {
            margenTotalCorteCuatro = "";
        }
        return margenTotalCorteCuatro;
    }

    public String getMargenTotalCorteCinco() {
        if (margenTotalCorteCinco == null) {
            margenTotalCorteCinco = "";
        }
        return margenTotalCorteCinco;
    }

    public String getMargenTotalCorteSeis() {
        if (margenTotalCorteSeis == null) {
            margenTotalCorteSeis = "";
        }
        return margenTotalCorteSeis;
    }

    public String getMargenTotalCorteSiete() {
        if (margenTotalCorteSiete == null) {
            margenTotalCorteSiete = "";
        }
        return margenTotalCorteSiete;
    }

    public String getMargenTotalCorteOcho() {
        if (margenTotalCorteOcho == null) {
            margenTotalCorteOcho = "";
        }
        return margenTotalCorteOcho;
    }

    public String getMargenTotalCorteNueve() {
        if (margenTotalCorteNueve == null) {
            margenTotalCorteNueve = "";
        }
        return margenTotalCorteNueve;
    }

    public String getMargenTotalCorteDiez() {
        if (margenTotalCorteDiez == null) {
            margenTotalCorteDiez = "";
        }
        return margenTotalCorteDiez;
    }

    public String getMargenTotalCorteOnce() {
        if (margenTotalCorteOnce == null) {
            margenTotalCorteOnce = "";
        }
        return margenTotalCorteOnce;
    }

    public String getMargenTotalCorteDoce() {
        if (margenTotalCorteDoce == null) {
            margenTotalCorteDoce = "";
        }
        return margenTotalCorteDoce;
    }

    public String getMargenTotalCorteTrece() {
        if (margenTotalCorteTrece == null) {
            margenTotalCorteTrece = "";
        }
        return margenTotalCorteTrece;
    }

    public String getPromedioCalCameCorteUno() {
        if (promedioCalCameCorteUno == null) {
            promedioCalCameCorteUno = "";
        }
        return promedioCalCameCorteUno;
    }

    public String getPromedioCalCameCorteDos() {
        if (promedioCalCameCorteDos == null) {
            promedioCalCameCorteDos = "";
        }
        return promedioCalCameCorteDos;
    }

    public String getPromedioCalCameCorteTres() {
        if (promedioCalCameCorteTres == null) {
            promedioCalCameCorteTres = "";
        }
        return promedioCalCameCorteTres;
    }

    public String getPromedioCalCameCorteCuatro() {
        if (promedioCalCameCorteCuatro == null) {
            promedioCalCameCorteCuatro = "";
        }
        return promedioCalCameCorteCuatro;
    }

    public String getPromedioCalCameCorteCinco() {
        if (promedioCalCameCorteCinco == null) {
            promedioCalCameCorteCinco = "";
        }
        return promedioCalCameCorteCinco;
    }

    public String getPromedioCalCameCorteSeis() {
        if (promedioCalCameCorteSeis == null) {
            promedioCalCameCorteSeis = "";
        }
        return promedioCalCameCorteSeis;
    }

    public String getPromedioCalCameCorteSiete() {
        if (promedioCalCameCorteSiete == null) {
            promedioCalCameCorteSiete = "";
        }
        return promedioCalCameCorteSiete;
    }

    public String getPromedioCalCameCorteOcho() {
        if (promedioCalCameCorteOcho == null) {
            promedioCalCameCorteOcho = "";
        }
        return promedioCalCameCorteOcho;
    }

    public String getPromedioCalCameCorteNueve() {
        if (promedioCalCameCorteNueve == null) {
            promedioCalCameCorteNueve = "";
        }
        return promedioCalCameCorteNueve;
    }

    public String getPromedioCalCameCorteDiez() {
        if (promedioCalCameCorteDiez == null) {
            promedioCalCameCorteDiez = "";
        }
        return promedioCalCameCorteDiez;
    }

    public String getPromedioCalCameCorteOnce() {
        if (promedioCalCameCorteOnce == null) {
            promedioCalCameCorteOnce = "";
        }
        return promedioCalCameCorteOnce;
    }

    public String getPromedioCalCameCorteDoce() {
        if (promedioCalCameCorteDoce == null) {
            promedioCalCameCorteDoce = "";
        }
        return promedioCalCameCorteDoce;
    }

    public String getPromedioCalCameCorteTrece() {
        if (promedioCalCameCorteTrece == null) {
            promedioCalCameCorteTrece = "";
        }
        return promedioCalCameCorteTrece;
    }

    public String getIndCameMargenSolvenciaCorteUno() {
        if (indCameMargenSolvenciaCorteUno == null) {
            indCameMargenSolvenciaCorteUno = "";
        }
        return indCameMargenSolvenciaCorteUno;
    }

    public String getIndCameMargenSolvenciaCorteDos() {
        if (indCameMargenSolvenciaCorteDos == null) {
            indCameMargenSolvenciaCorteDos = "";
        }
        return indCameMargenSolvenciaCorteDos;
    }

    public String getIndCameMargenSolvenciaCorteTres() {
        if (indCameMargenSolvenciaCorteTres == null) {
            indCameMargenSolvenciaCorteTres = "";
        }
        return indCameMargenSolvenciaCorteTres;
    }

    public String getIndCameMargenSolvenciaCorteCuatro() {
        if (indCameMargenSolvenciaCorteCuatro == null) {
            indCameMargenSolvenciaCorteCuatro = "";
        }
        return indCameMargenSolvenciaCorteCuatro;
    }

    public String getIndCameMargenSolvenciaCorteCinco() {
        if (indCameMargenSolvenciaCorteCinco == null) {
            indCameMargenSolvenciaCorteCinco = "";
        }
        return indCameMargenSolvenciaCorteCinco;
    }

    public String getIndCameMargenSolvenciaCorteSeis() {
        if (indCameMargenSolvenciaCorteSeis == null) {
            indCameMargenSolvenciaCorteSeis = "";
        }
        return indCameMargenSolvenciaCorteSeis;
    }

    public String getIndCameMargenSolvenciaCorteSiete() {
        if (indCameMargenSolvenciaCorteSiete == null) {
            indCameMargenSolvenciaCorteSiete = "";
        }
        return indCameMargenSolvenciaCorteSiete;
    }

    public String getIndCameMargenSolvenciaCorteOcho() {
        if (indCameMargenSolvenciaCorteOcho == null) {
            indCameMargenSolvenciaCorteOcho = "";
        }
        return indCameMargenSolvenciaCorteOcho;
    }

    public String getIndCameMargenSolvenciaCorteNueve() {
        if (indCameMargenSolvenciaCorteNueve == null) {
            indCameMargenSolvenciaCorteNueve = "";
        }
        return indCameMargenSolvenciaCorteNueve;
    }

    public String getIndCameMargenSolvenciaCorteDiez() {
        if (indCameMargenSolvenciaCorteDiez == null) {
            indCameMargenSolvenciaCorteDiez = "";
        }
        return indCameMargenSolvenciaCorteDiez;
    }

    public String getIndCameMargenSolvenciaCorteOnce() {
        if (indCameMargenSolvenciaCorteOnce == null) {
            indCameMargenSolvenciaCorteOnce = "";
        }
        return indCameMargenSolvenciaCorteOnce;
    }

    public String getIndCameMargenSolvenciaCorteDoce() {
        if (indCameMargenSolvenciaCorteDoce == null) {
            indCameMargenSolvenciaCorteDoce = "";
        }
        return indCameMargenSolvenciaCorteDoce;
    }

    public String getIndCameMargenSolvenciaCorteTrece() {
        if (indCameMargenSolvenciaCorteTrece == null) {
            indCameMargenSolvenciaCorteTrece = "";
        }
        return indCameMargenSolvenciaCorteTrece;
    }

    public String getIndCameCarteraVencidaCorteUno() {
        if (indCameCarteraVencidaCorteUno == null) {
            indCameCarteraVencidaCorteUno = "";
        }
        return indCameCarteraVencidaCorteUno;
    }

    public String getIndCameCarteraVencidaCorteDos() {
        if (indCameCarteraVencidaCorteDos == null) {
            indCameCarteraVencidaCorteDos = "";
        }
        return indCameCarteraVencidaCorteDos;
    }

    public String getIndCameCarteraVencidaCorteTres() {
        if (indCameCarteraVencidaCorteTres == null) {
            indCameCarteraVencidaCorteTres = "";
        }
        return indCameCarteraVencidaCorteTres;
    }

    public String getIndCameCarteraVencidaCorteCuatro() {
        if (indCameCarteraVencidaCorteCuatro == null) {
            indCameCarteraVencidaCorteCuatro = "";
        }
        return indCameCarteraVencidaCorteCuatro;
    }

    public String getIndCameCarteraVencidaCorteCinco() {
        if (indCameCarteraVencidaCorteCinco == null) {
            indCameCarteraVencidaCorteCinco = "";
        }
        return indCameCarteraVencidaCorteCinco;
    }

    public String getIndCameCarteraVencidaCorteSeis() {
        if (indCameCarteraVencidaCorteSeis == null) {
            indCameCarteraVencidaCorteSeis = "";
        }
        return indCameCarteraVencidaCorteSeis;
    }

    public String getIndCameCarteraVencidaCorteSiete() {
        if (indCameCarteraVencidaCorteSiete == null) {
            indCameCarteraVencidaCorteSiete = "";
        }
        return indCameCarteraVencidaCorteSiete;
    }

    public String getIndCameCarteraVencidaCorteOcho() {
        if (indCameCarteraVencidaCorteOcho == null) {
            indCameCarteraVencidaCorteOcho = "";
        }
        return indCameCarteraVencidaCorteOcho;
    }

    public String getIndCameCarteraVencidaCorteNueve() {
        if (indCameCarteraVencidaCorteNueve == null) {
            indCameCarteraVencidaCorteNueve = "";
        }
        return indCameCarteraVencidaCorteNueve;
    }

    public String getIndCameCarteraVencidaCorteDiez() {
        if (indCameCarteraVencidaCorteDiez == null) {
            indCameCarteraVencidaCorteDiez = "";
        }
        return indCameCarteraVencidaCorteDiez;
    }

    public String getIndCameCarteraVencidaCorteOnce() {
        if (indCameCarteraVencidaCorteOnce == null) {
            indCameCarteraVencidaCorteOnce = "";
        }
        return indCameCarteraVencidaCorteOnce;
    }

    public String getIndCameCarteraVencidaCorteDoce() {
        if (indCameCarteraVencidaCorteDoce == null) {
            indCameCarteraVencidaCorteDoce = "";
        }
        return indCameCarteraVencidaCorteDoce;
    }

    public String getIndCameCarteraVencidaCorteTrece() {
        if (indCameCarteraVencidaCorteTrece == null) {
            indCameCarteraVencidaCorteTrece = "";
        }
        return indCameCarteraVencidaCorteTrece;
    }

    public String getIndCameMargenTotalCorteUno() {
        if (indCameMargenTotalCorteUno == null) {
            indCameMargenTotalCorteUno = "";
        }
        return indCameMargenTotalCorteUno;
    }

    public String getIndCameMargenTotalCorteDos() {
        if (indCameMargenTotalCorteDos == null) {
            indCameMargenTotalCorteDos = "";
        }
        return indCameMargenTotalCorteDos;
    }

    public String getIndCameMargenTotalCorteTres() {
        if (indCameMargenTotalCorteTres == null) {
            indCameMargenTotalCorteTres = "";
        }
        return indCameMargenTotalCorteTres;
    }

    public String getIndCameMargenTotalCorteCuatro() {
        if (indCameMargenTotalCorteCuatro == null) {
            indCameMargenTotalCorteCuatro = "";
        }
        return indCameMargenTotalCorteCuatro;
    }

    public String getIndCameMargenTotalCorteCinco() {
        if (indCameMargenTotalCorteCinco == null) {
            indCameMargenTotalCorteCinco = "";
        }
        return indCameMargenTotalCorteCinco;
    }

    public String getIndCameMargenTotalCorteSeis() {
        if (indCameMargenTotalCorteSeis == null) {
            indCameMargenTotalCorteSeis = "";
        }
        return indCameMargenTotalCorteSeis;
    }

    public String getIndCameMargenTotalCorteSiete() {
        if (indCameMargenTotalCorteSiete == null) {
            indCameMargenTotalCorteSiete = "";
        }
        return indCameMargenTotalCorteSiete;
    }

    public String getIndCameMargenTotalCorteOcho() {
        if (indCameMargenTotalCorteOcho == null) {
            indCameMargenTotalCorteOcho = "";
        }
        return indCameMargenTotalCorteOcho;
    }

    public String getIndCameMargenTotalCorteNueve() {
        if (indCameMargenTotalCorteNueve == null) {
            indCameMargenTotalCorteNueve = "";
        }
        return indCameMargenTotalCorteNueve;
    }

    public String getIndCameMargenTotalCorteDiez() {
        if (indCameMargenTotalCorteDiez == null) {
            indCameMargenTotalCorteDiez = "";
        }
        return indCameMargenTotalCorteDiez;
    }

    public String getIndCameMargenTotalCorteOnce() {
        if (indCameMargenTotalCorteOnce == null) {
            indCameMargenTotalCorteOnce = "";
        }
        return indCameMargenTotalCorteOnce;
    }

    public String getIndCameMargenTotalCorteDoce() {
        if (indCameMargenTotalCorteDoce == null) {
            indCameMargenTotalCorteDoce = "";
        }
        return indCameMargenTotalCorteDoce;
    }

    public String getIndCameMargenTotalCorteTrece() {
        if (indCameMargenTotalCorteTrece == null) {
            indCameMargenTotalCorteTrece = "";
        }
        return indCameMargenTotalCorteTrece;
    }

    public String getIndCameActivoPasivoCorteUno() {
        if (indCameActivoPasivoCorteUno == null) {
            indCameActivoPasivoCorteUno = "";
        }
        return indCameActivoPasivoCorteUno;
    }

    public String getIndCameActivoPasivoCorteDos() {
        if (indCameActivoPasivoCorteDos == null) {
            indCameActivoPasivoCorteDos = "";
        }
        return indCameActivoPasivoCorteDos;
    }

    public String getIndCameActivoPasivoCorteTres() {
        if (indCameActivoPasivoCorteTres == null) {
            indCameActivoPasivoCorteTres = "";
        }
        return indCameActivoPasivoCorteTres;
    }

    public String getIndCameActivoPasivoCorteCuatro() {
        if (indCameActivoPasivoCorteCuatro == null) {
            indCameActivoPasivoCorteCuatro = "";
        }
        return indCameActivoPasivoCorteCuatro;
    }

    public String getIndCameActivoPasivoCorteCinco() {
        if (indCameActivoPasivoCorteCinco == null) {
            indCameActivoPasivoCorteCinco = "";
        }
        return indCameActivoPasivoCorteCinco;
    }

    public String getIndCameActivoPasivoCorteSeis() {
        if (indCameActivoPasivoCorteSeis == null) {
            indCameActivoPasivoCorteSeis = "";
        }
        return indCameActivoPasivoCorteSeis;
    }

    public String getIndCameActivoPasivoCorteSiete() {
        if (indCameActivoPasivoCorteSiete == null) {
            indCameActivoPasivoCorteSiete = "";
        }
        return indCameActivoPasivoCorteSiete;
    }

    public String getIndCameActivoPasivoCorteOcho() {
        if (indCameActivoPasivoCorteOcho == null) {
            indCameActivoPasivoCorteOcho = "";
        }
        return indCameActivoPasivoCorteOcho;
    }

    public String getIndCameActivoPasivoCorteNueve() {
        if (indCameActivoPasivoCorteNueve == null) {
            indCameActivoPasivoCorteNueve = "";
        }
        return indCameActivoPasivoCorteNueve;
    }

    public String getIndCameActivoPasivoCorteDiez() {
        if (indCameActivoPasivoCorteDiez == null) {
            indCameActivoPasivoCorteDiez = "";
        }
        return indCameActivoPasivoCorteDiez;
    }

    public String getIndCameActivoPasivoCorteOnce() {
        if (indCameActivoPasivoCorteOnce == null) {
            indCameActivoPasivoCorteOnce = "";
        }
        return indCameActivoPasivoCorteOnce;
    }

    public String getIndCameActivoPasivoCorteDoce() {
        if (indCameActivoPasivoCorteDoce == null) {
            indCameActivoPasivoCorteDoce = "";
        }
        return indCameActivoPasivoCorteDoce;
    }

    public String getIndCameActivoPasivoCorteTrece() {
        if (indCameActivoPasivoCorteTrece == null) {
            indCameActivoPasivoCorteTrece = "";
        }
        return indCameActivoPasivoCorteTrece;
    }

    public String getIndCameCostoAgenciaCorteUno() {
        if (indCameCostoAgenciaCorteUno == null) {
            indCameCostoAgenciaCorteUno = "";
        }
        return indCameCostoAgenciaCorteUno;
    }

    public String getIndCameCostoAgenciaCorteDos() {
        if (indCameCostoAgenciaCorteDos == null) {
            indCameCostoAgenciaCorteDos = "";
        }
        return indCameCostoAgenciaCorteDos;
    }

    public String getIndCameCostoAgenciaCorteTres() {
        if (indCameCostoAgenciaCorteTres == null) {
            indCameCostoAgenciaCorteTres = "";
        }
        return indCameCostoAgenciaCorteTres;
    }

    public String getIndCameCostoAgenciaCorteCuatro() {
        if (indCameCostoAgenciaCorteCuatro == null) {
            indCameCostoAgenciaCorteCuatro = "";
        }
        return indCameCostoAgenciaCorteCuatro;
    }

    public String getIndCameCostoAgenciaCorteCinco() {
        if (indCameCostoAgenciaCorteCinco == null) {
            indCameCostoAgenciaCorteCinco = "";
        }
        return indCameCostoAgenciaCorteCinco;
    }

    public String getIndCameCostoAgenciaCorteSeis() {
        if (indCameCostoAgenciaCorteSeis == null) {
            indCameCostoAgenciaCorteSeis = "";
        }
        return indCameCostoAgenciaCorteSeis;
    }

    public String getIndCameCostoAgenciaCorteSiete() {
        if (indCameCostoAgenciaCorteSiete == null) {
            indCameCostoAgenciaCorteSiete = "";
        }
        return indCameCostoAgenciaCorteSiete;
    }

    public String getIndCameCostoAgenciaCorteOcho() {
        if (indCameCostoAgenciaCorteOcho == null) {
            indCameCostoAgenciaCorteOcho = "";
        }
        return indCameCostoAgenciaCorteOcho;
    }

    public String getIndCameCostoAgenciaCorteNueve() {
        if (indCameCostoAgenciaCorteNueve == null) {
            indCameCostoAgenciaCorteNueve = "";
        }
        return indCameCostoAgenciaCorteNueve;
    }

    public String getIndCameCostoAgenciaCorteDiez() {
        if (indCameCostoAgenciaCorteDiez == null) {
            indCameCostoAgenciaCorteDiez = "";
        }
        return indCameCostoAgenciaCorteDiez;
    }

    public String getIndCameCostoAgenciaCorteOnce() {
        if (indCameCostoAgenciaCorteOnce == null) {
            indCameCostoAgenciaCorteOnce = "";
        }
        return indCameCostoAgenciaCorteOnce;
    }

    public String getIndCameCostoAgenciaCorteDoce() {
        if (indCameCostoAgenciaCorteDoce == null) {
            indCameCostoAgenciaCorteDoce = "";
        }
        return indCameCostoAgenciaCorteDoce;
    }

    public String getIndCameCostoAgenciaCorteTrece() {
        if (indCameCostoAgenciaCorteTrece == null) {
            indCameCostoAgenciaCorteTrece = "";
        }
        return indCameCostoAgenciaCorteTrece;
    }

    public String getIndCameDepositosCarteraCorteUno() {
        if (indCameDepositosCarteraCorteUno == null) {
            indCameDepositosCarteraCorteUno = "";
        }
        return indCameDepositosCarteraCorteUno;
    }

    public String getIndCameDepositosCarteraCorteDos() {
        if (indCameDepositosCarteraCorteDos == null) {
            indCameDepositosCarteraCorteDos = "";
        }
        return indCameDepositosCarteraCorteDos;
    }

    public String getIndCameDepositosCarteraCorteTres() {
        if (indCameDepositosCarteraCorteTres == null) {
            indCameDepositosCarteraCorteTres = "";
        }
        return indCameDepositosCarteraCorteTres;
    }

    public String getIndCameDepositosCarteraCorteCuatro() {
        if (indCameDepositosCarteraCorteCuatro == null) {
            indCameDepositosCarteraCorteCuatro = "";
        }
        return indCameDepositosCarteraCorteCuatro;
    }

    public String getIndCameDepositosCarteraCorteCinco() {
        if (indCameDepositosCarteraCorteCinco == null) {
            indCameDepositosCarteraCorteCinco = "";
        }
        return indCameDepositosCarteraCorteCinco;
    }

    public String getIndCameDepositosCarteraCorteSeis() {
        if (indCameDepositosCarteraCorteSeis == null) {
            indCameDepositosCarteraCorteSeis = "";
        }
        return indCameDepositosCarteraCorteSeis;
    }

    public String getIndCameDepositosCarteraCorteSiete() {
        if (indCameDepositosCarteraCorteSiete == null) {
            indCameDepositosCarteraCorteSiete = "";
        }
        return indCameDepositosCarteraCorteSiete;
    }

    public String getIndCameDepositosCarteraCorteOcho() {
        if (indCameDepositosCarteraCorteOcho == null) {
            indCameDepositosCarteraCorteOcho = "";
        }
        return indCameDepositosCarteraCorteOcho;
    }

    public String getIndCameDepositosCarteraCorteNueve() {
        if (indCameDepositosCarteraCorteNueve == null) {
            indCameDepositosCarteraCorteNueve = "";
        }
        return indCameDepositosCarteraCorteNueve;
    }

    public String getIndCameDepositosCarteraCorteDiez() {
        if (indCameDepositosCarteraCorteDiez == null) {
            indCameDepositosCarteraCorteDiez = "";
        }
        return indCameDepositosCarteraCorteDiez;
    }

    public String getIndCameDepositosCarteraCorteOnce() {
        if (indCameDepositosCarteraCorteOnce == null) {
            indCameDepositosCarteraCorteOnce = "";
        }
        return indCameDepositosCarteraCorteOnce;
    }

    public String getIndCameDepositosCarteraCorteDoce() {
        if (indCameDepositosCarteraCorteDoce == null) {
            indCameDepositosCarteraCorteDoce = "";
        }
        return indCameDepositosCarteraCorteDoce;
    }

    public String getIndCameDepositosCarteraCorteTrece() {
        if (indCameDepositosCarteraCorteTrece == null) {
            indCameDepositosCarteraCorteTrece = "";
        }
        return indCameDepositosCarteraCorteTrece;
    }

    public String getIndCameCarteraActivosCorteUno() {
        if (indCameCarteraActivosCorteUno == null) {
            indCameCarteraActivosCorteUno = "";
        }
        return indCameCarteraActivosCorteUno;
    }

    public String getIndCameCarteraActivosCorteDos() {
        if (indCameCarteraActivosCorteDos == null) {
            indCameCarteraActivosCorteDos = "";
        }
        return indCameCarteraActivosCorteDos;
    }

    public String getIndCameCarteraActivosCorteTres() {
        if (indCameCarteraActivosCorteTres == null) {
            indCameCarteraActivosCorteTres = "";
        }
        return indCameCarteraActivosCorteTres;
    }

    public String getIndCameCarteraActivosCorteCuatro() {
        if (indCameCarteraActivosCorteCuatro == null) {
            indCameCarteraActivosCorteCuatro = "";
        }
        return indCameCarteraActivosCorteCuatro;
    }

    public String getIndCameCarteraActivosCorteCinco() {
        if (indCameCarteraActivosCorteCinco == null) {
            indCameCarteraActivosCorteCinco = "";
        }
        return indCameCarteraActivosCorteCinco;
    }

    public String getIndCameCarteraActivosCorteSeis() {
        if (indCameCarteraActivosCorteSeis == null) {
            indCameCarteraActivosCorteSeis = "";
        }
        return indCameCarteraActivosCorteSeis;
    }

    public String getIndCameCarteraActivosCorteSiete() {
        if (indCameCarteraActivosCorteSiete == null) {
            indCameCarteraActivosCorteSiete = "";
        }
        return indCameCarteraActivosCorteSiete;
    }

    public String getIndCameCarteraActivosCorteOcho() {
        if (indCameCarteraActivosCorteOcho == null) {
            indCameCarteraActivosCorteOcho = "";
        }
        return indCameCarteraActivosCorteOcho;
    }

    public String getIndCameCarteraActivosCorteNueve() {
        if (indCameCarteraActivosCorteNueve == null) {
            indCameCarteraActivosCorteNueve = "";
        }
        return indCameCarteraActivosCorteNueve;
    }

    public String getIndCameCarteraActivosCorteDiez() {
        if (indCameCarteraActivosCorteDiez == null) {
            indCameCarteraActivosCorteDiez = "";
        }
        return indCameCarteraActivosCorteDiez;
    }

    public String getIndCameCarteraActivosCorteOnce() {
        if (indCameCarteraActivosCorteOnce == null) {
            indCameCarteraActivosCorteOnce = "";
        }
        return indCameCarteraActivosCorteOnce;
    }

    public String getIndCameCarteraActivosCorteDoce() {
        if (indCameCarteraActivosCorteDoce == null) {
            indCameCarteraActivosCorteDoce = "";
        }
        return indCameCarteraActivosCorteDoce;
    }

    public String getIndCameCarteraActivosCorteTrece() {
        if (indCameCarteraActivosCorteTrece == null) {
            indCameCarteraActivosCorteTrece = "";
        }
        return indCameCarteraActivosCorteTrece;
    }

    public String getIndCameActivoFijoCorteUno() {
        if (indCameActivoFijoCorteUno == null) {
            indCameActivoFijoCorteUno = "";
        }
        return indCameActivoFijoCorteUno;
    }

    public String getIndCameActivoFijoCorteDos() {
        if (indCameActivoFijoCorteDos == null) {
            indCameActivoFijoCorteDos = "";
        }
        return indCameActivoFijoCorteDos;
    }

    public String getIndCameActivoFijoCorteTres() {
        if (indCameActivoFijoCorteTres == null) {
            indCameActivoFijoCorteTres = "";
        }
        return indCameActivoFijoCorteTres;
    }

    public String getIndCameActivoFijoCorteCuatro() {
        if (indCameActivoFijoCorteCuatro == null) {
            indCameActivoFijoCorteCuatro = "";
        }
        return indCameActivoFijoCorteCuatro;
    }

    public String getIndCameActivoFijoCorteCinco() {
        if (indCameActivoFijoCorteCinco == null) {
            indCameActivoFijoCorteCinco = "";
        }
        return indCameActivoFijoCorteCinco;
    }

    public String getIndCameActivoFijoCorteSeis() {
        if (indCameActivoFijoCorteSeis == null) {
            indCameActivoFijoCorteSeis = "";
        }
        return indCameActivoFijoCorteSeis;
    }

    public String getIndCameActivoFijoCorteSiete() {
        if (indCameActivoFijoCorteSiete == null) {
            indCameActivoFijoCorteSiete = "";
        }
        return indCameActivoFijoCorteSiete;
    }

    public String getIndCameActivoFijoCorteOcho() {
        if (indCameActivoFijoCorteOcho == null) {
            indCameActivoFijoCorteOcho = "";
        }
        return indCameActivoFijoCorteOcho;
    }

    public String getIndCameActivoFijoCorteNueve() {
        if (indCameActivoFijoCorteNueve == null) {
            indCameActivoFijoCorteNueve = "";
        }
        return indCameActivoFijoCorteNueve;
    }

    public String getIndCameActivoFijoCorteDiez() {
        if (indCameActivoFijoCorteDiez == null) {
            indCameActivoFijoCorteDiez = "";
        }
        return indCameActivoFijoCorteDiez;
    }

    public String getIndCameActivoFijoCorteOnce() {
        if (indCameActivoFijoCorteOnce == null) {
            indCameActivoFijoCorteOnce = "";
        }
        return indCameActivoFijoCorteOnce;
    }

    public String getIndCameActivoFijoCorteDoce() {
        if (indCameActivoFijoCorteDoce == null) {
            indCameActivoFijoCorteDoce = "";
        }
        return indCameActivoFijoCorteDoce;
    }

    public String getIndCameActivoFijoCorteTrece() {
        if (indCameActivoFijoCorteTrece == null) {
            indCameActivoFijoCorteTrece = "";
        }
        return indCameActivoFijoCorteTrece;
    }

    public String getIndCameSuficienciaCorteUno() {
        if (indCameSuficienciaCorteUno == null) {
            indCameSuficienciaCorteUno = "";
        }
        return indCameSuficienciaCorteUno;
    }

    public String getIndCameSuficienciaCorteDos() {
        if (indCameSuficienciaCorteDos == null) {
            indCameSuficienciaCorteDos = "";
        }
        return indCameSuficienciaCorteDos;
    }

    public String getIndCameSuficienciaCorteTres() {
        if (indCameSuficienciaCorteTres == null) {
            indCameSuficienciaCorteTres = "";
        }
        return indCameSuficienciaCorteTres;
    }

    public String getIndCameSuficienciaCorteCuatro() {
        if (indCameSuficienciaCorteCuatro == null) {
            indCameSuficienciaCorteCuatro = "";
        }
        return indCameSuficienciaCorteCuatro;
    }

    public String getIndCameSuficienciaCorteCinco() {
        if (indCameSuficienciaCorteCinco == null) {
            indCameSuficienciaCorteCinco = "";
        }
        return indCameSuficienciaCorteCinco;
    }

    public String getIndCameSuficienciaCorteSeis() {
        if (indCameSuficienciaCorteSeis == null) {
            indCameSuficienciaCorteSeis = "";
        }
        return indCameSuficienciaCorteSeis;
    }

    public String getIndCameSuficienciaCorteSiete() {
        if (indCameSuficienciaCorteSiete == null) {
            indCameSuficienciaCorteSiete = "";
        }
        return indCameSuficienciaCorteSiete;
    }

    public String getIndCameSuficienciaCorteOcho() {
        if (indCameSuficienciaCorteOcho == null) {
            indCameSuficienciaCorteOcho = "";
        }
        return indCameSuficienciaCorteOcho;
    }

    public String getIndCameSuficienciaCorteNueve() {
        if (indCameSuficienciaCorteNueve == null) {
            indCameSuficienciaCorteNueve = "";
        }
        return indCameSuficienciaCorteNueve;
    }

    public String getIndCameSuficienciaCorteDiez() {
        if (indCameSuficienciaCorteDiez == null) {
            indCameSuficienciaCorteDiez = "";
        }
        return indCameSuficienciaCorteDiez;
    }

    public String getIndCameSuficienciaCorteOnce() {
        if (indCameSuficienciaCorteOnce == null) {
            indCameSuficienciaCorteOnce = "";
        }
        return indCameSuficienciaCorteOnce;
    }

    public String getIndCameSuficienciaCorteDoce() {
        if (indCameSuficienciaCorteDoce == null) {
            indCameSuficienciaCorteDoce = "";
        }
        return indCameSuficienciaCorteDoce;
    }

    public String getIndCameSuficienciaCorteTrece() {
        if (indCameSuficienciaCorteTrece == null) {
            indCameSuficienciaCorteTrece = "";
        }
        return indCameSuficienciaCorteTrece;
    }

    public String getIndCameMargenSolvenciaMinCorteUno() {
        if (indCameMargenSolvenciaMinCorteUno == null) {
            indCameMargenSolvenciaMinCorteUno = "";
        }
        return indCameMargenSolvenciaMinCorteUno;
    }

    public String getIndCameMargenSolvenciaMinCorteDos() {
        if (indCameMargenSolvenciaMinCorteDos == null) {
            indCameMargenSolvenciaMinCorteDos = "";
        }
        return indCameMargenSolvenciaMinCorteDos;
    }

    public String getIndCameMargenSolvenciaMinCorteTres() {
        if (indCameMargenSolvenciaMinCorteTres == null) {
            indCameMargenSolvenciaMinCorteTres = "";
        }
        return indCameMargenSolvenciaMinCorteTres;
    }

    public String getIndCameMargenSolvenciaMinCorteCuatro() {
        if (indCameMargenSolvenciaMinCorteCuatro == null) {
            indCameMargenSolvenciaMinCorteCuatro = "";
        }
        return indCameMargenSolvenciaMinCorteCuatro;
    }

    public String getIndCameMargenSolvenciaMinCorteCinco() {
        if (indCameMargenSolvenciaMinCorteCinco == null) {
            indCameMargenSolvenciaMinCorteCinco = "";
        }
        return indCameMargenSolvenciaMinCorteCinco;
    }

    public String getIndCameMargenSolvenciaMinCorteSeis() {
        if (indCameMargenSolvenciaMinCorteSeis == null) {
            indCameMargenSolvenciaMinCorteSeis = "";
        }
        return indCameMargenSolvenciaMinCorteSeis;
    }

    public String getIndCameMargenSolvenciaMinCorteSiete() {
        if (indCameMargenSolvenciaMinCorteSiete == null) {
            indCameMargenSolvenciaMinCorteSiete = "";
        }
        return indCameMargenSolvenciaMinCorteSiete;
    }

    public String getIndCameMargenSolvenciaMinCorteOcho() {
        if (indCameMargenSolvenciaMinCorteOcho == null) {
            indCameMargenSolvenciaMinCorteOcho = "";
        }
        return indCameMargenSolvenciaMinCorteOcho;
    }

    public String getIndCameMargenSolvenciaMinCorteNueve() {
        if (indCameMargenSolvenciaMinCorteNueve == null) {
            indCameMargenSolvenciaMinCorteNueve = "";
        }
        return indCameMargenSolvenciaMinCorteNueve;
    }

    public String getIndCameMargenSolvenciaMinCorteDiez() {
        if (indCameMargenSolvenciaMinCorteDiez == null) {
            indCameMargenSolvenciaMinCorteDiez = "";
        }
        return indCameMargenSolvenciaMinCorteDiez;
    }

    public String getIndCameMargenSolvenciaMinCorteOnce() {
        if (indCameMargenSolvenciaMinCorteOnce == null) {
            indCameMargenSolvenciaMinCorteOnce = "";
        }
        return indCameMargenSolvenciaMinCorteOnce;
    }

    public String getIndCameMargenSolvenciaMinCorteDoce() {
        if (indCameMargenSolvenciaMinCorteDoce == null) {
            indCameMargenSolvenciaMinCorteDoce = "";
        }
        return indCameMargenSolvenciaMinCorteDoce;
    }

    public String getIndCameMargenSolvenciaMinCorteTrece() {
        if (indCameMargenSolvenciaMinCorteTrece == null) {
            indCameMargenSolvenciaMinCorteTrece = "";
        }
        return indCameMargenSolvenciaMinCorteTrece;
    }

    public String getCalCameMargenSolvenciaCorteUno() {
        if (calCameMargenSolvenciaCorteUno == null) {
            calCameMargenSolvenciaCorteUno = "";
        }
        return calCameMargenSolvenciaCorteUno;
    }

    public String getCalCameMargenSolvenciaCorteDos() {
        if (calCameMargenSolvenciaCorteDos == null) {
            calCameMargenSolvenciaCorteDos = "";
        }
        return calCameMargenSolvenciaCorteDos;
    }

    public String getCalCameMargenSolvenciaCorteTres() {
        if (calCameMargenSolvenciaCorteTres == null) {
            calCameMargenSolvenciaCorteTres = "";
        }
        return calCameMargenSolvenciaCorteTres;
    }

    public String getCalCameMargenSolvenciaCorteCuatro() {
        if (calCameMargenSolvenciaCorteCuatro == null) {
            calCameMargenSolvenciaCorteCuatro = "";
        }
        return calCameMargenSolvenciaCorteCuatro;
    }

    public String getCalCameMargenSolvenciaCorteCinco() {
        if (calCameMargenSolvenciaCorteCinco == null) {
            calCameMargenSolvenciaCorteCinco = "";
        }
        return calCameMargenSolvenciaCorteCinco;
    }

    public String getCalCameMargenSolvenciaCorteSeis() {
        if (calCameMargenSolvenciaCorteSeis == null) {
            calCameMargenSolvenciaCorteSeis = "";
        }
        return calCameMargenSolvenciaCorteSeis;
    }

    public String getCalCameMargenSolvenciaCorteSiete() {
        if (calCameMargenSolvenciaCorteSiete == null) {
            calCameMargenSolvenciaCorteSiete = "";
        }
        return calCameMargenSolvenciaCorteSiete;
    }

    public String getCalCameMargenSolvenciaCorteOcho() {
        if (calCameMargenSolvenciaCorteOcho == null) {
            calCameMargenSolvenciaCorteOcho = "";
        }
        return calCameMargenSolvenciaCorteOcho;
    }

    public String getCalCameMargenSolvenciaCorteNueve() {
        if (calCameMargenSolvenciaCorteNueve == null) {
            calCameMargenSolvenciaCorteNueve = "";
        }
        return calCameMargenSolvenciaCorteNueve;
    }

    public String getCalCameMargenSolvenciaCorteDiez() {
        if (calCameMargenSolvenciaCorteDiez == null) {
            calCameMargenSolvenciaCorteDiez = "";
        }
        return calCameMargenSolvenciaCorteDiez;
    }

    public String getCalCameMargenSolvenciaCorteOnce() {
        if (calCameMargenSolvenciaCorteOnce == null) {
            calCameMargenSolvenciaCorteOnce = "";
        }
        return calCameMargenSolvenciaCorteOnce;
    }

    public String getCalCameMargenSolvenciaCorteDoce() {
        if (calCameMargenSolvenciaCorteDoce == null) {
            calCameMargenSolvenciaCorteDoce = "";
        }
        return calCameMargenSolvenciaCorteDoce;
    }

    public String getCalCameMargenSolvenciaCorteTrece() {
        if (calCameMargenSolvenciaCorteTrece == null) {
            calCameMargenSolvenciaCorteTrece = "";
        }
        return calCameMargenSolvenciaCorteTrece;
    }

    public String getCalCameCarteraVencidaCorteUno() {
        if (calCameCarteraVencidaCorteUno == null) {
            calCameCarteraVencidaCorteUno = "";
        }
        return calCameCarteraVencidaCorteUno;
    }

    public String getCalCameCarteraVencidaCorteDos() {
        if (calCameCarteraVencidaCorteDos == null) {
            calCameCarteraVencidaCorteDos = "";
        }
        return calCameCarteraVencidaCorteDos;
    }

    public String getCalCameCarteraVencidaCorteTres() {
        if (calCameCarteraVencidaCorteTres == null) {
            calCameCarteraVencidaCorteTres = "";
        }
        return calCameCarteraVencidaCorteTres;
    }

    public String getCalCameCarteraVencidaCorteCuatro() {
        if (calCameCarteraVencidaCorteCuatro == null) {
            calCameCarteraVencidaCorteCuatro = "";
        }
        return calCameCarteraVencidaCorteCuatro;
    }

    public String getCalCameCarteraVencidaCorteCinco() {
        if (calCameCarteraVencidaCorteCinco == null) {
            calCameCarteraVencidaCorteCinco = "";
        }
        return calCameCarteraVencidaCorteCinco;
    }

    public String getCalCameCarteraVencidaCorteSeis() {
        if (calCameCarteraVencidaCorteSeis == null) {
            calCameCarteraVencidaCorteSeis = "";
        }
        return calCameCarteraVencidaCorteSeis;
    }

    public String getCalCameCarteraVencidaCorteSiete() {
        if (calCameCarteraVencidaCorteSiete == null) {
            calCameCarteraVencidaCorteSiete = "";
        }
        return calCameCarteraVencidaCorteSiete;
    }

    public String getCalCameCarteraVencidaCorteOcho() {
        if (calCameCarteraVencidaCorteOcho == null) {
            calCameCarteraVencidaCorteOcho = "";
        }
        return calCameCarteraVencidaCorteOcho;
    }

    public String getCalCameCarteraVencidaCorteNueve() {
        if (calCameCarteraVencidaCorteNueve == null) {
            calCameCarteraVencidaCorteNueve = "";
        }
        return calCameCarteraVencidaCorteNueve;
    }

    public String getCalCameCarteraVencidaCorteDiez() {
        if (calCameCarteraVencidaCorteDiez == null) {
            calCameCarteraVencidaCorteDiez = "";
        }
        return calCameCarteraVencidaCorteDiez;
    }

    public String getCalCameCarteraVencidaCorteOnce() {
        if (calCameCarteraVencidaCorteOnce == null) {
            calCameCarteraVencidaCorteOnce = "";
        }
        return calCameCarteraVencidaCorteOnce;
    }

    public String getCalCameCarteraVencidaCorteDoce() {
        if (calCameCarteraVencidaCorteDoce == null) {
            calCameCarteraVencidaCorteDoce = "";
        }
        return calCameCarteraVencidaCorteDoce;
    }

    public String getCalCameCarteraVencidaCorteTrece() {
        if (calCameCarteraVencidaCorteTrece == null) {
            calCameCarteraVencidaCorteTrece = "";
        }
        return calCameCarteraVencidaCorteTrece;
    }

    public String getCalCameMargenTotalCorteUno() {
        if (calCameMargenTotalCorteUno == null) {
            calCameMargenTotalCorteUno = "";
        }
        return calCameMargenTotalCorteUno;
    }

    public String getCalCameMargenTotalCorteDos() {
        if (calCameMargenTotalCorteDos == null) {
            calCameMargenTotalCorteDos = "";
        }
        return calCameMargenTotalCorteDos;
    }

    public String getCalCameMargenTotalCorteTres() {
        if (calCameMargenTotalCorteTres == null) {
            calCameMargenTotalCorteTres = "";
        }
        return calCameMargenTotalCorteTres;
    }

    public String getCalCameMargenTotalCorteCuatro() {
        if (calCameMargenTotalCorteCuatro == null) {
            calCameMargenTotalCorteCuatro = "";
        }
        return calCameMargenTotalCorteCuatro;
    }

    public String getCalCameMargenTotalCorteCinco() {
        if (calCameMargenTotalCorteCinco == null) {
            calCameMargenTotalCorteCinco = "";
        }
        return calCameMargenTotalCorteCinco;
    }

    public String getCalCameMargenTotalCorteSeis() {
        if (calCameMargenTotalCorteSeis == null) {
            calCameMargenTotalCorteSeis = "";
        }
        return calCameMargenTotalCorteSeis;
    }

    public String getCalCameMargenTotalCorteSiete() {
        if (calCameMargenTotalCorteSiete == null) {
            calCameMargenTotalCorteSiete = "";
        }
        return calCameMargenTotalCorteSiete;
    }

    public String getCalCameMargenTotalCorteOcho() {
        if (calCameMargenTotalCorteOcho == null) {
            calCameMargenTotalCorteOcho = "";
        }
        return calCameMargenTotalCorteOcho;
    }

    public String getCalCameMargenTotalCorteNueve() {
        if (calCameMargenTotalCorteNueve == null) {
            calCameMargenTotalCorteNueve = "";
        }
        return calCameMargenTotalCorteNueve;
    }

    public String getCalCameMargenTotalCorteDiez() {
        if (calCameMargenTotalCorteDiez == null) {
            calCameMargenTotalCorteDiez = "";
        }
        return calCameMargenTotalCorteDiez;
    }

    public String getCalCameMargenTotalCorteOnce() {
        if (calCameMargenTotalCorteOnce == null) {
            calCameMargenTotalCorteOnce = "";
        }
        return calCameMargenTotalCorteOnce;
    }

    public String getCalCameMargenTotalCorteDoce() {
        if (calCameMargenTotalCorteDoce == null) {
            calCameMargenTotalCorteDoce = "";
        }
        return calCameMargenTotalCorteDoce;
    }

    public String getCalCameMargenTotalCorteTrece() {
        if (calCameMargenTotalCorteTrece == null) {
            calCameMargenTotalCorteTrece = "";
        }
        return calCameMargenTotalCorteTrece;
    }

    public String getCalCameActivoPasivoCorteUno() {
        if (calCameActivoPasivoCorteUno == null) {
            calCameActivoPasivoCorteUno = "";
        }
        return calCameActivoPasivoCorteUno;
    }

    public String getCalCameActivoPasivoCorteDos() {
        if (calCameActivoPasivoCorteDos == null) {
            calCameActivoPasivoCorteDos = "";
        }
        return calCameActivoPasivoCorteDos;
    }

    public String getCalCameActivoPasivoCorteTres() {
        if (calCameActivoPasivoCorteTres == null) {
            calCameActivoPasivoCorteTres = "";
        }
        return calCameActivoPasivoCorteTres;
    }

    public String getCalCameActivoPasivoCorteCuatro() {
        if (calCameActivoPasivoCorteCuatro == null) {
            calCameActivoPasivoCorteCuatro = "";
        }
        return calCameActivoPasivoCorteCuatro;
    }

    public String getCalCameActivoPasivoCorteCinco() {
        if (calCameActivoPasivoCorteCinco == null) {
            calCameActivoPasivoCorteCinco = "";
        }
        return calCameActivoPasivoCorteCinco;
    }

    public String getCalCameActivoPasivoCorteSeis() {
        if (calCameActivoPasivoCorteSeis == null) {
            calCameActivoPasivoCorteSeis = "";
        }
        return calCameActivoPasivoCorteSeis;
    }

    public String getCalCameActivoPasivoCorteSiete() {
        if (calCameActivoPasivoCorteSiete == null) {
            calCameActivoPasivoCorteSiete = "";
        }
        return calCameActivoPasivoCorteSiete;
    }

    public String getCalCameActivoPasivoCorteOcho() {
        if (calCameActivoPasivoCorteOcho == null) {
            calCameActivoPasivoCorteOcho = "";
        }
        return calCameActivoPasivoCorteOcho;
    }

    public String getCalCameActivoPasivoCorteNueve() {
        if (calCameActivoPasivoCorteNueve == null) {
            calCameActivoPasivoCorteNueve = "";
        }
        return calCameActivoPasivoCorteNueve;
    }

    public String getCalCameActivoPasivoCorteDiez() {
        if (calCameActivoPasivoCorteDiez == null) {
            calCameActivoPasivoCorteDiez = "";
        }
        return calCameActivoPasivoCorteDiez;
    }

    public String getCalCameActivoPasivoCorteOnce() {
        if (calCameActivoPasivoCorteOnce == null) {
            calCameActivoPasivoCorteOnce = "";
        }
        return calCameActivoPasivoCorteOnce;
    }

    public String getCalCameActivoPasivoCorteDoce() {
        if (calCameActivoPasivoCorteDoce == null) {
            calCameActivoPasivoCorteDoce = "";
        }
        return calCameActivoPasivoCorteDoce;
    }

    public String getCalCameActivoPasivoCorteTrece() {
        if (calCameActivoPasivoCorteTrece == null) {
            calCameActivoPasivoCorteTrece = "";
        }
        return calCameActivoPasivoCorteTrece;
    }

    public String getCalCameCostoAgenciaCorteUno() {
        if (calCameCostoAgenciaCorteUno == null) {
            calCameCostoAgenciaCorteUno = "";
        }
        return calCameCostoAgenciaCorteUno;
    }

    public String getCalCameCostoAgenciaCorteDos() {
        if (calCameCostoAgenciaCorteDos == null) {
            calCameCostoAgenciaCorteDos = "";
        }
        return calCameCostoAgenciaCorteDos;
    }

    public String getCalCameCostoAgenciaCorteTres() {
        if (calCameCostoAgenciaCorteTres == null) {
            calCameCostoAgenciaCorteTres = "";
        }
        return calCameCostoAgenciaCorteTres;
    }

    public String getCalCameCostoAgenciaCorteCuatro() {
        if (calCameCostoAgenciaCorteCuatro == null) {
            calCameCostoAgenciaCorteCuatro = "";
        }
        return calCameCostoAgenciaCorteCuatro;
    }

    public String getCalCameCostoAgenciaCorteCinco() {
        if (calCameCostoAgenciaCorteCinco == null) {
            calCameCostoAgenciaCorteCinco = "";
        }
        return calCameCostoAgenciaCorteCinco;
    }

    public String getCalCameCostoAgenciaCorteSeis() {
        if (calCameCostoAgenciaCorteSeis == null) {
            calCameCostoAgenciaCorteSeis = "";
        }
        return calCameCostoAgenciaCorteSeis;
    }

    public String getCalCameCostoAgenciaCorteSiete() {
        if (calCameCostoAgenciaCorteSiete == null) {
            calCameCostoAgenciaCorteSiete = "";
        }
        return calCameCostoAgenciaCorteSiete;
    }

    public String getCalCameCostoAgenciaCorteOcho() {
        if (calCameCostoAgenciaCorteOcho == null) {
            calCameCostoAgenciaCorteOcho = "";
        }
        return calCameCostoAgenciaCorteOcho;
    }

    public String getCalCameCostoAgenciaCorteNueve() {
        if (calCameCostoAgenciaCorteNueve == null) {
            calCameCostoAgenciaCorteNueve = "";
        }
        return calCameCostoAgenciaCorteNueve;
    }

    public String getCalCameCostoAgenciaCorteDiez() {
        if (calCameCostoAgenciaCorteDiez == null) {
            calCameCostoAgenciaCorteDiez = "";
        }
        return calCameCostoAgenciaCorteDiez;
    }

    public String getCalCameCostoAgenciaCorteOnce() {
        if (calCameCostoAgenciaCorteOnce == null) {
            calCameCostoAgenciaCorteOnce = "";
        }
        return calCameCostoAgenciaCorteOnce;
    }

    public String getCalCameCostoAgenciaCorteDoce() {
        if (calCameCostoAgenciaCorteDoce == null) {
            calCameCostoAgenciaCorteDoce = "";
        }
        return calCameCostoAgenciaCorteDoce;
    }

    public String getCalCameCostoAgenciaCorteTrece() {
        if (calCameCostoAgenciaCorteTrece == null) {
            calCameCostoAgenciaCorteTrece = "";
        }
        return calCameCostoAgenciaCorteTrece;
    }

    public String getCalCameDepositosCarteraCorteUno() {
        if (calCameDepositosCarteraCorteUno == null) {
            calCameDepositosCarteraCorteUno = "";
        }
        return calCameDepositosCarteraCorteUno;
    }

    public String getCalCameDepositosCarteraCorteDos() {
        if (calCameDepositosCarteraCorteDos == null) {
            calCameDepositosCarteraCorteDos = "";
        }
        return calCameDepositosCarteraCorteDos;
    }

    public String getCalCameDepositosCarteraCorteTres() {
        if (calCameDepositosCarteraCorteTres == null) {
            calCameDepositosCarteraCorteTres = "";
        }
        return calCameDepositosCarteraCorteTres;
    }

    public String getCalCameDepositosCarteraCorteCuatro() {
        if (calCameDepositosCarteraCorteCuatro == null) {
            calCameDepositosCarteraCorteCuatro = "";
        }
        return calCameDepositosCarteraCorteCuatro;
    }

    public String getCalCameDepositosCarteraCorteCinco() {
        if (calCameDepositosCarteraCorteCinco == null) {
            calCameDepositosCarteraCorteCinco = "";
        }
        return calCameDepositosCarteraCorteCinco;
    }

    public String getCalCameDepositosCarteraCorteSeis() {
        if (calCameDepositosCarteraCorteSeis == null) {
            calCameDepositosCarteraCorteSeis = "";
        }
        return calCameDepositosCarteraCorteSeis;
    }

    public String getCalCameDepositosCarteraCorteSiete() {
        if (calCameDepositosCarteraCorteSiete == null) {
            calCameDepositosCarteraCorteSiete = "";
        }
        return calCameDepositosCarteraCorteSiete;
    }

    public String getCalCameDepositosCarteraCorteOcho() {
        if (calCameDepositosCarteraCorteOcho == null) {
            calCameDepositosCarteraCorteOcho = "";
        }
        return calCameDepositosCarteraCorteOcho;
    }

    public String getCalCameDepositosCarteraCorteNueve() {
        if (calCameDepositosCarteraCorteNueve == null) {
            calCameDepositosCarteraCorteNueve = "";
        }
        return calCameDepositosCarteraCorteNueve;
    }

    public String getCalCameDepositosCarteraCorteDiez() {
        if (calCameDepositosCarteraCorteDiez == null) {
            calCameDepositosCarteraCorteDiez = "";
        }
        return calCameDepositosCarteraCorteDiez;
    }

    public String getCalCameDepositosCarteraCorteOnce() {
        if (calCameDepositosCarteraCorteOnce == null) {
            calCameDepositosCarteraCorteOnce = "";
        }
        return calCameDepositosCarteraCorteOnce;
    }

    public String getCalCameDepositosCarteraCorteDoce() {
        if (calCameDepositosCarteraCorteDoce == null) {
            calCameDepositosCarteraCorteDoce = "";
        }
        return calCameDepositosCarteraCorteDoce;
    }

    public String getCalCameDepositosCarteraCorteTrece() {
        if (calCameDepositosCarteraCorteTrece == null) {
            calCameDepositosCarteraCorteTrece = "";
        }
        return calCameDepositosCarteraCorteTrece;
    }

    public String getCalCameCarteraActivosCorteUno() {
        if (calCameCarteraActivosCorteUno == null) {
            calCameCarteraActivosCorteUno = "";
        }
        return calCameCarteraActivosCorteUno;
    }

    public String getCalCameCarteraActivosCorteDos() {
        if (calCameCarteraActivosCorteDos == null) {
            calCameCarteraActivosCorteDos = "";
        }
        return calCameCarteraActivosCorteDos;
    }

    public String getCalCameCarteraActivosCorteTres() {
        if (calCameCarteraActivosCorteTres == null) {
            calCameCarteraActivosCorteTres = "";
        }
        return calCameCarteraActivosCorteTres;
    }

    public String getCalCameCarteraActivosCorteCuatro() {
        if (calCameCarteraActivosCorteCuatro == null) {
            calCameCarteraActivosCorteCuatro = "";
        }
        return calCameCarteraActivosCorteCuatro;
    }

    public String getCalCameCarteraActivosCorteCinco() {
        if (calCameCarteraActivosCorteCinco == null) {
            calCameCarteraActivosCorteCinco = "";
        }
        return calCameCarteraActivosCorteCinco;
    }

    public String getCalCameCarteraActivosCorteSeis() {
        if (calCameCarteraActivosCorteSeis == null) {
            calCameCarteraActivosCorteSeis = "";
        }
        return calCameCarteraActivosCorteSeis;
    }

    public String getCalCameCarteraActivosCorteSiete() {
        if (calCameCarteraActivosCorteSiete == null) {
            calCameCarteraActivosCorteSiete = "";
        }
        return calCameCarteraActivosCorteSiete;
    }

    public String getCalCameCarteraActivosCorteOcho() {
        if (calCameCarteraActivosCorteOcho == null) {
            calCameCarteraActivosCorteOcho = "";
        }
        return calCameCarteraActivosCorteOcho;
    }

    public String getCalCameCarteraActivosCorteNueve() {
        if (calCameCarteraActivosCorteNueve == null) {
            calCameCarteraActivosCorteNueve = "";
        }
        return calCameCarteraActivosCorteNueve;
    }

    public String getCalCameCarteraActivosCorteDiez() {
        if (calCameCarteraActivosCorteDiez == null) {
            calCameCarteraActivosCorteDiez = "";
        }
        return calCameCarteraActivosCorteDiez;
    }

    public String getCalCameCarteraActivosCorteOnce() {
        if (calCameCarteraActivosCorteOnce == null) {
            calCameCarteraActivosCorteOnce = "";
        }
        return calCameCarteraActivosCorteOnce;
    }

    public String getCalCameCarteraActivosCorteDoce() {
        if (calCameCarteraActivosCorteDoce == null) {
            calCameCarteraActivosCorteDoce = "";
        }
        return calCameCarteraActivosCorteDoce;
    }

    public String getCalCameCarteraActivosCorteTrece() {
        if (calCameCarteraActivosCorteTrece == null) {
            calCameCarteraActivosCorteTrece = "";
        }
        return calCameCarteraActivosCorteTrece;
    }

    public String getCalCameActivoFijoCorteUno() {
        if (calCameActivoFijoCorteUno == null) {
            calCameActivoFijoCorteUno = "";
        }
        return calCameActivoFijoCorteUno;
    }

    public String getCalCameActivoFijoCorteDos() {
        if (calCameActivoFijoCorteDos == null) {
            calCameActivoFijoCorteDos = "";
        }
        return calCameActivoFijoCorteDos;
    }

    public String getCalCameActivoFijoCorteTres() {
        if (calCameActivoFijoCorteTres == null) {
            calCameActivoFijoCorteTres = "";
        }
        return calCameActivoFijoCorteTres;
    }

    public String getCalCameActivoFijoCorteCuatro() {
        if (calCameActivoFijoCorteCuatro == null) {
            calCameActivoFijoCorteCuatro = "";
        }
        return calCameActivoFijoCorteCuatro;
    }

    public String getCalCameActivoFijoCorteCinco() {
        if (calCameActivoFijoCorteCinco == null) {
            calCameActivoFijoCorteCinco = "";
        }
        return calCameActivoFijoCorteCinco;
    }

    public String getCalCameActivoFijoCorteSeis() {
        if (calCameActivoFijoCorteSeis == null) {
            calCameActivoFijoCorteSeis = "";
        }
        return calCameActivoFijoCorteSeis;
    }

    public String getCalCameActivoFijoCorteSiete() {
        if (calCameActivoFijoCorteSiete == null) {
            calCameActivoFijoCorteSiete = "";
        }
        return calCameActivoFijoCorteSiete;
    }

    public String getCalCameActivoFijoCorteOcho() {
        if (calCameActivoFijoCorteOcho == null) {
            calCameActivoFijoCorteOcho = "";
        }
        return calCameActivoFijoCorteOcho;
    }

    public String getCalCameActivoFijoCorteNueve() {
        if (calCameActivoFijoCorteNueve == null) {
            calCameActivoFijoCorteNueve = "";
        }
        return calCameActivoFijoCorteNueve;
    }

    public String getCalCameActivoFijoCorteDiez() {
        if (calCameActivoFijoCorteDiez == null) {
            calCameActivoFijoCorteDiez = "";
        }
        return calCameActivoFijoCorteDiez;
    }

    public String getCalCameActivoFijoCorteOnce() {
        if (calCameActivoFijoCorteOnce == null) {
            calCameActivoFijoCorteOnce = "";
        }
        return calCameActivoFijoCorteOnce;
    }

    public String getCalCameActivoFijoCorteDoce() {
        if (calCameActivoFijoCorteDoce == null) {
            calCameActivoFijoCorteDoce = "";
        }
        return calCameActivoFijoCorteDoce;
    }

    public String getCalCameActivoFijoCorteTrece() {
        if (calCameActivoFijoCorteTrece == null) {
            calCameActivoFijoCorteTrece = "";
        }
        return calCameActivoFijoCorteTrece;
    }

    public String getCalCameSuficienciaCorteUno() {
        if (calCameSuficienciaCorteUno == null) {
            calCameSuficienciaCorteUno = "";
        }
        return calCameSuficienciaCorteUno;
    }

    public String getCalCameSuficienciaCorteDos() {
        if (calCameSuficienciaCorteDos == null) {
            calCameSuficienciaCorteDos = "";
        }
        return calCameSuficienciaCorteDos;
    }

    public String getCalCameSuficienciaCorteTres() {
        if (calCameSuficienciaCorteTres == null) {
            calCameSuficienciaCorteTres = "";
        }
        return calCameSuficienciaCorteTres;
    }

    public String getCalCameSuficienciaCorteCuatro() {
        if (calCameSuficienciaCorteCuatro == null) {
            calCameSuficienciaCorteCuatro = "";
        }
        return calCameSuficienciaCorteCuatro;
    }

    public String getCalCameSuficienciaCorteCinco() {
        if (calCameSuficienciaCorteCinco == null) {
            calCameSuficienciaCorteCinco = "";
        }
        return calCameSuficienciaCorteCinco;
    }

    public String getCalCameSuficienciaCorteSeis() {
        if (calCameSuficienciaCorteSeis == null) {
            calCameSuficienciaCorteSeis = "";
        }
        return calCameSuficienciaCorteSeis;
    }

    public String getCalCameSuficienciaCorteSiete() {
        if (calCameSuficienciaCorteSiete == null) {
            calCameSuficienciaCorteSiete = "";
        }
        return calCameSuficienciaCorteSiete;
    }

    public String getCalCameSuficienciaCorteOcho() {
        if (calCameSuficienciaCorteOcho == null) {
            calCameSuficienciaCorteOcho = "";
        }
        return calCameSuficienciaCorteOcho;
    }

    public String getCalCameSuficienciaCorteNueve() {
        if (calCameSuficienciaCorteNueve == null) {
            calCameSuficienciaCorteNueve = "";
        }
        return calCameSuficienciaCorteNueve;
    }

    public String getCalCameSuficienciaCorteDiez() {
        if (calCameSuficienciaCorteDiez == null) {
            calCameSuficienciaCorteDiez = "";
        }
        return calCameSuficienciaCorteDiez;
    }

    public String getCalCameSuficienciaCorteOnce() {
        if (calCameSuficienciaCorteOnce == null) {
            calCameSuficienciaCorteOnce = "";
        }
        return calCameSuficienciaCorteOnce;
    }

    public String getCalCameSuficienciaCorteDoce() {
        if (calCameSuficienciaCorteDoce == null) {
            calCameSuficienciaCorteDoce = "";
        }
        return calCameSuficienciaCorteDoce;
    }

    public String getCalCameSuficienciaCorteTrece() {
        if (calCameSuficienciaCorteTrece == null) {
            calCameSuficienciaCorteTrece = "";
        }
        return calCameSuficienciaCorteTrece;
    }

}
