package soaint.ws.rest.bo.matAyC;

public class OtrosIndicadores {
    private String indicadoresLabelCorteUno;
    private String indicadoresLabelCorteDos;
    private String indicadoresLabelCorteTres;
    private String indicadoresLabelCorteCuatro;
    private String indicadoresLabelCorteCinco;
    private String indicadoresLabelCorteSeis;
    private String indicadoresLabelCorteSiete;
    private String indicadoresLabelCorteOcho;
    private String indicadoresLabelCorteNueve;
    private String indicadoresLabelCorteDiez;
    private String indicadoresLabelCorteOnce;
    private String indicadoresLabelCorteDoce;
    private String indicadoresLabelCorteTrece;
    private String cuentasCobrarActivosCorteUno;
    private String cuentasCobrarActivosCorteDos;
    private String cuentasCobrarActivosCorteTres;
    private String cuentasCobrarActivosCorteCuatro;
    private String cuentasCobrarActivosCorteCinco;
    private String cuentasCobrarActivosCorteSeis;
    private String cuentasCobrarActivosCorteSiete;
    private String cuentasCobrarActivosCorteOcho;
    private String cuentasCobrarActivosCorteNueve;
    private String cuentasCobrarActivosCorteDiez;
    private String cuentasCobrarActivosCorteOnce;
    private String cuentasCobrarActivosCorteDoce;
    private String cuentasCobrarActivosCorteTrece;
    private String otrosActivosCorteUno;
    private String otrosActivosCorteDos;
    private String otrosActivosCorteTres;
    private String otrosActivosCorteCuatro;
    private String otrosActivosCorteCinco;
    private String otrosActivosCorteSeis;
    private String otrosActivosCorteSiete;
    private String otrosActivosCorteOcho;
    private String otrosActivosCorteNueve;
    private String otrosActivosCorteDiez;
    private String otrosActivosCorteOnce;
    private String otrosActivosCorteDoce;
    private String otrosActivosCorteTrece;
    private String activosImproductivosCorteUno;
    private String activosImproductivosCorteDos;
    private String activosImproductivosCorteTres;
    private String activosImproductivosCorteCuatro;
    private String activosImproductivosCorteCinco;
    private String activosImproductivosCorteSeis;
    private String activosImproductivosCorteSiete;
    private String activosImproductivosCorteOcho;
    private String activosImproductivosCorteNueve;
    private String activosImproductivosCorteDiez;
    private String activosImproductivosCorteOnce;
    private String activosImproductivosCorteDoce;
    private String activosImproductivosCorteTrece;
    private String obligacionesFinancierasCorteUno;
    private String obligacionesFinancierasCorteDos;
    private String obligacionesFinancierasCorteTres;
    private String obligacionesFinancierasCorteCuatro;
    private String obligacionesFinancierasCorteCinco;
    private String obligacionesFinancierasCorteSeis;
    private String obligacionesFinancierasCorteSiete;
    private String obligacionesFinancierasCorteOcho;
    private String obligacionesFinancierasCorteNueve;
    private String obligacionesFinancierasCorteDiez;
    private String obligacionesFinancierasCorteOnce;
    private String obligacionesFinancierasCorteDoce;
    private String obligacionesFinancierasCorteTrece;
    private String gastosIngresoOperacCorteUno;
    private String gastosIngresoOperacCorteDos;
    private String gastosIngresoOperacCorteTres;
    private String gastosIngresoOperacCorteCuatro;
    private String gastosIngresoOperacCorteCinco;
    private String gastosIngresoOperacCorteSeis;
    private String gastosIngresoOperacCorteSiete;
    private String gastosIngresoOperacCorteOcho;
    private String gastosIngresoOperacCorteNueve;
    private String gastosIngresoOperacCorteDiez;
    private String gastosIngresoOperacCorteOnce;
    private String gastosIngresoOperacCorteDoce;
    private String gastosIngresoOperacCorteTrece;
    private String personalGeneralOperacionalCorteUno;
    private String personalGeneralOperacionalCorteDos;
    private String personalGeneralOperacionalCorteTres;
    private String personalGeneralOperacionalCorteCuatro;
    private String personalGeneralOperacionalCorteCinco;
    private String personalGeneralOperacionalCorteSeis;
    private String personalGeneralOperacionalCorteSiete;
    private String personalGeneralOperacionalCorteOcho;
    private String personalGeneralOperacionalCorteNueve;
    private String personalGeneralOperacionalCorteDiez;
    private String personalGeneralOperacionalCorteOnce;
    private String personalGeneralOperacionalCorteDoce;
    private String personalGeneralOperacionalCorteTrece;
    private String rentabilidadOperacionalCorteUno;
    private String rentabilidadOperacionalCorteDos;
    private String rentabilidadOperacionalCorteTres;
    private String rentabilidadOperacionalCorteCuatro;
    private String rentabilidadOperacionalCorteCinco;
    private String rentabilidadOperacionalCorteSeis;
    private String rentabilidadOperacionalCorteSiete;
    private String rentabilidadOperacionalCorteOcho;
    private String rentabilidadOperacionalCorteNueve;
    private String rentabilidadOperacionalCorteDiez;
    private String rentabilidadOperacionalCorteOnce;
    private String rentabilidadOperacionalCorteDoce;
    private String rentabilidadOperacionalCorteTrece;
    private String rentabilidadPatrimonioCorteUno;
    private String rentabilidadPatrimonioCorteDos;
    private String rentabilidadPatrimonioCorteTres;
    private String rentabilidadPatrimonioCorteCuatro;
    private String rentabilidadPatrimonioCorteCinco;
    private String rentabilidadPatrimonioCorteSeis;
    private String rentabilidadPatrimonioCorteSiete;
    private String rentabilidadPatrimonioCorteOcho;
    private String rentabilidadPatrimonioCorteNueve;
    private String rentabilidadPatrimonioCorteDiez;
    private String rentabilidadPatrimonioCorteOnce;
    private String rentabilidadPatrimonioCorteDoce;
    private String rentabilidadPatrimonioCorteTrece;

    public String getIndicadoresLabelCorteUno() {
        if (indicadoresLabelCorteUno == null) {
            indicadoresLabelCorteUno = "";
        }
        return indicadoresLabelCorteUno;
    }

    public String getIndicadoresLabelCorteDos() {
        if (indicadoresLabelCorteDos == null) {
            indicadoresLabelCorteDos = "";
        }
        return indicadoresLabelCorteDos;
    }

    public String getIndicadoresLabelCorteTres() {
        if (indicadoresLabelCorteTres == null) {
            indicadoresLabelCorteTres = "";
        }
        return indicadoresLabelCorteTres;
    }

    public String getIndicadoresLabelCorteCuatro() {
        if (indicadoresLabelCorteCuatro == null) {
            indicadoresLabelCorteCuatro = "";
        }
        return indicadoresLabelCorteCuatro;
    }

    public String getIndicadoresLabelCorteCinco() {
        if (indicadoresLabelCorteCinco == null) {
            indicadoresLabelCorteCinco = "";
        }
        return indicadoresLabelCorteCinco;
    }

    public String getIndicadoresLabelCorteSeis() {
        if (indicadoresLabelCorteSeis == null) {
            indicadoresLabelCorteSeis = "";
        }
        return indicadoresLabelCorteSeis;
    }

    public String getIndicadoresLabelCorteSiete() {
        if (indicadoresLabelCorteSiete == null) {
            indicadoresLabelCorteSiete = "";
        }
        return indicadoresLabelCorteSiete;
    }

    public String getIndicadoresLabelCorteOcho() {
        if (indicadoresLabelCorteOcho == null) {
            indicadoresLabelCorteOcho = "";
        }
        return indicadoresLabelCorteOcho;
    }

    public String getIndicadoresLabelCorteNueve() {
        if (indicadoresLabelCorteNueve == null) {
            indicadoresLabelCorteNueve = "";
        }
        return indicadoresLabelCorteNueve;
    }

    public String getIndicadoresLabelCorteDiez() {
        if (indicadoresLabelCorteDiez == null) {
            indicadoresLabelCorteDiez = "";
        }
        return indicadoresLabelCorteDiez;
    }

    public String getIndicadoresLabelCorteOnce() {
        if (indicadoresLabelCorteOnce == null) {
            indicadoresLabelCorteOnce = "";
        }
        return indicadoresLabelCorteOnce;
    }

    public String getIndicadoresLabelCorteDoce() {
        if (indicadoresLabelCorteDoce == null) {
            indicadoresLabelCorteDoce = "";
        }
        return indicadoresLabelCorteDoce;
    }

    public String getIndicadoresLabelCorteTrece() {
        if (indicadoresLabelCorteTrece == null) {
            indicadoresLabelCorteTrece = "";
        }
        return indicadoresLabelCorteTrece;
    }

    public String getCuentasCobrarActivosCorteUno() {
        if (cuentasCobrarActivosCorteUno == null) {
            cuentasCobrarActivosCorteUno = "";
        }
        return cuentasCobrarActivosCorteUno;
    }

    public String getCuentasCobrarActivosCorteDos() {
        if (cuentasCobrarActivosCorteDos == null) {
            cuentasCobrarActivosCorteDos = "";
        }
        return cuentasCobrarActivosCorteDos;
    }

    public String getCuentasCobrarActivosCorteTres() {
        if (cuentasCobrarActivosCorteTres == null) {
            cuentasCobrarActivosCorteTres = "";
        }
        return cuentasCobrarActivosCorteTres;
    }

    public String getCuentasCobrarActivosCorteCuatro() {
        if (cuentasCobrarActivosCorteCuatro == null) {
            cuentasCobrarActivosCorteCuatro = "";
        }
        return cuentasCobrarActivosCorteCuatro;
    }

    public String getCuentasCobrarActivosCorteCinco() {
        if (cuentasCobrarActivosCorteCinco == null) {
            cuentasCobrarActivosCorteCinco = "";
        }
        return cuentasCobrarActivosCorteCinco;
    }

    public String getCuentasCobrarActivosCorteSeis() {
        if (cuentasCobrarActivosCorteSeis == null) {
            cuentasCobrarActivosCorteSeis = "";
        }
        return cuentasCobrarActivosCorteSeis;
    }

    public String getCuentasCobrarActivosCorteSiete() {
        if (cuentasCobrarActivosCorteSiete == null) {
            cuentasCobrarActivosCorteSiete = "";
        }
        return cuentasCobrarActivosCorteSiete;
    }

    public String getCuentasCobrarActivosCorteOcho() {
        if (cuentasCobrarActivosCorteOcho == null) {
            cuentasCobrarActivosCorteOcho = "";
        }
        return cuentasCobrarActivosCorteOcho;
    }

    public String getCuentasCobrarActivosCorteNueve() {
        if (cuentasCobrarActivosCorteNueve == null) {
            cuentasCobrarActivosCorteNueve = "";
        }
        return cuentasCobrarActivosCorteNueve;
    }

    public String getCuentasCobrarActivosCorteDiez() {
        if (cuentasCobrarActivosCorteDiez == null) {
            cuentasCobrarActivosCorteDiez = "";
        }
        return cuentasCobrarActivosCorteDiez;
    }

    public String getCuentasCobrarActivosCorteOnce() {
        if (cuentasCobrarActivosCorteOnce == null) {
            cuentasCobrarActivosCorteOnce = "";
        }
        return cuentasCobrarActivosCorteOnce;
    }

    public String getCuentasCobrarActivosCorteDoce() {
        if (cuentasCobrarActivosCorteDoce == null) {
            cuentasCobrarActivosCorteDoce = "";
        }
        return cuentasCobrarActivosCorteDoce;
    }

    public String getCuentasCobrarActivosCorteTrece() {
        if (cuentasCobrarActivosCorteTrece == null) {
            cuentasCobrarActivosCorteTrece = "";
        }
        return cuentasCobrarActivosCorteTrece;
    }

    public String getOtrosActivosCorteUno() {
        if (otrosActivosCorteUno == null) {
            otrosActivosCorteUno = "";
        }
        return otrosActivosCorteUno;
    }

    public String getOtrosActivosCorteDos() {
        if (otrosActivosCorteDos == null) {
            otrosActivosCorteDos = "";
        }
        return otrosActivosCorteDos;
    }

    public String getOtrosActivosCorteTres() {
        if (otrosActivosCorteTres == null) {
            otrosActivosCorteTres = "";
        }
        return otrosActivosCorteTres;
    }

    public String getOtrosActivosCorteCuatro() {
        if (otrosActivosCorteCuatro == null) {
            otrosActivosCorteCuatro = "";
        }
        return otrosActivosCorteCuatro;
    }

    public String getOtrosActivosCorteCinco() {
        if (otrosActivosCorteCinco == null) {
            otrosActivosCorteCinco = "";
        }
        return otrosActivosCorteCinco;
    }

    public String getOtrosActivosCorteSeis() {
        if (otrosActivosCorteSeis == null) {
            otrosActivosCorteSeis = "";
        }
        return otrosActivosCorteSeis;
    }

    public String getOtrosActivosCorteSiete() {
        if (otrosActivosCorteSiete == null) {
            otrosActivosCorteSiete = "";
        }
        return otrosActivosCorteSiete;
    }

    public String getOtrosActivosCorteOcho() {
        if (otrosActivosCorteOcho == null) {
            otrosActivosCorteOcho = "";
        }
        return otrosActivosCorteOcho;
    }

    public String getOtrosActivosCorteNueve() {
        if (otrosActivosCorteNueve == null) {
            otrosActivosCorteNueve = "";
        }
        return otrosActivosCorteNueve;
    }

    public String getOtrosActivosCorteDiez() {
        if (otrosActivosCorteDiez == null) {
            otrosActivosCorteDiez = "";
        }
        return otrosActivosCorteDiez;
    }

    public String getOtrosActivosCorteOnce() {
        if (otrosActivosCorteOnce == null) {
            otrosActivosCorteOnce = "";
        }
        return otrosActivosCorteOnce;
    }

    public String getOtrosActivosCorteDoce() {
        if (otrosActivosCorteDoce == null) {
            otrosActivosCorteDoce = "";
        }
        return otrosActivosCorteDoce;
    }

    public String getOtrosActivosCorteTrece() {
        if (otrosActivosCorteTrece == null) {
            otrosActivosCorteTrece = "";
        }
        return otrosActivosCorteTrece;
    }

    public String getActivosImproductivosCorteUno() {
        if (activosImproductivosCorteUno == null) {
            activosImproductivosCorteUno = "";
        }
        return activosImproductivosCorteUno;
    }

    public String getActivosImproductivosCorteDos() {
        if (activosImproductivosCorteDos == null) {
            activosImproductivosCorteDos = "";
        }
        return activosImproductivosCorteDos;
    }

    public String getActivosImproductivosCorteTres() {
        if (activosImproductivosCorteTres == null) {
            activosImproductivosCorteTres = "";
        }
        return activosImproductivosCorteTres;
    }

    public String getActivosImproductivosCorteCuatro() {
        if (activosImproductivosCorteCuatro == null) {
            activosImproductivosCorteCuatro = "";
        }
        return activosImproductivosCorteCuatro;
    }

    public String getActivosImproductivosCorteCinco() {
        if (activosImproductivosCorteCinco == null) {
            activosImproductivosCorteCinco = "";
        }
        return activosImproductivosCorteCinco;
    }

    public String getActivosImproductivosCorteSeis() {
        if (activosImproductivosCorteSeis == null) {
            activosImproductivosCorteSeis = "";
        }
        return activosImproductivosCorteSeis;
    }

    public String getActivosImproductivosCorteSiete() {
        if (activosImproductivosCorteSiete == null) {
            activosImproductivosCorteSiete = "";
        }
        return activosImproductivosCorteSiete;
    }

    public String getActivosImproductivosCorteOcho() {
        if (activosImproductivosCorteOcho == null) {
            activosImproductivosCorteOcho = "";
        }
        return activosImproductivosCorteOcho;
    }

    public String getActivosImproductivosCorteNueve() {
        if (activosImproductivosCorteNueve == null) {
            activosImproductivosCorteNueve = "";
        }
        return activosImproductivosCorteNueve;
    }

    public String getActivosImproductivosCorteDiez() {
        if (activosImproductivosCorteDiez == null) {
            activosImproductivosCorteDiez = "";
        }
        return activosImproductivosCorteDiez;
    }

    public String getActivosImproductivosCorteOnce() {
        if (activosImproductivosCorteOnce == null) {
            activosImproductivosCorteOnce = "";
        }
        return activosImproductivosCorteOnce;
    }

    public String getActivosImproductivosCorteDoce() {
        if (activosImproductivosCorteDoce == null) {
            activosImproductivosCorteDoce = "";
        }
        return activosImproductivosCorteDoce;
    }

    public String getActivosImproductivosCorteTrece() {
        if (activosImproductivosCorteTrece == null) {
            activosImproductivosCorteTrece = "";
        }
        return activosImproductivosCorteTrece;
    }

    public String getObligacionesFinancierasCorteUno() {
        if (obligacionesFinancierasCorteUno == null) {
            obligacionesFinancierasCorteUno = "";
        }
        return obligacionesFinancierasCorteUno;
    }

    public String getObligacionesFinancierasCorteDos() {
        if (obligacionesFinancierasCorteDos == null) {
            obligacionesFinancierasCorteDos = "";
        }
        return obligacionesFinancierasCorteDos;
    }

    public String getObligacionesFinancierasCorteTres() {
        if (obligacionesFinancierasCorteTres == null) {
            obligacionesFinancierasCorteTres = "";
        }
        return obligacionesFinancierasCorteTres;
    }

    public String getObligacionesFinancierasCorteCuatro() {
        if (obligacionesFinancierasCorteCuatro == null) {
            obligacionesFinancierasCorteCuatro = "";
        }
        return obligacionesFinancierasCorteCuatro;
    }

    public String getObligacionesFinancierasCorteCinco() {
        if (obligacionesFinancierasCorteCinco == null) {
            obligacionesFinancierasCorteCinco = "";
        }
        return obligacionesFinancierasCorteCinco;
    }

    public String getObligacionesFinancierasCorteSeis() {
        if (obligacionesFinancierasCorteSeis == null) {
            obligacionesFinancierasCorteSeis = "";
        }
        return obligacionesFinancierasCorteSeis;
    }

    public String getObligacionesFinancierasCorteSiete() {
        if (obligacionesFinancierasCorteSiete == null) {
            obligacionesFinancierasCorteSiete = "";
        }
        return obligacionesFinancierasCorteSiete;
    }

    public String getObligacionesFinancierasCorteOcho() {
        if (obligacionesFinancierasCorteOcho == null) {
            obligacionesFinancierasCorteOcho = "";
        }
        return obligacionesFinancierasCorteOcho;
    }

    public String getObligacionesFinancierasCorteNueve() {
        if (obligacionesFinancierasCorteNueve == null) {
            obligacionesFinancierasCorteNueve = "";
        }
        return obligacionesFinancierasCorteNueve;
    }

    public String getObligacionesFinancierasCorteDiez() {
        if (obligacionesFinancierasCorteDiez == null) {
            obligacionesFinancierasCorteDiez = "";
        }
        return obligacionesFinancierasCorteDiez;
    }

    public String getObligacionesFinancierasCorteOnce() {
        if (obligacionesFinancierasCorteOnce == null) {
            obligacionesFinancierasCorteOnce = "";
        }
        return obligacionesFinancierasCorteOnce;
    }

    public String getObligacionesFinancierasCorteDoce() {
        if (obligacionesFinancierasCorteDoce == null) {
            obligacionesFinancierasCorteDoce = "";
        }
        return obligacionesFinancierasCorteDoce;
    }

    public String getObligacionesFinancierasCorteTrece() {
        if (obligacionesFinancierasCorteTrece == null) {
            obligacionesFinancierasCorteTrece = "";
        }
        return obligacionesFinancierasCorteTrece;
    }

    public String getGastosIngresoOperacCorteUno() {
        if (gastosIngresoOperacCorteUno == null) {
            gastosIngresoOperacCorteUno = "";
        }
        return gastosIngresoOperacCorteUno;
    }

    public String getGastosIngresoOperacCorteDos() {
        if (gastosIngresoOperacCorteDos == null) {
            gastosIngresoOperacCorteDos = "";
        }
        return gastosIngresoOperacCorteDos;
    }

    public String getGastosIngresoOperacCorteTres() {
        if (gastosIngresoOperacCorteTres == null) {
            gastosIngresoOperacCorteTres = "";
        }
        return gastosIngresoOperacCorteTres;
    }

    public String getGastosIngresoOperacCorteCuatro() {
        if (gastosIngresoOperacCorteCuatro == null) {
            gastosIngresoOperacCorteCuatro = "";
        }
        return gastosIngresoOperacCorteCuatro;
    }

    public String getGastosIngresoOperacCorteCinco() {
        if (gastosIngresoOperacCorteCinco == null) {
            gastosIngresoOperacCorteCinco = "";
        }
        return gastosIngresoOperacCorteCinco;
    }

    public String getGastosIngresoOperacCorteSeis() {
        if (gastosIngresoOperacCorteSeis == null) {
            gastosIngresoOperacCorteSeis = "";
        }
        return gastosIngresoOperacCorteSeis;
    }

    public String getGastosIngresoOperacCorteSiete() {
        if (gastosIngresoOperacCorteSiete == null) {
            gastosIngresoOperacCorteSiete = "";
        }
        return gastosIngresoOperacCorteSiete;
    }

    public String getGastosIngresoOperacCorteOcho() {
        if (gastosIngresoOperacCorteOcho == null) {
            gastosIngresoOperacCorteOcho = "";
        }
        return gastosIngresoOperacCorteOcho;
    }

    public String getGastosIngresoOperacCorteNueve() {
        if (gastosIngresoOperacCorteNueve == null) {
            gastosIngresoOperacCorteNueve = "";
        }
        return gastosIngresoOperacCorteNueve;
    }

    public String getGastosIngresoOperacCorteDiez() {
        if (gastosIngresoOperacCorteDiez == null) {
            gastosIngresoOperacCorteDiez = "";
        }
        return gastosIngresoOperacCorteDiez;
    }

    public String getGastosIngresoOperacCorteOnce() {
        if (gastosIngresoOperacCorteOnce == null) {
            gastosIngresoOperacCorteOnce = "";
        }
        return gastosIngresoOperacCorteOnce;
    }

    public String getGastosIngresoOperacCorteDoce() {
        if (gastosIngresoOperacCorteDoce == null) {
            gastosIngresoOperacCorteDoce = "";
        }
        return gastosIngresoOperacCorteDoce;
    }

    public String getGastosIngresoOperacCorteTrece() {
        if (gastosIngresoOperacCorteTrece == null) {
            gastosIngresoOperacCorteTrece = "";
        }
        return gastosIngresoOperacCorteTrece;
    }

    public String getPersonalGeneralOperacionalCorteUno() {
        if (personalGeneralOperacionalCorteUno == null) {
            personalGeneralOperacionalCorteUno = "";
        }
        return personalGeneralOperacionalCorteUno;
    }

    public String getPersonalGeneralOperacionalCorteDos() {
        if (personalGeneralOperacionalCorteDos == null) {
            personalGeneralOperacionalCorteDos = "";
        }
        return personalGeneralOperacionalCorteDos;
    }

    public String getPersonalGeneralOperacionalCorteTres() {
        if (personalGeneralOperacionalCorteTres == null) {
            personalGeneralOperacionalCorteTres = "";
        }
        return personalGeneralOperacionalCorteTres;
    }

    public String getPersonalGeneralOperacionalCorteCuatro() {
        if (personalGeneralOperacionalCorteCuatro == null) {
            personalGeneralOperacionalCorteCuatro = "";
        }
        return personalGeneralOperacionalCorteCuatro;
    }

    public String getPersonalGeneralOperacionalCorteCinco() {
        if (personalGeneralOperacionalCorteCinco == null) {
            personalGeneralOperacionalCorteCinco = "";
        }
        return personalGeneralOperacionalCorteCinco;
    }

    public String getPersonalGeneralOperacionalCorteSeis() {
        if (personalGeneralOperacionalCorteSeis == null) {
            personalGeneralOperacionalCorteSeis = "";
        }
        return personalGeneralOperacionalCorteSeis;
    }

    public String getPersonalGeneralOperacionalCorteSiete() {
        if (personalGeneralOperacionalCorteSiete == null) {
            personalGeneralOperacionalCorteSiete = "";
        }
        return personalGeneralOperacionalCorteSiete;
    }

    public String getPersonalGeneralOperacionalCorteOcho() {
        if (personalGeneralOperacionalCorteOcho == null) {
            personalGeneralOperacionalCorteOcho = "";
        }
        return personalGeneralOperacionalCorteOcho;
    }

    public String getPersonalGeneralOperacionalCorteNueve() {
        if (personalGeneralOperacionalCorteNueve == null) {
            personalGeneralOperacionalCorteNueve = "";
        }
        return personalGeneralOperacionalCorteNueve;
    }

    public String getPersonalGeneralOperacionalCorteDiez() {
        if (personalGeneralOperacionalCorteDiez == null) {
            personalGeneralOperacionalCorteDiez = "";
        }
        return personalGeneralOperacionalCorteDiez;
    }

    public String getPersonalGeneralOperacionalCorteOnce() {
        if (personalGeneralOperacionalCorteOnce == null) {
            personalGeneralOperacionalCorteOnce = "";
        }
        return personalGeneralOperacionalCorteOnce;
    }

    public String getPersonalGeneralOperacionalCorteDoce() {
        if (personalGeneralOperacionalCorteDoce == null) {
            personalGeneralOperacionalCorteDoce = "";
        }
        return personalGeneralOperacionalCorteDoce;
    }

    public String getPersonalGeneralOperacionalCorteTrece() {
        if (personalGeneralOperacionalCorteTrece == null) {
            personalGeneralOperacionalCorteTrece = "";
        }
        return personalGeneralOperacionalCorteTrece;
    }

    public String getRentabilidadOperacionalCorteUno() {
        if (rentabilidadOperacionalCorteUno == null) {
            rentabilidadOperacionalCorteUno = "";
        }
        return rentabilidadOperacionalCorteUno;
    }

    public String getRentabilidadOperacionalCorteDos() {
        if (rentabilidadOperacionalCorteDos == null) {
            rentabilidadOperacionalCorteDos = "";
        }
        return rentabilidadOperacionalCorteDos;
    }

    public String getRentabilidadOperacionalCorteTres() {
        if (rentabilidadOperacionalCorteTres == null) {
            rentabilidadOperacionalCorteTres = "";
        }
        return rentabilidadOperacionalCorteTres;
    }

    public String getRentabilidadOperacionalCorteCuatro() {
        if (rentabilidadOperacionalCorteCuatro == null) {
            rentabilidadOperacionalCorteCuatro = "";
        }
        return rentabilidadOperacionalCorteCuatro;
    }

    public String getRentabilidadOperacionalCorteCinco() {
        if (rentabilidadOperacionalCorteCinco == null) {
            rentabilidadOperacionalCorteCinco = "";
        }
        return rentabilidadOperacionalCorteCinco;
    }

    public String getRentabilidadOperacionalCorteSeis() {
        if (rentabilidadOperacionalCorteSeis == null) {
            rentabilidadOperacionalCorteSeis = "";
        }
        return rentabilidadOperacionalCorteSeis;
    }

    public String getRentabilidadOperacionalCorteSiete() {
        if (rentabilidadOperacionalCorteSiete == null) {
            rentabilidadOperacionalCorteSiete = "";
        }
        return rentabilidadOperacionalCorteSiete;
    }

    public String getRentabilidadOperacionalCorteOcho() {
        if (rentabilidadOperacionalCorteOcho == null) {
            rentabilidadOperacionalCorteOcho = "";
        }
        return rentabilidadOperacionalCorteOcho;
    }

    public String getRentabilidadOperacionalCorteNueve() {
        if (rentabilidadOperacionalCorteNueve == null) {
            rentabilidadOperacionalCorteNueve = "";
        }
        return rentabilidadOperacionalCorteNueve;
    }

    public String getRentabilidadOperacionalCorteDiez() {
        if (rentabilidadOperacionalCorteDiez == null) {
            rentabilidadOperacionalCorteDiez = "";
        }
        return rentabilidadOperacionalCorteDiez;
    }

    public String getRentabilidadOperacionalCorteOnce() {
        if (rentabilidadOperacionalCorteOnce == null) {
            rentabilidadOperacionalCorteOnce = "";
        }
        return rentabilidadOperacionalCorteOnce;
    }

    public String getRentabilidadOperacionalCorteDoce() {
        if (rentabilidadOperacionalCorteDoce == null) {
            rentabilidadOperacionalCorteDoce = "";
        }
        return rentabilidadOperacionalCorteDoce;
    }

    public String getRentabilidadOperacionalCorteTrece() {
        if (rentabilidadOperacionalCorteTrece == null) {
            rentabilidadOperacionalCorteTrece = "";
        }
        return rentabilidadOperacionalCorteTrece;
    }

    public String getRentabilidadPatrimonioCorteUno() {
        if (rentabilidadPatrimonioCorteUno == null) {
            rentabilidadPatrimonioCorteUno = "";
        }
        return rentabilidadPatrimonioCorteUno;
    }

    public String getRentabilidadPatrimonioCorteDos() {
        if (rentabilidadPatrimonioCorteDos == null) {
            rentabilidadPatrimonioCorteDos = "";
        }
        return rentabilidadPatrimonioCorteDos;
    }

    public String getRentabilidadPatrimonioCorteTres() {
        if (rentabilidadPatrimonioCorteTres == null) {
            rentabilidadPatrimonioCorteTres = "";
        }
        return rentabilidadPatrimonioCorteTres;
    }

    public String getRentabilidadPatrimonioCorteCuatro() {
        if (rentabilidadPatrimonioCorteCuatro == null) {
            rentabilidadPatrimonioCorteCuatro = "";
        }
        return rentabilidadPatrimonioCorteCuatro;
    }

    public String getRentabilidadPatrimonioCorteCinco() {
        if (rentabilidadPatrimonioCorteCinco == null) {
            rentabilidadPatrimonioCorteCinco = "";
        }
        return rentabilidadPatrimonioCorteCinco;
    }

    public String getRentabilidadPatrimonioCorteSeis() {
        if (rentabilidadPatrimonioCorteSeis == null) {
            rentabilidadPatrimonioCorteSeis = "";
        }
        return rentabilidadPatrimonioCorteSeis;
    }

    public String getRentabilidadPatrimonioCorteSiete() {
        if (rentabilidadPatrimonioCorteSiete == null) {
            rentabilidadPatrimonioCorteSiete = "";
        }
        return rentabilidadPatrimonioCorteSiete;
    }

    public String getRentabilidadPatrimonioCorteOcho() {
        if (rentabilidadPatrimonioCorteOcho == null) {
            rentabilidadPatrimonioCorteOcho = "";
        }
        return rentabilidadPatrimonioCorteOcho;
    }

    public String getRentabilidadPatrimonioCorteNueve() {
        if (rentabilidadPatrimonioCorteNueve == null) {
            rentabilidadPatrimonioCorteNueve = "";
        }
        return rentabilidadPatrimonioCorteNueve;
    }

    public String getRentabilidadPatrimonioCorteDiez() {
        if (rentabilidadPatrimonioCorteDiez == null) {
            rentabilidadPatrimonioCorteDiez = "";
        }
        return rentabilidadPatrimonioCorteDiez;
    }

    public String getRentabilidadPatrimonioCorteOnce() {
        if (rentabilidadPatrimonioCorteOnce == null) {
            rentabilidadPatrimonioCorteOnce = "";
        }
        return rentabilidadPatrimonioCorteOnce;
    }

    public String getRentabilidadPatrimonioCorteDoce() {
        if (rentabilidadPatrimonioCorteDoce == null) {
            rentabilidadPatrimonioCorteDoce = "";
        }
        return rentabilidadPatrimonioCorteDoce;
    }

    public String getRentabilidadPatrimonioCorteTrece() {
        if (rentabilidadPatrimonioCorteTrece == null) {
            rentabilidadPatrimonioCorteTrece = "";
        }
        return rentabilidadPatrimonioCorteTrece;
    }

}
