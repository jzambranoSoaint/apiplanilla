package soaint.ws.rest.bo.matAyC;

public class ParametrosModeloAlertaTemprana {
    private ModeloAlertaTemprana modeloAlertaTemprana;

    public ModeloAlertaTemprana getModeloAlertaTemprana() {
        if (modeloAlertaTemprana == null) {
            modeloAlertaTemprana = new ModeloAlertaTemprana();
        }
        return modeloAlertaTemprana;
    }

    public void setModeloAlertaTemprana(ModeloAlertaTemprana modeloAlertaTemprana) {
        this.modeloAlertaTemprana = modeloAlertaTemprana;
    }
}
