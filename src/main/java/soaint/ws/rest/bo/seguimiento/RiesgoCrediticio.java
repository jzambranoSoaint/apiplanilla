package soaint.ws.rest.bo.seguimiento;

public class RiesgoCrediticio {
    private String labelRiesCreCorteUno;
    private String labelRiesCreCorteDos;
    private String labelRiesCreCorteTres;
    private String labelRiesCreCorteCuatro;
    private String indCalCarteraCorteUno;
    private String indCalCarteraCorteDos;
    private String indCalCarteraCorteTres;
    private String indCalCarteraCorteCuatro;
    private String indCarImproductivaCorteUno;
    private String indCarImproductivaCorteDos;
    private String indCarImproductivaCorteTres;
    private String indCarImproductivaCorteCuatro;
    private String cubCarBrutaCorteUno;
    private String cubCarBrutaCorteDos;
    private String cubCarBrutaCorteTres;
    private String cubCarBrutaCorteCuatro;
    private String cubCarVencidaCorteUno;
    private String cubCarVencidaCorteDos;
    private String cubCarVencidaCorteTres;
    private String cubCarVencidaCorteCuatro;
    private String indReestructuracionesCorteUno;
    private String indReestructuracionesCorteDos;
    private String indReestructuracionesCorteTres;
    private String indReestructuracionesCorteCuatro;
    private String carCastigadaCorteUno;
    private String carCastigadaCorteDos;
    private String carCastigadaCorteTres;
    private String carCastigadaCorteCuatro;


    public String getLabelRiesCreCorteUno() {
        if (labelRiesCreCorteUno == null) {
            labelRiesCreCorteUno = "";
        }
        return labelRiesCreCorteUno;
    }

    public String getLabelRiesCreCorteDos() {
        if (labelRiesCreCorteDos == null) {
            labelRiesCreCorteDos = "";
        }
        return labelRiesCreCorteDos;
    }

    public String getLabelRiesCreCorteTres() {
        if (labelRiesCreCorteTres == null) {
            labelRiesCreCorteTres = "";
        }
        return labelRiesCreCorteTres;
    }

    public String getLabelRiesCreCorteCuatro() {
        if (labelRiesCreCorteCuatro == null) {
            labelRiesCreCorteCuatro = "";
        }
        return labelRiesCreCorteCuatro;
    }

    public String getIndCalCarteraCorteUno() {
        if (indCalCarteraCorteUno == null) {
            indCalCarteraCorteUno = "";
        }
        return indCalCarteraCorteUno;
    }

    public String getIndCalCarteraCorteDos() {
        if (indCalCarteraCorteDos == null) {
            indCalCarteraCorteDos = "";
        }
        return indCalCarteraCorteDos;
    }

    public String getIndCalCarteraCorteTres() {
        if (indCalCarteraCorteTres == null) {
            indCalCarteraCorteTres = "";
        }
        return indCalCarteraCorteTres;
    }

    public String getIndCalCarteraCorteCuatro() {
        if (indCalCarteraCorteCuatro == null) {
            indCalCarteraCorteCuatro = "";
        }
        return indCalCarteraCorteCuatro;
    }

    public String getIndCarImproductivaCorteUno() {
        if (indCarImproductivaCorteUno == null) {
            indCarImproductivaCorteUno = "";
        }
        return indCarImproductivaCorteUno;
    }

    public String getIndCarImproductivaCorteDos() {
        if (indCarImproductivaCorteDos == null) {
            indCarImproductivaCorteDos = "";
        }
        return indCarImproductivaCorteDos;
    }

    public String getIndCarImproductivaCorteTres() {
        if (indCarImproductivaCorteTres == null) {
            indCarImproductivaCorteTres = "";
        }
        return indCarImproductivaCorteTres;
    }

    public String getIndCarImproductivaCorteCuatro() {
        if (indCarImproductivaCorteCuatro == null) {
            indCarImproductivaCorteCuatro = "";
        }
        return indCarImproductivaCorteCuatro;
    }

    public String getCubCarBrutaCorteUno() {
        if (cubCarBrutaCorteUno == null) {
            cubCarBrutaCorteUno = "";
        }
        return cubCarBrutaCorteUno;
    }

    public String getCubCarBrutaCorteDos() {
        if (cubCarBrutaCorteDos == null) {
            cubCarBrutaCorteDos = "";
        }
        return cubCarBrutaCorteDos;
    }

    public String getCubCarBrutaCorteTres() {
        if (cubCarBrutaCorteTres == null) {
            cubCarBrutaCorteTres = "";
        }
        return cubCarBrutaCorteTres;
    }
    public String getCubCarBrutaCorteCuatro() {
        if (cubCarBrutaCorteCuatro == null) {
            cubCarBrutaCorteCuatro = "";
        }
        return cubCarBrutaCorteCuatro;
    }

    public String getCubCarVencidaCorteUno() {
        if (cubCarVencidaCorteUno == null) {
            cubCarVencidaCorteUno = "";
        }
        return cubCarVencidaCorteUno;
    }

    public String getCubCarVencidaCorteDos() {
        if (cubCarVencidaCorteDos == null) {
            cubCarVencidaCorteDos = "";
        }
        return cubCarVencidaCorteDos;
    }

    public String getCubCarVencidaCorteTres() {
        if (cubCarVencidaCorteTres == null) {
            cubCarVencidaCorteTres = "";
        }
        return cubCarVencidaCorteTres;
    }

    public String getCubCarVencidaCorteCuatro() {
        if (cubCarVencidaCorteCuatro == null) {
            cubCarVencidaCorteCuatro = "";
        }
        return cubCarVencidaCorteCuatro;
    }

    public String getIndReestructuracionesCorteUno() {
        if (indReestructuracionesCorteUno == null) {
            indReestructuracionesCorteUno = "";
        }
        return indReestructuracionesCorteUno;
    }

    public String getIndReestructuracionesCorteDos() {
        if (indReestructuracionesCorteDos == null) {
            indReestructuracionesCorteDos = "";
        }
        return indReestructuracionesCorteDos;
    }

    public String getIndReestructuracionesCorteTres() {
        if (indReestructuracionesCorteTres == null) {
            indReestructuracionesCorteTres = "";
        }
        return indReestructuracionesCorteTres;
    }

    public String getIndReestructuracionesCorteCuatro() {
        if (indReestructuracionesCorteCuatro == null) {
            indReestructuracionesCorteCuatro = "";
        }
        return indReestructuracionesCorteCuatro;
    }

    public String getCarCastigadaCorteUno() {
        if (carCastigadaCorteUno == null) {
            carCastigadaCorteUno = "";
        }
        return carCastigadaCorteUno;
    }

    public String getCarCastigadaCorteDos() {
        if (carCastigadaCorteDos == null) {
            carCastigadaCorteDos = "";
        }
        return carCastigadaCorteDos;
    }

    public String getCarCastigadaCorteTres() {
        if (carCastigadaCorteTres == null) {
            carCastigadaCorteTres = "";
        }
        return carCastigadaCorteTres;
    }

    public String getCarCastigadaCorteCuatro() {
        if (carCastigadaCorteCuatro == null) {
            carCastigadaCorteCuatro = "";
        }
        return carCastigadaCorteCuatro;
    }

}
