package soaint.ws.rest.bo.seguimiento;

public class ParametrosOtrosDatosGenerales {
    private OtrosDatosGenerales otrosDatosGenerales;

    public OtrosDatosGenerales getOtrosDatosGenerales() {
        if (otrosDatosGenerales == null) {
            otrosDatosGenerales = new OtrosDatosGenerales();
        }
        return otrosDatosGenerales;
    }

    public void setOtrosDatosGenerales(OtrosDatosGenerales otrosDatosGenerales) {
        this.otrosDatosGenerales = otrosDatosGenerales;
    }
}
