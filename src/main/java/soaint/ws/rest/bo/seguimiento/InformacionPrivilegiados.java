package soaint.ws.rest.bo.seguimiento;

public class InformacionPrivilegiados {
    private String tasaPromCapGraAhorradoresCorteUno;
    private String tasaPromCapGraAhorradoresCorteDos;
    private String tasaPromCapGraAhorradoresCorteTres;
    private String tasaPromCapGraAhorradoresCorteCuatro;
    private String tasaPromColGraDeudoresCorteUno;
    private String tasaPromColGraDeudoresCorteDos;
    private String tasaPromColGraDeudoresCorteTres;
    private String tasaPromColGraDeudoresCorteCuatro;
    private String tasaPromCapMiemPrivilegiadosCorteUno;
    private String tasaPromCapMiemPrivilegiadosCorteDos;
    private String tasaPromCapMiemPrivilegiadosCorteTres;
    private String tasaPromCapMiemPrivilegiadosCorteCuatro;
    private String tasaPromColMiemPrivilegiadosCorteUno;
    private String tasaPromColMiemPrivilegiadosCorteDos;
    private String tasaPromColMiemPrivilegiadosCorteTres;
    private String tasaPromColMiemPrivilegiadosCorteCuatro;
    private String posNetaPrivilegiadosCorteUno;
    private String posNetaPrivilegiadosCorteDos;
    private String posNetaPrivilegiadosCorteTres;
    private String posNetaPrivilegiadosCorteCuatro;
    private String maxCalCarteraPrivilegiosCorteUno;
    private String maxCalCarteraPrivilegiosCorteDos;
    private String maxCalCarteraPrivilegiosCorteTres;
    private String maxCalCarteraPrivilegiosCorteCuatro;


    public String getTasaPromCapGraAhorradoresCorteUno() {
        if (tasaPromCapGraAhorradoresCorteUno == null) {
            tasaPromCapGraAhorradoresCorteUno = "";
        }
        return tasaPromCapGraAhorradoresCorteUno;
    }

    public String getTasaPromCapGraAhorradoresCorteDos() {
        if (tasaPromCapGraAhorradoresCorteDos == null) {
            tasaPromCapGraAhorradoresCorteDos = "";
        }
        return tasaPromCapGraAhorradoresCorteDos;
    }

    public String getTasaPromCapGraAhorradoresCorteTres() {
        if (tasaPromCapGraAhorradoresCorteTres == null) {
            tasaPromCapGraAhorradoresCorteTres = "";
        }
        return tasaPromCapGraAhorradoresCorteTres;
    }

    public String getTasaPromCapGraAhorradoresCorteCuatro() {
        if (tasaPromCapGraAhorradoresCorteCuatro == null) {
            tasaPromCapGraAhorradoresCorteCuatro = "";
        }
        return tasaPromCapGraAhorradoresCorteCuatro;
    }

    public String getTasaPromColGraDeudoresCorteUno() {
        if (tasaPromColGraDeudoresCorteUno == null) {
            tasaPromColGraDeudoresCorteUno = "";
        }
        return tasaPromColGraDeudoresCorteUno;
    }

    public String getTasaPromColGraDeudoresCorteDos() {
        if (tasaPromColGraDeudoresCorteDos == null) {
            tasaPromColGraDeudoresCorteDos = "";
        }
        return tasaPromColGraDeudoresCorteDos;
    }

    public String getTasaPromColGraDeudoresCorteTres() {
        if (tasaPromColGraDeudoresCorteTres == null) {
            tasaPromColGraDeudoresCorteTres = "";
        }
        return tasaPromColGraDeudoresCorteTres;
    }

    public String getTasaPromColGraDeudoresCorteCuatro() {
        if (tasaPromColGraDeudoresCorteCuatro == null) {
            tasaPromColGraDeudoresCorteCuatro = "";
        }
        return tasaPromColGraDeudoresCorteCuatro;
    }

    public String getTasaPromCapMiemPrivilegiadosCorteUno() {
        if (tasaPromCapMiemPrivilegiadosCorteUno == null) {
            tasaPromCapMiemPrivilegiadosCorteUno = "";
        }
        return tasaPromCapMiemPrivilegiadosCorteUno;
    }

    public String getTasaPromCapMiemPrivilegiadosCorteDos() {
        if (tasaPromCapMiemPrivilegiadosCorteDos == null) {
            tasaPromCapMiemPrivilegiadosCorteDos = "";
        }
        return tasaPromCapMiemPrivilegiadosCorteDos;
    }

    public String getTasaPromCapMiemPrivilegiadosCorteTres() {
        if (tasaPromCapMiemPrivilegiadosCorteTres == null) {
            tasaPromCapMiemPrivilegiadosCorteTres = "";
        }
        return tasaPromCapMiemPrivilegiadosCorteTres;
    }

    public String getTasaPromCapMiemPrivilegiadosCorteCuatro() {
        if (tasaPromCapMiemPrivilegiadosCorteCuatro == null) {
            tasaPromCapMiemPrivilegiadosCorteCuatro = "";
        }
        return tasaPromCapMiemPrivilegiadosCorteCuatro;
    }

    public String getTasaPromColMiemPrivilegiadosCorteUno() {
        if (tasaPromColMiemPrivilegiadosCorteUno == null) {
            tasaPromColMiemPrivilegiadosCorteUno = "";
        }
        return tasaPromColMiemPrivilegiadosCorteUno;
    }

    public String getTasaPromColMiemPrivilegiadosCorteDos() {
        if (tasaPromColMiemPrivilegiadosCorteDos == null) {
            tasaPromColMiemPrivilegiadosCorteDos = "";
        }
        return tasaPromColMiemPrivilegiadosCorteDos;
    }

    public String getTasaPromColMiemPrivilegiadosCorteTres() {
        if (tasaPromColMiemPrivilegiadosCorteTres == null) {
            tasaPromColMiemPrivilegiadosCorteTres = "";
        }
        return tasaPromColMiemPrivilegiadosCorteTres;
    }

    public String getTasaPromColMiemPrivilegiadosCorteCuatro() {
        if (tasaPromColMiemPrivilegiadosCorteCuatro == null) {
            tasaPromColMiemPrivilegiadosCorteCuatro = "";
        }
        return tasaPromColMiemPrivilegiadosCorteCuatro;
    }

    public String getPosNetaPrivilegiadosCorteUno() {
        if (posNetaPrivilegiadosCorteUno == null) {
            posNetaPrivilegiadosCorteUno = "";
        }
        return posNetaPrivilegiadosCorteUno;
    }

    public String getPosNetaPrivilegiadosCorteDos() {
        if (posNetaPrivilegiadosCorteDos == null) {
            posNetaPrivilegiadosCorteDos = "";
        }
        return posNetaPrivilegiadosCorteDos;
    }

    public String getPosNetaPrivilegiadosCorteTres() {
        if (posNetaPrivilegiadosCorteTres == null) {
            posNetaPrivilegiadosCorteTres = "";
        }
        return posNetaPrivilegiadosCorteTres;
    }

    public String getPosNetaPrivilegiadosCorteCuatro() {
        if (posNetaPrivilegiadosCorteCuatro == null) {
            posNetaPrivilegiadosCorteCuatro = "";
        }
        return posNetaPrivilegiadosCorteCuatro;
    }

    public String getMaxCalCarteraPrivilegiosCorteUno() {
        if (maxCalCarteraPrivilegiosCorteUno == null) {
            maxCalCarteraPrivilegiosCorteUno = "";
        }
        return maxCalCarteraPrivilegiosCorteUno;
    }

    public String getMaxCalCarteraPrivilegiosCorteDos() {
        if (maxCalCarteraPrivilegiosCorteDos == null) {
            maxCalCarteraPrivilegiosCorteDos = "";
        }
        return maxCalCarteraPrivilegiosCorteDos;
    }

    public String getMaxCalCarteraPrivilegiosCorteTres() {
        if (maxCalCarteraPrivilegiosCorteTres == null) {
            maxCalCarteraPrivilegiosCorteTres = "";
        }
        return maxCalCarteraPrivilegiosCorteTres;
    }

    public String getMaxCalCarteraPrivilegiosCorteCuatro() {
        if (maxCalCarteraPrivilegiosCorteCuatro == null) {
            maxCalCarteraPrivilegiosCorteCuatro = "";
        }
        return maxCalCarteraPrivilegiosCorteCuatro;
    }

}
