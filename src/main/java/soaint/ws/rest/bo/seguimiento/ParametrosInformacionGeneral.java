package soaint.ws.rest.bo.seguimiento;

public class ParametrosInformacionGeneral {
    private InformacionGeneral informacionGeneral;

    public InformacionGeneral getInformacionGeneral() {
        if (informacionGeneral == null) {
            informacionGeneral = new InformacionGeneral();
        }
        return informacionGeneral;
    }

    public void setInformacionGeneral(InformacionGeneral informacionGeneral) {
        this.informacionGeneral = informacionGeneral;
    }
}
