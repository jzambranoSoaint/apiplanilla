package soaint.ws.rest.bo.seguimiento;

public class RiesgoMercado {
    private String tasaActPromedioCorteUno;
    private String tasaActPromedioCorteDos;
    private String tasaActPromedioCorteTres;
    private String tasaActPromedioCorteCuatro;
    private String tasaPasivaCDATCorteUno;
    private String tasaPasivaCDATCorteDos;
    private String tasaPasivaCDATCorteTres;
    private String tasaPasivaCDATCorteCuatro;
    private String tasaPasivaPromCorteUno;
    private String tasaPasivaPromCorteDos;
    private String tasaPasivaPromCorteTres;
    private String tasaPasivaPromCorteCuatro;
    private String margenTasaCorteUno;
    private String margenTasaCorteDos;
    private String margenTasaCorteTres;
    private String margenTasaCorteCuatro;
    private String dftVigCorteCorteUno;
    private String dftVigCorteCorteDos;
    private String dftVigCorteCorteTres;
    private String dftVigCorteCorteCuatro;
    private String plaPromCaptacionesCorteUno;
    private String plaPromCaptacionesCorteDos;
    private String plaPromCaptacionesCorteTres;
    private String plaPromCaptacionesCorteCuatro;
    private String plaPromCarteraCorteUno;
    private String plaPromCarteraCorteDos;
    private String plaPromCarteraCorteTres;
    private String plaPromCarteraCorteCuatro;


    public String getTasaActPromedioCorteUno() {
        if (tasaActPromedioCorteUno == null) {
            tasaActPromedioCorteUno = "";
        }
        return tasaActPromedioCorteUno;
    }

    public String getTasaActPromedioCorteDos() {
        if (tasaActPromedioCorteDos == null) {
            tasaActPromedioCorteDos = "";
        }
        return tasaActPromedioCorteDos;
    }

    public String getTasaActPromedioCorteTres() {
        if (tasaActPromedioCorteTres == null) {
            tasaActPromedioCorteTres = "";
        }
        return tasaActPromedioCorteTres;
    }

    public String getTasaActPromedioCorteCuatro() {
        if (tasaActPromedioCorteCuatro == null) {
            tasaActPromedioCorteCuatro = "";
        }
        return tasaActPromedioCorteCuatro;
    }

    public String getTasaPasivaCDATCorteUno() {
        if (tasaPasivaCDATCorteUno == null) {
            tasaPasivaCDATCorteUno = "";
        }
        return tasaPasivaCDATCorteUno;
    }

    public String getTasaPasivaCDATCorteDos() {
        if (tasaPasivaCDATCorteDos == null) {
            tasaPasivaCDATCorteDos = "";
        }
        return tasaPasivaCDATCorteDos;
    }

    public String getTasaPasivaCDATCorteTres() {
        if (tasaPasivaCDATCorteTres == null) {
            tasaPasivaCDATCorteTres = "";
        }
        return tasaPasivaCDATCorteTres;
    }

    public String getTasaPasivaCDATCorteCuatro() {
        if (tasaPasivaCDATCorteCuatro == null) {
            tasaPasivaCDATCorteCuatro = "";
        }
        return tasaPasivaCDATCorteCuatro;
    }

    public String getTasaPasivaPromCorteUno() {
        if (tasaPasivaPromCorteUno == null) {
            tasaPasivaPromCorteUno = "";
        }
        return tasaPasivaPromCorteUno;
    }

    public String getTasaPasivaPromCorteDos() {
        if (tasaPasivaPromCorteDos == null) {
            tasaPasivaPromCorteDos = "";
        }
        return tasaPasivaPromCorteDos;
    }

    public String getTasaPasivaPromCorteTres() {
        if (tasaPasivaPromCorteTres == null) {
            tasaPasivaPromCorteTres = "";
        }
        return tasaPasivaPromCorteTres;
    }

    public String getTasaPasivaPromCorteCuatro() {
        if (tasaPasivaPromCorteCuatro == null) {
            tasaPasivaPromCorteCuatro = "";
        }
        return tasaPasivaPromCorteCuatro;
    }

    public String getMargenTasaCorteUno() {
        if (margenTasaCorteUno == null) {
            margenTasaCorteUno = "";
        }
        return margenTasaCorteUno;
    }

    public String getMargenTasaCorteDos() {
        if (margenTasaCorteDos == null) {
            margenTasaCorteDos = "";
        }
        return margenTasaCorteDos;
    }

    public String getMargenTasaCorteTres() {
        if (margenTasaCorteTres == null) {
            margenTasaCorteTres = "";
        }
        return margenTasaCorteTres;
    }

    public String getMargenTasaCorteCuatro() {
        if (margenTasaCorteCuatro == null) {
            margenTasaCorteCuatro = "";
        }
        return margenTasaCorteCuatro;
    }

    public String getDftVigCorteCorteUno() {
        if (dftVigCorteCorteUno == null) {
            dftVigCorteCorteUno = "";
        }
        return dftVigCorteCorteUno;
    }

    public String getDftVigCorteCorteDos() {
        if (dftVigCorteCorteDos == null) {
            dftVigCorteCorteDos = "";
        }
        return dftVigCorteCorteDos;
    }

    public String getDftVigCorteCorteTres() {
        if (dftVigCorteCorteTres == null) {
            dftVigCorteCorteTres = "";
        }
        return dftVigCorteCorteTres;
    }

    public String getDftVigCorteCorteCuatro() {
        if (dftVigCorteCorteCuatro == null) {
            dftVigCorteCorteCuatro = "";
        }
        return dftVigCorteCorteCuatro;
    }

    public String getPlaPromCaptacionesCorteUno() {
        if (plaPromCaptacionesCorteUno == null) {
            plaPromCaptacionesCorteUno = "";
        }
        return plaPromCaptacionesCorteUno;
    }

    public String getPlaPromCaptacionesCorteDos() {
        if (plaPromCaptacionesCorteDos == null) {
            plaPromCaptacionesCorteDos = "";
        }
        return plaPromCaptacionesCorteDos;
    }

    public String getPlaPromCaptacionesCorteTres() {
        if (plaPromCaptacionesCorteTres == null) {
            plaPromCaptacionesCorteTres = "";
        }
        return plaPromCaptacionesCorteTres;
    }

    public String getPlaPromCaptacionesCorteCuatro() {
        if (plaPromCaptacionesCorteCuatro == null) {
            plaPromCaptacionesCorteCuatro = "";
        }
        return plaPromCaptacionesCorteCuatro;
    }

    public String getPlaPromCarteraCorteUno() {
        if (plaPromCarteraCorteUno == null) {
            plaPromCarteraCorteUno = "";
        }
        return plaPromCarteraCorteUno;
    }

    public String getPlaPromCarteraCorteDos() {
        if (plaPromCarteraCorteDos == null) {
            plaPromCarteraCorteDos = "";
        }
        return plaPromCarteraCorteDos;
    }

    public String getPlaPromCarteraCorteTres() {
        if (plaPromCarteraCorteTres == null) {
            plaPromCarteraCorteTres = "";
        }
        return plaPromCarteraCorteTres;
    }

    public String getPlaPromCarteraCorteCuatro() {
        if (plaPromCarteraCorteCuatro == null) {
            plaPromCarteraCorteCuatro = "";
        }
        return plaPromCarteraCorteCuatro;
    }

}
