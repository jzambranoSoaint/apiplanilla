package soaint.ws.rest.bo.seguimiento;

public class InformacionGeneral {
    private String gerente;
    private String tipoCooperativa;
    private String fechaConstitucion;
    private String fechaAutorizacion;
    private String fechaInscripcion;
    private String tipoInscripcion;
    private String fechaConvenio;
    private String localizacionPrincipal;
    private String perfilAsociados;
    private String localizacionOficinas;

    public String getGerente() {
        if (gerente == null) {
            gerente = "";
        }
        return gerente;
    }

    public String getTipoCooperativa() {
        if (tipoCooperativa == null) {
            tipoCooperativa = "";
        }
        return tipoCooperativa;
    }

    public String getFechaConstitucion() {
        if (fechaConstitucion == null) {
            fechaConstitucion = "";
        }
        return fechaConstitucion;
    }

    public String getFechaAutorizacion() {
        if (fechaAutorizacion == null) {
            fechaAutorizacion = "";
        }
        return fechaAutorizacion;
    }

    public String getFechaInscripcion() {
        if (fechaInscripcion == null) {
            fechaInscripcion = "";
        }
        return fechaInscripcion;
    }

    public String getTipoInscripcion() {
        if (tipoInscripcion == null) {
            tipoInscripcion = "";
        }
        return tipoInscripcion;
    }

    public String getFechaConvenio() {
        if (fechaConvenio == null) {
            fechaConvenio = "";
        }
        return fechaConvenio;
    }

    public String getLocalizacionPrincipal() {
        if (localizacionPrincipal == null) {
            localizacionPrincipal = "";
        }
        return localizacionPrincipal;
    }

    public String getPerfilAsociados() {
        if (perfilAsociados == null) {
            perfilAsociados = "";
        }
        return perfilAsociados;
    }

    public String getLocalizacionOficinas() {
        if (localizacionOficinas == null) {
            localizacionOficinas = "";
        }
        return localizacionOficinas;
    }

}
