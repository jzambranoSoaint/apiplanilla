package soaint.ws.rest.bo.seguimiento;

public class ParametrosCriteriosInscripcion {
    private CriteriosInscripcion criteriosInscripcion;

    public CriteriosInscripcion getCriteriosInscripcion() {
        if (criteriosInscripcion == null) {
            criteriosInscripcion = new CriteriosInscripcion();
        }
        return criteriosInscripcion;
    }

    public void setCriteriosInscripcion(CriteriosInscripcion criteriosInscripcion) {
        this.criteriosInscripcion = criteriosInscripcion;
    }
}
