package soaint.ws.rest.bo.seguimiento;

public class OtrosDatosGenerales {
    private String con5MayoresAhorradoresCorteUno;
    private String con5MayoresAhorradoresCorteDos;
    private String con5MayoresAhorradoresCorteTres;
    private String con5MayoresAhorradoresCorteCuatro;
    private String con5MayoresDeudoresCorteUno;
    private String con5MayoresDeudoresCorteDos;
    private String con5MayoresDeudoresCorteTres;
    private String con5MayoresDeudoresCorteCuatro;
    private String numAhorradoresCorteUno;
    private String numAhorradoresCorteDos;
    private String numAhorradoresCorteTres;
    private String numAhorradoresCorteCuatro;
    private String numDeudoresCorteUno;
    private String numDeudoresCorteDos;
    private String numDeudoresCorteTres;
    private String numDeudoresCorteCuatro;
    private String numAsociadosCorteUno;
    private String numAsociadosCorteDos;
    private String numAsociadosCorteTres;
    private String numAsociadosCorteCuatro;
    private String numEmpleadosCorteUno;
    private String numEmpleadosCorteDos;
    private String numEmpleadosCorteTres;
    private String numEmpleadosCorteCuatro;
    private String numOficinasCorteUno;
    private String numOficinasCorteDos;
    private String numOficionasCorteTres;
    private String numOficionasCorteCuatro;

    public String getCon5MayoresAhorradoresCorteUno() {
        if (con5MayoresAhorradoresCorteUno == null) {
            con5MayoresAhorradoresCorteUno = "";
        }
        return con5MayoresAhorradoresCorteUno;
    }

    public String getCon5MayoresAhorradoresCorteDos() {
        if (con5MayoresAhorradoresCorteDos == null) {
            con5MayoresAhorradoresCorteDos = "";
        }
        return con5MayoresAhorradoresCorteDos;
    }

    public String getCon5MayoresAhorradoresCorteTres() {
        if (con5MayoresAhorradoresCorteTres == null) {
            con5MayoresAhorradoresCorteTres = "";
        }
        return con5MayoresAhorradoresCorteTres;
    }

    public String getCon5MayoresAhorradoresCorteCuatro() {
        if (con5MayoresAhorradoresCorteCuatro == null) {
            con5MayoresAhorradoresCorteCuatro = "";
        }
        return con5MayoresAhorradoresCorteCuatro;
    }

    public String getCon5MayoresDeudoresCorteUno() {
        if (con5MayoresDeudoresCorteUno == null) {
            con5MayoresDeudoresCorteUno = "";
        }
        return con5MayoresDeudoresCorteUno;
    }

    public String getCon5MayoresDeudoresCorteDos() {
        if (con5MayoresDeudoresCorteDos == null) {
            con5MayoresDeudoresCorteDos = "";
        }
        return con5MayoresDeudoresCorteDos;
    }

    public String getCon5MayoresDeudoresCorteTres() {
        if (con5MayoresDeudoresCorteTres == null) {
            con5MayoresDeudoresCorteTres = "";
        }
        return con5MayoresDeudoresCorteTres;
    }

    public String getCon5MayoresDeudoresCorteCuatro() {
        if (con5MayoresDeudoresCorteCuatro == null) {
            con5MayoresDeudoresCorteCuatro = "";
        }
        return con5MayoresDeudoresCorteCuatro;
    }

    public String getNumAhorradoresCorteUno() {
        if (numAhorradoresCorteUno == null) {
            numAhorradoresCorteUno = "";
        }
        return numAhorradoresCorteUno;
    }

    public String getNumAhorradoresCorteDos() {
        if (numAhorradoresCorteDos == null) {
            numAhorradoresCorteDos = "";
        }
        return numAhorradoresCorteDos;
    }

    public String getNumAhorradoresCorteTres() {
        if (numAhorradoresCorteTres == null) {
            numAhorradoresCorteTres = "";
        }
        return numAhorradoresCorteTres;
    }

    public String getNumAhorradoresCorteCuatro() {
        if (numAhorradoresCorteCuatro == null) {
            numAhorradoresCorteCuatro = "";
        }
        return numAhorradoresCorteCuatro;
    }

    public String getNumDeudoresCorteUno() {
        if (numDeudoresCorteUno == null) {
            numDeudoresCorteUno = "";
        }
        return numDeudoresCorteUno;
    }

    public String getNumDeudoresCorteDos() {
        if (numDeudoresCorteDos == null) {
            numDeudoresCorteDos = "";
        }
        return numDeudoresCorteDos;
    }

    public String getNumDeudoresCorteTres() {
        if (numDeudoresCorteTres == null) {
            numDeudoresCorteTres = "";
        }
        return numDeudoresCorteTres;
    }

    public String getNumDeudoresCorteCuatro() {
        if (numDeudoresCorteCuatro == null) {
            numDeudoresCorteCuatro = "";
        }
        return numDeudoresCorteCuatro;
    }

    public String getNumAsociadosCorteUno() {
        if (numAsociadosCorteUno == null) {
            numAsociadosCorteUno = "";
        }
        return numAsociadosCorteUno;
    }

    public String getNumAsociadosCorteDos() {
        if (numAsociadosCorteDos == null) {
            numAsociadosCorteDos = "";
        }
        return numAsociadosCorteDos;
    }

    public String getNumAsociadosCorteTres() {
        if (numAsociadosCorteTres == null) {
            numAsociadosCorteTres = "";
        }
        return numAsociadosCorteTres;
    }

    public String getNumAsociadosCorteCuatro() {
        if (numAsociadosCorteCuatro == null) {
            numAsociadosCorteCuatro = "";
        }
        return numAsociadosCorteCuatro;
    }

    public String getNumEmpleadosCorteUno() {
        if (numEmpleadosCorteUno == null) {
            numEmpleadosCorteUno = "";
        }
        return numEmpleadosCorteUno;
    }

    public String getNumEmpleadosCorteDos() {
        if (numEmpleadosCorteDos == null) {
            numEmpleadosCorteDos = "";
        }
        return numEmpleadosCorteDos;
    }

    public String getNumEmpleadosCorteTres() {
        if (numEmpleadosCorteTres == null) {
            numEmpleadosCorteTres = "";
        }
        return numEmpleadosCorteTres;
    }

    public String getNumEmpleadosCorteCuatro() {
        if (numEmpleadosCorteCuatro == null) {
            numEmpleadosCorteCuatro = "";
        }
        return numEmpleadosCorteCuatro;
    }

    public String getNumOficinasCorteUno() {
        if (numOficinasCorteUno == null) {
            numOficinasCorteUno = "";
        }
        return numOficinasCorteUno;
    }

    public String getNumOficinasCorteDos() {
        if (numOficinasCorteDos == null) {
            numOficinasCorteDos = "";
        }
        return numOficinasCorteDos;
    }

    public String getNumOficionasCorteTres() {
        if (numOficionasCorteTres == null) {
            numOficionasCorteTres = "";
        }
        return numOficionasCorteTres;
    }

    public String getNumOficionasCorteCuatro() {
        if (numOficionasCorteCuatro == null) {
            numOficionasCorteCuatro = "";
        }
        return numOficionasCorteCuatro;
    }

}
