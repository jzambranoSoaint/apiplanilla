package soaint.ws.rest.bo.seguimiento;

public class ParametrosRiesgoMercado {
    private RiesgoMercado riesgoMercado;

    public RiesgoMercado getRiesgoMercado() {
        if (riesgoMercado == null) {
            riesgoMercado = new RiesgoMercado();
        }
        return riesgoMercado;
    }

    public void setRiesgoMercado(RiesgoMercado riesgoMercado) {
        this.riesgoMercado = riesgoMercado;
    }
}
