package soaint.ws.rest.bo.seguimiento;

public class ParametrosRiesgoCrediticio {
    private RiesgoCrediticio riesgoCrediticio;

    public RiesgoCrediticio getRiesgoCrediticio() {
        if (riesgoCrediticio == null) {
            riesgoCrediticio = new RiesgoCrediticio();
        }
        return riesgoCrediticio;
    }

    public void setRiesgoCrediticio(RiesgoCrediticio riesgoCrediticio) {
        this.riesgoCrediticio = riesgoCrediticio;
    }
}
