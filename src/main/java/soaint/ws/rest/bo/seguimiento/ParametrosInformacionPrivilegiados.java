package soaint.ws.rest.bo.seguimiento;

public class ParametrosInformacionPrivilegiados {
    private InformacionPrivilegiados informacionPrivilegiados;

    public InformacionPrivilegiados getInformacionPrivilegiados() {
        if (informacionPrivilegiados == null) {
            informacionPrivilegiados = new InformacionPrivilegiados();
        }
        return informacionPrivilegiados;
    }

    public void setInformacionPrivilegiados(InformacionPrivilegiados informacionPrivilegiados) {
        this.informacionPrivilegiados = informacionPrivilegiados;
    }
}
