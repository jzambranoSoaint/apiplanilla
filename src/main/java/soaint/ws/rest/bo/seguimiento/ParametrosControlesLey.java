package soaint.ws.rest.bo.seguimiento;

public class ParametrosControlesLey {
    private ControlesLey controlesLey;

    public ControlesLey getControlesLey() {
        if (controlesLey == null) {
            controlesLey = new ControlesLey();
        }
        return controlesLey;
    }

    public void setControlesLey(ControlesLey controlesLey) {
        this.controlesLey = controlesLey;
    }
}
