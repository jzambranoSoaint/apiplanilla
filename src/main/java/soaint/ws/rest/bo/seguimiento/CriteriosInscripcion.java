package soaint.ws.rest.bo.seguimiento;

public class CriteriosInscripcion {
    private String matFinancieroCorteUno;
    private String matFinancieroCorteDos;
    private String matFinancieroCorteTres;
    private String matFinancieroCorteCuatro;
    private String cameCorteUno;
    private String cameCorteDos;
    private String cameCorteTres;
    private String cameCorteCuatro;
    private String irlCorteUno;
    private String irlCorteDos;
    private String irlCorteTres;
    private String irlCorteCuatro;
    private String banda15DiasCorteUno;
    private String banda15DiasCorteDos;
    private String banda15DiasCorteTres;
    private String banda15DiasCorteCuatro;
    private String banda30DiasCorteUno;
    private String banda30DiasCorteDos;
    private String banda30DiasCorteTres;
    private String banda30DiasCorteCuatro;

    private String banda60DiasCorteUno;
    private String banda60DiasCorteDos;
    private String banda60DiasCorteTres;
    private String banda60DiasCorteCuatro;

    private String seguroDepositoCorteUno;
    private String seguroDepositoCorteDos;
    private String seguroDepositoCorteTres;
    private String seguroDepositoCorteCuatro;
    private String coberturaSeguroCorteUno;
    private String coberturaSeguroCorteDos;
    private String coberturaSeguroCorteTres;
    private String coberturaSeguroCorteCuatro;
    private String coberturaAhorradoresCorteUno;
    private String coberturaAhorradoresCorteDos;
    private String coberturaAhorradoresCorteTres;
    private String coberturaAhorradoresCorteCuatro;

    public String getMatFinancieroCorteUno() {
        if (matFinancieroCorteUno == null) {
            matFinancieroCorteUno = "";
        }
        return matFinancieroCorteUno;
    }

    public String getMatFinancieroCorteDos() {
        if (matFinancieroCorteDos == null) {
            matFinancieroCorteDos = "";
        }
        return matFinancieroCorteDos;
    }

    public String getMatFinancieroCorteTres() {
        if (matFinancieroCorteTres == null) {
            matFinancieroCorteTres = "";
        }
        return matFinancieroCorteTres;
    }

    public String getMatFinancieroCorteCuatro() {
        if (matFinancieroCorteCuatro == null) {
            matFinancieroCorteCuatro = "";
        }
        return matFinancieroCorteCuatro;
    }

    public String getCameCorteUno() {
        if (cameCorteUno == null) {
            cameCorteUno = "";
        }
        return cameCorteUno;
    }

    public String getCameCorteDos() {
        if (cameCorteDos == null) {
            cameCorteDos = "";
        }
        return cameCorteDos;
    }

    public String getCameCorteTres() {
        if (cameCorteTres == null) {
            cameCorteTres = "";
        }
        return cameCorteTres;
    }

    public String getCameCorteCuatro() {
        if (cameCorteCuatro == null) {
            cameCorteCuatro = "";
        }
        return cameCorteCuatro;
    }

    public String getIrlCorteUno() {
        if (irlCorteUno == null) {
            irlCorteUno = "";
        }
        return irlCorteUno;
    }

    public String getIrlCorteDos() {
        if (irlCorteDos == null) {
            irlCorteDos = "";
        }
        return irlCorteDos;
    }

    public String getIrlCorteTres() {
        if (irlCorteTres == null) {
            irlCorteTres = "";
        }
        return irlCorteTres;
    }

    public String getIrlCorteCuatro() {
        if (irlCorteCuatro == null) {
            irlCorteCuatro = "";
        }
        return irlCorteCuatro;
    }

    public String getBanda15DiasCorteUno() {
        if (banda15DiasCorteUno == null) {
            banda15DiasCorteUno = "";
        }
        return banda15DiasCorteUno;
    }

    public String getBanda15DiasCorteDos() {
        if (banda15DiasCorteDos == null) {
            banda15DiasCorteDos = "";
        }
        return banda15DiasCorteDos;
    }

    public String getBanda15DiasCorteTres() {
        if (banda15DiasCorteTres == null) {
            banda15DiasCorteTres = "";
        }
        return banda15DiasCorteTres;
    }

    public String getBanda15DiasCorteCuatro() {
        if (banda15DiasCorteCuatro == null) {
            banda15DiasCorteCuatro = "";
        }
        return banda15DiasCorteCuatro;
    }

    public String getBanda30DiasCorteUno() {
        if (banda30DiasCorteUno == null) {
            banda30DiasCorteUno = "";
        }
        return banda30DiasCorteUno;
    }

    public String getBanda30DiasCorteDos() {
        if (banda30DiasCorteDos == null) {
            banda30DiasCorteDos = "";
        }
        return banda30DiasCorteDos;
    }

    public String getBanda30DiasCorteTres() {
        if (banda30DiasCorteTres == null) {
            banda30DiasCorteTres = "";
        }
        return banda30DiasCorteTres;
    }

    public String getBanda30DiasCorteCuatro() {
        if (banda30DiasCorteCuatro == null) {
            banda30DiasCorteCuatro = "";
        }
        return banda30DiasCorteCuatro;
    }

    public String getBanda60DiasCorteUno() {
        if (banda60DiasCorteUno == null) {
            banda60DiasCorteUno = "";
        }
        return banda60DiasCorteUno;
    }

    public String getBanda60DiasCorteDos() {
        if (banda60DiasCorteDos == null) {
            banda60DiasCorteDos = "";
        }
        return banda60DiasCorteDos;
    }

    public String getBanda60DiasCorteTres() {
        if (banda60DiasCorteTres == null) {
            banda60DiasCorteTres = "";
        }
        return banda60DiasCorteTres;
    }

    public String getBanda60DiasCorteCuatro() {
        if (banda60DiasCorteCuatro == null) {
            banda60DiasCorteCuatro = "";
        }
        return banda60DiasCorteCuatro;
    }


    public String getSeguroDepositoCorteUno() {
        if (seguroDepositoCorteUno == null) {
            seguroDepositoCorteUno = "";
        }
        return seguroDepositoCorteUno;
    }

    public String getSeguroDepositoCorteDos() {
        if (seguroDepositoCorteDos == null) {
            seguroDepositoCorteDos = "";
        }
        return seguroDepositoCorteDos;
    }

    public String getSeguroDepositoCorteTres() {
        if (seguroDepositoCorteTres == null) {
            seguroDepositoCorteTres = "";
        }
        return seguroDepositoCorteTres;
    }

    public String getSeguroDepositoCorteCuatro() {
        if (seguroDepositoCorteCuatro == null) {
            seguroDepositoCorteCuatro = "";
        }
        return seguroDepositoCorteCuatro;
    }

    public String getCoberturaSeguroCorteUno() {
        if (coberturaSeguroCorteUno == null) {
            coberturaSeguroCorteUno = "";
        }
        return coberturaSeguroCorteUno;
    }

    public String getCoberturaSeguroCorteDos() {
        if (coberturaSeguroCorteDos == null) {
            coberturaSeguroCorteDos = "";
        }
        return coberturaSeguroCorteDos;
    }

    public String getCoberturaSeguroCorteTres() {
        if (coberturaSeguroCorteTres == null) {
            coberturaSeguroCorteTres = "";
        }
        return coberturaSeguroCorteTres;
    }

    public String getCoberturaSeguroCorteCuatro() {
        if (coberturaSeguroCorteCuatro == null) {
            coberturaSeguroCorteCuatro = "";
        }
        return coberturaSeguroCorteCuatro;
    }

    public String getCoberturaAhorradoresCorteUno() {
        if (coberturaAhorradoresCorteUno == null) {
            coberturaAhorradoresCorteUno = "";
        }
        return coberturaAhorradoresCorteUno;
    }

    public String getCoberturaAhorradoresCorteDos() {
        if (coberturaAhorradoresCorteDos == null) {
            coberturaAhorradoresCorteDos = "";
        }
        return coberturaAhorradoresCorteDos;
    }

    public String getCoberturaAhorradoresCorteTres() {
        if (coberturaAhorradoresCorteTres == null) {
            coberturaAhorradoresCorteTres = "";
        }
        return coberturaAhorradoresCorteTres;
    }

    public String getCoberturaAhorradoresCorteCuatro() {
        if (coberturaAhorradoresCorteCuatro == null) {
            coberturaAhorradoresCorteCuatro = "";
        }
        return coberturaAhorradoresCorteCuatro;
    }

}
