package soaint.ws.rest.bo.seguimiento;

public class CalculosVerificacion {
    private String indicMoroCalculadaCorteUno;
    private String indicMoroCalculadaCorteDos;
    private String indicMoroCalculadaCorteTres;
    private String indicMoroCalculadaCorteCuatro;
    private String indCalCalculadoCorteUno;
    private String indCalCalculadoCorteDos;
    private String indCalCalculadoCorteTres;
    private String indCalCalculadoCorteCuatro;
    private String defEstProvisionesCorteUno;
    private String defEstProvisionesCorteDos;
    private String defEstOrivisionesCorteTres;
    private String defEstOrivisionesCorteCuatro;
    private String renConCarProductivaCorteUno;
    private String renConCarProductivaCorteDos;
    private String renConCarProductivaCorteTres;
    private String renConCarProductivaCorteCuatro;
    private String cosConDepositosCorteUno;
    private String cosConDepositosCorteDos;
    private String cosConDepositosCorteTres;
    private String cosConDepositosCorteCuatro;
    private String cosConOblFinancierasCorteUno;
    private String cosConOblFinancierasCorteDos;
    private String cosConOblFinancierasCorteTres;
    private String cosConOblFinancierasCorteCuatro;
    private String conGarAdmisiblesCorteUno;
    private String conGarAdmisiblesCorteDos;
    private String conGarAdmisiblesCorteTres;
    private String conGarAdmisiblesCorteCuatro;

    public String getIndicMoroCalculadaCorteUno() {
        if (indicMoroCalculadaCorteUno == null) {
            indicMoroCalculadaCorteUno = "";
        }
        return indicMoroCalculadaCorteUno;
    }

    public String getIndicMoroCalculadaCorteDos() {
        if (indicMoroCalculadaCorteDos == null) {
            indicMoroCalculadaCorteDos = "";
        }
        return indicMoroCalculadaCorteDos;
    }

    public String getIndicMoroCalculadaCorteTres() {
        if (indicMoroCalculadaCorteTres == null) {
            indicMoroCalculadaCorteTres = "";
        }
        return indicMoroCalculadaCorteTres;
    }

    public String getIndicMoroCalculadaCorteCuatro() {
        if (indicMoroCalculadaCorteCuatro == null) {
            indicMoroCalculadaCorteCuatro = "";
        }
        return indicMoroCalculadaCorteCuatro;
    }

    public String getIndCalCalculadoCorteUno() {
        if (indCalCalculadoCorteUno == null) {
            indCalCalculadoCorteUno = "";
        }
        return indCalCalculadoCorteUno;
    }

    public String getIndCalCalculadoCorteDos() {
        if (indCalCalculadoCorteDos == null) {
            indCalCalculadoCorteDos = "";
        }
        return indCalCalculadoCorteDos;
    }

    public String getIndCalCalculadoCorteTres() {
        if (indCalCalculadoCorteTres == null) {
            indCalCalculadoCorteTres = "";
        }
        return indCalCalculadoCorteTres;
    }

    public String getIndCalCalculadoCorteCuatro() {
        if (indCalCalculadoCorteCuatro == null) {
            indCalCalculadoCorteCuatro = "";
        }
        return indCalCalculadoCorteCuatro;
    }

    public String getDefEstProvisionesCorteUno() {
        if (defEstProvisionesCorteUno == null) {
            defEstProvisionesCorteUno = "";
        }
        return defEstProvisionesCorteUno;
    }

    public String getDefEstProvisionesCorteDos() {
        if (defEstProvisionesCorteDos == null) {
            defEstProvisionesCorteDos = "";
        }
        return defEstProvisionesCorteDos;
    }

    public String getDefEstOrivisionesCorteTres() {
        if (defEstOrivisionesCorteTres == null) {
            defEstOrivisionesCorteTres = "";
        }
        return defEstOrivisionesCorteTres;
    }

    public String getDefEstOrivisionesCorteCuatro() {
        if (defEstOrivisionesCorteCuatro == null) {
            defEstOrivisionesCorteCuatro = "";
        }
        return defEstOrivisionesCorteCuatro;
    }

    public String getRenConCarProductivaCorteUno() {
        if (renConCarProductivaCorteUno == null) {
            renConCarProductivaCorteUno = "";
        }
        return renConCarProductivaCorteUno;
    }

    public String getRenConCarProductivaCorteDos() {
        if (renConCarProductivaCorteDos == null) {
            renConCarProductivaCorteDos = "";
        }
        return renConCarProductivaCorteDos;
    }

    public String getRenConCarProductivaCorteTres() {
        if (renConCarProductivaCorteTres == null) {
            renConCarProductivaCorteTres = "";
        }
        return renConCarProductivaCorteTres;
    }

    public String getRenConCarProductivaCorteCuatro() {
        if (renConCarProductivaCorteCuatro == null) {
            renConCarProductivaCorteCuatro = "";
        }
        return renConCarProductivaCorteCuatro;
    }

    public String getCosConDepositosCorteUno() {
        if (cosConDepositosCorteUno == null) {
            cosConDepositosCorteUno = "";
        }
        return cosConDepositosCorteUno;
    }

    public String getCosConDepositosCorteDos() {
        if (cosConDepositosCorteDos == null) {
            cosConDepositosCorteDos = "";
        }
        return cosConDepositosCorteDos;
    }

    public String getCosConDepositosCorteTres() {
        if (cosConDepositosCorteTres == null) {
            cosConDepositosCorteTres = "";
        }
        return cosConDepositosCorteTres;
    }

    public String getCosConDepositosCorteCuatro() {
        if (cosConDepositosCorteCuatro == null) {
            cosConDepositosCorteCuatro = "";
        }
        return cosConDepositosCorteCuatro;
    }

    public String getCosConOblFinancierasCorteUno() {
        if (cosConOblFinancierasCorteUno == null) {
            cosConOblFinancierasCorteUno = "";
        }
        return cosConOblFinancierasCorteUno;
    }

    public String getCosConOblFinancierasCorteDos() {
        if (cosConOblFinancierasCorteDos == null) {
            cosConOblFinancierasCorteDos = "";
        }
        return cosConOblFinancierasCorteDos;
    }

    public String getCosConOblFinancierasCorteTres() {
        if (cosConOblFinancierasCorteTres == null) {
            cosConOblFinancierasCorteTres = "";
        }
        return cosConOblFinancierasCorteTres;
    }

    public String getCosConOblFinancierasCorteCuatro() {
        if (cosConOblFinancierasCorteCuatro == null) {
            cosConOblFinancierasCorteCuatro = "";
        }
        return cosConOblFinancierasCorteCuatro;
    }

    public String getConGarAdmisiblesCorteUno() {
        if (conGarAdmisiblesCorteUno == null) {
            conGarAdmisiblesCorteUno = "";
        }
        return conGarAdmisiblesCorteUno;
    }

    public String getConGarAdmisiblesCorteDos() {
        if (conGarAdmisiblesCorteDos == null) {
            conGarAdmisiblesCorteDos = "";
        }
        return conGarAdmisiblesCorteDos;
    }

    public String getConGarAdmisiblesCorteTres() {
        if (conGarAdmisiblesCorteTres == null) {
            conGarAdmisiblesCorteTres = "";
        }
        return conGarAdmisiblesCorteTres;
    }

    public String getConGarAdmisiblesCorteCuatro() {
        if (conGarAdmisiblesCorteCuatro == null) {
            conGarAdmisiblesCorteCuatro = "";
        }
        return conGarAdmisiblesCorteCuatro;
    }

}
