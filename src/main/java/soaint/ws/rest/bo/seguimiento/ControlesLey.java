package soaint.ws.rest.bo.seguimiento;

public class ControlesLey {
    private String numAhorradoresAsociadosCorteUno;
    private String numAhorradoresAsociadosCorteDos;
    private String numAhorradoresAsociadosCorteTres;
    private String numAhorradoresAsociadosCorteCuatro;
    private String montoDepositosCorteUno;
    private String montoDepositosCorteDos;
    private String montoDepositosCorteTres;
    private String montoDepositosCorteCuatro;
    private String cartera10NoAdmisibleCorteUno;
    private String cartera10NoAdmisibleCorteDos;
    private String cartera10NoAdmisibleCorteTres;
    private String cartera10NoAdmisibleCorteCuatro;
    private String cap25PatrimonioTecnicoCorteUno;
    private String cap25PatrimonioTecnicoCorteDos;
    private String cap25PatrimonioTecnicoCorteTres;
    private String cap25PatrimonioTecnicoCorteCuatro;
    private String inversionesCorteUno;
    private String inversionesCorteDos;
    private String inversionesCorteTres;
    private String inversionesCorteCuatro;
    private String numCreditosMaxLegalCorteUno;
    private String numCreditosMaxLegalCorteDos;
    private String numCreditosMaxLegalCorteTres;
    private String numCreditosMaxLegalCorteCuatro;
    private String capitalSocialCorteUno;
    private String capitalSocialCorteDos;
    private String capitalSocialCorteTres;
    private String capitalSocialCorteCuatro;
    private String capitalMinRequeridoCorteUno;
    private String capitalMinRequeridoCorteDos;
    private String capitalMinRequeridoCorteTres;
    private String capitalMinRequeridoCorteCuatro;

    public String getNumAhorradoresAsociadosCorteUno() {
        if (numAhorradoresAsociadosCorteUno == null) {
            numAhorradoresAsociadosCorteUno = "";
        }
        return numAhorradoresAsociadosCorteUno;
    }

    public String getNumAhorradoresAsociadosCorteDos() {
        if (numAhorradoresAsociadosCorteDos == null) {
            numAhorradoresAsociadosCorteDos = "";
        }
        return numAhorradoresAsociadosCorteDos;
    }

    public String getNumAhorradoresAsociadosCorteTres() {
        if (numAhorradoresAsociadosCorteTres == null) {
            numAhorradoresAsociadosCorteTres = "";
        }
        return numAhorradoresAsociadosCorteTres;
    }

    public String getNumAhorradoresAsociadosCorteCuatro() {
        if (numAhorradoresAsociadosCorteCuatro == null) {
            numAhorradoresAsociadosCorteCuatro = "";
        }
        return numAhorradoresAsociadosCorteCuatro;
    }

    public String getMontoDepositosCorteUno() {
        if (montoDepositosCorteUno == null) {
            montoDepositosCorteUno = "";
        }
        return montoDepositosCorteUno;
    }

    public String getMontoDepositosCorteDos() {
        if (montoDepositosCorteDos == null) {
            montoDepositosCorteDos = "";
        }
        return montoDepositosCorteDos;
    }

    public String getMontoDepositosCorteTres() {
        if (montoDepositosCorteTres == null) {
            montoDepositosCorteTres = "";
        }
        return montoDepositosCorteTres;
    }

    public String getMontoDepositosCorteCuatro() {
        if (montoDepositosCorteCuatro == null) {
            montoDepositosCorteCuatro = "";
        }
        return montoDepositosCorteCuatro;
    }

    public String getCartera10NoAdmisibleCorteUno() {
        if (cartera10NoAdmisibleCorteUno == null) {
            cartera10NoAdmisibleCorteUno = "";
        }
        return cartera10NoAdmisibleCorteUno;
    }

    public String getCartera10NoAdmisibleCorteDos() {
        if (cartera10NoAdmisibleCorteDos == null) {
            cartera10NoAdmisibleCorteDos = "";
        }
        return cartera10NoAdmisibleCorteDos;
    }

    public String getCartera10NoAdmisibleCorteTres() {
        if (cartera10NoAdmisibleCorteTres == null) {
            cartera10NoAdmisibleCorteTres = "";
        }
        return cartera10NoAdmisibleCorteTres;
    }

    public String getCartera10NoAdmisibleCorteCuatro() {
        if (cartera10NoAdmisibleCorteCuatro == null) {
            cartera10NoAdmisibleCorteCuatro = "";
        }
        return cartera10NoAdmisibleCorteCuatro;
    }

    public String getCap25PatrimonioTecnicoCorteUno() {
        if (cap25PatrimonioTecnicoCorteUno == null) {
            cap25PatrimonioTecnicoCorteUno = "";
        }
        return cap25PatrimonioTecnicoCorteUno;
    }

    public String getCap25PatrimonioTecnicoCorteDos() {
        if (cap25PatrimonioTecnicoCorteDos == null) {
            cap25PatrimonioTecnicoCorteDos = "";
        }
        return cap25PatrimonioTecnicoCorteDos;
    }

    public String getCap25PatrimonioTecnicoCorteTres() {
        if (cap25PatrimonioTecnicoCorteTres == null) {
            cap25PatrimonioTecnicoCorteTres = "";
        }
        return cap25PatrimonioTecnicoCorteTres;
    }

    public String getCap25PatrimonioTecnicoCorteCuatro() {
        if (cap25PatrimonioTecnicoCorteCuatro == null) {
            cap25PatrimonioTecnicoCorteCuatro = "";
        }
        return cap25PatrimonioTecnicoCorteCuatro;
    }

    public String getInversionesCorteUno() {
        if (inversionesCorteUno == null) {
            inversionesCorteUno = "";
        }
        return inversionesCorteUno;
    }

    public String getInversionesCorteDos() {
        if (inversionesCorteDos == null) {
            inversionesCorteDos = "";
        }
        return inversionesCorteDos;
    }

    public String getInversionesCorteTres() {
        if (inversionesCorteTres == null) {
            inversionesCorteTres = "";
        }
        return inversionesCorteTres;
    }

    public String getInversionesCorteCuatro() {
        if (inversionesCorteCuatro == null) {
            inversionesCorteCuatro = "";
        }
        return inversionesCorteCuatro;
    }

    public String getNumCreditosMaxLegalCorteUno() {
        if (numCreditosMaxLegalCorteUno == null) {
            numCreditosMaxLegalCorteUno = "";
        }
        return numCreditosMaxLegalCorteUno;
    }

    public String getNumCreditosMaxLegalCorteDos() {
        if (numCreditosMaxLegalCorteDos == null) {
            numCreditosMaxLegalCorteDos = "";
        }
        return numCreditosMaxLegalCorteDos;
    }

    public String getNumCreditosMaxLegalCorteTres() {
        if (numCreditosMaxLegalCorteTres == null) {
            numCreditosMaxLegalCorteTres = "";
        }
        return numCreditosMaxLegalCorteTres;
    }

    public String getNumCreditosMaxLegalCorteCuatro() {
        if (numCreditosMaxLegalCorteCuatro == null) {
            numCreditosMaxLegalCorteCuatro = "";
        }
        return numCreditosMaxLegalCorteCuatro;
    }

    public String getCapitalSocialCorteUno() {
        if (capitalSocialCorteUno == null) {
            capitalSocialCorteUno = "";
        }
        return capitalSocialCorteUno;
    }

    public String getCapitalSocialCorteDos() {
        if (capitalSocialCorteDos == null) {
            capitalSocialCorteDos = "";
        }
        return capitalSocialCorteDos;
    }

    public String getCapitalSocialCorteTres() {
        if (capitalSocialCorteTres == null) {
            capitalSocialCorteTres = "";
        }
        return capitalSocialCorteTres;
    }

    public String getCapitalSocialCorteCuatro() {
        if (capitalSocialCorteCuatro == null) {
            capitalSocialCorteCuatro = "";
        }
        return capitalSocialCorteCuatro;
    }

    public String getCapitalMinRequeridoCorteUno() {
        if (capitalMinRequeridoCorteUno == null) {
            capitalMinRequeridoCorteUno = "";
        }
        return capitalMinRequeridoCorteUno;
    }

    public String getCapitalMinRequeridoCorteDos() {
        if (capitalMinRequeridoCorteDos == null) {
            capitalMinRequeridoCorteDos = "";
        }
        return capitalMinRequeridoCorteDos;
    }

    public String getCapitalMinRequeridoCorteTres() {
        if (capitalMinRequeridoCorteTres == null) {
            capitalMinRequeridoCorteTres = "";
        }
        return capitalMinRequeridoCorteTres;
    }

    public String getCapitalMinRequeridoCorteCuatro() {
        if (capitalMinRequeridoCorteCuatro == null) {
            capitalMinRequeridoCorteCuatro = "";
        }
        return capitalMinRequeridoCorteCuatro;
    }

}
