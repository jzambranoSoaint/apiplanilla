package soaint.ws.rest.bo.seguimiento;

public class ParametrosCalculosVerificacion {
    private CalculosVerificacion calculosVerificacion;

    public CalculosVerificacion getCalculosVerificacion() {
        if (calculosVerificacion == null) {
            calculosVerificacion = new CalculosVerificacion();
        }
        return calculosVerificacion;
    }

    public void setCalculosVerificacion(CalculosVerificacion calculosVerificacion) {
        this.calculosVerificacion = calculosVerificacion;
    }
}
