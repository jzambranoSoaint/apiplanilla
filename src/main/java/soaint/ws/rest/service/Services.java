package soaint.ws.rest.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import soaint.marcaaguadocx.bo.PropiedadesGeneralesTabla;
import soaint.ws.rest.bo.balanceAhorroCredito.ParametrosBalanceCuenta;
import soaint.ws.rest.bo.balanceAhorroCredito.ParametrosBalanceVariacion;
import soaint.ws.rest.bo.matAyC.ParametrosModeloAlertaTemprana;
import soaint.ws.rest.bo.matAyC.ParametrosOtrosIndicadores;
import soaint.ws.rest.bo.posicionNeta.ParametrosPosicionNeta;
import soaint.ws.rest.bo.pygAhorroCredito.ParametrosEstadoResultadosCuenta;
import soaint.ws.rest.bo.pygAhorroCredito.ParametrosPygVariacion;
import soaint.ws.rest.bo.seguimiento.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JsonDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.apache.log4j.Logger;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import soaint.marcaaguadocx.bo.PropiedadDocumento;
import soaint.marcaaguadocx.util.FileUtil;
import soaint.utils.ConfigProperties;
import soaint.utils.Utilidades;
import soaint.utils.docx.UtilsDocx;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("/fogacoop")
public class Services {

    final static Logger logger = Logger.getLogger(Services.class);
    private final Timestamp timestamp;
    private String pathOut;

    public Services() {
        super();
        this.timestamp = new Timestamp(System.currentTimeMillis());
    }

    @POST
    @Path("planilla/seguimiento")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response generarSeguimiento(String strJson) throws JRException, IOException {
        JsonObject jsonObject = new Gson().fromJson(strJson, JsonObject.class);

        /*Objetos JSON Seguimiento*/
        Gson gsonInfoGeneral = new GsonBuilder().create();
        Gson gsonRiesgoCrediticio = new GsonBuilder().create();
        Gson gsonRiesgoMercado = new GsonBuilder().create();
        Gson gsonCalculosVerificacion = new GsonBuilder().create();
        Gson gsonCriteriosInscripcion = new GsonBuilder().create();
        Gson gsonControlesLey = new GsonBuilder().create();
        Gson gsonInformacionPrivilegiados = new GsonBuilder().create();
        Gson gsonOtrosDatosGenerales = new GsonBuilder().create();

        /*Objetos JSON MAT A y C*/
        Gson gsonModeloAlertaTemprana = new GsonBuilder().create();
        Gson gsonOtrosIndicadores = new GsonBuilder().create();

        /*Objetos JSON Balance A y C*/
        Gson gsonBalanceCuenta = new GsonBuilder().create();
        Gson gsonBalanceVariacion = new GsonBuilder().create();

        /*Objetos JSON P y G Ahorro y Crédito*/
        Gson gsonEstadoResultadosCuenta = new GsonBuilder().create();
        Gson gsonPygVariacion = new GsonBuilder().create();

        /*Objetos JSON Posición Neta*/
        Gson gsonInformePosicionNeta = new GsonBuilder().create();

        /*--------------------------------------------------------------------------------------*/

        /*Parametros Seguimiento*/
        ParametrosInformacionGeneral parametrosInformacionGeneral = gsonInfoGeneral.fromJson(jsonObject, ParametrosInformacionGeneral.class);
        ParametrosRiesgoCrediticio parametrosRiesgoCrediticio = gsonRiesgoCrediticio.fromJson(jsonObject, ParametrosRiesgoCrediticio.class);
        ParametrosRiesgoMercado parametrosRiesgoMercado = gsonRiesgoMercado.fromJson(jsonObject, ParametrosRiesgoMercado.class);
        ParametrosCalculosVerificacion parametrosCalculosVerificacion = gsonCalculosVerificacion.fromJson(jsonObject, ParametrosCalculosVerificacion.class);
        ParametrosCriteriosInscripcion parametrosCriteriosInscripcion = gsonCriteriosInscripcion.fromJson(jsonObject, ParametrosCriteriosInscripcion.class);
        ParametrosControlesLey parametrosControlesLey = gsonControlesLey.fromJson(jsonObject, ParametrosControlesLey.class);
        ParametrosInformacionPrivilegiados parametrosInformacionPrivilegiados = gsonInformacionPrivilegiados.fromJson(jsonObject, ParametrosInformacionPrivilegiados.class);
        ParametrosOtrosDatosGenerales parametrosOtrosDatosGenerales = gsonOtrosDatosGenerales.fromJson(jsonObject, ParametrosOtrosDatosGenerales.class);

        /*Parametros MAT A y C*/
        ParametrosModeloAlertaTemprana parametrosModeloAlertaTemprana = gsonModeloAlertaTemprana.fromJson(jsonObject, ParametrosModeloAlertaTemprana.class);
        ParametrosOtrosIndicadores parametrosOtrosIndicadores = gsonOtrosIndicadores.fromJson(jsonObject, ParametrosOtrosIndicadores.class);

        /*Parametros Balance A y C*/
        ParametrosBalanceCuenta parametrosBalanceCuenta = gsonBalanceCuenta.fromJson(jsonObject, ParametrosBalanceCuenta.class);
        ParametrosBalanceVariacion parametrosBalanceVariacion = gsonBalanceVariacion.fromJson(jsonObject, ParametrosBalanceVariacion.class);

        /*Parametros P y G Ahorro y Crédito*/
        ParametrosEstadoResultadosCuenta parametrosEstadoResultadosCuenta = gsonEstadoResultadosCuenta.fromJson(jsonObject, ParametrosEstadoResultadosCuenta.class);
        ParametrosPygVariacion parametrosPygVariacion = gsonPygVariacion.fromJson(jsonObject, ParametrosPygVariacion.class);

        /*Parametros Posición Neta*/
        ParametrosPosicionNeta parametrosPosicionNeta = gsonInformePosicionNeta.fromJson(jsonObject, ParametrosPosicionNeta.class);

        JsonDataSource tablaPosicionNeta;

        JsonObject jsonValue = new JsonObject();

        try {
            /*Mapa de Parametros de Seguimiento*/
            Map<String, Object> parametersSeguimiento = new HashMap<>();

            ServicesSeguimiento.parametosInformacionGeneral(parametrosInformacionGeneral.getInformacionGeneral(), parametersSeguimiento);
            ServicesSeguimiento.parametrosRiesgoCrediticio(parametrosRiesgoCrediticio.getRiesgoCrediticio(), parametersSeguimiento);
            ServicesSeguimiento.parametrosRiesgoMercado(parametrosRiesgoMercado.getRiesgoMercado(), parametersSeguimiento);
            ServicesSeguimiento.parametrosCalculosVerificacion(parametrosCalculosVerificacion.getCalculosVerificacion(), parametersSeguimiento);
            ServicesSeguimiento.parametrosCriteriosInscripcion(parametrosCriteriosInscripcion.getCriteriosInscripcion(), parametersSeguimiento);
            ServicesSeguimiento.parametrosControlesLey(parametrosControlesLey.getControlesLey(), parametersSeguimiento);
            ServicesSeguimiento.parametrosInformacionPrivilegiados(parametrosInformacionPrivilegiados.getInformacionPrivilegiados(), parametersSeguimiento);
            ServicesSeguimiento.parametrosOtrosDatosGenerales(parametrosOtrosDatosGenerales.getOtrosDatosGenerales(), parametersSeguimiento);

            /*Mapa de Parametros de MAT A y C*/
            Map<String, Object> parametersMatAyC = new HashMap<>();

            ServicesMatAyC.parametrosModeloAlertaTemprana(parametrosModeloAlertaTemprana.getModeloAlertaTemprana(), parametersMatAyC);
            ServicesMatAyC.parametrosOtrosIndicadores(parametrosOtrosIndicadores.getOtrosIndicadores(), parametersMatAyC);

            /*Mapa de Parametros Balance A y C*/
            Map<String, Object> parametersBalanceAyC = new HashMap<>();

            ServicesBalanceAhorroCredito.parametrosBalanceCuenta(parametrosBalanceCuenta.getBalanceCuenta(), parametersBalanceAyC);
            ServicesBalanceAhorroCredito.parametrosBalanceVariacion(parametrosBalanceVariacion.getBalanceVariacion(), parametersBalanceAyC);

            /*Mapa de Parametros P y G Ahorro y Crédito*/
            Map<String, Object> parametersPyG = new HashMap<>();

            ServicesPygAhorroCredito.parametrosEstadoResultadosCuenta(parametrosEstadoResultadosCuenta.getEstadoResultadosCuenta(), parametersPyG);
            ServicesPygAhorroCredito.parametrosPygVariacion(parametrosPygVariacion.getPygVariacion(), parametersPyG);

            /*Mapa de Posición Neta*/
            Map<String, Object> parametersPosicionNeta = new HashMap<>();

            ServicesPosicionNeta.parametrosInformePosicionNeta(parametrosPosicionNeta.getInformePosicionNeta(), parametersPosicionNeta);

            JsonArray jsonArrayPosicionNeta = jsonObject.getAsJsonArray("tablaInforme");
            String tablaInformePosicionNeta = jsonArrayPosicionNeta.toString();
            ByteArrayInputStream jsonDataPosicionNeta = new ByteArrayInputStream(tablaInformePosicionNeta.getBytes(StandardCharsets.UTF_8));
            tablaPosicionNeta = new JsonDataSource(jsonDataPosicionNeta);

            /*--------------------------------------------------------------------------------------------------------------------------------------------------------------*/
            /*JasperReport de Seguimiento*/
            JasperReport reportSeguimiento = (JasperReport) JRLoader.loadObject(new File(ConfigProperties.getProperty("pathFile") + "SeguimientoAyC.jasper"));

            /*JasperReport de MAT A y C*/
            JasperReport reportMatAyC = (JasperReport) JRLoader.loadObject(new File(ConfigProperties.getProperty("pathFile") + "MAT_AyC.jasper"));

            /*JasperReport de Balance A y C*/
            JasperReport reportBalanceAyC = (JasperReport) JRLoader.loadObject(new File(ConfigProperties.getProperty("pathFile") + "BalanceAhorroYCredito.jasper"));

            /*JasperReport de P y G Ahorro y Crédito*/
            JasperReport reportPyG = (JasperReport) JRLoader.loadObject(new File(ConfigProperties.getProperty("pathFile") + "PyGAhorroYCredito.jasper"));

            /*JasperReport de Posición Neta*/
            JasperReport reportPosicionNeta = (JasperReport) JRLoader.loadObject(new File(ConfigProperties.getProperty("pathFile") + "PosicionNeta.jasper"));

            /*--------------------------------------------------------------------------------------------------------------------------------------------------------------*/
            /*Relleno de Jasper Seguimiento*/
            JasperPrint jasperPrintSeguimiento = JasperFillManager.fillReport(reportSeguimiento, parametersSeguimiento, new JREmptyDataSource());

            /*Relleno de Jasper MAT A y C*/
            JasperPrint jasperPrintMatAyC = JasperFillManager.fillReport(reportMatAyC, parametersMatAyC, new JREmptyDataSource());

            /*Relleno de Jasper Balance A y C*/
            JasperPrint jasperPrintBalanceAyC = JasperFillManager.fillReport(reportBalanceAyC, parametersBalanceAyC, new JREmptyDataSource());

            /*Relleno de Jasper P y G Ahorro y Crédito*/
            JasperPrint jasperPrintPyG = JasperFillManager.fillReport(reportPyG, parametersPyG, new JREmptyDataSource());

            /*Relleno de Jasper Posición Neta*/
            JasperPrint jasperPrintPosicionNeta = JasperFillManager.fillReport(reportPosicionNeta, parametersPosicionNeta, tablaPosicionNeta);

            ArrayList<JasperPrint> sheets = new ArrayList<>();
            sheets.add(jasperPrintSeguimiento);
            sheets.add(jasperPrintMatAyC);
            sheets.add(jasperPrintBalanceAyC);
            sheets.add(jasperPrintPyG);
            sheets.add(jasperPrintPosicionNeta);

            SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
            configuration.setOnePagePerSheet(true);
            configuration.setDetectCellType(true);
            configuration.setCollapseRowSpan(false);
            configuration.setWhitePageBackground(false);
            configuration.setRemoveEmptySpaceBetweenRows(true);
            configuration.setSheetNames(new String[]{"Seguimiento A y C", "MAT A y C", "Balance A y C", "P y G Ahorro y Crédito", "Posición Neta"});

            pathOut = ConfigProperties.getProperty("pathFile") + timestamp.getTime() + ".xlsx";
            File outputFile = new File(pathOut);

            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(sheets));
            exporter.setConfiguration(configuration);
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputFile));
            exporter.exportReport();

            String encodeFile = Utilidades.encodeFileToBase64Binary(pathOut);

            jsonValue.addProperty("base64", encodeFile);
            jsonValue.addProperty("mensaje", "OK");

        } catch (Exception e) {
            logger.error("ocurrio un error en la generación del documento excel: " + e);
            jsonValue.addProperty("mensaje", "ERROR: " + e.getMessage());
        }
        return Response.status(200).entity(jsonValue.toString()).build();
    }

    @POST
    @Path("planilla/documento")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response generarDocumentoWord(String strJson) {
        JsonObject jsonValue = new JsonObject();
        File fichero = null;

        try {
            PropiedadDocumento propiedadDocumento = new PropiedadDocumento(strJson);

            fichero = FileUtil.createTemporaryFile(FileUtil.FILE_TYPE_DOCX);
            FileUtil.createFile(propiedadDocumento.getBase64(), fichero);

            pathOut = fichero.getAbsolutePath();

            if (pathOut != null && !pathOut.equals("")) {
                fichero = new File(pathOut);
                WordprocessingMLPackage template = WordprocessingMLPackage.load(fichero);
                List<Object> texts = UtilsDocx.mapearDoc(template);

                boolean remplaza = UtilsDocx.remplazaTags(texts, propiedadDocumento.getTags(), template, pathOut, "");

                List<PropiedadesGeneralesTabla> tablas = propiedadDocumento.getTables();

                if (tablas != null && tablas.size() > 0) {
                    ObjectMapper objectMapper = new ObjectMapper();
                    for (int i = 0; i < tablas.size(); i++) {
                        Gson gson = new GsonBuilder().create();
                        HashMap<String, String> result = new HashMap<String, String>();
                        List<String> labels = new ArrayList<>();
                        ArrayList values = new ArrayList();

                        if (tablas.get(i).getValues() != null && tablas.get(i).getValues().size() > 0) {
                            result = objectMapper.readValue(gson.toJson(tablas.get(i)), HashMap.class);
                            result.keySet().removeIf(key -> key.equals("labels"));

                            for (Object value : result.values()) {
                                values = (ArrayList) value;
                            }
                        }

                        if (tablas.get(i).getLabels() != null && tablas.get(i).getLabels().size() > 0) {
                            for (int j = 0; j < tablas.get(i).getLabels().size(); j++) {
                                labels.add(tablas.get(i).getLabels().get(j));
                            }
                        }
                        boolean replaceTable = UtilsDocx.replaceTable(labels, values, template);
                    }
                } else {
                    System.out.println("No hay tablas para remplazar");
                }

                if (pathOut != null && !pathOut.equals("")) {
                    template.save(fichero);
                }

            }

            String encodeFile = Utilidades.encodeFileToBase64Binary(pathOut);

            jsonValue.addProperty("base64", encodeFile);
            jsonValue.addProperty("mensaje", "OK");

        } catch (Exception e) {
            logger.error("Ocurrio un error en la genración del documento word: " + e);
            jsonValue.addProperty("mensaje", "ERROR: " + e.getMessage());
        } finally {
            if (fichero != null) {
                fichero.delete();
            }
        }
        return Response.status(200).entity(jsonValue.toString()).build();
    }

    @POST
    @Path("type/planillaSeguimientoDos")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response generarSeguimientoDos(String strJson) throws JRException, IOException {
        JsonObject jsonObject = new Gson().fromJson(strJson, JsonObject.class);

        JsonObject jsonValue = new JsonObject();
        try {
            logger.info("Salio bien");
            System.out.println("Seguimiento dos: ");
            jsonValue.addProperty("Mensaje", "Respuesta OK");
        } catch (Exception e) {
            logger.error("Ocurrio un error " + e);
            System.out.println("Sorry, something wrong! " + e);
            jsonValue.addProperty("Mensaje", "Error: " + e.getMessage());
        }
        return Response.status(200).entity(jsonValue.toString()).build();

    }
}
