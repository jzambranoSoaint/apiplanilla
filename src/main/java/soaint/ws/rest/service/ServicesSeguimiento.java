package soaint.ws.rest.service;

import soaint.ws.rest.bo.seguimiento.*;

import java.util.Map;

public class ServicesSeguimiento {

    public static Map<String, Object> parametosInformacionGeneral(InformacionGeneral informacionGeneral, Map<String, Object> parametros) {
        parametros.put("gerente", informacionGeneral.getGerente());
        parametros.put("tipoCooperativa", informacionGeneral.getTipoCooperativa());
        parametros.put("fechaConstitucion", informacionGeneral.getFechaConstitucion());
        parametros.put("fechaAutorizacion", informacionGeneral.getFechaAutorizacion());
        parametros.put("fechaInscripcion", informacionGeneral.getFechaInscripcion());
        parametros.put("tipoInscripcion", informacionGeneral.getTipoInscripcion());
        parametros.put("fechaConvenio", informacionGeneral.getFechaConvenio());
        parametros.put("localizacionPrincipal", informacionGeneral.getLocalizacionPrincipal());
        parametros.put("perfilAsociados", informacionGeneral.getPerfilAsociados());
        parametros.put("localizacionOficinas", informacionGeneral.getLocalizacionOficinas());

        return parametros;
    }

    public static Map<String, Object> parametrosRiesgoCrediticio(RiesgoCrediticio riesgoCrediticio, Map<String, Object> parametros) {
        parametros.put("labelRiesCreCorteUno", riesgoCrediticio.getLabelRiesCreCorteUno());
        parametros.put("labelRiesCreCorteDos", riesgoCrediticio.getLabelRiesCreCorteDos());
        parametros.put("labelRiesCreCorteTres", riesgoCrediticio.getLabelRiesCreCorteTres());
        parametros.put("labelRiesCreCorteCuatro", riesgoCrediticio.getLabelRiesCreCorteCuatro());
        parametros.put("indCalCarteraCorteUno", riesgoCrediticio.getIndCalCarteraCorteUno());
        parametros.put("indCalCarteraCorteDos", riesgoCrediticio.getIndCalCarteraCorteDos());
        parametros.put("indCalCarteraCorteTres", riesgoCrediticio.getIndCalCarteraCorteTres());
        parametros.put("indCalCarteraCorteCuatro", riesgoCrediticio.getIndCalCarteraCorteCuatro());
        parametros.put("indCarImproductivaCorteUno", riesgoCrediticio.getIndCarImproductivaCorteUno());
        parametros.put("indCarImproductivaCorteDos", riesgoCrediticio.getIndCarImproductivaCorteDos());
        parametros.put("indCarImproductivaCorteTres", riesgoCrediticio.getIndCarImproductivaCorteTres());
        parametros.put("indCarImproductivaCorteCuatro", riesgoCrediticio.getIndCarImproductivaCorteCuatro());
        parametros.put("cubCarBrutaCorteUno", riesgoCrediticio.getCubCarBrutaCorteUno());
        parametros.put("cubCarBrutaCorteDos", riesgoCrediticio.getCubCarBrutaCorteDos());
        parametros.put("cubCarBrutaCorteTres", riesgoCrediticio.getCubCarBrutaCorteTres());
        parametros.put("cubCarBrutaCorteCuatro", riesgoCrediticio.getCubCarBrutaCorteCuatro());
        parametros.put("cubCarVencidaCorteUno", riesgoCrediticio.getCubCarVencidaCorteUno());
        parametros.put("cubCarVencidaCorteDos", riesgoCrediticio.getCubCarVencidaCorteDos());
        parametros.put("cubCarVencidaCorteTres", riesgoCrediticio.getCubCarVencidaCorteTres());
        parametros.put("cubCarVencidaCorteCuatro", riesgoCrediticio.getCubCarVencidaCorteCuatro());
        parametros.put("indReestructuracionesCorteUno", riesgoCrediticio.getIndReestructuracionesCorteUno());
        parametros.put("indReestructuracionesCorteDos", riesgoCrediticio.getIndReestructuracionesCorteDos());
        parametros.put("indReestructuracionesCorteTres", riesgoCrediticio.getIndReestructuracionesCorteTres());
        parametros.put("indReestructuracionesCorteCuatro", riesgoCrediticio.getIndReestructuracionesCorteCuatro());
        parametros.put("carCastigadaCorteUno", riesgoCrediticio.getCarCastigadaCorteUno());
        parametros.put("carCastigadaCorteDos", riesgoCrediticio.getCarCastigadaCorteDos());
        parametros.put("carCastigadaCorteTres", riesgoCrediticio.getCarCastigadaCorteTres());
        parametros.put("carCastigadaCorteCuatro", riesgoCrediticio.getCarCastigadaCorteCuatro());

        return parametros;
    }

    public static Map<String, Object> parametrosRiesgoMercado(RiesgoMercado riesgoMercado, Map<String, Object> parametros) {
        parametros.put("tasaActPromedioCorteUno", riesgoMercado.getTasaActPromedioCorteUno());
        parametros.put("tasaActPromedioCorteDos", riesgoMercado.getTasaActPromedioCorteDos());
        parametros.put("tasaActPromedioCorteTres", riesgoMercado.getTasaActPromedioCorteTres());
        parametros.put("tasaActPromedioCorteCuatro", riesgoMercado.getTasaActPromedioCorteCuatro());
        parametros.put("tasaPasivaCDATCorteUno", riesgoMercado.getTasaPasivaCDATCorteUno());
        parametros.put("tasaPasivaCDATCorteDos", riesgoMercado.getTasaPasivaCDATCorteDos());
        parametros.put("tasaPasivaCDATCorteTres", riesgoMercado.getTasaPasivaCDATCorteTres());
        parametros.put("tasaPasivaCDATCorteCuatro", riesgoMercado.getTasaPasivaCDATCorteCuatro());
        parametros.put("tasaPasivaPromCorteUno", riesgoMercado.getTasaPasivaPromCorteUno());
        parametros.put("tasaPasivaPromCorteDos", riesgoMercado.getTasaPasivaPromCorteDos());
        parametros.put("tasaPasivaPromCorteTres", riesgoMercado.getTasaPasivaPromCorteTres());
        parametros.put("tasaPasivaPromCorteCuatro", riesgoMercado.getTasaPasivaPromCorteCuatro());
        parametros.put("margenTasaCorteUno", riesgoMercado.getMargenTasaCorteUno());
        parametros.put("margenTasaCorteDos", riesgoMercado.getMargenTasaCorteDos());
        parametros.put("margenTasaCorteTres", riesgoMercado.getMargenTasaCorteTres());
        parametros.put("margenTasaCorteCuatro", riesgoMercado.getMargenTasaCorteCuatro());
        parametros.put("dftVigCorteCorteUno", riesgoMercado.getDftVigCorteCorteUno());
        parametros.put("dftVigCorteCorteDos", riesgoMercado.getDftVigCorteCorteDos());
        parametros.put("dftVigCorteCorteTres", riesgoMercado.getDftVigCorteCorteTres());
        parametros.put("dftVigCorteCorteCuatro", riesgoMercado.getDftVigCorteCorteCuatro());
        parametros.put("plaPromCaptacionesCorteUno", riesgoMercado.getPlaPromCaptacionesCorteUno());
        parametros.put("plaPromCaptacionesCorteDos", riesgoMercado.getPlaPromCaptacionesCorteDos());
        parametros.put("plaPromCaptacionesCorteTres", riesgoMercado.getPlaPromCaptacionesCorteTres());
        parametros.put("plaPromCaptacionesCorteCuatro", riesgoMercado.getPlaPromCaptacionesCorteCuatro());
        parametros.put("plaPromCarteraCorteUno", riesgoMercado.getPlaPromCarteraCorteUno());
        parametros.put("plaPromCarteraCorteDos", riesgoMercado.getPlaPromCarteraCorteDos());
        parametros.put("plaPromCarteraCorteTres", riesgoMercado.getPlaPromCarteraCorteTres());
        parametros.put("plaPromCarteraCorteCuatro", riesgoMercado.getPlaPromCarteraCorteCuatro());

        return parametros;
    }

    public static Map<String, Object> parametrosCalculosVerificacion(CalculosVerificacion calculosVerificacion, Map<String, Object> parametros) {
        parametros.put("indicMoroCalculadaCorteUno", calculosVerificacion.getIndicMoroCalculadaCorteUno());
        parametros.put("indicMoroCalculadaCorteDos", calculosVerificacion.getIndicMoroCalculadaCorteDos());
        parametros.put("indicMoroCalculadaCorteTres", calculosVerificacion.getIndicMoroCalculadaCorteTres());
        parametros.put("indicMoroCalculadaCorteCuatro", calculosVerificacion.getIndicMoroCalculadaCorteCuatro());
        parametros.put("indCalCalculadoCorteUno", calculosVerificacion.getIndCalCalculadoCorteUno());
        parametros.put("indCalCalculadoCorteDos", calculosVerificacion.getIndCalCalculadoCorteDos());
        parametros.put("indCalCalculadoCorteTres", calculosVerificacion.getIndCalCalculadoCorteTres());
        parametros.put("indCalCalculadoCorteCuatro", calculosVerificacion.getIndCalCalculadoCorteCuatro());
        parametros.put("defEstProvisionesCorteUno", calculosVerificacion.getDefEstProvisionesCorteUno());
        parametros.put("defEstProvisionesCorteDos", calculosVerificacion.getDefEstProvisionesCorteDos());
        parametros.put("defEstOrivisionesCorteTres", calculosVerificacion.getDefEstOrivisionesCorteTres());
        parametros.put("defEstOrivisionesCorteCuatro", calculosVerificacion.getDefEstOrivisionesCorteCuatro());
        parametros.put("renConCarProductivaCorteUno", calculosVerificacion.getRenConCarProductivaCorteUno());
        parametros.put("renConCarProductivaCorteDos", calculosVerificacion.getRenConCarProductivaCorteDos());
        parametros.put("renConCarProductivaCorteTres", calculosVerificacion.getRenConCarProductivaCorteTres());
        parametros.put("renConCarProductivaCorteCuatro", calculosVerificacion.getRenConCarProductivaCorteCuatro());
        parametros.put("cosConDepositosCorteUno", calculosVerificacion.getCosConDepositosCorteUno());
        parametros.put("cosConDepositosCorteDos", calculosVerificacion.getCosConDepositosCorteDos());
        parametros.put("cosConDepositosCorteTres", calculosVerificacion.getCosConDepositosCorteTres());
        parametros.put("cosConDepositosCorteCuatro", calculosVerificacion.getCosConDepositosCorteCuatro());
        parametros.put("cosConOblFinancierasCorteUno", calculosVerificacion.getCosConOblFinancierasCorteUno());
        parametros.put("cosConOblFinancierasCorteDos", calculosVerificacion.getCosConOblFinancierasCorteDos());
        parametros.put("cosConOblFinancierasCorteTres", calculosVerificacion.getCosConOblFinancierasCorteTres());
        parametros.put("cosConOblFinancierasCorteCuatro", calculosVerificacion.getCosConOblFinancierasCorteCuatro());
        parametros.put("conGarAdmisiblesCorteUno", calculosVerificacion.getConGarAdmisiblesCorteUno());
        parametros.put("conGarAdmisiblesCorteDos", calculosVerificacion.getConGarAdmisiblesCorteDos());
        parametros.put("conGarAdmisiblesCorteTres", calculosVerificacion.getConGarAdmisiblesCorteTres());
        parametros.put("conGarAdmisiblesCorteCuatro", calculosVerificacion.getConGarAdmisiblesCorteCuatro());

        return parametros;
    }

    public static Map<String, Object> parametrosCriteriosInscripcion(CriteriosInscripcion criteriosInscripcion, Map<String, Object> parametros) {
        parametros.put("matFinancieroCorteUno", criteriosInscripcion.getMatFinancieroCorteUno());
        parametros.put("matFinancieroCorteDos", criteriosInscripcion.getMatFinancieroCorteDos());
        parametros.put("matFinancieroCorteTres", criteriosInscripcion.getMatFinancieroCorteTres());
        parametros.put("matFinancieroCorteCuatro", criteriosInscripcion.getMatFinancieroCorteCuatro());
        parametros.put("cameCorteUno", criteriosInscripcion.getCameCorteUno());
        parametros.put("cameCorteDos", criteriosInscripcion.getCameCorteDos());
        parametros.put("cameCorteTres", criteriosInscripcion.getCameCorteTres());
        parametros.put("cameCorteCuatro", criteriosInscripcion.getCameCorteCuatro());
        parametros.put("irlCorteUno", criteriosInscripcion.getIrlCorteUno());
        parametros.put("irlCorteDos", criteriosInscripcion.getIrlCorteDos());
        parametros.put("irlCorteTres", criteriosInscripcion.getIrlCorteTres());
        parametros.put("irlCorteCuatro", criteriosInscripcion.getIrlCorteCuatro());
        parametros.put("banda15DiasCorteUno", criteriosInscripcion.getBanda15DiasCorteUno());
        parametros.put("banda15DiasCorteDos", criteriosInscripcion.getBanda15DiasCorteDos());
        parametros.put("banda15DiasCorteTres", criteriosInscripcion.getBanda15DiasCorteTres());
        parametros.put("banda15DiasCorteCuatro", criteriosInscripcion.getBanda15DiasCorteCuatro());
        parametros.put("banda30DiasCorteUno", criteriosInscripcion.getBanda30DiasCorteUno());
        parametros.put("banda30DiasCorteDos", criteriosInscripcion.getBanda30DiasCorteDos());
        parametros.put("banda30DiasCorteTres", criteriosInscripcion.getBanda30DiasCorteTres());
        parametros.put("banda30DiasCorteCuatro", criteriosInscripcion.getBanda30DiasCorteCuatro());
        parametros.put("banda60DiasCorteUno", criteriosInscripcion.getBanda60DiasCorteUno());
        parametros.put("banda60DiasCorteDos", criteriosInscripcion.getBanda60DiasCorteDos());
        parametros.put("banda60DiasCorteTres", criteriosInscripcion.getBanda60DiasCorteTres());
        parametros.put("banda60DiasCorteCuatro", criteriosInscripcion.getBanda60DiasCorteCuatro());
        parametros.put("seguroDepositoCorteUno", criteriosInscripcion.getSeguroDepositoCorteUno());
        parametros.put("seguroDepositoCorteDos", criteriosInscripcion.getSeguroDepositoCorteDos());
        parametros.put("seguroDepositoCorteTres", criteriosInscripcion.getSeguroDepositoCorteTres());
        parametros.put("seguroDepositoCorteCuatro", criteriosInscripcion.getSeguroDepositoCorteCuatro());
        parametros.put("coberturaSeguroCorteUno", criteriosInscripcion.getCoberturaSeguroCorteUno());
        parametros.put("coberturaSeguroCorteDos", criteriosInscripcion.getCoberturaSeguroCorteDos());
        parametros.put("coberturaSeguroCorteTres", criteriosInscripcion.getCoberturaSeguroCorteTres());
        parametros.put("coberturaSeguroCorteCuatro", criteriosInscripcion.getCoberturaSeguroCorteCuatro());
        parametros.put("coberturaAhorradoresCorteUno", criteriosInscripcion.getCoberturaAhorradoresCorteUno());
        parametros.put("coberturaAhorradoresCorteDos", criteriosInscripcion.getCoberturaAhorradoresCorteDos());
        parametros.put("coberturaAhorradoresCorteTres", criteriosInscripcion.getCoberturaAhorradoresCorteTres());
        parametros.put("coberturaAhorradoresCorteCuatro", criteriosInscripcion.getCoberturaAhorradoresCorteCuatro());

        return parametros;
    }

    public static Map<String, Object> parametrosControlesLey(ControlesLey controlesLey, Map<String, Object> parametros) {
        parametros.put("numAhorradoresAsociadosCorteUno", controlesLey.getNumAhorradoresAsociadosCorteUno());
        parametros.put("numAhorradoresAsociadosCorteDos", controlesLey.getNumAhorradoresAsociadosCorteDos());
        parametros.put("numAhorradoresAsociadosCorteTres", controlesLey.getNumAhorradoresAsociadosCorteTres());
        parametros.put("numAhorradoresAsociadosCorteCuatro", controlesLey.getNumAhorradoresAsociadosCorteCuatro());
        parametros.put("montoDepositosCorteUno", controlesLey.getMontoDepositosCorteUno());
        parametros.put("montoDepositosCorteDos", controlesLey.getMontoDepositosCorteDos());
        parametros.put("montoDepositosCorteTres", controlesLey.getMontoDepositosCorteTres());
        parametros.put("montoDepositosCorteCuatro", controlesLey.getMontoDepositosCorteCuatro());
        parametros.put("cartera10NoAdmisibleCorteUno", controlesLey.getCartera10NoAdmisibleCorteUno());
        parametros.put("cartera10NoAdmisibleCorteDos", controlesLey.getCartera10NoAdmisibleCorteDos());
        parametros.put("cartera10NoAdmisibleCorteTres", controlesLey.getCartera10NoAdmisibleCorteTres());
        parametros.put("cartera10NoAdmisibleCorteCuatro", controlesLey.getCartera10NoAdmisibleCorteCuatro());
        parametros.put("cap25PatrimonioTecnicoCorteUno", controlesLey.getCap25PatrimonioTecnicoCorteUno());
        parametros.put("cap25PatrimonioTecnicoCorteDos", controlesLey.getCap25PatrimonioTecnicoCorteDos());
        parametros.put("cap25PatrimonioTecnicoCorteTres", controlesLey.getCap25PatrimonioTecnicoCorteTres());
        parametros.put("cap25PatrimonioTecnicoCorteCuatro", controlesLey.getCap25PatrimonioTecnicoCorteCuatro());
        parametros.put("inversionesCorteUno", controlesLey.getInversionesCorteUno());
        parametros.put("inversionesCorteDos", controlesLey.getInversionesCorteDos());
        parametros.put("inversionesCorteTres", controlesLey.getInversionesCorteTres());
        parametros.put("inversionesCorteCuatro", controlesLey.getInversionesCorteCuatro());
        parametros.put("numCreditosMaxLegalCorteUno", controlesLey.getNumCreditosMaxLegalCorteUno());
        parametros.put("numCreditosMaxLegalCorteDos", controlesLey.getNumCreditosMaxLegalCorteDos());
        parametros.put("numCreditosMaxLegalCorteTres", controlesLey.getNumCreditosMaxLegalCorteTres());
        parametros.put("numCreditosMaxLegalCorteCuatro", controlesLey.getNumCreditosMaxLegalCorteCuatro());
        parametros.put("capitalSocialCorteUno", controlesLey.getCapitalSocialCorteUno());
        parametros.put("capitalSocialCorteDos", controlesLey.getCapitalSocialCorteDos());
        parametros.put("capitalSocialCorteTres", controlesLey.getCapitalSocialCorteTres());
        parametros.put("capitalSocialCorteCuatro", controlesLey.getCapitalSocialCorteCuatro());
        parametros.put("capitalMinRequeridoCorteUno", controlesLey.getCapitalMinRequeridoCorteUno());
        parametros.put("capitalMinRequeridoCorteDos", controlesLey.getCapitalMinRequeridoCorteDos());
        parametros.put("capitalMinRequeridoCorteTres", controlesLey.getCapitalMinRequeridoCorteTres());
        parametros.put("capitalMinRequeridoCorteCuatro", controlesLey.getCapitalMinRequeridoCorteCuatro());

        return parametros;
    }

    public static Map<String, Object> parametrosInformacionPrivilegiados(InformacionPrivilegiados informacionPrivilegiados, Map<String, Object> parametros) {
        parametros.put("tasaPromCapGraAhorradoresCorteUno", informacionPrivilegiados.getTasaPromCapGraAhorradoresCorteUno());
        parametros.put("tasaPromCapGraAhorradoresCorteDos", informacionPrivilegiados.getTasaPromCapGraAhorradoresCorteDos());
        parametros.put("tasaPromCapGraAhorradoresCorteTres", informacionPrivilegiados.getTasaPromCapGraAhorradoresCorteTres());
        parametros.put("tasaPromCapGraAhorradoresCorteCuatro", informacionPrivilegiados.getTasaPromCapGraAhorradoresCorteCuatro());
        parametros.put("tasaPromColGraDeudoresCorteUno", informacionPrivilegiados.getTasaPromColGraDeudoresCorteUno());
        parametros.put("tasaPromColGraDeudoresCorteDos", informacionPrivilegiados.getTasaPromColGraDeudoresCorteDos());
        parametros.put("tasaPromColGraDeudoresCorteTres", informacionPrivilegiados.getTasaPromColGraDeudoresCorteTres());
        parametros.put("tasaPromColGraDeudoresCorteCuatro", informacionPrivilegiados.getTasaPromColGraDeudoresCorteCuatro());
        parametros.put("tasaPromCapMiemPrivilegiadosCorteUno", informacionPrivilegiados.getTasaPromCapMiemPrivilegiadosCorteUno());
        parametros.put("tasaPromCapMiemPrivilegiadosCorteDos", informacionPrivilegiados.getTasaPromCapMiemPrivilegiadosCorteDos());
        parametros.put("tasaPromCapMiemPrivilegiadosCorteTres", informacionPrivilegiados.getTasaPromCapMiemPrivilegiadosCorteTres());
        parametros.put("tasaPromCapMiemPrivilegiadosCorteCuatro", informacionPrivilegiados.getTasaPromCapMiemPrivilegiadosCorteCuatro());
        parametros.put("tasaPromColMiemPrivilegiadosCorteUno", informacionPrivilegiados.getTasaPromColMiemPrivilegiadosCorteUno());
        parametros.put("tasaPromColMiemPrivilegiadosCorteDos", informacionPrivilegiados.getTasaPromColMiemPrivilegiadosCorteDos());
        parametros.put("tasaPromColMiemPrivilegiadosCorteTres", informacionPrivilegiados.getTasaPromColMiemPrivilegiadosCorteTres());
        parametros.put("tasaPromColMiemPrivilegiadosCorteCuatro", informacionPrivilegiados.getTasaPromColMiemPrivilegiadosCorteCuatro());
        parametros.put("posNetaPrivilegiadosCorteUno", informacionPrivilegiados.getPosNetaPrivilegiadosCorteUno());
        parametros.put("posNetaPrivilegiadosCorteDos", informacionPrivilegiados.getPosNetaPrivilegiadosCorteDos());
        parametros.put("posNetaPrivilegiadosCorteTres", informacionPrivilegiados.getPosNetaPrivilegiadosCorteTres());
        parametros.put("posNetaPrivilegiadosCorteCuatro", informacionPrivilegiados.getPosNetaPrivilegiadosCorteCuatro());
        parametros.put("maxCalCarteraPrivilegiosCorteUno", informacionPrivilegiados.getMaxCalCarteraPrivilegiosCorteUno());
        parametros.put("maxCalCarteraPrivilegiosCorteDos", informacionPrivilegiados.getMaxCalCarteraPrivilegiosCorteDos());
        parametros.put("maxCalCarteraPrivilegiosCorteTres", informacionPrivilegiados.getMaxCalCarteraPrivilegiosCorteTres());
        parametros.put("maxCalCarteraPrivilegiosCorteCuatro", informacionPrivilegiados.getMaxCalCarteraPrivilegiosCorteCuatro());

        return parametros;
    }

    public static Map<String, Object> parametrosOtrosDatosGenerales(OtrosDatosGenerales otrosDatosGenerales, Map<String, Object> parametros) {
        parametros.put("con5MayoresAhorradoresCorteUno", otrosDatosGenerales.getCon5MayoresAhorradoresCorteUno());
        parametros.put("con5MayoresAhorradoresCorteDos", otrosDatosGenerales.getCon5MayoresAhorradoresCorteDos());
        parametros.put("con5MayoresAhorradoresCorteTres", otrosDatosGenerales.getCon5MayoresAhorradoresCorteTres());
        parametros.put("con5MayoresAhorradoresCorteCuatro", otrosDatosGenerales.getCon5MayoresAhorradoresCorteCuatro());
        parametros.put("con5MayoresDeudoresCorteUno", otrosDatosGenerales.getCon5MayoresDeudoresCorteUno());
        parametros.put("con5MayoresDeudoresCorteDos", otrosDatosGenerales.getCon5MayoresDeudoresCorteDos());
        parametros.put("con5MayoresDeudoresCorteTres", otrosDatosGenerales.getCon5MayoresDeudoresCorteTres());
        parametros.put("con5MayoresDeudoresCorteCuatro", otrosDatosGenerales.getCon5MayoresDeudoresCorteCuatro());
        parametros.put("numAhorradoresCorteUno", otrosDatosGenerales.getNumAhorradoresCorteUno());
        parametros.put("numAhorradoresCorteDos", otrosDatosGenerales.getNumAhorradoresCorteDos());
        parametros.put("numAhorradoresCorteTres", otrosDatosGenerales.getNumAhorradoresCorteTres());
        parametros.put("numAhorradoresCorteCuatro", otrosDatosGenerales.getNumAhorradoresCorteCuatro());
        parametros.put("numDeudoresCorteUno", otrosDatosGenerales.getNumDeudoresCorteUno());
        parametros.put("numDeudoresCorteDos", otrosDatosGenerales.getNumDeudoresCorteDos());
        parametros.put("numDeudoresCorteTres", otrosDatosGenerales.getNumDeudoresCorteTres());
        parametros.put("numDeudoresCorteCuatro", otrosDatosGenerales.getNumDeudoresCorteCuatro());
        parametros.put("numAsociadosCorteUno", otrosDatosGenerales.getNumAsociadosCorteUno());
        parametros.put("numAsociadosCorteDos", otrosDatosGenerales.getNumAsociadosCorteDos());
        parametros.put("numAsociadosCorteTres", otrosDatosGenerales.getNumAsociadosCorteTres());
        parametros.put("numAsociadosCorteCuatro", otrosDatosGenerales.getNumAsociadosCorteCuatro());
        parametros.put("numEmpleadosCorteUno", otrosDatosGenerales.getNumEmpleadosCorteUno());
        parametros.put("numEmpleadosCorteDos", otrosDatosGenerales.getNumEmpleadosCorteDos());
        parametros.put("numEmpleadosCorteTres", otrosDatosGenerales.getNumEmpleadosCorteTres());
        parametros.put("numEmpleadosCorteCuatro", otrosDatosGenerales.getNumEmpleadosCorteCuatro());
        parametros.put("numOficinasCorteUno", otrosDatosGenerales.getNumOficinasCorteUno());
        parametros.put("numOficinasCorteDos", otrosDatosGenerales.getNumOficinasCorteDos());
        parametros.put("numOficionasCorteTres", otrosDatosGenerales.getNumOficionasCorteTres());
        parametros.put("numOficionasCorteCuatro", otrosDatosGenerales.getNumOficionasCorteCuatro());

        return parametros;
    }

}
