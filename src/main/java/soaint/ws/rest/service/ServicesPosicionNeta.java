package soaint.ws.rest.service;

import soaint.ws.rest.bo.posicionNeta.InformePosicionNeta;

import java.util.Map;

public class ServicesPosicionNeta {
    public static Map<String, Object> parametrosInformePosicionNeta(InformePosicionNeta informePosicionNeta, Map<String, Object> parametros) {
        parametros.put("entidad", informePosicionNeta.getEntidad());
        parametros.put("nombreEntidad", informePosicionNeta.getNombreEntidad());
        parametros.put("fechaCorte", informePosicionNeta.getFechaCorte());
        parametros.put("seguroDeposito", informePosicionNeta.getSeguroDeposito());
        parametros.put("cobertura", informePosicionNeta.getCobertura());
        parametros.put("grandesCaptaciones", informePosicionNeta.getGrandesCaptaciones());
        parametros.put("grandesPromCaptaciones", informePosicionNeta.getGrandesPromCaptaciones());
        parametros.put("grandesCartera", informePosicionNeta.getGrandesCartera());
        parametros.put("grandesPromColocaciones", informePosicionNeta.getGrandesPromColocaciones());
        parametros.put("grandesAportes", informePosicionNeta.getGrandesAportes());
        parametros.put("grandesNeto", informePosicionNeta.getGrandesNeto());
        parametros.put("privilegiadosCaptaciones", informePosicionNeta.getPrivilegiadosCaptaciones());
        parametros.put("privilegiadosPromCaptaciones", informePosicionNeta.getPrivilegiadosPromCaptaciones());
        parametros.put("privilegiadosCartera", informePosicionNeta.getPrivilegiadosCartera());
        parametros.put("privilegiadosPromColocaciones", informePosicionNeta.getPrivilegiadosPromColocaciones());
        parametros.put("privilegiadosAportes", informePosicionNeta.getPrivilegiadosAportes());
        parametros.put("privilegiadosNeto", informePosicionNeta.getPrivilegiadosNeto());
        parametros.put("totalCaptaciones", informePosicionNeta.getTotalCaptaciones());
        parametros.put("totalPromCaptaciones", informePosicionNeta.getTotalPromCaptaciones());
        parametros.put("totalCartera", informePosicionNeta.getTotalCartera());
        parametros.put("totalPromColocaciones", informePosicionNeta.getTotalPromColocaciones());
        parametros.put("totalAportes", informePosicionNeta.getTotalAportes());
        parametros.put("totalNeto", informePosicionNeta.getTotalNeto());
        parametros.put("brechaTotalPrivil", informePosicionNeta.getBrechaTotalPrivil());

        return parametros;
    }
}
