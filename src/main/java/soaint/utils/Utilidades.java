package soaint.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class Utilidades {
    public static String encodeFileToBase64Binary(final String pathFile) throws IOException {

        File file = new File(pathFile);
        byte[] encoded = Base64.encodeBase64(FileUtils.readFileToByteArray(file));
        if (file != null) {
            file.delete();
        }


        return new String(encoded, StandardCharsets.US_ASCII);
    }

    public static String fileToUrl(String pathFile) throws IOException {

        File file = new File(pathFile);

        System.out.println("1. Absolute file path :" + file.getAbsolutePath());

        //Convert local path to URL
        URL url = file.toURI().toURL();
        System.out.println("2. URL of given file is:" + url);

        //Convert local path to URI
        URI uri = file.toURI();
        System.out.println("3. URI of given file is:" + uri);

        System.out.println("HOT: " + url.getHost() + " PORT: " + url.getPort() + " File: " + url.getFile());

        readFile(pathFile);

        return url.toString();

    }

    private static String readFile(String file) throws IOException {

        BufferedReader reader = null;
        StringBuilder stringBuilder = null;

        try {

            reader = new BufferedReader(new FileReader(file));
            String line = null;
            stringBuilder = new StringBuilder();
            String ls = System.getProperty("line.separator");

            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }

        } catch (Exception e) {
            // TODO: handle exception
        } finally {
            if (reader != null) {
                reader.close();
                reader = null;
            }
        }
        System.out.println(stringBuilder.toString());

        return stringBuilder.toString();
    }
}
