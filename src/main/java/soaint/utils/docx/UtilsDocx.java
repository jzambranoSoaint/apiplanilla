package soaint.utils.docx;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.docx4j.XmlUtils;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.wml.*;
import soaint.marcaaguadocx.bo.PropiedadesGeneral;
import soaint.ws.rest.service.Services;

public class UtilsDocx {
    final static Logger logger = Logger.getLogger(Services.class);

    private static List<Object> getAllElementFromObject(Object obj, Class<?> toSearch) {
        List<Object> result = new ArrayList<Object>();
        if (obj instanceof JAXBElement) obj = ((JAXBElement<?>) obj).getValue();

        if (obj.getClass().equals(toSearch))
            result.add(obj);
        else if (obj instanceof ContentAccessor) {
            List<?> children = ((ContentAccessor) obj).getContent();
            for (Object child : children) {
                result.addAll(getAllElementFromObject(child, toSearch));
            }

        }

        return result;
    }

    public static List<Object> mapearDoc(WordprocessingMLPackage open) {
        try {
            List<Object> texts = getAllElementFromObject(open.getMainDocumentPart(), Text.class);

            return texts;
        } catch (Exception e) {
            logger.error("Error al mapear el documento: " + e);
            return null;
        }
    }

    public static boolean remplazaTags(List<Object> texts, List<PropiedadesGeneral> prop, WordprocessingMLPackage op,
                                       String rutaDocumento, String colorTextRemplazo) {
        try {
            for (Object text : texts) {
                Text textElement = (Text) text;
                for (int i = 0; i < prop.size(); i++) {
                    if (textElement.getValue().trim().equals(prop.get(i).getNombre().trim())) {
                        textElement.setValue(prop.get(i).getValor().toString().trim());
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error al remplazar los tags en el documento: " + e);
            return false;
        }
        return true;
    }

    public static boolean replaceTable(List<String> placeholders, List<Map<String, String>> textToAdd, WordprocessingMLPackage template) throws Docx4JException, JAXBException, IOException {
        try {
            List<Object> tables = getAllElementFromObject(template.getMainDocumentPart(), Tbl.class);
            Tbl tempTable = getTemplateTable(tables, placeholders.get(0));
            List<Object> rows = getAllElementFromObject(tempTable, Tr.class);

            if (rows.size() == 2) {
                Tr templateRow = (Tr) rows.get(1);
                for (Map<String, String> replacements : textToAdd) {
                    addRowToTable(tempTable, templateRow, replacements);
                }
                tempTable.getContent().remove(templateRow);
                /*writeDocxToStream(template, "C:/Users/KUBIT/Desktop/templateRemplazo.docx");*/
            }
        } catch (Exception e) {
            logger.error("Ocurrio un error en el remplazo de la tabla: " + e);
            return false;
        }
        return true;
    }

    private static Tbl getTemplateTable(List<Object> tables, String templateKey) throws Docx4JException, JAXBException {
        return (Tbl) getTemplateObj(tables, templateKey, false);
    }

    private static Object getTemplateObj(List<Object> objects, String placeholder, boolean f) {
        List<Object> objectList = new ArrayList<>();
        for (Iterator<Object> iterator = objects.iterator(); iterator.hasNext(); ) {
            Object tbl = iterator.next();
            List<?> textElements = getAllElementFromObject(tbl, Text.class);
            for (Object text : textElements) {
                Text textElement = (Text) text;
                if (textElement.getValue() != null && textElement.getValue().equals("${" + placeholder + "}")) {
                    if (!f) {
                        return tbl;
                    } else {
                        objectList.add(tbl);
                    }
                }
            }
        }
        return objectList.isEmpty() ? null : objectList;
    }

    private static void addRowToTable(Tbl reviewtable, Tr templateRow, Map<String, String> replacements) {
        Tr workingRow = (Tr) XmlUtils.deepCopy(templateRow);
        replaceTexts(workingRow, replacements);
        reviewtable.getContent().add(workingRow);
    }

    private static void replaceTexts(Object working, Map<String, String> replacements) {
        List<?> textElements = getAllElementFromObject(working, Text.class);
        for (Object object : textElements) {
            Text text = (Text) object;
            String keyStr = getPlaceholderStr(text.getValue());
            if (keyStr != null && !keyStr.isEmpty()) {
                String replacementValue = replacements.get(keyStr);
                if (replacementValue != null) {
                    text.setValue(replacementValue);
                } else {
                    text.setValue("--");
                }
            }
        }
    }

    private static String getPlaceholderStr(String str) {
        if (str != null && !str.isEmpty()) {
            Pattern p = Pattern.compile("\\$\\{(.*?)\\}");
            Matcher m = p.matcher(str);
            if (m.find()) {
                return m.group(1);
            }
        }
        return null;
    }

}
