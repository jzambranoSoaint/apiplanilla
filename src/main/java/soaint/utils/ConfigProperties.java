package soaint.utils;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public final class ConfigProperties {
    private final Configuration configuration;

    protected ConfigProperties() {
        Configuration config = null;
        try {
            config = new PropertiesConfiguration("config.properties");
        } catch (ConfigurationException e) {
            e.printStackTrace();
            new Exception(e);
        }
        this.configuration = config;
    }

    private static class PropertiesHolder {
        private final static ConfigProperties INSTANCE = new ConfigProperties();
    }

    private static ConfigProperties getInstance() {
        return PropertiesHolder.INSTANCE;
    }

    public static  String getProperty(String property) {
        return ConfigProperties.getInstance().configuration.getString(property);
    }

    public static Configuration getConfig() {
        return ConfigProperties.getInstance().configuration;
    }
}
